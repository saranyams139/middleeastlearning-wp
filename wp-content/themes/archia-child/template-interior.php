<?php
/* banner-php */
/**
 * Template Name: Interior Design Page Template
 *
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 */


get_header(); 
?>
    <div class="page-content">
        <?php get_template_part('template-parts/header/singular'); ?>
        <div class="content-block">
            <div class="section-full content-inner">
            
                        <?php
                            $queryArgs = array(
                                'post_type' => 'services',
                                'orderby' => 'date',
                                'order' => 'asc',
                                'paged' => $paged,
                                'tax_query' => array(
                                    array(
                                    'taxonomy' => 'services_category',
                                    'field'    => 'slug',
                                    'terms'    => array( 'interior-design' ),
                                    'operator'          => 'AND'
                                    ),
                                ),
                            );
                            $wp_query = new WP_Query($queryArgs);
                            $loop_count = 0;

                        ?>
                        <?php while ($wp_query->have_posts()) : $wp_query->the_post();
                        ?>
                                <!---------------------------------->
                                <?php archia_get_template_part('template-parts/services/content','listing', [ 'loop_count' => $count ]);?>
                        <?php  
                        $count++; 
                        ?>

                            <?php endwhile; ?>   
                            <?php
                                wp_reset_query();
                   ?>


            </div>
        </div>
    </div>
<?php get_footer();
