<?php
/**
 * @package Archia - Architecture & Interior WordPress Theme
 * @author CTHthemes - http://themeforest.net/user/cththemes
 * @date 20-05-2019
 * @since 1.0.0
 * @version 1.0.0
 * @copyright Copyright ( C ) 2014 - 2019 cththemes.com . All rights reserved.
 * @license GNU General Public License version 3 or later; see LICENSE
 */
// Your php code goes here
function archia_child_enqueue_styles() {
    $parent_style = 'archia-style'; // This is 'archia-style' for the Anakual theme.
    wp_enqueue_style( 
        $parent_style, 
        get_template_directory_uri() . '/style.css', 
        array(
            'archia-fonts', 
            'font-awesome', 
            'line-awesome', 
            'magnific-popup', 
            'swiper',
            'animate',
            'bootstrap-select',
            'bootstrap',
            'owl-carousel',
            'scrollbar',
        ), 
        null 
    );
    wp_enqueue_style( 'archia-child-style',
        get_stylesheet_directory_uri() . '/style.css',
        array( $parent_style, 'archia-template' , 'archia-color'),
        wp_get_theme()->get('Version')
    );
}
add_action( 'wp_enqueue_scripts', 'archia_child_enqueue_styles' );
function get_breadcrumb() {
    echo '<a href="http://middleeastbuilders.parel.co/projects/" rel="nofollow">Project</a>';
    if (is_category() || is_single()) {
        echo "&nbsp;&nbsp;&#187;&nbsp;&nbsp;";
        the_category(' &bull; ');
            if (is_single()) {
                echo " &nbsp;&nbsp;&#187;&nbsp;&nbsp; ";
                the_title();
            }
    } 
}

//Services Category

function my_taxonomies_services() {
  $labels = array(
    'name'              => _x( 'Services Category', 'taxonomy general name' ),
    'singular_name'     => _x( 'Services Category', 'taxonomy singular name' ),
    'search_items'      => __( 'Search Services Category' ),
    'all_items'         => __( 'All Services Category' ),
    'parent_item'       => __( 'Parent Services Category' ),
    'parent_item_colon' => __( 'Parent Services Category:' ),
    'edit_item'         => __( 'Edit Services Category' ), 
    'update_item'       => __( 'Update Services Category' ),
    'add_new_item'      => __( 'Add New Services Category' ),
    'new_item_name'     => __( 'New Services Category' ),
    'menu_name'         => __( 'Services Category' ),
  );
  $args = array(
    'labels' => $labels,
    'hierarchical' => true,
    'show_in_rest' => true,
  );
  register_taxonomy( 'services_category', 'services', $args );
}
add_action( 'init', 'my_taxonomies_services', 0 );