<?php


if( get_post_meta(get_the_ID(),'_cth_show_header',true ) ): ?>
    <!-- inner page banner -->
    <div class="dlab-bnr-inr dlab-bnr-inr-lg overlay-primary-middle bg-pt" style="background-image:url(<?php echo esc_url( get_post_meta( get_the_ID(), '_cth_header_bg', true ) );?>);">
        <div class="container">
            <div class="dlab-bnr-inr-entry">
                <?php if( get_post_meta(get_the_ID(),'_cth_show_title',true ) ) : ?>
                <h1 class="head-sec-title text-white"><span><?php single_post_title();?></span></h1>
                <?php endif ; ?>
                <?php 
                if( $header_intro = get_post_meta(get_the_ID(),'_cth_header_intro',true ) )
                    echo '<div class="head-sec-intro">' . wp_kses_post( get_post_meta(get_the_ID(),'_cth_header_intro',true ) ) .'</div>';
                ?>
                <?php  
                if( get_post_meta(get_the_ID(),'_cth_show_breadcrumb',true ) ){ ?> 
                <!-- Breadcrumb row -->
                    <?php  
                    archia_breadcrumbs_project();
                    ?>
                <!-- Breadcrumb row END -->
                <?php } ?>
            </div>
        </div>
    </div>
    <!-- inner page banner END -->
<?php  endif; ?>