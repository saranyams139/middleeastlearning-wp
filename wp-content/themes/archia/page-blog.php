<?php
/* banner-php */
/**
 * Template Name: Page Blog
 *
 */


get_header(); 
?>
    <div class="page-content">
        <?php get_template_part('template-parts/header/blog'); ?>
        <div class="content-block">
            <div class="section-full content-inner">
               <?php while(have_posts()) : the_post(); ?>

                    <?php the_content(); ?>
                    <?php wp_link_pages(); ?>

                <?php endwhile; ?>   
            </div>
        </div>
    </div>
<?php get_footer();
