<?php
/* banner-php */
/**
 * The template for displaying comments
 *
 * This is the template that displays the area of the page that contains both the current comments
 * and the comment form.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 */

/*
 * If the current post is protected by a password and
 * the visitor has not yet entered the password we will
 * return early without loading the comments.
 */
if ( post_password_required() ) {
	return;
}
?>

<div class="clear m-b30" id="comment-list">
	<div class="comments-area" id="comments">
	<?php if ( have_comments() ) : ?>
		<h6 class="comments-title"><?php echo sprintf( _n( '<span>%s</span> Comment', '<span>%s</span> Comments',  get_comments_number() < 2 ? 1 : get_comments_number() , 'archia' ), number_format_i18n( get_comments_number() ) );?></h6>
		<div>
			<?php 
			$args = array(
				'walker'            => null,
				'max_depth'         => '',
				'style'             => 'li',
				'callback'          => 'archia_comments',
				'end-callback'      => null,
				'type'              => 'all',
				'reply_text'        => esc_html__('Reply','archia'),
				'page'              => '',
				'per_page'          => '',
				'avatar_size'       => 150,
				'reverse_top_level' => null,
				'reverse_children'  => '',
				'format'            => 'html5', //or xhtml if no HTML5 theme support
				'short_ping'        => false, // @since 3.6,
			    'echo'     			=> true, // boolean, default is true
			);
			?>

		    <ol class="comments-wrap comment-list">
		        <?php wp_list_comments($args);?>
		    </ol>

		    <?php
			// Are there comments to navigate through?
			if ( get_comment_pages_count() > 1 && get_option( 'page_comments' ) ) :
			?>
			<div class="comments-nav">
				<ul class="pager clearfix">
					<li class="previous"><?php previous_comments_link( wp_kses(__( '<i class="fa fa-angle-left"></i> Previous Comments', 'archia' ), array('i'=>array('class'=>array())) ) ); ?></li>
					<li class="next"><?php next_comments_link( wp_kses(__( 'Next Comments <i class="fa fa-angle-right"></i>', 'archia' ), array('i'=>array('class'=>array())) ) ); ?></li>
				</ul>
			</div>
			<?php endif; // Check for comment navigation ?>

		</div>
	<?php endif; // Check for has_comments ?>
		<div>

		  	<?php if ( ! comments_open() && get_comments_number() && post_type_supports( get_post_type(), 'comments' ) ) : ?>
				<p class="no-comments"><?php esc_html_e( 'Comments are closed.' , 'archia' ); ?></p>
			<?php endif; ?>


			<?php if(comments_open( )) : ?>
			<!-- list-single-main-item -->   
		    <div class="comment-post-form comment-respond" id="post-respond">
		        
		        <?php
		    		$commenter = wp_get_current_commenter();
		    		$req = get_option( 'require_name_email' );
					$aria_req = ( $req ? " required='required' aria-required='true'" : '' );
					$char_req = ( $req ? '*' : '' );
					$char_req_lbl = ( $req ? '<span class="required">*</span>' : '' );

					$comment_args = array(
					'title_reply'=> esc_html__('Add Comment','archia'),
					'fields' => apply_filters( 'comment_form_default_fields', 
					array(                     
							'author' => '<div class="comment-form-input comment-form-author"><label for="author">'.esc_html__( 'Name', 'archia' ).$char_req_lbl.'</label><input type="text" class="has-icon" id="author" name="author" placeholder="'.esc_attr__('Name','archia'). $char_req .'" value="' . esc_attr( $commenter['comment_author'] ) . '" ' . $aria_req . ' size="40"></div>',
							'email' =>'<div class="comment-form-input comment-form-email"><label for="email">'.esc_html__( 'Email', 'archia' ).$char_req_lbl.'</label><input class="has-icon" id="email" name="email" type="email" placeholder="'.esc_attr__('Email','archia'). $char_req .'" value="' . esc_attr(  $commenter['comment_author_email'] ) .'" ' . $aria_req . ' size="40"></div>',
							) 
						),
					'comment_field' => '<div class="comment-form-input comment-form-comment"><textarea name="comment" id="comment" cols="40" rows="3" placeholder="'.esc_attr__('Your Comment','archia').'" '.$aria_req.'></textarea></div>',
					'id_form'=>'commentform',
					'class_form'=>'add-comment comment-form',
					'id_submit' => 'submit',
					'class_submit'=>'btn black btn-lg outline outline-2 radius-xl',
					'label_submit' => esc_html__('Submit Comment','archia'),
					'must_log_in'=> '<p class="not-empty comment-must-log-in">' .  sprintf( wp_kses(__( 'You must be <a href="%s">logged in</a> to post a comment.' ,'archia'),array('a'=>array('href'=>array(),'title'=>array(),'target'=>array())) ), wp_login_url( apply_filters( 'the_permalink', get_permalink( ) ) ) ) . '</p>',
					'logged_in_as' => '<p class="not-empty comment-logged-in-as">' . sprintf( wp_kses(__( 'Logged in as <a href="%1$s">%2$s</a>. <a href="%3$s" title="Log out of this account">Log out?</a>','archia' ),array('a'=>array('href'=>array(),'title'=>array(),'target'=>array())) ), admin_url( 'profile.php' ), $user_identity, wp_logout_url( apply_filters( 'the_permalink', get_permalink( ) ) ) ) . '</p>',
					'comment_notes_before' => '<p class="comment-note-before">'.esc_html__('Your email is safe with us.','archia').'</p>',
					'comment_notes_after' => '',
					);
				?>
				<?php comment_form($comment_args); ?>
		    </div>
		    <!-- list-single-main-item end --> 

		<?php endif;?>

		</div>
	</div>
</div>
