<?php
/* banner-php */
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 */

?><!DOCTYPE html>
<html <?php language_attributes();?> class="no-js no-svg" itemscope>
	<head>
		<meta charset="<?php bloginfo('charset');?>">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="profile" href="//gmpg.org/xfn/11">
		<?php wp_head();?>
	</head>

	<body <?php body_class();?>>
		<div class="page-wraper">
			<?php if (archia_get_option('show_loader')): ?>
			<div id="loading-area" class="loading-1"><h1 class="loader1"><?php esc_html_e('LOADING', 'archia');?></h1></div>
		<?php endif;?>
		<!-- header -->
		<?php

		$plugin_header_style = apply_filters('archia_plugin_header_style', false);
		if ($plugin_header_style == false) {
		    get_template_part('template-parts/header/default');
		} else {
		    do_action('archia_header_style');
		}
		?>
		<!-- header END -->
		<!--  wrapper  -->
		<div id="wrapper" class="page-content bg-white">
		    <!-- Content-->
		    <div class="content">