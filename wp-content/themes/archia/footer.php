<?php
/* banner-php */
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 */

                do_action( 'archia_footer_before');
?>
        </div>
        <!-- Content end -->
    </div>
    <!-- wrapper end -->

    <!-- Footer -->
    <?php   
    $plugin_footer_style = apply_filters( 'archia_plugin_footer_style', false ); 
    if($plugin_footer_style == false){
        get_template_part( 'template-parts/footer/default');
    }else{
        do_action( 'archia_footer_style' );
    }
    ?>
    <!-- Footer END-->
    <button class="scroltop fa fa-chevron-up"></button> 
</div>   		
<?php wp_footer(); ?>
                
    </body>
</html>
