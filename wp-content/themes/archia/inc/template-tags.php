<?php
/* banner-php */
/**
 * Custom template tags for this theme
 *
 * Eventually, some of the functionality here could be replaced by core features.
 *
 */
function archia_get_option( $setting, $default = null ) {
    global $archia_options;

    $default_options = array(

        // header
        // thumbnail sizes
        'enable_custom_sizes' => '0',
        'thumb_size_opt_2' => array(
            'width'     => '555',
            'height'    => '399',
            'hard_crop' => '1'
        ),
        'thumb_size_opt_4' => array(
            'width'     => '424',
            'height'    => '280',
            'hard_crop' => '1',
        ),
        'thumb_size_opt_5' => array( 
            'width'     => '388',
            'height'    => '257',
            'hard_crop' => '1',
        ),
        'thumb_size_opt_6' => array(
            'width'     => '795',
            'height'    => '257',
            'hard_crop' => '1',
        ),
        'thumb_size_opt_7' => array(
            'width'     => '1200',
            'height'    => '532',
            'hard_crop' => '1',
        ),
        'thumb_size_opt_8' => array(
            'width'     => '381',
            'height'    => '240',
            'hard_crop' => '1',
        ),


        'thumb_size_opt_9' => array(
            'width'     => '806',
            'height'    => '530',
            'hard_crop' => '1',
        ),
        'thumb_size_opt_10' => array(
            'width'     => '806',
            'height'    => '530',
            'hard_crop' => '1',
        ),
        'thumb_size_opt_11' => array(
            'width'     => '82',
            'height'    => '54',
            'hard_crop' => '1',
        ),
        //
        'single_featured' => true,
        'blog_show_format' => true,

        'single_comments' => true,
        'single_author' => true,
        'single_date' => true,
        'single_cats' => true,
        'single_tags' => true,
        'single_like' => true,

        'blog_comments' => true,
        'blog_author' => true,
        'blog_date' => true,
        'blog_cats' => true,
        'blog_tags' => true,
        'blog_like' => true,
        // blog
        
        
        // footer
        'footer_copyright'          => '<span class="ft-copy">&#169; CTHthemes 2019. All rights reserved.</span>',

        'back_home_link'            => home_url('/'),
        '404_message'               => 'We’re sorry, but the page you were looking for doesn’t exist..',
        '404_bg'                    => array(
            'url'                   => '',
        ),
        '404_intro'                 => 'Stay informed on our news or events!',

        'loader_icon'               => '',

        'show_mini_cart'            => true,

        'user_menu_style'           => 'two',

        'search_icon'               => get_template_directory_uri().'/assets/images/search-icon.png',
        // new blog
        'blog_layout'               => 'right_sidebar',
        'blog_content_width'        => 'big-container',
        'blog-grid-style'           => 'grid',
        'blog-cols'                 => 'one',
        'blog-pad'                  => 'big',

        'show_blog_header'          => false,
        'blog_header_image'         =>  array(
            'url'       => get_template_directory_uri().'/assets/images/banner/bnr3.jpg',
        ),
        'blog_head_title'           => 'Our Blog',
        'blog_head_title_desc'      => '<p>Stay informed on our news or events!</p>',
        'single_nav'                => true,

        'color-preset'              => 'skin-1',

        // portfolio
        'folio_header'              => true,
        'folio_title'               => 'Our Portfolios',
        'folio_intro'               => '<p>Stay informed on our news or events!</p>',
        'folio_bg_image'            =>  array(
            'url'       => get_template_directory_uri().'/assets/images/banner/bnr3.jpg',
        ),
        'folio_style'               => 'default',
        'folio-cols'                => 'three',
        'folio-pad'                 => 'big',
        'folio_posts'               => '12',
        'folio_orderby'             => 'date',
        'folio_order'               => 'DESC',
    );

    if( is_customize_preview() ) $archia_options = get_option( 'archia_options', array() );

    $value = false;
    if ( isset( $archia_options[ $setting ] ) ) {
        $value = $archia_options[ $setting ];
    }else {
        if(isset($default)){
            $value = $default;
        }else if( isset( $default_options[ $setting ] ) ){
            $value = $default_options[ $setting ];
        }
    }
    return $value;
}

/**
 * Blog post nav
 *
 * @since archia 1.0
 */
if (!function_exists('archia_post_nav')) {
    function archia_post_nav() {
        
        

        $prev_post = get_adjacent_post( archia_get_option('nav_same_term', false ) , '', true );
        $next_post = get_adjacent_post( archia_get_option('nav_same_term', false ) , '', false );

        if ( is_a( $prev_post, 'WP_Post' ) || is_a( $next_post, 'WP_Post' ) ) :
?>
<div class="post-nav single-post-nav fl-wrap">
    <?php
        if ( is_a( $prev_post, 'WP_Post' ) ) :
        ?>
        <a href="<?php echo esc_url( get_permalink( $prev_post->ID ) ); ?>" class="post-link prev-post-link" title="<?php echo esc_attr( get_the_title($prev_post->ID ) ); ?>"><i class="fa fa-angle-left"></i><?php esc_html_e('Prev','archia' );?><span class="clearfix"><?php echo get_the_title($prev_post->ID ); ?></span></a>
        <?php 
        endif ; ?>
    <?php
        if ( is_a( $next_post, 'WP_Post' ) ) :
        ?>
        <a href="<?php echo esc_url( get_permalink( $next_post->ID ) ); ?>" class="post-link next-post-link" title="<?php echo esc_attr( get_the_title($next_post->ID ) ); ?>"><i class="fa fa-angle-right"></i><?php esc_html_e('Next','archia' );?><span class="clearfix"><?php echo get_the_title($next_post->ID ); ?></span></a>
        <?php 
        endif ; ?>
</div>
    <?php
        endif;
    }
}

/**
 * Single Portfolio Slider nav
 *
 * @since archia 1.0
 */
if (!function_exists('archia_folio_slider_nav')) {
    function archia_folio_slider_nav() {
        
        if(archia_get_option('folio_show_nav', true ) == false) return ;

        $prev_post = get_adjacent_post( archia_get_option('folio_nav_same_term', false ) , '', true , 'portfolio_cat');

        $next_post = get_adjacent_post( archia_get_option('folio_nav_same_term', false ) , '', false , 'portfolio_cat');

        if ( is_a( $prev_post, 'WP_Post' ) || is_a( $next_post, 'WP_Post' ) ) :
    ?>
    <!-- swiper-slide-->  
    <div class="swiper-slide portfolio-nav-slide">
        <div class="slider-content-nav-wrap full-height">
            <div class="slider-content-nav fl-wrap">
                <ul>
                    <?php
                    if ( is_a( $prev_post, 'WP_Post' ) ) :
                    ?>
                    <li class="prev-post">
                        <span><?php esc_html_e('Prev','archia' );?></span>
                        <a href="<?php echo esc_url( get_permalink( $prev_post->ID ) ); ?>" title="<?php echo esc_attr( get_the_title($prev_post->ID ) ); ?>"><?php echo get_the_title($prev_post->ID ); ?></a>
                    </li>
                    <?php 
                    endif ; ?>
                    <?php
                    if ( is_a( $next_post, 'WP_Post' ) ) :
                    ?>
                    <li class="next-post">
                        <span><?php esc_html_e('Next','archia' );?></span>
                        <a href="<?php echo esc_url( get_permalink( $next_post->ID ) ); ?>" title="<?php echo esc_attr( get_the_title($next_post->ID ) ); ?>"><?php echo get_the_title($next_post->ID ); ?></a>
                    </li>
                    <?php 
                    endif ; ?>
                </ul>
            </div>
        </div>
    </div>
    <!-- swiper-slide end--> 
    <?php 
    endif;
    }
}

/**
 * Single Portfolio Slider nav
 *
 * @since archia 1.0
 */
if (!function_exists('archia_folio_nav')) {
    function archia_folio_nav() {
        
        if(archia_get_option('folio_show_nav', true ) == false) return ;

        $prev_post = get_adjacent_post( archia_get_option('folio_nav_same_term', false ) , '', true , 'portfolio_cat');

        $next_post = get_adjacent_post( archia_get_option('folio_nav_same_term', false ) , '', false , 'portfolio_cat');

        if ( is_a( $prev_post, 'WP_Post' ) || is_a( $next_post, 'WP_Post' ) ) :
    ?> 
    <div class="content-nav fl-wrap">
        <ul>
            <?php
            if ( is_a( $prev_post, 'WP_Post' ) ) :
            ?>
            <li class="prev-post">
                <span><?php esc_html_e('Prev','archia' );?></span>
                <a href="<?php echo esc_url( get_permalink( $prev_post->ID ) ); ?>" title="<?php echo esc_attr( get_the_title($prev_post->ID ) ); ?>"><?php echo get_the_title($prev_post->ID ); ?></a>
            </li>
            <?php 
            endif ; ?>
            <?php
            if ( is_a( $next_post, 'WP_Post' ) ) :
            ?>
            <li class="next-post">
                <span><?php esc_html_e('Next','archia' );?></span>
                <a href="<?php echo esc_url( get_permalink( $next_post->ID ) ); ?>" title="<?php echo esc_attr( get_the_title($next_post->ID ) ); ?>"><?php echo get_the_title($next_post->ID ); ?></a>
            </li>
            <?php 
            endif ; ?>
        </ul>
    </div> 
    <?php 
    endif;
    }
} 

/**
 * Custom comments list
 *
 * @since archia 1.0
 */
if (!function_exists('archia_comments')) {
    function archia_comments($comment, $args, $depth) {
        $GLOBALS['comment'] = $comment;
        extract($args, EXTR_SKIP);
        
        if ('div' == $args['style']) {
            $tag = 'div';
            $add_below = 'comment';
        } 
        else {
            $tag = 'li';
            $add_below = 'div-comment';
        }
?>
        <<?php
        echo esc_attr($tag); ?> <?php
        comment_class(empty($args['has_children']) ? 'comment-item comment-nochild' : 'comment-item comment-haschild') ?> id="comment-<?php
        comment_ID() ?>">
        <?php
        if ('div' != $args['style']): ?>
        <div id="div-comment-<?php
            comment_ID() ?>" class="comment-body thecomment">
        <?php
        endif; ?>

            <div class="comment-avatar comment-author vcard">
                <?php if ($args['avatar_size'] != 0) echo get_avatar($comment, $args['avatar_size'], 'https://0.gravatar.com/avatar/ad516503a11cd5ca435acc9bb6523536?s='.$args['avatar_size'], get_comment_author( $comment->comment_ID ) ); ?>
            </div>
            <div class="comment-text">
                <h4 class="fn"><?php echo get_comment_author_link($comment->comment_ID); ?></h4>
                <?php comment_text(); ?>
                
                <div class="comment-meta"><?php echo get_comment_date(esc_html__('F j, Y g:i a', 'archia')); ?></div>
                <div class="reply"><?php comment_reply_link(array_merge($args, array('add_below' => $add_below, 'depth' => $depth, 'max_depth' => $args['max_depth']))); ?></div>
                <?php
                if ($comment->comment_approved == '0'): ?>
                        <em class="comment-awaiting-moderation alignleft"><?php
                    esc_html_e('Your comment is awaiting moderation.', 'archia'); ?></em>
                        <br />
                    <?php
                endif; ?> 
            </div>
            


        
        <?php
        if ('div' != $args['style']): ?>
        </div> 
        <?php
        endif; ?>

    <?php
    }
}


if (!function_exists('archia_pagination')) {
    function archia_pagination(){

        the_posts_pagination( array(
            'prev_text' =>  wp_kses(__('<i class="fa fa-caret-left"></i>','archia'),array('i'=>array('class'=>array(),),) ) ,
            'next_text' =>  wp_kses(__('<i class="fa fa-caret-right"></i>','archia'),array('i'=>array('class'=>array(),),) ) ,
        ) );

    }
}

/**
 * Pagination for Portfolio page templates
 *
 * @since archia 1.0
 */
if (!function_exists('archia_custom_pagination')) {
    function archia_custom_pagination($pages = '', $range = 2, $current_query = '', $sec_wrap = false) {
        // var_dump($pages);die;
        $showitems = ($range * 2) + 1;
        
        if ($current_query == '') {
            global $paged;
            if (empty($paged)) $paged = 1;
        } 
        else {
            $paged = $current_query->query_vars['paged'];
        }
        
        if ($pages == '') {
            if ($current_query == '') {
                global $wp_query;
                $pages = $wp_query->max_num_pages;
                if (!$pages) {
                    $pages = 1;
                }
            } 
            else {
                $pages = $current_query->max_num_pages;
            }
        }
        
        if (1 < $pages) {
        echo '<div class="pagination-container pagination">';
            echo '<div class="nav-links">';
            if ($paged > 1) echo '<a href="' . get_pagenum_link($paged - 1) . '" class="page-numbers prevposts-link">'.wp_kses(__('<i class="fa fa-chevron-left"></i>','archia'),array('i'=>array('class'=>array(),),) ).'</a>';
            for ($i = 1; $i <= $pages; $i++) {
                if (1 != $pages && (!($i >= $paged + $range + 1 || $i <= $paged - $range - 1) || $pages <= $showitems)) {
                    if($paged == $i)
                        echo "<a class='blog-page page-numbers current-page current' href='javascript:void(0);'>" . esc_html( $i ) . "</a>";
                    else
                        echo "<a href='" . get_pagenum_link($i) . "' class='blog-page page-numbers'>" . esc_html( $i ) . "</a>";
                    
                }
            }
            if ($paged < $pages) echo '<a href="' . get_pagenum_link($paged + 1) . '" class="page-numbers nextposts-link">'.wp_kses(__('<i class="fa fa-chevron-right"></i>','archia'),array('i'=>array('class'=>array(),),) ).'</a>';
            echo '</div>';
        echo '</div>';
        }

    }
}

/**
 * Pagination for Portfolio Slider page templates
 *
 * @since archia 1.0
 */
if (!function_exists('archia_folio_slider_pagination')) {
    function archia_folio_slider_pagination($pages = '', $range = 2, $current_query = '', $sec_wrap = false) {

        $showitems = ($range * 2) + 1;
        
        if ($current_query == '') {
            global $paged;
            if (empty($paged)) $paged = 1;
        } 
        else {
            $paged = $current_query->query_vars['paged'];
        }
        
        if ($pages == '') {
            if ($current_query == '') {
                global $wp_query;
                $pages = $wp_query->max_num_pages;
                if (!$pages) {
                    $pages = 1;
                }
            } 
            else {
                $pages = $current_query->max_num_pages;
            }
        }
        
        if (1 < $pages) {
        	?>
        	<div class="swiper-slide">
                <div class="slider-content-nav-wrap full-height">
                    <div class="slider-content-nav fl-wrap">
                        <ul>
                            <li><?php if ($paged > 1) : ?>
                            	<span><?php esc_html_e('Prev','archia' );?></span>
                                <a href="<?php echo get_pagenum_link($paged - 1) ; ?>"><?php esc_html_e('Previous Projects','archia' );?></a>
                            <?php endif;?></li>
                            <li><?php if ($paged < $pages) : ?>
                            	<span><?php esc_html_e('Next','archia' );?></span>
                                <a href="<?php echo get_pagenum_link($paged + 1) ; ?>"><?php esc_html_e('Next Projects','archia' );?></a>
                            <?php endif;?></li>
                        </ul>
                    </div>
                </div>
            </div>
            <?php 
            
        }

    }
}



function archia_breadcrumbs($classes='') {
           
    // Settings
    $breadcrums_id      = 'breadcrumbs';
    $breadcrums_class   = 'breadcrumbs fl-wrap breadcrumb-row'.$classes;
    $home_title         = esc_html__('Home','archia');
    $blog_title         = esc_html__('Blog','archia');


    // If you have any custom post types with custom taxonomies, put the taxonomy name below (e.g. product_cat)
    $custom_taxonomy    = '';

    $custom_post_types = array();
      
    // Get the query & post information
    global $post;
      
    // Do not display on the homepage
    if ( !is_front_page() ) {
      
        // Build the breadcrums
        echo '<div id="' . esc_attr($breadcrums_id ) . '" class="' . esc_attr($breadcrums_class ) . '">';
        echo '<ul class="list-inline">';
        echo '<li class="breadcrumb-li"><a href="'.esc_url(home_url()).'"><i class="fa fa-home"></i></a></li>';
        // Home page
        if(is_home()){
            // Blog page
            echo '<li class="breadcrumb-li">' . esc_html( $blog_title ) . '</li>';
            
        }
          
        if ( is_archive() && !is_tax() ) {

            // If post is a custom post type
            $post_type = get_post_type();

            if($post_type && array_key_exists($post_type, $custom_post_types)){
                echo '<li class="breadcrumb-li"><span class="breadcrumb-current breadcrumb-item-custom-post-type-' . esc_attr( $post_type ) . '">' . esc_html( $custom_post_types[$post_type] ) . '</span></li>';
            }else{
                echo '<li class="breadcrumb-li"><span class="breadcrumb-current breadcrumb-item-archive">' . get_the_archive_title() . '</span></li>';
            }
             
        } else if ( is_archive() && is_tax() ) {
             
            // If post is a custom post type
            $post_type = get_post_type();
             
            // If it is a custom post type display name and link
            if($post_type && $post_type != 'post') {
                 
                $post_type_object = get_post_type_object($post_type);
                $post_type_archive = get_post_type_archive_link($post_type);
             
                echo '<li class="breadcrumb-li"><a class="breadcrumb-link breadcrumb-custom-post-type-' . esc_attr( $post_type ) . '" href="' . esc_url($post_type_archive ) . '" title="' . esc_attr( $post_type_object->labels->name ) . '">' . esc_html( $post_type_object->labels->name ) . '</a></li>';
             
            }
             
            $custom_tax_name = get_queried_object()->name;
            echo '<li class="breadcrumb-li"><span class="breadcrumb-current bread-item-archive">' . esc_html( $custom_tax_name ) . '</span></li>';
             
        } else if ( is_single() ) {
            
            // If post is a custom post type
            $post_type = get_post_type();
            $last_category = '';
            // If it is a custom post type (not support custom taxonomy) display name and link
            if( !in_array( $post_type, array('post','listing') ) ) {
                 
                $post_type_object = get_post_type_object($post_type);
                $post_type_archive = get_post_type_archive_link($post_type);

                if(array_key_exists($post_type, $custom_post_types)){
                    echo '<li class="breadcrumb-li"><a class="breadcrumb-link breadcrumb-cat breadcrumb-custom-post-type-' . esc_attr( $post_type ) . '" href="' . esc_url($post_type_archive ) . '" title="' . esc_attr( $custom_post_types[$post_type] ) . '">' . esc_html( $custom_post_types[$post_type] ) . '</a></li>';
                }else{
                    echo '<li class="breadcrumb-li"><a class="breadcrumb-link breadcrumb-cat breadcrumb-custom-post-type-' . esc_attr( $post_type ) . '" href="' . esc_url($post_type_archive ) . '" title="' . esc_attr( $post_type_object->labels->name ) . '">' . esc_html( $post_type_object->labels->name ) . '</a></li>';
                }
                
                echo '<li class="breadcrumb-li"><span class="breadcrumb-current breadcrumb-item-' . esc_attr( $post->ID ) . '" title="' . esc_attr( get_the_title() ) . '">' . get_the_title() . '</span></li>';
            }elseif($post_type == 'post'){
                // Get post category info
                $category = get_the_category();
                 
                // Get last category post is in
                
                if($category){
                    $last_cateogries = array_values($category);
                    $last_category = end($last_cateogries);
                 
                    // Get parent any categories and create array
                    $get_cat_parents = rtrim(get_category_parents($last_category->term_id, true, ','),',');
                    $cat_parents = explode(',',$get_cat_parents);
                     
                    // Loop through parent categories and store in variable $cat_display
                    $cat_display = '';
                    foreach($cat_parents as $parents) {
                        $cat_display .= '<li class="breadcrumb-li"> '.wp_kses_post( $parents ) .'</li>';
                        
                    }
                }
                
                if(!empty($last_category)) {
                    echo wp_kses_post($cat_display );
                    echo '<li class="breadcrumb-li">' . get_the_title() . '</li>';
                     
                // Else if post is in a custom taxonomy
                }
            }
                
                 
            // If it's a custom post type within a custom taxonomy
            if(empty($last_category) && !empty($custom_taxonomy)) {
                $custom_taxonomy_arr = explode(",", $custom_taxonomy) ;
                foreach ($custom_taxonomy_arr as $key => $custom_taxonomy_val) {
                    $taxonomy_terms = get_the_terms( $post->ID, $custom_taxonomy_val );
                    if($taxonomy_terms && !($taxonomy_terms instanceof WP_Error) ){
                        $cat_id         = $taxonomy_terms[0]->term_id;
                        $cat_nicename   = $taxonomy_terms[0]->slug;
                        $cat_link       = get_term_link($taxonomy_terms[0]->term_id, $custom_taxonomy_val);
                        $cat_name       = $taxonomy_terms[0]->name;

                        if(!empty($cat_id)) {
                     
                            echo '<li class="breadcrumb-li"><a class="breadcrumb-link bread-cat-' . esc_attr( $cat_id ) . ' bread-cat-' . esc_attr( $cat_nicename ) . '" href="' . esc_url($cat_link ) . '" title="' . esc_attr( $cat_name ). '">' . esc_html( $cat_name ) . '</a></li>';
                            
                            echo '<li class="breadcrumb-li"><span class="breadcrumb-current breadcrumb-item-' . esc_attr( $post->ID ) . '" title="' . esc_attr( get_the_title() ) . '">' . get_the_title() . '</span></li>';
                         
                        }
                    }

                 } 
                
              
            }
             
            
             
        } else if ( is_category() ) {
              
            // Category page
            echo '<li class="breadcrumb-li"><span class="breadcrumb-current breadcrumb-item-cat-' . esc_attr( $category[0]->term_id ) . ' bread-cat-' . esc_attr( $category[0]->category_nicename ) . '">' . esc_html( $category[0]->cat_name ) . '</span></li>';
              
        } else if ( is_page() ) {
            

            // Standard page
            if( $post->post_parent ){

                $parents = '';
                  
                // If child page, get parents 
                $anc = get_post_ancestors( $post->ID );
                  
                // Get parents in the right order
                $anc = array_reverse($anc);
                  
                // Parent page loop
                foreach ( $anc as $ancestor ) {
                    $parents .= '<li class="breadcrumb-li"><a class="breadcrumb-link breadcrumb-parent-' . esc_attr( $ancestor ) . '" href="' . esc_url(get_permalink($ancestor) ) . '" title="' . esc_attr( get_the_title($ancestor) ) . '">' . get_the_title($ancestor) . '</a></li>';
                    
                }
                  
                // Display parent pages
                echo wp_kses_post($parents );


                  
                    // Current page
                    echo '<li class="breadcrumb-li">' . get_the_title() . '</li>';

                
                  
            } else {
                  
                
                    // Current page
                    echo '<li class="breadcrumb-li">' . get_the_title() . '</li>';

                  
            }
              
        } else if ( is_tag() ) {
              
            // Tag page
              
            // Get tag information
            $term_id = get_query_var('tag_id');
            $taxonomy = 'post_tag';
            $args ='include=' . $term_id;
            $terms = get_terms( $taxonomy, $args );
              
            // Display the tag name
            echo '<li class="breadcrumb-li"><span class="breadcrumb-current breadcrumb-item-tag-' . esc_attr( $terms[0]->term_id ) . ' bread-tag-' . esc_attr( $terms[0]->slug ) . '">' . esc_html( $terms[0]->name ) . '</span></li>';
          
        } elseif ( is_day() ) {
              
            // Day archive
              
            // Year link
            echo '<li class="breadcrumb-li"><a class="breadcrumb-link breadcrumb-year bread-year-' . get_the_time('Y') . '" href="' . get_year_link( get_the_time('Y') ) . '" title="' . get_the_time('Y') . '">' . get_the_time('Y') . esc_html__(' Archives','archia').'</a></li>';
            
              
            // Month link
            echo '<li class="breadcrumb-li"><a class="breadcrumb-link breadcrumb-month bread-month-' . get_the_time('m') . '" href="' . get_month_link( get_the_time('Y'), get_the_time('m') ) . '" title="' . get_the_time('M') . '">' . get_the_time('M') . esc_html__(' Archives','archia').'</a></li>';
            
              
            // Day display
            echo '<li class="breadcrumb-li"><span class="breadcrumb-current bread-' . get_the_time('j') . '"> ' . get_the_time('jS') . ' ' . get_the_time('M') .  esc_html__(' Archives','archia').'</span></li>';
              
        } else if ( is_month() ) {
              
            // Month Archive
              
            // Year link
            echo '<li class="breadcrumb-li"><a class="breadcrumb-link breadcrumb-year bread-year-' . get_the_time('Y') . '" href="' . get_year_link( get_the_time('Y') ) . '" title="' . get_the_time('Y') . '">' . get_the_time('Y') . esc_html__(' Archives','archia').'</a></li>';
            
              
            // Month display
            echo '<li class="breadcrumb-li"><span class="breadcrumb-current breadcrumb-month breadcrumb-month-' . get_the_time('m') . '" title="' . get_the_time('M') . '">' . get_the_time('M') . esc_html__(' Archives','archia').'</span></li>';
              
        } else if ( is_year() ) {
              
            // Display year archive
            echo '<li class="breadcrumb-li"><strong class="breadcrumb-current breadcrumb-current-' . get_the_time('Y') . '" title="' . get_the_time('Y') . '">' . get_the_time('Y') . esc_html__(' Archives','archia').'</span></li>';
              
        } else if ( is_author() ) {
              
            // Auhor archive
              
            // Get the author information
            global $author;
            $userdata = get_userdata( $author );
              
            // Display author name
            echo '<li class="breadcrumb-li"><span class="breadcrumb-current breadcrumb-current-' . esc_attr( $userdata->user_nicename ) . '" title="' . esc_attr( $userdata->display_name ) . '">' .  esc_html__(' Author: ','archia') . esc_html( $userdata->display_name ) . '</span></li>';
          
        } else if ( get_query_var('paged') ) {
              
            // Paginated archives
            echo '<li class="breadcrumb-li"><a href="#" class="breadcrumb-current breadcrumb-current-' . get_query_var('paged') . '" title="'.esc_html__('Page','archia') . get_query_var('paged') . '">'.esc_html__('Page','archia') . ' ' . get_query_var('paged') . '</a></li>';
              
        } else if ( is_search() ) {
          
            // Search results page
            echo '<li class="breadcrumb-li"><span class="breadcrumb-current breadcrumb-current-' . get_search_query() . '" title="'.esc_html__('Search results for: ','archia') . get_search_query() . '">'.esc_html__('Search results for: ','archia') . get_search_query() . '</span></li>';
          
        } elseif ( is_404() ) {
              
            // 404 page
            echo '<li class="breadcrumb-li"><span class="breadcrumb-current breadcrumb-current-404">' . esc_html__('Error 404','archia') . '</span></li>';
        }
            echo '</ul>';
        echo '</div>';
          
    }
      
}


//BREADCRUMB FOR PROJECT 


function archia_breadcrumbs_project($classes='') {
           
    // Settings
    $breadcrums_id      = 'breadcrumbs';
    $breadcrums_class   = 'breadcrumbs fl-wrap breadcrumb-row'.$classes;
    $home_title         = esc_html__('Home','archia');
    $blog_title         = esc_html__('Blog','archia');


    // If you have any custom post types with custom taxonomies, put the taxonomy name below (e.g. product_cat)
    $custom_taxonomy    = '';

    $custom_post_types = array();
      
    // Get the query & post information
    global $post;
      
    // Do not display on the homepage
    if ( !is_front_page() ) 
    {
      
        // Build the breadcrums
        echo '<div id="' . esc_attr($breadcrums_id ) . '" class="' . esc_attr($breadcrums_class ) . '">';
        echo '<ul class="list-inline">';
        echo '<li class="breadcrumb-li"><a href="'.esc_url(home_url()).'"><i class="fa fa-home"></i></a></li>';
        // Home page
        if(is_home())
        {
            // Blog page
            echo '<li class="breadcrumb-li">' . esc_html( $blog_title ) . '</li>';
            
        }
          
        if ( is_archive() && !is_tax() ) 
        {

            // If post is a custom post type
            $post_type = get_post_type();

            if($post_type && array_key_exists($post_type, $custom_post_types))
            {
                echo '<li class="breadcrumb-li"><span class="breadcrumb-current breadcrumb-item-custom-post-type-' . esc_attr( $post_type ) . '">' . esc_html( $custom_post_types[$post_type] ) . '</span></li>';
            }
            else
            {
                echo '<li class="breadcrumb-li"><span class="breadcrumb-current breadcrumb-item-archive">' . get_the_archive_title() . '</span></li>';
            }
             
        } 
        else if ( is_archive() && is_tax() ) 
        {
             
            // If post is a custom post type
            $post_type = get_post_type();
             
            // If it is a custom post type display name and link
            if($post_type && $post_type != 'post') 
            {
                 
                $post_type_object = get_post_type_object($post_type);
                $post_type_archive = get_post_type_archive_link($post_type);
             
                echo '<li class="breadcrumb-li"><a class="breadcrumb-link breadcrumb-custom-post-type-' . esc_attr( $post_type ) . '" href="http://middleeastbuilders.parel.co/projects/" title="' . esc_attr( $post_type_object->labels->name ) . '">Projects</a></li>';
             
            }
             
            $custom_tax_name = get_queried_object()->name;
            echo '<li class="breadcrumb-li"><span class="breadcrumb-current bread-item-archive">' . esc_html( $custom_tax_name ) . '</span></li>';
             
        } 
        else if ( is_single() ) 
        {
            
            // If post is a custom post type
            $post_type = get_post_type();
            $last_category = '';
            // If it is a custom post type (not support custom taxonomy) display name and link
            if( !in_array( $post_type, array('post','listing') ) ) 
            {
                 
                $post_type_object = get_post_type_object($post_type);
                $post_type_archive = get_post_type_archive_link($post_type);

                if(array_key_exists($post_type, $custom_post_types))
                {
                    echo '<li class="breadcrumb-li"><a class="breadcrumb-link breadcrumb-cat breadcrumb-custom-post-type-' . esc_attr( $post_type ) . '" href="http://middleeastbuilders.parel.co/projects/" title="' . esc_attr( $custom_post_types[$post_type] ) . '">Projects</a></li>';
                }
                else
                {
                    echo '<li class="breadcrumb-li"><a class="breadcrumb-link breadcrumb-cat breadcrumb-custom-post-type-' . esc_attr( $post_type ) . '" href="http://middleeastbuilders.parel.co/projects/" title="' . esc_attr( $post_type_object->labels->name ) . '">Projects</a></li>';
                }
                
                echo '<li class="breadcrumb-li"><span class="breadcrumb-current breadcrumb-item-' . esc_attr( $post->ID ) . '" title="' . esc_attr( get_the_title() ) . '">' . get_the_title() . '</span></li>';
            }
              
        }
                
       
    } 
            echo '</ul>';
        echo '</div>';
      
}




if ( ! function_exists( 'archia_edit_link' ) ) :
/**
 * Returns an accessibility-friendly link to edit a post or page.
 *
 * This also gives us a little context about what exactly we're editing
 * (post or page?) so that users understand a bit more where they are in terms
 * of the template hierarchy and their content. Helpful when/if the single-page
 * layout with multiple posts/pages shown gets confusing.
 */
function archia_edit_link() {
	edit_post_link(
		sprintf(
			/* translators: %s: Name of current post */
			__( 'Edit<span class="screen-reader-text"> "%s"</span>', 'archia' ),
			get_the_title()
		),
		'<span class="edit-link">',
		'</span>'
	);
}
endif;

/** 
 * Check for existing shortcode or not
 * https://codex.wordpress.org/Function_Reference/get_shortcode_regex
 */ 
if ( !function_exists('archia_check_shortcode') ) {
    function archia_check_shortcode($content,$shortcode=''){

        if ( !empty($shortcode) && !shortcode_exists( $shortcode ) ) {

            $pattern = get_shortcode_regex(array($shortcode));

            return preg_replace('/'. $pattern .'/s', '', $content );

        }else {
            return $content;
        }  
    }
}

/**
 * Woocommerce support
 *
 */

if ( ! function_exists( 'archia_is_woocommerce_activated' ) ) {
    function archia_is_woocommerce_activated() {
        if ( class_exists( 'woocommerce' ) ) { return true; } else { return false; }
    }
}

function archia_get_header_cart_link(){
    if(archia_is_woocommerce_activated() && archia_get_option('show_mini_cart') ){
        global $woocommerce;
        $my_cart_count = $woocommerce->cart->cart_contents_count;
        if($my_cart_count > 0){
            $url = wc_get_page_permalink( 'cart' );
        }else{
            $url = wc_get_page_permalink( 'shop' );
        }
        return array('url'=>$url,'count'=>$my_cart_count);
    }else{
        return false;
    }
}
function archia_get_option_single($option = '', $default = null){
    $global = archia_get_option($option, $default);
    if(is_singular()){
        $singular = get_post_meta( get_the_ID(), '_cth_'.$option, true );
        if( $singular != '' && $singular != 'use_global') $global = $singular;
    }
    return $global;
}


