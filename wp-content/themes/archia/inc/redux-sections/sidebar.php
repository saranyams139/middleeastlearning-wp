<?php
/* banner-php */
// -> START Header Settings

Redux::setSection( $opt_name, array(
    'title' => esc_html__('Sidebar Menu Options', 'archia'),
    'id'         => 'sidebar-menu-optons',
    'subsection' => true,
    'fields' => array(

        
        
        

        array(
            'id' => 'menusb_header_info',
            'type' => 'textarea',
            'title' => esc_html__('Header Info', 'archia'),
            'default' => '<ul>
<li><a href="#" target="_blank"> <span>Call :</span> +7(111)123456789</a></li>
<li><a href="#" target="_blank"> <span>Write :</span> yourmail@domain.com</a></li>
</ul>',
        ),

        array(
            'id' => 'menusb_logo',
            'type' => 'media',
            'url' => true,
            'title' => esc_html__('Logo', 'archia'),

            'default' => array('url' => get_template_directory_uri().'/images/logo.png'),
        ),

        array(
            'id' => 'menusb_copyright',
            'type' => 'textarea',
            'title' => esc_html__('Copyright', 'archia'),
            'default' => '<p>&copy; archia 2017. All rights reserved.</p>',
        ), 

          
    ),
));