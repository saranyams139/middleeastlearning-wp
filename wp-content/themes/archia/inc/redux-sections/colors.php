<?php
/* banner-php */
// -> START Header Settings
Redux::setSection( $opt_name, array(
    'title' => esc_html__('Colors', 'archia'),
    'id'         => 'styling-settings',
    'subsection' => false,
    
    'icon'       => 'el-icon-magic',
    'fields' => array(
        array(
            'id'       => 'color-preset',
            'type'     => 'image_select',
            'title'    => esc_html__( 'Theme Color', 'archia' ),
            'options'  => array(
                'skin-1' => array(
                    'alt' => 'Skin 1',
                    'img' => get_template_directory_uri(). '/inc/assets/skin1.png'
                ),
                'skin-2' => array(
                    'alt' => 'Skin 2',
                    'img' => get_template_directory_uri(). '/inc/assets/skin2.png'
                ),
                'skin-3' => array(
                    'alt' => 'Skin 3',
                    'img' => get_template_directory_uri(). '/inc/assets/skin3.png'
                ),
                'skin-4' => array(
                    'alt' => 'Skin 4',
                    'img' => get_template_directory_uri(). '/inc/assets/skin4.png'
                ),
                'color/skin-1' => array(
                    'alt' => 'Skin 1 - Full',
                    'img' => get_template_directory_uri(). '/inc/assets/skin1-2.png'
                ),
                'color/skin-2' => array(
                    'alt' => 'Skin 2 - Full',
                    'img' => get_template_directory_uri(). '/inc/assets/skin2-2.png'
                ),
                'color/skin-3' => array(
                    'alt' => 'Skin 3 - Full',
                    'img' => get_template_directory_uri(). '/inc/assets/skin3-2.png'
                ),
                'color/skin-4' => array(
                    'alt' => 'Skin 4 - Full',
                    'img' => get_template_directory_uri(). '/inc/assets/skin4-2.png'
                ),
            ),
            'default'  => 'skin-1'
        ),

        array(
            'id' => 'use_custom_color',
            'type' => 'switch',
            'on'=> esc_html__('Yes','archia'),
            'off'=> esc_html__('No','archia'),
            'title' => esc_html__('Use Custom Color', 'archia'),
            'subtitle' => wp_kses(__('Set this option to <b>Yes</b> if you want to use color variants bellow.', 'archia'), array('b'=>array(),'strong'=>array(),'p'=>array()) ),
            'default' => false
        ),

        array(
            'id' => 'theme-color',
            'type' => 'color',
            'title' => esc_html__('Theme Color', 'archia'),
            'transparent'      => false,
            'subtitle' => esc_html__('Default: #a8ca1e', 'archia'),
            'default' => '#a8ca1e',
            'validate' => 'color',
        ),

        array(
            'id' => 'overlay-color',
            'type' => 'color',
            'title' => esc_html__('Overlay Color', 'archia'),
            'transparent'      => false,
            'subtitle' => esc_html__('Default: #191c0a', 'archia'),
            'default' => '#191c0a',
            'validate' => 'color',
        ),

    ),
) );