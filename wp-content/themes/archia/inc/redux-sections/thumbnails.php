<?php
/* banner-php */
// -> START Header Settings
Redux::setSection( $opt_name, array(
    'title' => esc_html__('Media', 'archia'),
    'id'         => 'thumbnail_images',
    'subsection' => false,
    'desc'       => wp_kses(__( '<p>These settings affect the display and dimensions of images in your pages.</p>
        <p><em> Enter 9999 as Width value and uncheck Hard Crop to make your thumbnail dynamic width.</em></p>
        <p><em> Enter 9999 as Height value and uncheck Hard Crop to make your thumbnail dynamic height.</em></p>
        <p><em> Enter 9999 as Width and Height values to use full size image.</em></p>
After changing these settings you may need to ', 'archia' ), array('p'=>array(),'a'=>array('class'=>array(),'href'=>array(),'target'=>array(),),'strong'=>array(),'em'=>array(),) ) .'<a href="'.esc_url('http://wordpress.org/extend/plugins/regenerate-thumbnails/' ).'" target="_blank">regenerate your thumbnails</a>',
    'icon'       => 'el-icon-picture',
    'fields' => array(
        array(
            'id' => 'enable_custom_sizes',
            'type' => 'switch',
            'on'=> esc_html__('Yes','archia'),
            'off'=> esc_html__('No','archia'),
            'title' => esc_html__('Enable Custom Image Sizes', 'archia'),
            'default' => false
        ), 

        

        array(
            'id'       => 'thumb_size_opt_5',
            'type'     => 'thumbnail_size',
            'title' => esc_html__('Portfolio Size', 'archia'),
            'subtitle' => esc_html__('Demo: Width - 360, Height - 425, Hard crop - checked', 'archia'),
            'default'  => array(
                'width'   => '360', 
                'height'  => '425',
                'hard_crop'  => 1
            ),
        ),
        array(
            'id'       => 'thumb_size_opt_6',
            'type'     => 'thumbnail_size',
            'title' => esc_html__('Portfolio x2 Size', 'archia'),
            'subtitle' => esc_html__('Demo: Width - 750, Height - 425, Hard crop - checked', 'archia'),
            'default'  => array(
                'width'   => '750', 
                'height'  => '425',
                'hard_crop'  => 1
            ),
        ),

        array(
            'id'       => 'thumb_size_opt_7',
            'type'     => 'thumbnail_size',
            'title' => esc_html__('Portfolio x3 Size', 'archia'),
            'subtitle' => esc_html__('Demo: Width - 1140, Height - 425, Hard crop - checked', 'archia'),
            'default'  => array(
                'width'   => '1140', 
                'height'  => '425',
                'hard_crop'  => 1
            ),
        ),

        
        

        
        array(
            'id'       => 'thumb_size_opt_8',
            'type'     => 'thumbnail_size',
            'title' => esc_html__('Member Thumbnail Size', 'archia'),
            'subtitle' => esc_html__('Demo: Width - 263, Height - 263, Hard crop - checked', 'archia'),
            'default'  => array(
                'width'   => '263', 
                'height'  => '263',
                'hard_crop'  => 1
            ),
        ),



        array(
            'id'       => 'thumb_size_opt_9',
            'type'     => 'thumbnail_size',
            'title' => esc_html__('Blog Thumbnail Size', 'archia'),
            'subtitle' => esc_html__('Demo: Width - 750, Height - 540, Hard crop - checked', 'archia'),
            'default'  => array(
                'width'   => '750', 
                'height'  => '540',
                'hard_crop'  => 1
            ),
        ),

        array(
            'id'       => 'thumb_size_opt_10',
            'type'     => 'thumbnail_size',
            'title' => esc_html__('Blog Single Size', 'archia'),
            'subtitle' => esc_html__('Demo: Width - 750, Height - 540, Hard crop - checked', 'archia'),
            'default'  => array(
                'width'   => '750', 
                'height'  => '540',
                'hard_crop'  => 1
            ),
        ),

        array(
            'id'       => 'thumb_size_opt_11',
            'type'     => 'thumbnail_size',
            'title' => esc_html__('Related Posts Thumbnail', 'archia'),
            'subtitle' => esc_html__('Demo: Width - 360, Height - 260, Hard crop - checked', 'archia'),
            'default'  => array(
                'width'   => '360', 
                'height'  => '260',
                'hard_crop'  => 1
            ),
        ),

        array(
            'id'       => 'thumb_size_opt_12',
            'type'     => 'thumbnail_size',
            'title' => esc_html__('Recent Posts Thumbnail', 'archia'),
            'subtitle' => esc_html__('Demo: Width - 100, Height - 82, Hard crop - checked', 'archia'),
            'default'  => array(
                'width'   => '100', 
                'height'  => '82',
                'hard_crop'  => 1
            ),
        ),
        

        

    ),
) );