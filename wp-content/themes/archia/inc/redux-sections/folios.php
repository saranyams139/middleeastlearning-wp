<?php
/* banner-php */
// -> START Header Settings
Redux::setSection( $opt_name, array(
    'title' => esc_html__('Portfolio', 'archia'),
    'desc' => esc_html__('Config Header, Footer and Layout for portfolio archive page. Effected pages are: ', 'archia').'<br>'.esc_html__('Portfolio List page: ', 'archia').'<a href="'.esc_url(home_url('/?post_type=portfolio' )).'" target="_blank">'.esc_url(home_url('/?post_type=portfolio' )).'</a><br>'.esc_html__('Portfolio List page: ', 'archia').'<a href="'.esc_url(home_url('/portfolio/' )).'" target="_blank">'.esc_url(home_url('/portfolio/' )).'</a><br>'.esc_html__('Portfolio Category page: ', 'archia').'<a href="'.esc_url(home_url('/portfolio_cat/nature/' )).'" target="_blank">'.esc_url(home_url('/portfolio_cat/nature/' )).'</a>',
    'id'         => 'portfolio-settings',
    'subsection' => false,
    'icon'       => 'el-icon-briefcase',
    'fields' => array(

        array(
            'id'       => 'folio_header',
            'type'     => 'switch',
            'on'=> esc_html__('Yes', 'archia'),
            'off'=> esc_html__('No', 'archia'),
            'title'    => esc_html__( 'Show Header', 'archia' ),
            'default'  => true,
        ),
        array(
            'id' => 'folio_title',
            'type' => 'text',
            'title' => esc_html__('Header Title', 'archia'),
            'default' => 'Our Portfolios'
        ),
        array(
            'id' => 'folio_intro',
            'type' => 'textarea',
            'title' => esc_html__('Intro Text', 'archia'),
            'default' => '<p>Stay informed on our news or events!</p>'
        ),

        
        array(
            'id' => 'folio_bg_image',
            'type' => 'media',
            'url' => true,
            'title' => esc_html__('Header Image', 'archia'),
            'default' => array(
                'url' => get_template_directory_uri().'/assets/images/banner/bnr3.jpg'
            ),
        ),
        

        array(
            'id' => 'folio_style',
            'type' => 'select',
            'title' => esc_html__('Layout Style', 'archia'),

            'options' => array(
                            'default' => esc_html__('Default - Home 1','archia'),
                            'tile' => esc_html__('Tiles - Portfolio 2','archia'),
                            'masonry' => esc_html__('Masonry - Portfolio 1','archia'),
                            'home2' => esc_html__('Home 2 Style','archia'),
                            'home4' => esc_html__('Home 4 Style','archia'),
                            'home5' => esc_html__('Home 5 Style','archia'),
             ), //Must provide key => value pairs for select options
            'default' => 'default'
        ),

        array(
            'id' => 'folio-cols',
            'type' => 'select',
            'title' => esc_html__('Portfolio Columns', 'archia'),
            'options' => array(
                            'one' => esc_html__('One Column', 'archia'),
                            'two' => esc_html__('Two Columns', 'archia'),
                            'three' => esc_html__('Three Columns', 'archia'),
                            'four' => esc_html__('Four Columns', 'archia'),
                            'five' => esc_html__('Five Columns', 'archia'),
             ), //Must provide key => value pairs for select options
            'default' => 'two'
        ),

        array(
            'id' => 'folio-pad',
            'type' => 'select',
            'title' => esc_html__('Spacing', 'archia'),
            'options' => array(
                            'extrasmall' => esc_html__('Extra Small', 'archia'),
                            'small' => esc_html__('Small', 'archia'),
                            'medium' => esc_html__('Medium', 'archia'),
                            'big' => esc_html__('Big', 'archia'),
                            'no' => esc_html__('None', 'archia'),
             ), //Must provide key => value pairs for select options
            'default' => 'big'
        ),


        array(
            'id'       => 'folio_posts',
            'type'     => 'text',
            'title'    => esc_html__( 'Posts per page', 'archia' ),
            'subtitle' => esc_html__( 'Number of post to show per page, -1 to show all posts.', 'archia' ),
            'desc' => esc_html__( 'To use Loadmore or Infinit scroll to load features you have set this value less than total portfolio items number.', 'archia' ),
            'default'  => 12,
        ),
        
        array(
            'id' => 'folio_orderby',
            'type' => 'select',
            'title' => esc_html__('Order Portfolio By', 'archia'),
            'options' => array(
                            'none' => esc_html__( 'None', 'archia' ), 
                            'ID' => esc_html__( 'Post ID', 'archia' ), 
                            'author' => esc_html__( 'Post Author', 'archia' ),
                            'title' => esc_html__( 'Post title', 'archia' ), 
                            'name' => esc_html__( 'Post name (post slug)', 'archia' ),
                            'date' => esc_html__( 'Created Date', 'archia' ),
                            'modified' => esc_html__( 'Last modified date', 'archia' ),
                            'parent' => esc_html__( 'Post Parent id', 'archia' ),
                            'rand' => esc_html__( 'Random', 'archia' ),
                        ), //Must provide key => value pairs for select options
            'default' => 'date'
        ),
        array(
            'id' => 'folio_order',
            'type' => 'select',
            'title' => esc_html__('Order Portfolio', 'archia'),
            'options' => array(
                            'DESC' => esc_html__( 'Descending', 'archia' ),
                            'ASC' => esc_html__( 'Ascending', 'archia' ), 
                            
                        ), //Must provide key => value pairs for select options
            'default' => 'DESC'
        ),
        
        
    ),
) );

Redux::setSection( $opt_name, array(
    'title' => esc_html__('Filter Options', 'archia'),
    'id'         => 'folio-filter-optons',
    'subsection' => true,
    'fields' => array(

        
        array(
            'id'       => 'folio_show_filter',
            'type'     => 'switch',
            'on'=> esc_html__('Yes','archia'),
            'off'=> esc_html__('No','archia'),
            'title'    => esc_html__( 'Show Filter', 'archia' ),
            'default'  => true,
        ),

        

        array(
            'id' => 'folio_filter_orderby',
            'type' => 'select',
            'title' => esc_html__('Order Filter By', 'archia'),
            'options' => array(
                            'id' => esc_html__( 'ID', 'archia' ), 
                            'count' => esc_html__( 'Count', 'archia' ),
                            'name' => esc_html__( 'Name', 'archia' ), 
                            'slug' => esc_html__( 'Slug', 'archia' ),
                            'none' => esc_html__( 'None', 'archia' )
                        ), //Must provide key => value pairs for select options
            'default' => 'name'
        ),
        array(
            'id' => 'folio_filter_order',
            'type' => 'select',
            'title' => esc_html__('Order Filter', 'archia'),
            'options' => array(
                            'ASC' => esc_html__( 'Ascending', 'archia' ), 
                            'DESC' => esc_html__( 'Descending', 'archia' ),
                        ), //Must provide key => value pairs for select options
            'default' => 'ASC'
        ),

       

        array(
            'id'       => 'folio_filter_all',
            'type'     => 'switch',
            'on'=> esc_html__('Yes','archia'),
            'off'=> esc_html__('No','archia'),
            'title'    => esc_html__( 'Show All in filter', 'archia' ),
            'default'  => true,
        ),

    ),
));
