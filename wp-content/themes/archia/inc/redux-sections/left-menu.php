<?php
/* banner-php */
// -> START Header Settings

Redux::setSection( $opt_name, array(
    'title' => esc_html__('Left Sidebar', 'archia'),
    'id'         => 'left-hand-settings',
    'subsection' => false,
    
    'icon'       => 'fa fa-th-list',
    'fields' => array(

        array(
            'id' => 'show_left_bar',
            'type' => 'switch',
            'title' => esc_html__('Show Left Bar', 'archia'),
            'desc' => esc_html__('Show left Blank bar on your site', 'archia'),
            'default' => true
        ),
        array(
            'id' => 'show_fixed_title',
            'type' => 'switch',
            'title' => esc_html__('Show Left Title', 'archia'),
            'desc' => esc_html__('Show title on the left of your page.', 'archia'),
            'default' => true
        ),
        array(
            'id' => 'left_bar_width',
            'type' => 'text',
            'title' => esc_html__('Left Bar Width', 'archia'),
            'desc' => esc_html__('Default: 80px. If you add more info to the footer section you must increase the value here.', 'archia'),
            'default' => '80px',
        ),
        array(
            'id' => 'left_socials',
            'type' => 'ace_editor',
            'title' => esc_html__('Left Bar Icons', 'archia'),
            'mode'=>'html',
            'full_width'=>true,
            'desc' => esc_html__('You should replace href attribute value with your social links, and get social icons from:', 'archia').' <a href="http://fontawesome.io/icons/#brand" target="_blank">'.esc_html__(' Awesome Brand Icons','archia' ).'</a>',
            'default' => '<ul>
<li><a href="#" target="_blank" ><i class="fa fa-facebook"></i></a></li>
<li><a href="#" target="_blank"><i class="fa fa-twitter"></i></a></li>
<li><a href="#" target="_blank" ><i class="fa fa-instagram"></i></a></li>
<li><a href="#" target="_blank" ><i class="fa fa-pinterest"></i></a></li>
<li><a href="#" target="_blank" ><i class="fa fa-tumblr"></i></a></li>
</ul>',
        ),

    ),
) );