<?php
/* banner-php */
// -> START Header Settings
Redux::setSection( $opt_name, array(
    'title' => esc_html__('Protected Post Page Options', 'archia'),
    'id'         => 'protected_post_page_options',
    'subsection' => false,
    'icon'       => 'el el-lock-alt',
    'fields' => array(
        array(
            'id' => 'password_page_intro',
            'type' => 'textarea',
            'title' => esc_html__('Additional Message', 'archia'),
            'default' => ''
        ),
        
        
    ),
) );

Redux::setSection( $opt_name, array(
    'title' => esc_html__('404 Page', 'archia'),
    'id'         => '404-intro-text-settings',
    'subsection' => false,
    'icon'       => 'el el-info-circle',
    'fields' => array(
        array(
            'id' => '404_bg',
            'type' => 'media',
            'url' => true,
            'title' => esc_html__('Header Background', 'archia'),
            'default' => array(
                'url' => get_template_directory_uri().'/assets/images/banner/bnr2.jpg'
            ),
        ),
        array(
            'id' => '404_intro',
            'type' => 'textarea',
            'title' => esc_html__('Head Intro', 'archia'),
            
            'default' => 'Stay informed on our news or events!'
        ),
        array(
            'id' => '404_message',
            'type' => 'textarea',
            'title' => esc_html__('404 Message', 'archia'),
            
            'default' => 'We’re sorry, but the page you were looking for doesn’t exist..'
        ),
        array(
            'id' => 'back_home_link',
            'type' => 'text',
            'title' => esc_html__('Back Home Link', 'archia'),
            'default' => esc_url( home_url('/' ) )
        ),
        
        
    ),
) );