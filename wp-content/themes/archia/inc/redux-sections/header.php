<?php
/* banner-php */
// -> START Header Settings

Redux::setSection( $opt_name, array(
    'title' => esc_html__('Header', 'archia'),
    'id'         => 'header-settings',
    'subsection' => false,
    
    'icon'       => 'fa fa-header',
    'fields' => array(
        array(
            'id' => 'show_search',
            'type' => 'switch',
            'on'=> esc_html__('Yes','archia'),
            'off'=> esc_html__('No','archia'),
            'title' => esc_html__('Show Topbar Search', 'archia'),
            'default' => true
        ),

        array(
            'id' => 'default_header_contact',
            'type' => 'textarea',
            'title' => esc_html__('Default Header Contact Info', 'archia'),
            'default' => '<ul>
                            <li>
                                <div  class="contact-info">
                                    <i class="fa fa-phone"></i>
                                    <span>Phone</span>
                                    <h4 class="m-b0">003 746 87 92</h4>
                                </div>
                            </li>
                            <li>
                                <a id="quik-search-btn" href="javascript:;" class="search-btn"><i class="fa fa-search"></i></a>
                            </li>
                        </ul>',
        ),
        array(
            'id' => 'default_header_social',
            'type' => 'textarea',
            'title' => esc_html__('Default Header Social', 'archia'),
            'default' => '<div class="social-menu">
                    <ul>
                        <li><a href="javascript:void(0);"><i class="fa fa-facebook"></i></a></li>
                        <li><a href="javascript:void(0);"><i class="fa fa-twitter"></i></a></li>
                        <li><a href="javascript:void(0);"><i class="fa fa-google-plus"></i></a></li>
                        <li><a href="javascript:void(0);"><i class="fa fa-facebook"></i></a></li>
                    </ul>
                    <p class="copyright-head">© 2019 Archia</p>
                </div>',
        ),
        array(
            'id' => 'share_names',
            'type' => 'text',
            'title' => esc_html__('Share Names', 'archia'),
            'subtitle' => wp_kses('Enter your social share names separated by a comma.<br> List bellow are available names:<strong><br> facebook,twitter,linkedin,in1,tumblr,<br>digg,googleplus,reddit,pinterest<br>,stumbleupon,email,vk</strong>', array('br'=>array(),'strong'=>array()) ),
            
            'default' => "",
        ),

        array(
            'id' => 'menu_position',
            'type' => 'select',
            'title' => esc_html__('Main Menu Position', 'archia'),

            'options' => array(
                            'top' => esc_html__('Top Bar','archia'),
                            'sidebar' => esc_html__('Side Bar','archia'),
             ), 
            'default' => 'top'
        ),

        

    ),
) );