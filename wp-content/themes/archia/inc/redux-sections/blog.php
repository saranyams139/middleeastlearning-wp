<?php
/* banner-php */
// -> START Header Settings
Redux::setSection( $opt_name, array(
    'title' => esc_html__('Blog', 'archia'),
    'id'         => 'blog-settings',
    'subsection' => false,
    
    'icon'       => 'el-icon-website',
    'fields' => array(
        
        array(
            'id'       => 'show_blog_header',
            'type'     => 'switch',
            'on'=> esc_html__('Yes', 'archia'),
            'off'=> esc_html__('No', 'archia'),
            'title'    => esc_html__( 'Show Header', 'archia' ),
            'default'  => true,
        ),
        array(
                'id' => 'blog_head_title',
                'type' => 'text',
                'title' => esc_html__('Header Title', 'archia'),
                'default' => 'Our Blog'
            ),
        array(
                'id' => 'blog_head_title_desc',
                'type' => 'textarea',
                'title' => esc_html__('Blog Intro Text', 'archia'),
                'default' => '<p>Stay informed on our news or events!</p>'
            ),

        
        array(
            'id' => 'blog_header_image',
            'type' => 'media',
            'url' => true,
            'title' => esc_html__('Blog Header Image', 'archia'),
            'default' => array(
                'url' => get_template_directory_uri().'/assets/images/banner/bnr3.jpg'
            ),
        ),
        
        
        array(
                'id'       => 'blog_layout',
                'type'     => 'image_select',
                'title'    => esc_html__( 'Blog Sidebar Layout', 'archia' ),
                'subtitle' => esc_html__( 'Select main content and sidebar layout. The option 4 is default layout with right parallax image for archia theme.', 'archia' ),
                'options'  => array(
                    'fullwidth' => array(
                        'alt' => 'Fullwidth',
                        'img' => ReduxFramework::$_url . 'assets/img/1col.png'
                    ),
                    'left_sidebar' => array(
                        'alt' => 'Left Sidebar',
                        'img' => ReduxFramework::$_url . 'assets/img/2cl.png'
                    ),
                    'right_sidebar' => array(
                        'alt' => 'Right Sidebar',
                        'img' => ReduxFramework::$_url . 'assets/img/2cr.png'
                    ),
                    
                ),
                'default'  => 'right_sidebar'
            ),

        array(
            'id' => 'blog-sidebar-width',
            'type' => 'select',
            'title' => esc_html__('Blog Sidebar Width', 'archia'),
            'subtitle' => esc_html__( 'Based on Bootstrap 12 columns.', 'archia' ),
            'options' => array(
                            '2' => esc_html__('2 Columns', 'archia'),
                            '3' => esc_html__('3 Columns', 'archia'),
                            '4' => esc_html__('4 Columns', 'archia'),
                            '5' => esc_html__('5 Columns', 'archia'),
                            '6' => esc_html__('6 Columns', 'archia'),
             ), //Must provide key => value pairs for select options
            'default' => '4'
        ),

        

        array(
            'id' => 'blog-grid-style',
            'type' => 'select',
            'title' => esc_html__('Blog Style', 'archia'),
            'options' => array(
                            'grid' => esc_html__('Grid', 'archia'),
                            'masonary' => esc_html__('Masonry', 'archia'),
                            'listing' => esc_html__('Listing', 'archia'),
             ), //Must provide key => value pairs for select options
            'default' => 'grid'
        ),

        
        array(
            'id' => 'blog-cols',
            'type' => 'select',
            'title' => esc_html__('Blog Columns', 'archia'),
            'subtitle' => esc_html__('For Grid and Masonry style only.', 'archia'),
            'options' => array(
                            'one' => esc_html__('One Column', 'archia'),
                            'two' => esc_html__('Two Columns', 'archia'),
                            'three' => esc_html__('Three Columns', 'archia'),
                            'four' => esc_html__('Four Columns', 'archia'),
                            'five' => esc_html__('Five Columns', 'archia'),
             ), //Must provide key => value pairs for select options
            'default' => 'two'
        ),

        array(
            'id' => 'blog-pad',
            'type' => 'select',
            'title' => esc_html__('Spacing', 'archia'),
            
            'options' => array(
                            'extrasmall' => esc_html__('Extra Small', 'archia'),
                            'small' => esc_html__('Small', 'archia'),
                            'medium' => esc_html__('Medium', 'archia'),
                            'big' => esc_html__('Big', 'archia'),
                            'no' => esc_html__('None', 'archia'),
             ), //Must provide key => value pairs for select options
            'default' => 'big'
        ),

        array(
            'id'       => 'blog_list_show_format',
            'type'     => 'switch',
            'on'        => esc_html__('Yes','archia'),
            'off'       => esc_html__('No','archia'),
            'title'    => esc_html__( 'Show Post Format on blog grid', 'archia' ),
            'default'  => false,
        ),

        array(
            'id'       => 'blog_author',
            'type'     => 'switch',
            'on'        => esc_html__('Yes','archia'),
            'off'       => esc_html__('No','archia'),
            'title'    => esc_html__( 'Show Author', 'archia' ),
            'default'  => true,
        ),
        
        array(
            'id'       => 'blog_date',
            'type'     => 'switch',
            'on'        => esc_html__('Yes','archia'),
            'off'       => esc_html__('No','archia'),
            'title'    => esc_html__( 'Show Date', 'archia' ),
            'default'  => true,
        ),
        
        array(
            'id'       => 'blog_cats',
            'type'     => 'switch',
            'on'        => esc_html__('Yes','archia'),
            'off'       => esc_html__('No','archia'),
            'title'    => esc_html__( 'Show Categories', 'archia' ),
            'default'  => true,
        ),
        array(
            'id'       => 'blog_tags',
            'type'     => 'switch',
            'on'        => esc_html__('Yes','archia'),
            'off'       => esc_html__('No','archia'),
            'title'    => esc_html__( 'Show Tags', 'archia' ),
            'default'  => true,
        ),
        
        array(
            'id'       => 'blog_comments',
            'type'     => 'switch',
            'on'        => esc_html__('Yes','archia'),
            'off'       => esc_html__('No','archia'),
            'title'    => esc_html__( 'Show Comments', 'archia' ),
            'default'  => true,
        ),

        array(
            'id'        => 'blog_like',
            'type'      => 'switch',
            'on'        => esc_html__('Yes','archia'),
            'off'       => esc_html__('No','archia'),
            'title'     => esc_html__( 'Show Like post', 'archia' ),
            'default'  => true,
        ),

        array(
            'id'        => 'blog_views',
            'type'      => 'switch',
            'on'        => esc_html__('Yes','archia'),
            'off'       => esc_html__('No','archia'),
            'title'     => esc_html__( 'Show post Views', 'archia' ),
            'default'  => true,
        ),

    ),
) );


Redux::setSection( $opt_name, array(
    'title' => esc_html__('Blog Single', 'archia'),
    'id'         => 'blog-single-optons',
    'subsection' => true,
    'fields' => array(

        
        
        array(
            'id' => 'blog-single-sidebar-width',
            'type' => 'select',
            'title' => esc_html__('Single Sidebar Width', 'archia'),
            'subtitle' => esc_html__( 'Based on Bootstrap 12 columns.', 'archia' ),
            'options' => array(
                            '2' => esc_html__('2 Columns', 'archia'),
                            '3' => esc_html__('3 Columns', 'archia'),
                            '4' => esc_html__('4 Columns', 'archia'),
                            '5' => esc_html__('5 Columns', 'archia'),
                            '6' => esc_html__('6 Columns', 'archia'),
             ), //Must provide key => value pairs for select options
            'default' => '4'
        ),

        array(
            'id'       => 'blog_single_title',
            'type'     => 'switch',
            'on'        => esc_html__('Yes','archia'),
            'off'       => esc_html__('No','archia'),
            'title'    => esc_html__( 'Show Title', 'archia' ),
            'default'  => false,
        ),
        array(
            'id'       => 'single_featured',
            'type'     => 'switch',
            'on'        => esc_html__('Yes','archia'),
            'off'       => esc_html__('No','archia'),
            'title'    => esc_html__( 'Show Featured image', 'archia' ),
            'default'  => true,
        ),
        
        array(
            'id'       => 'single_date',
            'type'     => 'switch',
            'on'        => esc_html__('Yes','archia'),
            'off'       => esc_html__('No','archia'),
            'title'    => esc_html__( 'Show Date', 'archia' ),
            'default'  => true,
        ),
        array(
            'id'       => 'single_cats',
            'type'     => 'switch',
            'on'        => esc_html__('Yes','archia'),
            'off'       => esc_html__('No','archia'),
            'title'    => esc_html__( 'Show Categories', 'archia' ),
            'default'  => true,
        ),
        array(
            'id'       => 'single_tags',
            'type'     => 'switch',
            'on'        => esc_html__('Yes','archia'),
            'off'       => esc_html__('No','archia'),
            'title'    => esc_html__( 'Show Tags', 'archia' ),
            'default'  => true,
        ),

        array(
            'id'        => 'single_like',
            'type'      => 'switch',
            'on'        => esc_html__('Yes','archia'),
            'off'       => esc_html__('No','archia'),
            'title'     => esc_html__( 'Show Like post', 'archia' ),
            'default'  => true,
        ),

        array(
            'id'       => 'single_author',
            'type'     => 'switch',
            'on'        => esc_html__('Yes','archia'),
            'off'       => esc_html__('No','archia'),
            'title'    => esc_html__( 'Show Author Block', 'archia' ),
            'default'  => true,
        ),

        array(
            'id'       => 'single_author_block',
            'type'     => 'switch',
            'on'=> esc_html__('Yes', 'archia'),
            'off'=> esc_html__('No', 'archia'),
            'title'    => esc_html__( 'Show author box ', 'archia' ),
            'default'  => true,
        ),
        array(
            'id'       => 'single_recent_posts',
            'type'     => 'switch',
            'on'=> esc_html__('Yes', 'archia'),
            'off'=> esc_html__('No', 'archia'),
            'title'    => esc_html__( 'Show recent posts ', 'archia' ),
            'default'  => true,
        ),
        
        array(
            'id'       => 'single_nav',
            'type'     => 'switch',
            'on'        => esc_html__('Yes','archia'),
            'off'       => esc_html__('No','archia'),
            'title'    => esc_html__( 'Show posts navigation', 'archia' ),
            'default'  => true,
        ),
        array(
            'id'        => 'nav_same_term',
            'type'      => 'switch',
            'on'        => esc_html__('Yes','archia'),
            'off'       => esc_html__('No','archia'),
            'title'     => esc_html__( 'Next/Prev posts should be in same category', 'archia' ),
            'default'  => false,
        ),
          
    ),
));