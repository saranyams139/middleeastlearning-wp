<?php
/* banner-php */
// -> START General Settings

Redux::setSection( $opt_name, array(
    'title' => esc_html__('General', 'archia'),
    'id'         => 'general-settings',
    'subsection' => false,
    
    'icon'       => 'el-icon-cogs',
    'fields' => array(
        

        array(
            'id' => 'show_loader',
            'type' => 'switch',
            'on'=> esc_html__('Yes','archia'),
            'off'=> esc_html__('No','archia'),
            'title' => esc_html__('Show loadder', 'archia'),
            'default' => true
        ),
        
        

        
        array(
            'id' => 'footer_copyright',
            'type' => 'textarea',
            'title' => esc_html__('Footer Copyright', 'archia'),
            'default' => '<span class="ft-copy">&#169; CTHthemes 2019. All rights reserved.</span>',
        ),

        
    ),
) );