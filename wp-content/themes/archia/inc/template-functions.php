<?php
/* banner-php */
/**
 * Adds custom classes to the array of body classes.
 *
 * @param array $classes Classes for the body element.
 * @return array
 */
function archia_body_classes( $classes ) {
	// Add class for fullscreen and fixed footer


    if(post_password_required()) $classes[] = 'is-protected-page';

	// Add class of group-blog to blogs with more than 1 published author.
	if ( is_multi_author() ) {
		$classes[] = 'group-blog';
	}

	// Add class of hfeed to non-singular pages.
	if ( is_singular() ) {
		$classes[] = 'archia-singular';
        if( false == get_post_meta( get_the_ID(), '_cth_show_header', true ) ) $classes[] = 'archia-no-head-sec';
	}else{
        $classes[] = 'hfeed';
        if( false == archia_get_option('show_blog_header') ) $classes[] = 'archia-no-head-sec';
    }

	// Add class if we're viewing the Customizer for easier styling of theme options.
	if ( is_customize_preview() ) {
		$classes[] = 'archia-customizer';
	}

	// Add class on front page.
	if ( is_front_page() && 'posts' !== get_option( 'show_on_front' ) ) {
		$classes[] = 'archia-front-page';
	}

    if(isset($_GET['dark_theme']) && $_GET['dark_theme'] = true ) $classes[] = 'archia-dark';

	return $classes;
}
add_filter( 'body_class', 'archia_body_classes' );



/**
 * Return attachment image link by using wp_get_attachment_image_src function
 *
 */
function archia_get_attachment_thumb_link( $id, $size = 'thumbnail' ){
    $image_attributes = wp_get_attachment_image_src( $id, $size, false );
    if ( $image_attributes ) {
        return $image_attributes[0];
    }
    return '';
}

/**
 * Add custom image sizes attribute to enhance responsive image functionality
 * for content images.
 *
 * @since archia 1.2
 *
 * @param string $sizes A source size value for use in a 'sizes' attribute.
 * @param array  $size  Image size. Accepts an array of width and height
 *                      values in pixels (in that order).
 * @return string A source size value for use in a content image 'sizes' attribute.
 */
function archia_content_image_sizes_attr( $sizes, $size ) {
    return '';
}

add_filter( 'wp_calculate_image_sizes', 'archia_content_image_sizes_attr', 10, 2 );


if(!function_exists('archia_get_template_part')){
    /**
     * Load a template part into a template
     *
     * Makes it easy for a theme to reuse sections of code in a easy to overload way
     * for child themes.
     *
     * Includes the named template part for a theme or if a name is specified then a
     * specialised part will be included. If the theme contains no {slug}.php file
     * then no template will be included.
     *
     * The template is included using require, not require_once, so you may include the
     * same template part multiple times.
     *
     * For the $name parameter, if the file is called "{slug}-special.php" then specify
     * "special".
      * For the var parameter, simple create an array of variables you want to access in the template
     * and then access them e.g. 
     * 
     * array("var1=>"Something","var2"=>"Another One","var3"=>"heres a third";
     * 
     * becomes
     * 
     * $var1, $var2, $var3 within the template file.
     *
     * @since 1.0.3
     *
     * @param string $slug The slug name for the generic template.
     * @param string $name The name of the specialised template.
     * @param array $vars The list of variables to carry over to the template
     * @author CTHthemes 
     * @ref http://www.zmastaa.com/2015/02/06/php-2/wordpress-passing-variables-get_template_part
     * @ref http://keithdevon.com/passing-variables-to-get_template_part-in-wordpress/
     */
    function archia_get_template_part( $slug, $name = null, $vars = null ) {
        $template_name = "{$slug}.php";
        $name      = (string) $name;
        if ( '' !== $name ) {
            $template_name = "{$slug}-{$name}.php";
        }
        $located = locate_template($template_name, false);
        if($located !== ''){
            /*
             * This use of extract() cannot be removed. There are many possible ways that
             * templates could depend on variables that it creates existing, and no way to
             * detect and deprecate it.
             *
             * Passing the EXTR_SKIP flag is the safest option, ensuring globals and
             * function variables cannot be overwritten.
             */
            // phpcs:ignore WordPress.PHP.DontExtract.extract_extract
            if(isset($vars)) extract($vars, EXTR_SKIP);
            include $located;
        }
    }
}
if(!function_exists('archia_get_the_password_form')){
    function archia_get_the_password_form($post = 0){
        $post = get_post( $post );
        $label = 'pwbox-' . ( empty($post->ID) ? rand() : $post->ID );
        $output = '<form action="' . esc_url( site_url( 'wp-login.php?action=postpass', 'login_post' ) ) . '" class="post-password-form" method="post">
        <p>' . esc_html__( 'This content is password protected. To view it please enter your password below:' , 'archia') . '</p>
        <p class="post-password-fields"><label for="' . $label . '"><span class="screen-reader-text">' . esc_html__( 'Password:', 'archia' ) . '</span><input name="post_password" id="' . esc_attr( $label ). '" type="password" size="20" /></label><button class="btn radius-xl btn-lg" type="submit">'.esc_html_x( 'Enter', 'post password form', 'archia' ).'</button></p></form>
        ';

        return $output ;
    }
}
add_filter('the_password_form','archia_get_the_password_form' );

if(!function_exists('archia_get_kirki_dynamic_css')){
    function archia_get_kirki_dynamic_css($styles){
        if(archia_get_option('use_custom_color', false)){
            return $styles;
        }else{
            return '';
        }
    }
}
add_filter('kirki/archia_configs/dynamic_css','archia_get_kirki_dynamic_css' );


/**
 * Modify category count format
 *
 * @since archia 1.0
 */
function archia_custom_category_count_widget($output) {
    return preg_replace("/<\/a>\s*(\([\d]+\))\s*<\//", '</a><span>$1</span></', $output);
}
add_filter('wp_list_categories', 'archia_custom_category_count_widget');



add_filter( 'widget_tag_cloud_args', 'change_tag_cloud_font_sizes');
/**
 * Change the Tag Cloud's Font Sizes.
 *
 * @since 1.0.3
 *
 * @param array $args
 *
 * @return array
 */
function change_tag_cloud_font_sizes( array $args ) {
    $args['smallest'] = '10.5';
    $args['largest'] = '18';

    return $args;
}

/**
 * Modify archive count format
 *
 * @since archia 1.0
 */
function archia_custom_archives_count_widget($link_html) {
    return preg_replace("/&nbsp;([\s(\d)]*)/", '<span>$1</span>', $link_html);
}
add_filter('get_archives_link', 'archia_custom_archives_count_widget');


function archia_style_widget_title($title){
    if(!$title) return '&nbsp;';

    return $title;
}


function archia_relative_protocol_url(){
    return is_ssl() ? 'https' : 'http';
}


function archia_post_meta($avatar = true){
    if( archia_get_option( 'blog_date') || archia_get_option( 'blog_cats') || archia_get_option( 'blog_like') || archia_get_option( 'blog_comments') ) :
    ?>
        <div class="dlab-post-meta">
            <ul class="d-flexs align-items-centers">
                <?php if( archia_get_option( 'blog_date') ) :?>
                    <li class="post-date"> <?php the_time(get_option('date_format')); ?></li>
                <?php endif; ?>
                <?php if( archia_get_option( 'blog_cats') ) :?>
                    <li class="post-cats"><?php echo sprintf( esc_html__( 'in %s', 'archia' ), get_the_category_list() ) ?></li>
                <?php endif; ?>
                
                <?php if( archia_get_option('blog_like') && function_exists('archia_addons_get_likes_stats') ) echo '<li class="post-like">'.archia_addons_get_likes_stats(get_the_ID()).'</li>'; ?>
                
                <?php if( archia_get_option( 'blog_comments' ) ):?>
                    <li class="post-comment"><i class="fa fa-comments-o"></i> <?php comments_popup_link( esc_html_x('0','comment counter None format' ,'archia'), esc_html_x('1','comment counter One format', 'archia'), esc_html_x('%','comment counter Plural format', 'archia') ); ?> </li>
                <?php endif; ?>
            </ul>
        </div>
    <?php
    endif;
}
function archia_post_single_meta($avatar = true){
    if( archia_get_option( 'single_date') || archia_get_option( 'single_cats') || archia_get_option( 'single_like') || archia_get_option( 'single_comments') ) :
    ?>
        <div class="dlab-post-meta">
            <ul class="d-flexs align-items-centers">
                <?php if($avatar && is_single()): ?>
                    <li class="dlab-post-name"> 
                    <?php 
                        echo get_avatar(get_the_author_meta('user_email'), '80', 'https://0.gravatar.com/avatar/ad516503a11cd5ca435acc9bb6523536?s=80', get_the_author_meta( 'display_name' ) );
                    ?> 
                    <?php echo sprintf( esc_html__( 'Posted by %s', 'archia' ), get_the_author_link() ) ?>
                    </li>
                <?php endif; ?>
                <?php if( archia_get_option( 'single_date') ) :?>
                    <li class="post-date"> <?php the_time(get_option('date_format')); ?></li>
                <?php endif; ?>
                <?php if( archia_get_option( 'single_cats') ) :?>
                    <li class="post-cats"><?php echo sprintf( esc_html__( 'in %s', 'archia' ), get_the_category_list() ) ?></li>
                <?php endif; ?>
                
                <?php if( archia_get_option('single_like') && function_exists('archia_addons_get_likes_button') ) echo archia_addons_get_likes_button(get_the_ID()); ?>
                
                <?php if( archia_get_option( 'single_comments' ) ):?>
                    <li class="post-comment"><i class="fa fa-comments-o"></i> <?php comments_popup_link( esc_html_x('0','comment counter None format' ,'archia'), esc_html_x('1','comment counter One format', 'archia'), esc_html_x('%','comment counter Plural format', 'archia') ); ?> </li>
                <?php endif; ?>
            </ul>
        </div>
    <?php
    endif;
}
function archia_blog_media($classes = 'blog-media', $image_size = 'archia-featured'){
    if(has_post_thumbnail()):
?>
    <div class="<?php echo esc_attr($classes); ?>">
        <a href="<?php the_permalink() ?>">
            <?php the_post_thumbnail($image_size) ?>
        </a>
    </div>
<?php
    endif;
}

function archia_blog_excerpt($classes = ''){
    ?>
        <div class="dlab-post-text <?php echo esc_attr($classes); ?> ">
            <?php the_excerpt(); ?>
        </div>
    <?php
}

function archia_link_pages(){
    wp_link_pages( 
        array(
            'before'      => '<div class="page-links"><span class="page-links-title">' . esc_html__( 'Pages:', 'archia' ) . '</span>',
            'after'       => '</div>',
            'link_before' => '<span>',
            'link_after'  => '</span>',
        ) 
    );
}
// https://developer.wordpress.org/reference/functions/wp_nav_menu/
add_filter( 'nav_menu_submenu_css_class', function($classes, $args, $depth){
    $display_depth = ( $depth + 1); // because it counts the first submenu as 0
    $classes[] = 'menu-depth-' . $display_depth;
    if( $display_depth % 2 ){
        $classes[] = 'menu-odd';
    }else{
        $classes[] = 'menu-even';
    }
    if( $display_depth >= 2 )
        $classes[] = 'sub-sub-menu leftsm';

    return $classes;
}, 10, 3 );

