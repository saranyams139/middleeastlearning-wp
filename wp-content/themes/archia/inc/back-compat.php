<?php
/* banner-php */
/**
 * archia back compat functionality
 *
 * Prevents archia from running on WordPress versions prior to 4.7,
 * since this theme is not meant to be backward compatible beyond that and
 * relies on many newer functions and markup changes introduced in 4.7.
 *
 */

/**
 * Prevent switching to archia on old versions of WordPress.
 *
 * Switches to the default theme.
 *
 * @since archia 1.0
 */
function archia_switch_theme() {
	switch_theme( WP_DEFAULT_THEME );
	unset( $_GET['activated'] );
	add_action( 'admin_notices', 'archia_upgrade_notice' );
}
add_action( 'after_switch_theme', 'archia_switch_theme' );

/**
 * Adds a message for unsuccessful theme switch.
 *
 * Prints an update nag after an unsuccessful attempt to switch to
 * archia on WordPress versions prior to 4.7.
 *
 * @since archia 1.0
 *
 * @global string $wp_version WordPress version.
 */
function archia_upgrade_notice() {
	$message = sprintf( esc_html__( 'archia requires at least WordPress version 4.7. You are running version %s. Please upgrade and try again.', 'archia' ), $GLOBALS['wp_version'] );
	printf( '<div class="error"><p>%s</p></div>', $message );
}

/**
 * Prevents the Customizer from being loaded on WordPress versions prior to 4.7.
 *
 * @since archia 1.0
 *
 * @global string $wp_version WordPress version.
 */
function archia_customize() {
	wp_die( sprintf( esc_html__( 'archia requires at least WordPress version 4.7. You are running version %s. Please upgrade and try again.', 'archia' ), $GLOBALS['wp_version'] ), '', array(
		'back_link' => true,
	) );
}
add_action( 'load-customize.php', 'archia_customize' );

/**
 * Prevents the Theme Preview from being loaded on WordPress versions prior to 4.7.
 *
 * @since archia 1.0
 *
 * @global string $wp_version WordPress version.
 */
function archia_preview() {
	if ( isset( $_GET['preview'] ) ) {
		wp_die( sprintf( esc_html__( 'archia requires at least WordPress version 4.7. You are running version %s. Please upgrade and try again.', 'archia' ), $GLOBALS['wp_version'] ) );
	}
}
add_action( 'template_redirect', 'archia_preview' );
