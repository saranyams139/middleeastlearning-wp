<?php
/* banner-php */

?>
<form role="search" method="get" action="<?php echo esc_url(home_url( '/' ) ); ?>" class="fl-wrap searchform">
    <div class="input-group">
    	<input name="s" type="text" class="form-control radius-no bg-black" placeholder="<?php echo esc_attr_x( 'Type and hit Enter...', 'search input placeholder','archia' ) ?>" value="<?php echo get_search_query() ?>" />
    
    	<span class="input-group-btn">
            <button type="submit" class="btn radius-no white"><img src="<?php echo esc_url( archia_get_option('search_icon') ); ?>" alt="<?php esc_attr_e( 'search icon', 'archia' ); ?>"></button>
        </span> 
    </div>
</form>