<?php


if(archia_get_option('folio_header')) : 
    $bgimage = archia_get_option('folio_bg_image');
    $bgurl = $bgimage['url'];
    if(isset($bgimage['id'])) $bgurl = wp_get_attachment_url( $bgimage['id'] );
    ?>
    <!-- inner page banner -->
    <div class="dlab-bnr-inr dlab-bnr-inr-lg overlay-primary-middle bg-pt cthbg" data-bg="<?php echo esc_url( $bgurl ); ?>">
        <div class="container">
            <div class="dlab-bnr-inr-entry">
                <?php if( $head_title = archia_get_option('folio_title') ) : ?>
                <h1 class="head-sec-title text-white"><span><?php echo esc_html($head_title); ?></span></h1>
                <?php endif ; ?>

                <?php 
                if( $head_intro = archia_get_option('folio_intro') )
                    echo '<div class="head-sec-intro">' . wp_kses_post( $head_intro ) .'</div>';
                ?>
                <!-- Breadcrumb row -->
                <?php  
                    archia_breadcrumbs();
                ?>
                <!-- Breadcrumb row END -->
            </div>
        </div>
    </div>
    <!-- inner page banner END -->
<?php  endif; ?>