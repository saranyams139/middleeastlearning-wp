<?php


if(archia_get_option('show_blog_header')) : 
    $bgimage = archia_get_option('blog_header_image');
    $bgurl = $bgimage['url'];
    if(isset($bgimage['id'])) $bgurl = wp_get_attachment_url( $bgimage['id'] );
    ?>
    <!-- inner page banner -->
    <div class="dlab-bnr-inr dlab-bnr-inr-lg overlay-primary-middle bg-pt cthbg" data-bg="<?php echo esc_url( $bgurl ); ?>">
        <div class="container">
            <div class="dlab-bnr-inr-entry">
                <?php if ( have_posts() ) : ?>
                    <h1 class="head-sec-title text-white"><?php printf( esc_html__( 'Search Results for: %s', 'archia' ), '<span>' . get_search_query() . '</span>' ); ?></h1>
                <?php else : ?>
                    <h1 class="head-sec-title text-white"><?php esc_html_e( 'Nothing Found', 'archia' ); ?></h1>
                <?php endif; ?>

                <?php
                    archia_breadcrumbs();
                ?>
            </div>
        </div>
    </div>
    <!-- inner page banner END -->
<?php  endif; ?>