<?php


if(archia_get_option('show_blog_header')) : 
    $bgimage = archia_get_option('blog_header_image');
    $bgurl = $bgimage['url'];
    if(isset($bgimage['id'])) $bgurl = wp_get_attachment_url( $bgimage['id'] );
    ?>
    <!-- inner page banner -->
    <div class="dlab-bnr-inr dlab-bnr-inr-lg overlay-primary-middle bg-pt cthbg" data-bg="<?php echo esc_url( $bgurl ); ?>">
        <div class="container">
            <div class="dlab-bnr-inr-entry">
                <?php
                    the_archive_title( '<h1 class="head-sec-title text-white"><span>', '</span></h1>' );
                    the_archive_description('<div class="archive-desc">','</div>');
                    archia_breadcrumbs();
                ?>
            </div>
        </div>
    </div>
    <!-- inner page banner END -->
<?php  endif; ?>