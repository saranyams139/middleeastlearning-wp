<?php
/* banner-php */
?>
<header class="site-header header header-transparent header-archia mo-left">
	<!-- main header -->
    <div class="sticky-header main-bar-wraper navbar-expand-lg">
        <div class="main-bar clearfix">
            <div class="container">
				<!-- website logo -->
				<div class="logo-header mostion">
					<?php 
                        if(has_custom_logo()) the_custom_logo(); 
                        else echo '<a class="custom-logo-link logo-text" href="'.esc_url( home_url('/' ) ).'"><h2>'.get_bloginfo( 'name' ).'</h2></a>'; 
                    ?>
				</div>
				<!-- nav toggle button -->
				<button class="navbar-toggler collapsed navicon justify-content-end" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
					<span></span>
					<span></span>
					<span></span>
				</button>
				<!-- extra nav -->
				<div class="extra-nav">
					<div class="extra-cell">
						<?php $default_header_contact = archia_get_option('default_header_contact'); 
						if (isset($default_header_contact) && $default_header_contact != '') {
							echo wp_kses_post( $default_header_contact );
						}
						?>
					</div>
				</div>
				<!-- Quik search -->
				<div class="dlab-quik-search">
					<form method="get" action="<?php echo esc_url(home_url( '/' ) ); ?>">
						<input name="s" value="<?php echo get_search_query() ?>" type="text" class="form-control" placeholder="<?php esc_attr_e( 'Enter Your Keyword ...', 'archia' ); ?>">
						<span  id="quik-search-remove"><i class="ti-close"></i></span>
					</form>
				</div>
				<!-- main nav -->
				<div class="header-nav navbar-collapse collapse justify-content-end mo-nav" id="navbarNavDropdown">
					<div class="logo-header">
						<?php 
	                        if(has_custom_logo()) the_custom_logo(); 
	                        else echo '<a class="custom-logo-link logo-text" href="'.esc_url( home_url('/' ) ).'"><h2>'.get_bloginfo( 'name' ).'</h2></a>'; 
	                    ?>
					</div>
					<?php if ( has_nav_menu( 'top' ) ) : ?>
                		<?php get_template_part( 'template-parts/navigation/navigation', 'top' ); ?>
            		<?php endif; ?>
					<?php $default_header_social = archia_get_option('default_header_social'); 
						if (isset($default_header_social) && $default_header_social != '') {
							echo wp_kses_post( $default_header_social );
						}
						?>
				</div>
			</div>
        </div>
    </div>
    <!-- main header END -->
</header>