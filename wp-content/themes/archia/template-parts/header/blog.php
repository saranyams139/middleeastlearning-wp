<?php


if(archia_get_option('show_blog_header')) : 
    $bgimage = archia_get_option('blog_header_image');
    $bgurl = $bgimage['url'];
    if(isset($bgimage['id'])) $bgurl = wp_get_attachment_url( $bgimage['id'] );
    ?>
    <!-- inner page banner -->
    <div class="dlab-bnr-inr dlab-bnr-inr-lg overlay-primary-middle bg-pt cthbg" data-bg="<?php echo esc_url( $bgurl ); ?>">
        <div class="container">
            <div class="dlab-bnr-inr-entry">
                <?php if( $blog_title = archia_get_option('blog_head_title') ) : ?>
                <h1 class="head-sec-title text-white"><span><?php echo esc_html($blog_title); ?></span></h1>
                <?php endif ; ?>

                <?php 
                if( $blog_head_title_desc = archia_get_option('blog_head_title_desc') )
                    echo '<div class="head-sec-intro">' . $blog_head_title_desc .'</div>';
                ?>
                <!-- Breadcrumb row -->
                <?php  
                    archia_breadcrumbs();
                ?>
                <!-- Breadcrumb row END -->
            </div>
        </div>
    </div>
    <!-- inner page banner END -->
<?php  endif; ?>