<?php
/* banner-php */
$footer_copyright = archia_get_option('footer_copyright');
?>
 <footer class="site-footer archi-footer">
 	<?php if($footer_copyright != ''): ?>
	<div class="footer-bottom">
		<div class="container">
			<div class="col-12 text-center footer-bot-col">
				<?php 
				echo wp_kses_post( $footer_copyright );
				?>
			</div>
		</div>
    </div>
	<?php endif; ?>
</footer>