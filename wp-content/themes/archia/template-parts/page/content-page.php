<?php
/* banner-php */
/**
 * Template part for displaying page content in page.php
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<div class="list-single-main-item-title fl-wrap">
		<?php if( false == get_post_meta(get_the_ID(),'_cth_show_header',true ) && false == get_post_meta(get_the_ID(),'_cth_show_title',true ) ) the_title( '<h3 class="entry-title">', '</h3>' ); ?>
		<?php archia_edit_link( get_the_ID() ); ?>
	</div><!-- .list-single-main-item-title-->
	<div class="entry-content clearfix">
		<?php
			the_content();
		?>
	</div><!-- .entry-content -->
	<?php
		wp_link_pages( array(
			'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'archia' ),
			'after'  => '</div>',
		) );
	?>
</article><!-- #post-## -->
