<?php
/* banner-php */
/**
 * Template part for displaying page content in page.php
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 */
get_header(); 
$bg = archia_get_option('404_bg');
?>
	
	<!-- inner page banner -->
    <div class="dlab-bnr-inr dlab-bnr-inr-lg overlay-primary-middle bg-pt cthbg"<?php if(isset($bg['url']) && $bg['url'] != ''): ?> data-bg="<?php echo esc_url( $bg['url'] ); ?>"<?php endif;?>>
        <div class="container">
            <div class="dlab-bnr-inr-entry">
                <h1 class="text-white"><?php esc_html_e( 'Protected Post', 'archia' ); ?></h1>
                <?php  
                archia_breadcrumbs();
                ?>
            </div>
        </div>
    </div>
    <!-- inner page banner END -->

	<div class="section-full content-inner-2">
        <div class="container">
            <div class="error-page text-center">
                <div class="error-head"><?php the_title( );?></div>
                <div class="m-b30">
                    <div class="subscribe-form p-a0">
                        <?php echo get_the_password_form(); ?>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>

<?php     
get_footer();
