<?php 
    if(!isset($loop_count)) $loop_count = 0;
    $image_left = true;
    $ani_cls = 'wow fadeInRight';
    if( ($loop_count + 1) % 2 == 0){
        $image_left = false;
        $ani_cls = 'wow fadeInLeft';
    }
?>
<article id="post-<?php the_ID(); ?>" <?php post_class($ani_cls); ?> data-wow-duration="2s" data-wow-delay="0.2s">
    <div class="blog-post blog-half blog-half1">
        <?php if( $image_left == false ): ?>
            <div class="post-info text-right">
                <div class="dlab-post-title right">
                    <h3 class="post-title font-weight-600"><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></h3>
                </div>
                <?php 
                    archia_post_meta();
                ?>
                <div class="dlab-post-text">
                    <?php the_excerpt(); ?>
                </div>
                <div class="dlab-post-readmore blog-share"> 
                    <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>" rel="bookmark" class="btn outline outline-2 button-lg black radius-xl btn-aware"><?php esc_html_e('Read More','archia') ?><span></span></a>        
                </div>
            </div>
        <?php endif; ?>
        <?php archia_blog_media() ?>
        <?php if( $image_left ): ?>
            <div class="post-info">
                <div class="dlab-post-title left">
                    <h3 class="post-title font-weight-600"><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></h3>
                </div>
                <?php 
                    archia_post_meta();
                ?>
                <?php archia_blog_excerpt() ?>
                <?php archia_link_pages(); ?>
                <div class="dlab-post-readmore blog-share"> 
                    <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>" rel="bookmark" class="btn outline outline-2 button-lg black radius-xl btn-aware"><?php esc_html_e('Read More','archia') ?><span></span></a>        
                </div>
            </div>
        <?php endif; ?>
    </div>
</article>