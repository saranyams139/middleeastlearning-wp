<?php
/* banner-php */
/**
 * Template part for displaying image posts
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 */

?>
<!-- article> --> 
<article id="post-<?php the_ID(); ?>" <?php post_class('pformat-image folio-item'); ?> >
	<div class="blog-post blog-grid blog-rounded radius-sm shadow">
		<?php archia_blog_media('dlab-post-media dlab-img-effect') ?>
		<div class="dlab-info p-lr50 p-tb40">
			
			<?php archia_post_meta(); ?>
			<div class="dlab-post-title">
				<h4 class="post-title"><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></h4>
			</div>
            <?php archia_blog_excerpt() ?>

            <?php archia_link_pages(); ?>

			<div class="dlab-post-name"> 
				<?php echo sprintf(esc_html__( 'Posted by %s', 'archia' ), get_the_author_link() ) ;?>
			</div>
		</div>
	</div>
</article>
<!-- article end -->   