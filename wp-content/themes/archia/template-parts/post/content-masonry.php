


<article id="post-<?php the_ID(); ?>" <?php post_class('card-container folio-item wow fadeInUp'); ?> data-wow-duration="2s" data-wow-delay="0.2s">
    <div class="blog-post blog-grid">
        <div class="post-info">
            <div class="dlab-post-title">
                <h3 class="post-title font-weight-600"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
            </div>
            <?php 
                archia_post_meta();
            ?>
        </div>
        <?php archia_blog_media() ?>
        <?php archia_blog_excerpt('p-t20')  ?>
        <?php archia_link_pages(); ?>
    </div>
</article>
<!-- article end -->