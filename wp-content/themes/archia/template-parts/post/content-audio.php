<?php
/* banner-php */
/**
 * Template part for displaying audio posts
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 */

?>
<!-- article> --> 
<article id="post-<?php the_ID(); ?>" <?php post_class('pformat-audio folio-item'); ?> >
    <div class="blog-post blog-grid blog-rounded radius-sm shadow">
    	<?php 
		if(get_post_meta(get_the_ID(), '_cth_embed_video', true) != ""){ ?>	
		    <div class="dlab-post-media dlab-img-effect">
		    	<?php
		    		$audio_url = get_post_meta(get_the_ID(), '_cth_embed_video', true);
					if(preg_match('/(.mp3|.ogg|.wma|.m4a|.wav)$/i', $audio_url )){
						$attr = array(
							'src'      => $audio_url,
							'loop'     => '',
							'autoplay' => '',
							'preload'  => 'none'
						);
						echo wp_audio_shortcode( $attr );
					}else{
				?>
					<div class="resp-audio">
						<?php echo wp_oembed_get(esc_url( $audio_url ) , array('height'=>'166') ); ?>
					</div>
				<?php } ?>
		    	
	        </div>
		<?php
		}elseif(has_post_thumbnail( )) 
			archia_blog_media('dlab-post-media dlab-img-effect'); ?>

        <div class="dlab-info p-lr50 p-tb40">
            
            <?php archia_post_meta(); ?>
            <div class="dlab-post-title">
                <h4 class="post-title"><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></h4>
            </div>
            <?php archia_blog_excerpt() ?>

            <?php archia_link_pages(); ?>

            <div class="dlab-post-name"> 
                <?php echo sprintf(esc_html__( 'Posted by %s', 'archia' ), get_the_author_link() ) ;?>
            </div>
        </div>
    </div>
</article>
<!-- article end -->   
