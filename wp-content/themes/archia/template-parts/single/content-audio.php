<?php
/* banner-php */
/**
 * Template part for displaying audio posts
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 */

?>
<div class="blog-post blog-single blog-post-style-2 sidebar">
    <!-- article> --> 
    <article id="post-<?php the_ID(); ?>" <?php post_class('pos-single pformat-video'); ?>>
        <div class="dlab-post-info">
            <div class="dlab-post-title">
                <h2 class="post-title"><?php the_title(); ?></h2>
            </div>
            
                <?php archia_post_single_meta(); ?>
            
        </div>
        <?php 
        if(archia_get_option('single_featured' )): ?>
            <?php
            if(get_post_meta(get_the_ID(), '_cth_embed_video', true)!=""){  ?>
            <div class="dlab-post-media blog-single">
                <?php
                    $audio_url = get_post_meta(get_the_ID(), '_cth_embed_video', true);
                    if(preg_match('/(.mp3|.ogg|.wma|.m4a|.wav)$/i', $audio_url )){
                        $attr = array(
                            'src'      => $audio_url,
                            'loop'     => '',
                            'autoplay' => '',
                            'preload'  => 'none'
                        );
                        echo wp_audio_shortcode( $attr );
                    }else{
                ?>
                    <div class="resp-audio">
                        <?php echo wp_oembed_get(esc_url( $audio_url ) , array('height'=>'166') ); ?>
                    </div>
                <?php } ?>
            </div>
            <?php
            }elseif(has_post_thumbnail( )){ ?>
            <div class="dlab-post-media blog-single post-link wow fadeIn" data-wow-duration="2s" data-wow-delay="0.2s">
                <?php if(has_post_thumbnail()) the_post_thumbnail('archia-single'); ?>
            </div>
            <?php } 
            ?>
        <?php 
        endif; ?>
        <div class="dlab-post-info">
            <div class="dlab-post-text text clearfix">
                <?php the_content(); ?>

                <?php archia_link_pages(); ?>
            </div>
            <div class="dlab-post-tags d-flex">
                <?php if( archia_get_option('single_tags' ) && has_tag() ): ?>
                    <div class="post-tags">
                        <?php esc_html_e('Tags: ', 'archia') ?>
                        <?php the_tags('','',''); ?>
                    </div>
                <?php endif; ?>
                <?php if(function_exists('archia_addons_echo_socials_share')) archia_addons_echo_socials_share(); ?>
            </div>
        </div>
        
    </article>
    <!-- article end -->       
    <span class="section-separator"></span>
</div>
