<?php
/* banner-php */
/**
 * Template part for displaying posts
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 */

?>
<!-- Author blog -->
<div class="author-box blog-user m-b60">
    <div class="author-profile-info">
        <div class="author-profile-pic">
            <?php 
                echo get_avatar(get_the_author_meta('user_email'), '150', 'https://0.gravatar.com/avatar/ad516503a11cd5ca435acc9bb6523536?s=150', get_the_author_meta( 'display_name' ) );
            ?> 
        </div>
        <div class="author-profile-content">
            <h5><?php echo get_the_author_meta('user_nicename'); ?></h5>
            
            <p><?php echo get_the_author_meta('description'); ?></p>
            <?php if ( 'no' !== esc_html_x( 'yes', 'Show author socials on single post page: yes or no', 'archia' ) ) : ?>
            <div class="author-social">
                <ul class="list-inline m-b0">
                    <?php if(get_user_meta(get_the_author_meta('ID'), '_cth_twitterurl' ,true)!=''){ ?>
                        <li><a class="btn-link" title="<?php esc_attr_e('Follow on Twitter','archia');?>" href="<?php echo esc_url(get_user_meta(get_the_author_meta('ID'), '_cth_twitterurl' ,true)); ?>" target="_blank"><i class="fa fa-twitter"></i></a></li>
                    <?php } ?>
                    <?php if(get_user_meta(get_the_author_meta('ID'), '_cth_facebookurl' ,true)!=''){ ?>
                        <li><a class="btn-link" title="<?php esc_attr_e('Like on Facebook','archia');?>" href="<?php echo esc_url(get_user_meta(get_the_author_meta('ID'), '_cth_facebookurl' ,true)); ?>" target="_blank"><i class="fa fa-facebook"></i></a></li>
                    <?php } ?>
                    <?php if(get_user_meta(get_the_author_meta('ID'), '_cth_googleplusurl' ,true)!=''){ ?>
                        <li><a class="btn-link" title="<?php esc_attr_e('Circle on Google Plus','archia');?>" href="<?php echo esc_url(get_user_meta(get_the_author_meta('ID'), '_cth_googleplusurl' ,true)) ;?>" target="_blank"><i class="fa fa-google-plus"></i></a></li>
                    <?php } ?>
                    <?php if(get_user_meta(get_the_author_meta('ID'), '_cth_linkedinurl' ,true)!=''){ ?>
                        <li><a class="btn-link" title="<?php esc_attr_e('Be Friend on Linkedin','archia');?>" href="<?php echo esc_url(get_user_meta(get_the_author_meta('ID'), '_cth_linkedinurl' ,true) ); ?>" target="_blank"><i class="fa fa-linkedin"></i></a></li>
                    <?php } ?>
                    <?php if(get_user_meta(get_the_author_meta('ID'), '_cth_instagramurl' ,true)!=''){ ?>
                        <li><a class="btn-link" title="<?php esc_attr_e('Follow on Instagram','archia');?>" href="<?php echo esc_url(get_user_meta(get_the_author_meta('ID'), '_cth_instagramurl' ,true) ); ?>" target="_blank"><i class="fa fa-instagram"></i></a></li>
                    <?php } ?>
                    <?php if(get_user_meta(get_the_author_meta('ID'), '_cth_tumblrurl' ,true)!=''){ ?>
                        <li><a class="btn-link" title="<?php esc_attr_e('Follow on  Tumblr','archia');?>" href="<?php echo esc_url(get_user_meta(get_the_author_meta('ID'), '_cth_tumblrurl' ,true) ); ?>" target="_blank"><i class="fa fa-tumblr"></i></a></li>
                    <?php } ?>  
                </ul>
            </div>
            <?php endif; ?>
        </div>
    </div>
</div>
<!-- Author blog end -->