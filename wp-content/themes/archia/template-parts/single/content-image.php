<?php
/* banner-php */
/**
 * Template part for displaying image posts
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 */


?>
<div class="blog-post blog-single blog-post-style-2 sidebar">
    <!-- article> --> 
    <article id="post-<?php the_ID(); ?>" <?php post_class('pos-single pformat-image'); ?>>
        <div class="dlab-post-info">
            <div class="dlab-post-title">
                <h2 class="post-title"><?php the_title(); ?></h2>
            </div>
            
                <?php archia_post_single_meta(); ?>
            
        </div>
        <?php 
        if(archia_get_option('single_featured' )): 
            if(has_post_thumbnail( )){ ?>
            <div class="dlab-post-media blog-single post-link wow fadeIn" data-wow-duration="2s" data-wow-delay="0.2s">
                <?php if(has_post_thumbnail()) the_post_thumbnail('archia-single'); ?>
            </div>
            <?php } 
        endif; ?>

        
        <div class="dlab-post-info">
            <div class="dlab-post-text text clearfix">
                <?php the_content(); ?>
                <?php archia_link_pages(); ?>
            </div>
            <div class="dlab-post-tags d-flex">
                <?php if( archia_get_option('single_tags' ) && has_tag() ): ?>
                    <div class="post-tags">
                        <?php esc_html_e('Tags: ', 'archia') ?>
                        <?php the_tags('','',''); ?>
                    </div>
                <?php endif; ?>
                <?php if(function_exists('archia_addons_echo_socials_share')) archia_addons_echo_socials_share(); ?>
            </div>
        </div>
        
    </article>
    <!-- article end -->       
    <span class="section-separator"></span>
</div>