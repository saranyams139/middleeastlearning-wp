<?php
/* banner-php */
/**
 * Template part for displaying posts
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 */
$posttags = get_the_tags();
$posttags_arr = array();
if ($posttags) {
  foreach($posttags as $tag) {
    $posttags_arr[] = $tag->term_id;
  }
}
if(empty($posttags_arr)) return;



$args=array(
    'post_type' => 'post',
    'post__not_in' => array(get_the_ID()),
    'tag__in' => $posttags_arr,
);
$my_query = new WP_Query( $args );

if( $my_query->have_posts()){

?>
<div class="related-post-bx">
    <div class="m-b30">
        <h3 class="m-b10"><?php esc_html_e('Related Post','archia'); ?></h3>
        <div class="dlab-separator bg-black"></div>
    </div>
    <div class="related-post owl-carousel">
        <?php  
            while ($my_query->have_posts()){
                $my_query->the_post();
        ?>
            <div class="item">
                <div class="blog-post blog-grid recent-post-bx">
                    <?php archia_blog_media('dlab-post-media dlab-img-effect', 'archia-related') ?>
                    <div class="dlab-info text-center">
                        <div class="dlab-post-title">
                            <h4 class="post-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
                        </div>
                        <?php archia_post_meta(false); ?>
                        <div class="dlab-post-text">
                           <?php the_excerpt(); ?>
                        </div>
                        <?php archia_link_pages(); ?>
                        <div class="dlab-post-readmore blog-share"> 
                            <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>" rel="bookmark" class="btn outline outline-2 button-lg black radius-xl btn-aware"><?php esc_html_e('Read More','archia'); ?><span></span></a>       
                        </div>
                    </div>
                </div>
            </div>
        <?php } ?>
    </div>
</div>
<?php } ?>

<?php
wp_reset_postdata();
