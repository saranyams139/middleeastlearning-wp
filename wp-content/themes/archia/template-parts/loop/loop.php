<?php 
$grid_cls = 'blog-posts-wrapper';
if(archia_get_option('blog-grid-style') == 'masonary'){
    $grid_cls .= ' blog-posts-masonry folio-items folio-isotope';
    $grid_cls .= ' folio-'.archia_get_option('blog-cols').'-cols';
    $grid_cls .= ' folio-'.archia_get_option('blog-pad').'-pad';
}elseif(archia_get_option('blog-grid-style') == 'grid'){
    $grid_cls .= ' blog-posts-grid folio-items folio-isotope';
    $grid_cls .= ' folio-'.archia_get_option('blog-cols').'-cols';
    $grid_cls .= ' folio-'.archia_get_option('blog-pad').'-pad';
}


if (have_posts()) : ?>
    <div class="<?php echo esc_attr($grid_cls );?>">
    <?php
    echo '<div class="grid-sizer"></div>';
    $count = 0;
    while (have_posts()) : the_post();
        /*
         * Include the Post-Format-specific template for the content.
         * If you want to override this in a child theme, then include a file
         * called content-___.php (where ___ is the Post Format name) and that will be used instead.
         */
        if(archia_get_option('blog-grid-style') == 'listing'){
            
            archia_get_template_part('template-parts/post/content','listing', array('loop_count'=>$count));
            
        }else if(archia_get_option('blog-grid-style') == 'masonary'){

            get_template_part('template-parts/post/content','masonry');

        }else if(archia_get_option('blog_show_format', true )){
            get_template_part( 'template-parts/post/content', ( post_type_supports( get_post_type(), 'post-formats' ) ? get_post_format() : get_post_type() ) );
        }else{
            get_template_part( 'template-parts/post/content' );
        }
        $count++; 
    endwhile; ?>
    </div>
    <?php
else :
    get_template_part( 'template-parts/post/content', 'none' );

endif;
?>

<?php archia_pagination() ?>
<!-- Pagination END -->
