<?php
/* banner-php */
/**
 * Displays top navigation
 *
 */

?>
<nav id="site-navigation" class="main-navigation" aria-label="<?php esc_attr_e( 'Top Menu', 'archia' ); ?>">
    <?php 
    wp_nav_menu( 
    	array(
			'theme_location'     => 'top',
			'container'          => '',
            'container_class'    => '',
            'container_id'       => '',
			'menu_id'            => 'top-menu',
			'menu_class'        => 'nav navbar-nav',
		) 
	); 
	?>
</nav><!-- #site-navigation -->
