<?php
/* banner-php */
/**
 * archia functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 */

if ( file_exists(get_template_directory() . '/inc/redux-config.php')) {
    require_once get_template_directory() . '/inc/redux-config.php';
}
/**
 * archia only works in WordPress 4.7 or later.
 */
if ( version_compare( $GLOBALS['wp_version'], '4.7-alpha', '<' ) ) {
	require get_template_directory() . '/inc/back-compat.php';
	return;
}

if(!isset($archia_options)) $archia_options = get_option( 'archia_options', array() ); 

/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function archia_setup() {
	/*
	 * Make theme available for translation.
	 */
	load_theme_textdomain( 'archia' , get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
	 */
	add_theme_support( 'post-thumbnails' );

	archia_get_thumbnail_sizes();

	// Set the default content width.
	$GLOBALS['content_width'] = 650;

	// This theme uses wp_nav_menu() in two locations.
	register_nav_menus( array(
		'top'    => esc_html__( 'Top Menu', 'archia' ),
		'social' => esc_html__( 'Social Links Menu', 'archia' ),
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form',
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
	) );

	/*
	 * Enable support for Post Formats.
	 *
	 * See: https://codex.wordpress.org/Post_Formats
	 */
	add_theme_support( 'post-formats', array(
		'aside',
		'image',
		'video',
		'quote',
		'link',
		'gallery',
		'audio',
	) );

	// Add theme support for Custom Logo.
	add_theme_support( 'custom-logo', array(
		'width'       => 225,
		'height'      => 44,
		'flex-width'  => true,
		'flex-height' => true,
		'header-text' => array( 'site-title', 'site-description' ),

	) );

	// Add theme support for selective refresh for widgets.
	add_theme_support( 'customize-selective-refresh-widgets' );

	/*
	 * This theme styles the visual editor to resemble the theme style,
	 * specifically font, colors, and column width.
 	 */
	add_editor_style( array( 'assets/css/editor-style.css', archia_fonts_url() ) );

	
}
add_action( 'after_setup_theme', 'archia_setup' );

if(!function_exists('archia_get_thumbnail_sizes')){
    function archia_get_thumbnail_sizes(){
    	// options default must have these values
    	if( false == archia_get_option('enable_custom_sizes') ) return;
        $option_sizes = array(
        	'archia-folio'=>'thumb_size_opt_5',
        	'archia-folio-two'=>'thumb_size_opt_6',
        	'archia-folio-three'=>'thumb_size_opt_7',
        	'archia-member'=>'thumb_size_opt_8',
        	'archia-featured'=>'thumb_size_opt_9',
        	'archia-single'=>'thumb_size_opt_10',
        	'archia-related'=>'thumb_size_opt_11',
            'archia-recent'=>'thumb_size_opt_12',
        	
        );

       	foreach ($option_sizes as $name => $opt) {
       		$option_size = archia_get_option($opt);
       		if($option_size !== false && is_array($option_size)){
       			$size_val = array(
       				'width' => (isset($option_size['width']) && !empty($option_size['width']) )? (int)$option_size['width'] : (int)'9999',
       				'height' => (isset($option_size['height']) && !empty($option_size['height']) )? (int)$option_size['height'] : (int)'9999',
       				'hard_crop' => (isset($option_size['hard_crop']) && !empty($option_size['hard_crop']) )? (bool)$option_size['hard_crop'] : (bool)'0',
       			);

       			add_image_size( $name, $size_val['width'], $size_val['height'], $size_val['hard_crop'] );
       		}
       	}
    }
}
/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function archia_content_width() {

	$content_width = $GLOBALS['content_width'];


	// Check if is single post and there is no sidebar.
	if ( is_single() && ! is_active_sidebar( 'sidebar-1' ) ) {
		$content_width = 1040;
	}

	/**
	 * Filter archia content width of the theme.
	 *
	 * @since archia 1.0
	 *
	 * @param int $content_width Content width in pixels.
	 */
	$GLOBALS['content_width'] = apply_filters( 'archia_content_width', $content_width );
}
add_action( 'template_redirect', 'archia_content_width', 0 );



/**
 * Add preconnect for Google Fonts.
 *
 * @since archia 1.0
 *
 * @param array  $urls           URLs to print for resource hints.
 * @param string $relation_type  The relation type the URLs are printed.
 * @return array $urls           URLs to print for resource hints.
 */
function archia_resource_hints( $urls, $relation_type ) {
	if ( wp_style_is( 'archia-fonts', 'queue' ) && 'preconnect' === $relation_type ) {
		$urls[] = array(
			'href' => 'https://fonts.gstatic.com',
			'crossorigin',
		);
	}

	return $urls;
}
add_filter( 'wp_resource_hints', 'archia_resource_hints', 10, 2 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function archia_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Blog Sidebar', 'archia' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here to appear in your sidebar on blog posts and archive pages.', 'archia' ),
		'before_widget' => '<div id="%1$s" class="box-widget-item fl-wrap archia-mainsidebar-widget main-sidebar-widget widget widget-bx %2$s">', 
        'before_title' => '<h5 class="widget-title">', 
        'after_title' => '</h5>',
        'after_widget' => '</div>',
	) );

	register_sidebar( array(
		'name'          => esc_html__( 'Page Sidebar', 'archia' ),
		'id'            => 'sidebar-2',
		'description' => esc_html__('Appears in the sidebar section of the page template.', 'archia'), 
        'before_widget' => '<div id="%1$s" class="box-widget-item fl-wrap archia-mainsidebar-widget main-sidebar-widget widget widget-bx %2$s">', 
        'before_title' => '<h5 class="widget-title">', 
        'after_title' => '</h5>',
        'after_widget' => '</div>',
	) );

    register_sidebar( array(
        'name'          => esc_html__( 'Shop Sidebar', 'archia' ),
        'id'            => 'sidebar-shop',
        'description' => esc_html__('Appears in the sidebar section of the shop pages.', 'archia'), 
        'before_widget' => '<div id="%1$s" class="box-widget-item fl-wrap archia-mainsidebar-widget main-sidebar-widget widget widget-bx %2$s">', 
        'before_title' => '<h5 class="widget-title">', 
        'after_title' => '</h5>',
        'after_widget' => '</div>',
    ) );
}
add_action( 'widgets_init', 'archia_widgets_init' );


/**
 * Replaces "[...]" (appended to automatically generated excerpts) with ... and
 * a 'Continue reading' link.
 *
 * @since archia 1.0
 *
 * @param string $link Link to single post/page.
 * @return string 'Continue reading' link prepended with an ellipsis.
 */
function archia_excerpt_more( $link ) {
	
	return ' &hellip; ';
}
add_filter( 'excerpt_more', 'archia_excerpt_more' );


/**
 * Add a pingback url auto-discovery header for singularly identifiable articles.
 */
function archia_pingback_header() {
	if ( is_singular() && pings_open() ) {
		printf( '<link rel="pingback" href="%s">' . "\n", get_bloginfo( 'pingback_url' ) );
	}
}
add_action( 'wp_head', 'archia_pingback_header' );


/**
 * Register custom fonts.
 */
function archia_fonts_url() {
	$fonts_url = '';
    $font_families     = array();

    
    if ( 'off' !== esc_html_x( 'on', 'Poppins font: on or off', 'archia' ) ) {
        $font_families[] = 'Poppins:400,400i,500,500i,600,600i,700,700i,800,800i,900,900i';
    }

    if ( 'off' !== esc_html_x( 'on', 'Montserrat font: on or off', 'archia' ) ) {
        $font_families[] = 'Montserrat:500,500i';
    }
    if ( $font_families ) {
    	$query_args = array(
			'family' => urlencode( implode( '|', $font_families ) ),
			'subset' => urlencode( 'cyrillic,cyrillic-ext,latin-ext,vietnamese' ),
		);

        $fonts_url = add_query_arg( $query_args, 'https://fonts.googleapis.com/css' );
    }

    return esc_url_raw( $fonts_url );

}

/**
 * Enqueue scripts and styles.
 */
function archia_scripts() {
	// Add custom fonts, used in the main stylesheet.
	wp_enqueue_style( 'archia-fonts', archia_fonts_url(), array(), null );
    wp_enqueue_style( 'font-awesome', get_template_directory_uri() . '/assets/css/font-awesome.min.css', array(), '4.7.0');
    wp_enqueue_style( 'line-awesome', get_template_directory_uri() . '/assets/css/line-awesome.min.css', array(), null);
    wp_enqueue_style( 'magnific-popup', get_template_directory_uri() . '/assets/css/magnific-popup.min.css', array(), '1.2.13'); 
    wp_enqueue_style( 'swiper', get_template_directory_uri() . '/assets/js/swiper/css/swiper.min.css', array(), '1.2.13'); 
    wp_enqueue_style( 'animate', get_theme_file_uri( '/assets/css/animate.min.css' ), array(  ), null );
    wp_enqueue_style( 'bootstrap-select', get_theme_file_uri( '/assets/css/bootstrap-select.min.css' ), array(  ), null );
    wp_enqueue_style( 'bootstrap', get_theme_file_uri( '/assets/css/bootstrap.min.css' ), array(  ), null );
    wp_enqueue_style( 'owl-carousel', get_theme_file_uri( '/assets/css/owl.carousel.css' ), array(  ), null );
    wp_enqueue_style( 'scrollbar', get_theme_file_uri( '/assets/css/scrollbar.css' ), array(  ), null );
	// Theme stylesheet.
	wp_enqueue_style( 'archia-style', get_stylesheet_uri() );
    wp_enqueue_style( 'archia-template', get_theme_file_uri( '/assets/css/theme-css/template.min.css' ), array(  ), null );
    wp_enqueue_style( 'archia-color', get_theme_file_uri( '/assets/css/skin/'.apply_filters( 'archia_color_skin', archia_get_option('color-preset') ).'.css' ), array(  ), null );
    wp_enqueue_style( 'archia-niceselect-style', get_template_directory_uri() . '/assets/css/nice-select.css', array(),false);
    if(archia_get_option('use_custom_color', false) && archia_get_option('theme-color') != '#a8ca1e'){
        wp_add_inline_style( 'archia-color', archia_overridestyle() );
    }

    
    wp_enqueue_script('jquery-easing', get_theme_file_uri( '/assets/js/jquery.easing.min.js' ), array('jquery'), '1.4.0', true);
    wp_enqueue_script('singlepagenav', get_theme_file_uri( '/assets/js/navigation.js' ) , array(), null, true);
    wp_enqueue_script('anime', get_theme_file_uri( '/assets/js/anime.js' ) , array(), null, true);
    wp_enqueue_script('anime-app', get_theme_file_uri( '/assets/js/anime-app.js' ) , array(), null, true);
    wp_enqueue_script('bootstrap-select', get_theme_file_uri( '/assets/js/bootstrap-select.min.js' ) , array(), null, true);
    wp_enqueue_script('bootstrap', get_theme_file_uri( '/assets/js/bootstrap.min.js' ) , array(), null, true);
    wp_enqueue_script('counterup', get_theme_file_uri( '/assets/js/counterup.min.js' ) , array(), null, true);
    wp_enqueue_script('magnific-popup', get_theme_file_uri( '/assets/js/magnific-popup.js' ) , array(), null, true);
    wp_enqueue_script('isotope', get_theme_file_uri( '/assets/js/isotope.pkgd.min.js' ) , array(), null, true);
    wp_enqueue_script('owl-carousel', get_theme_file_uri( '/assets/js/owl.carousel.js' ) , array(), null, true);
    wp_enqueue_script('popper', get_theme_file_uri( '/assets/js/popper.min.js' ) , array(), null, true);
    wp_enqueue_script('scrollbar', get_theme_file_uri( '/assets/js/scrollbar.min.js' ) , array(), null, true);
    wp_enqueue_script('tilt-jquery', get_theme_file_uri( '/assets/js/tilt.jquery.js' ) , array(), null, true);
    wp_enqueue_script('wow', get_theme_file_uri( '/assets/js/wow.js' ) , array(), null, true);
    wp_enqueue_script('swiper', get_theme_file_uri( '/assets/js/swiper/js/swiper.min.js' ) , array(), null, true);

	wp_enqueue_script( 'archia-scripts', get_theme_file_uri( '/assets/js/scripts.js' ), array( 'jquery', 'imagesloaded' ), null , true );
    wp_enqueue_script( 'archia-selectmin-js', get_template_directory_uri() .  '/assets/js/jquery.nice-select.min.js', false );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'archia_scripts' );

// modify tag cloud
function archia_widget_tag_cloud_args($args = array()){
    $args['number'] = 7; // default 45

    return $args;
}
add_filter( 'widget_tag_cloud_args', 'archia_widget_tag_cloud_args' );
/**
 * Custom template tags for this theme.
 */
require get_parent_theme_file_path( '/inc/template-tags.php' );

/**
 * Additional features to allow styling of the templates.
 */
require get_parent_theme_file_path( '/inc/template-functions.php' );

/**
 * SVG icons functions and filters.
 */
require get_parent_theme_file_path( '/inc/color-patterns.php' );


/**
 * Implement the One Click Demo Import plugin
 *
 * @since archia 1.0
 */
require_once get_parent_theme_file_path( '/inc/one-click-import-data.php' );

// require_once get_parent_theme_file_path( '/lib/update/cththemes-auto-update.php' );


/**
 * Include the TGM_Plugin_Activation class.
 */
require_once get_parent_theme_file_path( '/lib/class-tgm-plugin-activation.php' );

add_action('tgmpa_register', 'archia_register_required_plugins');

/**
 * Register the required plugins for this theme.
 *
 * In this example, we register five plugins:
 * - one included with the TGMPA library
 * - two from an external source, one from an arbitrary source, one from a GitHub repository
 * - two from the .org repo, where one demonstrates the use of the `is_callable` argument
 *
 * The variables passed to the `tgmpa()` function should be:
 * - an array of plugin arrays;
 * - optionally a configuration array.
 * If you are not changing anything in the configuration array, you can remove the array and remove the
 * variable from the function call: `tgmpa( $plugins );`.
 * In that case, the TGMPA default settings will be used.
 *
 * This function is hooked into `tgmpa_register`, which is fired on the WP `init` action on priority 10.
 */
function archia_register_required_plugins() {
    /*
     * Array of plugin arrays. Required keys are name and slug.
     * If the source is NOT from the .org repo, then source is also required.
     */
    $plugins = array(

    	array('name' => esc_html__('WPBakery Page Builder','archia'),
             // The plugin name.
            'slug' => 'js_composer',
             // The plugin slug (typically the folder name).
            'source' => 'http://assets.cththemes.com/plugins/js_composer.zip',
             // The plugin source.
            'required' => true,
            'external_url' => esc_url(archia_relative_protocol_url().'://codecanyon.net/item/visual-composer-page-builder-for-wordpress/242431' ),
             // If set, overrides default API URL and points to an external URL.
            'function_to_check'         => '',
            'class_to_check'            => 'Vc_Manager'
        ), 

        array('name' => esc_html__('Slider Revolution','archia'),
             // The plugin name.
            'slug' => 'revslider',
             // The plugin slug (typically the folder name).
            'source' => 'http://assets.cththemes.com/plugins/revslider.zip',
             // The plugin source.
            'required' => true,
            'external_url' => esc_url(archia_relative_protocol_url().'://codecanyon.net/item/slider-revolution-responsive-wordpress-plugin/2751380' ),
             // If set, overrides default API URL and points to an external URL.
            'function_to_check'         => '',
            'class_to_check'            => 'RevSliderBase'
        ), 
        
        array('name' => esc_html__('Redux Framework','archia'),
             // The plugin name.
            'slug' => 'redux-framework',
             // The plugin source.
            'required' => true,
             // If false, the plugin is only 'recommended' instead of required.
            'external_url' => esc_url(archia_relative_protocol_url().'://wordpress.org/plugins/redux-framework/' ),
             // If set, overrides default API URL and points to an external URL.
            'function_to_check'         => '',
            'class_to_check'            => 'ReduxFramework'
        ), 



        array(
            'name' => esc_html__('Contact Form 7','archia'),
             // The plugin name.
            'slug' => 'contact-form-7',
             // The plugin slug (typically the folder name).
            'required' => true,
             // If false, the plugin is only 'recommended' instead of required.
            'external_url' => esc_url(archia_relative_protocol_url().'://wordpress.org/plugins/contact-form-7/' ),
             // If set, overrides default API URL and points to an external URL.

            'function_to_check'         => 'wpcf7',
            'class_to_check'            => 'WPCF7'
        ), 

        array(
            'name' => esc_html__('CMB2','archia'),
             // The plugin name.
            'slug' => 'cmb2',
             // The plugin slug (typically the folder name).
            'required' => true,
             // If false, the plugin is only 'recommended' instead of required.
            'external_url' => esc_url(archia_relative_protocol_url().'://wordpress.org/support/plugin/cmb2'),
             // If set, overrides default API URL and points to an external URL.

            'function_to_check'         => 'cmb2_bootstrap',
            'class_to_check'            => 'CMB2_Base'
        ),
        
        array(
            'name' 						=> esc_html__('Archia Add-Ons','archia' ),
             // The plugin name.
            'slug' 						=> 'archia-add-ons',
             // The plugin slug (typically the folder name).
            'source' 					=> 'archia-add-ons.zip',
             // The plugin source.
            'required' 					=> true,
             // If false, the plugin is only 'recommended' instead of required.

            'force_deactivation'		=> false,

            'function_to_check'         => '',
            'class_to_check'            => 'Archia_Addons'
        ), 

        

        array(
            'name' => esc_html__('Loco Translate','archia'),
             // The plugin name.
            'slug' => 'loco-translate',
             // The plugin slug (typically the folder name).
            'required' => false,
             // If false, the plugin is only 'recommended' instead of required.
            'external_url' => esc_url(archia_relative_protocol_url().'://wordpress.org/plugins/loco-translate/'),
             // If set, overrides default API URL and points to an external URL.

            'function_to_check'         => 'loco_autoload',
            'class_to_check'            => 'Loco_Locale'
        ), 
        

        array('name' => esc_html__('One Click Demo Import','archia'),
             // The plugin name.
            'slug' => 'one-click-demo-import',
             // The plugin slug (typically the folder name).
            'required' => false,
             // If false, the plugin is only 'recommended' instead of required.
            'external_url' => esc_url(archia_relative_protocol_url().'://wordpress.org/plugins/one-click-demo-import/'),
             // If set, overrides default API URL and points to an external URL.

            'function_to_check'         => '',
            'class_to_check'            => 'OCDI_Plugin'
        ),



        array('name' => esc_html__('Regenerate Thumbnails','archia'),
             // The plugin name.
            'slug' => 'regenerate-thumbnails',
             // The plugin slug (typically the folder name).
            'required' => false,
             // If false, the plugin is only 'recommended' instead of required.
            'external_url' => esc_url(archia_relative_protocol_url().'://wordpress.org/plugins/regenerate-thumbnails/' ),
             // If set, overrides default API URL and points to an external URL.

            'function_to_check'         => 'RegenerateThumbnails',
            'class_to_check'            => 'RegenerateThumbnails'
        ),

        
    );

    /*
     * Array of configuration settings. Amend each line as needed.
     *
     * TGMPA will start providing localized text strings soon. If you already have translations of our standard
     * strings available, please help us make TGMPA even better by giving us access to these translations or by
     * sending in a pull-request with .po file(s) with the translations.
     *
     * Only uncomment the strings in the config array if you want to customize the strings.
     */
    $config = array(
        'id'           => 'archia',                 // Unique ID for hashing notices for multiple instances of TGMPA.
        'default_path' => get_template_directory() . '/lib/plugins/',                      // Default absolute path to bundled plugins.
        'menu'         => 'tgmpa-install-plugins', // Menu slug.
        'has_notices'  => true,                    // Show admin notices or not.
        'dismissable'  => true,                    // If false, a user cannot dismiss the nag message.
        'dismiss_msg'  => '',                      // If 'dismissable' is false, this message will be output at top of nag.
        'is_automatic' => false,                   // Automatically activate plugins after installation or not.
        'message'      => '',                      // Message to output right before the plugins table.

        
    );

    tgmpa( $plugins, $config );
}
