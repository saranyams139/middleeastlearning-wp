<?php
/* banner-php */
?>
<p><?php esc_html_e( 'Enter your purchase code for the theme to receive demo data and its add-ons plugins.', 'archia' ); ?></p>
<p>
    <?php esc_html_e( 'It also allow you receive updates from WordPress themes/plugins screens like others.', 'archia' ); ?>

    <?php esc_html_e( ' You do not need to delete and reinstall add-ons plugins anymore.', 'archia' ); ?> <a href="https://docs.cththemes.com/docs/installation/new-update-system/" target="_blank"><?php esc_html_e( 'See guide -->', 'archia' ); ?></a>
</p>