<?php
/* banner-php */
/**
 * The template for displaying archive pages
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 */
get_header(); 

$sb_w = archia_get_option('blog-sidebar-width','4');

get_template_part('template-parts/header/portfolio'); ?>
        <!-- blog grid -->
        <div class="content-block">
            <!-- blog grid -->
            <div class="section-full content-inner">
                <div class="container">

                    <div class="row">

                    
                        <?php if( archia_get_option('blog_layout') ==='left_sidebar' && is_active_sidebar('sidebar-1')):?>
                            <div class="col-md-<?php echo esc_attr($sb_w );?> blog-sidebar-column">
                                <div class="blog-sidebar box-widget-wrap fl-wrap left-sidebar side-bar sticky-top">
                                    <?php  
                                        get_sidebar(); 
                                    ?>
                                </div>
                            </div>
                        <?php endif;?>

                        <?php if( archia_get_option('blog_layout') ==='fullwidth' || !is_active_sidebar('sidebar-1')):?>
                            <div class="col-md-12 display-posts  nosidebar">
                        <?php else:?>
                            <div class="col-md-<?php echo (12 - $sb_w);?> col-wrap display-posts hassidebar <?php echo esc_attr( archia_get_option('blog_layout') ); ?>">
                        <?php endif;?>
                            <?php  
                                get_template_part('template-parts/loop/loop'); 
                            ?>
                            </div>
                            <!-- end display-posts col-md-8 -->
                            <?php if( archia_get_option('blog_layout') === 'right_sidebar' && is_active_sidebar('sidebar-1')):?>
                                <div class="col-md-<?php echo esc_attr($sb_w );?> blog-sidebar-column">
                                    <div class="blog-sidebar box-widget-wrap fl-wrap right-sidebar side-bar sticky-top">
                                        <?php 
                                            get_sidebar(); 
                                        ?>
                                    </div>
                                </div>
                            <?php endif;?>
                    </div>
                    
                </div>

            </div>
            <!-- blog grid END -->
        </div>
        <!-- blog grid END -->

<?php get_footer();
