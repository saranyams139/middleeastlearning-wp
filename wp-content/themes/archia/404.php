<?php
/* banner-php */
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 */


get_header(); 

$bg = archia_get_option('404_bg');
?>
    <!-- inner page banner -->
    <div class="dlab-bnr-inr dlab-bnr-inr-lg overlay-primary-middle bg-pt cthbg"<?php if(isset($bg['url']) && $bg['url'] != ''): ?> data-bg="<?php echo esc_url( $bg['url'] ); ?>"<?php endif;?>>
        <div class="container">
            <div class="dlab-bnr-inr-entry">
                <h1 class="text-white"><?php esc_html_e( 'Error Page', 'archia' ); ?></h1>
                <?php 
                if( $header_intro = archia_get_option('404_intro') )
                    echo '<div class="head-sec-intro">' . wp_kses_post( $header_intro ) .'</div>';
                ?>
                <?php  
                archia_breadcrumbs();
                ?>
            </div>
        </div>
    </div>
    <!-- inner page banner END -->

    <div class="section-full content-inner-2">
        <div class="container">
            <div class="error-page text-center">
                <div class="dz_error"><?php esc_html_e( '404','archia' ); ?></div>
                <div class="error-head"><?php echo wp_kses_post( archia_get_option('404_message') );?></div>
                <div class="m-b30">
                    <div class="subscribe-form p-a0">
                        <?php get_search_form();?>
                    </div>
                </div>
                <div class="clearfix"></div>
                <?php 
                if (archia_get_option('back_home_link') != '') : 
                ?>
                <a href="<?php echo esc_url( archia_get_option('back_home_link') );?>" class="btn radius-xl btn-lg"><?php esc_html_e( 'Return to Home', 'archia' ); ?></a>
                <?php 
                endif; ?>
            </div>
        </div>
    </div>


<?php get_footer();
