<?php
/* banner-php */
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 */
if ( post_password_required() ) {
    get_template_part( 'template-parts/page/protected', 'page' );
    return;
}
get_header(); 
$sb_w = archia_get_option('blog-single-sidebar-width','4');

get_template_part('template-parts/header/singular');

?>

<div class="content-block">
    <!--section -->   
    <div class="section-full content-inner bg-white" id="sec1">
        <div class="container">
            <div class="row">
                <?php if( archia_get_option('blog_layout') ==='left_sidebar' && is_active_sidebar('sidebar-1')):?>
                <div class="col-md-<?php echo esc_attr($sb_w );?> blog-sidebar-column">
                    <div class="blog-sidebar box-widget-wrap fl-wrap left-sidebar side-bar sticky-top">
                        <?php 
                            get_sidebar(); 
                        ?>                 
                    </div>
                </div>
                <?php endif;?>
                <?php if( archia_get_option('blog_layout') ==='fullwidth' || !is_active_sidebar('sidebar-1')):?>
                <div class="col-md-12 display-post nosidebar">
                    <div class="blog-post blog-single blog-post-style-2 nosidebar">
                <?php else:?>
                <div class="col-md-<?php echo (12 - $sb_w);?> col-wrap display-post hassidebar">
                    
                <?php endif;?>
                    <?php
                        /* Start the Loop */
                        while ( have_posts() ) : the_post();
                            // set post view
                            if(function_exists('archia_addons_set_post_views')){
                                archia_addons_set_post_views(get_the_ID());
                            }

                            get_template_part( 'template-parts/single/content', get_post_format() );

                            if( archia_get_option('single_nav' )) archia_post_nav();
                            if( archia_get_option('single_author_block' ) && get_the_author_meta('description') !='' ) get_template_part( 'template-parts/single/author', 'block' );
                            if(archia_get_option('single_recent_posts')) get_template_part('template-parts/single/related', 'posts');

                            // If comments are open or we have at least one comment, load up the comment template.
                            if ( comments_open() || get_comments_number() ) :
                                comments_template();
                            endif;

                        endwhile; // End of the loop.
                        
                            
                    ?>
                </div>
                <!-- end display-posts col-md-8 -->

                <?php if( archia_get_option('blog_layout') === 'right_sidebar' && is_active_sidebar('sidebar-1')):?>
                <div class="col-md-<?php echo esc_attr($sb_w );?> blog-sidebar-column">
                    <div class="blog-sidebar box-widget-wrap fl-wrap right-sidebar side-bar sticky-top">
                        <?php 
                            get_sidebar(); 
                        ?>                 
                    </div>
                </div>
                <?php endif;?>

            </div>
            <!-- end row -->
        </div>
        <!-- end container -->
    </div>
    <!-- section end -->
</div>
<?php get_footer();
