 <?php
/* add_ons_php */


$el_class = $css = $posts_per_page = $order_by = $order = $ids = $show_avatar = $show_count = $show_title = $show_rating = $show_excerpt = $link_single = $mem_style = $mem_socials ='';
$columns_grid = $spacing = '';
$ani_type = $ani_time = $ani_delays = '';
// $show_navigation = $show_dots = $mousewheel = $keyboard = $autoplay = $loop = $direction = $speed = $effect = $responsive = $spacing = '';
$atts = vc_map_get_attributes( $this->getShortcode(), $atts );
extract( $atts );

$css_classes = array(
    'cth-members-wrap',
    $el_class,
    vc_shortcode_custom_css_class( $css ),
);
$css_class = preg_replace( '/\s+/', ' ', implode( ' ', array_filter( $css_classes ) ) );

if(is_front_page()) {
    $paged = (get_query_var('page')) ? get_query_var('page') : 1;
} else {
    $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
}
if(!empty($ids)){
    $ids = explode(",", $ids);
    $post_args = array(
        'post_type' => 'cth_member',
        'paged' => $paged,
        'posts_per_page'=> $posts_per_page,
        'post__in' => $ids,
        'orderby'=> $order_by,
        'order'=> $order,

        'post_status'       => 'publish',
    );
}elseif(!empty($ids_not)){
    $ids_not = explode(",", $ids_not);
    $post_args = array(
        'post_type' => 'cth_member',
        'paged' => $paged,
        'posts_per_page'=> $posts_per_page,
        'post__not_in' => $ids_not,
        'orderby'=> $order_by,
        'order'=> $order,

        'post_status'       => 'publish',
    );
}else{
    $post_args = array(
        'post_type' => 'cth_member',
        'paged' => $paged,
        'posts_per_page'=> $posts_per_page,
        'orderby'=> $order_by,
        'order'=> $order,

        'post_status'       => 'publish',
    );
}
$ani_delays_arr = explode(',', $ani_delays);
$query_posts = new WP_Query($post_args); 
if($query_posts->have_posts()): 
    $grid_cls = 'cth-members-inner folio-items folio-flex';
    $grid_cls .= ' folio-'.$columns_grid.'-cols';
    $grid_cls .= ' folio-'.$spacing.'-pad';

    ?>
<div class="<?php echo esc_attr( $css_class ); ?>">
    <div class="<?php echo esc_attr( $grid_cls ); ?>">
        <?php 
        $idx = 0;       
        while($query_posts->have_posts()) : $query_posts->the_post();  
            $facebook = get_post_meta( get_the_ID(), BBT_META_PREFIX.'facebookurl', true );
            $twitter = get_post_meta( get_the_ID(), BBT_META_PREFIX.'twitterurl', true );
            $googleplus = get_post_meta( get_the_ID(), BBT_META_PREFIX.'googleplusurl', true );
            $instagram = get_post_meta( get_the_ID(), BBT_META_PREFIX.'instagramurl', true );

            $item_cls = 'folio-item our-team '.$mem_style;
            $ani_attrs = '';
            if ( !empty($ani_type) ) {
                $item_cls .= ' wow ' . $ani_type;
                if($ani_time != '')
                    $ani_attrs .= ' data-wow-duration="'.esc_attr( $ani_time ).'"';
                if( isset($ani_delays_arr[$idx]) && $ani_delays_arr[$idx] != '' )
                    $ani_attrs .= ' data-wow-delay="'.esc_attr( $ani_delays_arr[$idx] ).'"';
                else
                    $ani_attrs .= ' data-wow-delay="0.2s"';
            }

        ?>
        <div class="<?php echo esc_attr( $item_cls ); ?>"<?php echo $ani_attrs; ?>>
            <div class="dlab-media radius-sm">
                <?php the_post_thumbnail( 'archia-member', array('class'=>'archia-member') );?>
            </div>
            <div class="team-title-bx text-center">
                <h2 class="team-title"><?php the_title(); ?></h2>
                <span class="clearfix"><?php echo get_post_meta( get_the_ID(), BBT_META_PREFIX.'field', true ); ?></span>

                <?php if( $mem_socials == 'yes' && ($facebook || $twitter != '' || $googleplus != '' || $instagram != '' ) ): ?>
                <ul class="memb-icons">
                    <?php if(!empty($facebook)): ?>
                    <li><a href="<?php echo $facebook; ?>"><i class="fa fa-facebook-f"></i></a></li>
                    <?php endif; ?>
                    <?php if(!empty($twitter)): ?>
                    <li><a href="<?php echo $twitter; ?>"><i class="fa fa-twitter"></i></a></li>
                    <?php endif; ?>
                    <?php if(!empty($googleplus)): ?>
                    <li><a href="<?php echo $googleplus; ?>"><i class="fa fa-google-plus"></i></a></li>
                    <?php endif; ?>
                    <?php if(!empty($instagram)): ?>
                    <li><a href="<?php echo $instagram; ?>"><i class="fa fa-instagram"></i></a></li>
                    <?php endif; ?>
                </ul>
                <?php endif; ?>

            </div>
        </div>
        <?php 
        $idx++;
        endwhile; ?>
    </div>
</div>
<?php
endif;
/* Restore original Post Data */
wp_reset_postdata();

