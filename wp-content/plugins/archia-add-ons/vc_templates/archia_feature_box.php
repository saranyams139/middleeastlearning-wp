<?php
/* add_ons_php */
$css = $el_class = $img = $title = $col_title = $style = $number = $link = '';
$ani_type = $ani_time = $ani_delay = '';
$atts = vc_map_get_attributes( $this->getShortcode(), $atts );
extract( $atts );
$css_classes = array(
    'icon-bx-wraper',
    $style,
    $el_class,
    vc_shortcode_custom_css_class( $css ),
);
$css_class = preg_replace( '/\s+/', ' ', implode( ' ', array_filter( $css_classes ) ) );

$ani_attrs = '';
if($ani_type != ''){
    $css_class .= ' wow ' . $ani_type;
    if($ani_time != '')
        $ani_attrs .= ' data-wow-duration="'.esc_attr( $ani_time ).'"';
    if($ani_delay != '')
        $ani_attrs .= ' data-wow-delay="'.esc_attr( $ani_delay ).'"';
    // else
//           $ani_attrs .= ' data-wow-delay="0.2s"';
}   
    
?>
<div class="<?php echo esc_attr( $css_class ); ?>"<?php echo $ani_attrs; ?>>
	<?php if($style == 'sr-iconbox'): ?>
		<div class="icon-lg m-b20">
			<a href="<?php echo esc_url($link); ?>" class="icon-cell"><?php echo wp_get_attachment_image( $img, 'full', false, '' ); ?></a>
		</div>
		<div class="icon-content">
			<h4 class="dlab-tilte"><?php echo $title; ?></h4>
			<?php echo wpb_js_remove_wpautop( $content, true ); ?>
		</div>
	<?php endif; ?>

	<?php if($style == 'sr-numbox'): ?>
		<div class="icon-content">
			<div class="d-flex m-b10">
				<h2 class="box-number"><?php echo $number; ?></h2>
				<h4 class="dlab-tilte text-uppercase font-weight-800"><?php echo $title; ?></h4>
			</div>
			<?php echo wpb_js_remove_wpautop( $content, true ); ?>
		</div>
	<?php endif; ?>

</div>
