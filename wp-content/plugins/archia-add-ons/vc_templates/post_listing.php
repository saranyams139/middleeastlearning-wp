<?php
/* add_ons_php */

$css = $posts_per_page = $el_class= $order = $order_by  = '';
$atts = vc_map_get_attributes( $this->getShortcode(), $atts );
extract( $atts );
$css_classes = array(
    $el_class,
    vc_shortcode_custom_css_class( $css ),
);
$css_class = preg_replace( '/\s+/', ' ', implode( ' ', array_filter( $css_classes ) ) );
// $href = vc_build_link( $href_bt);
if(is_front_page()) {
    $paged = (get_query_var('page')) ? get_query_var('page') : 1;
} else {
    $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
}
if(!empty($ids)){
    $ids = explode(",", $ids);
    $post_args = array(
        'post_type' => 'post',
        'paged' => $paged,
        'posts_per_page'=> $posts_per_page,
        'post__in' => $ids,
        'orderby'=> $order_by,
        'order'=> $order,

        'post_status'       => 'publish',
    );
}elseif(!empty($ids_not)){
    $ids_not = explode(",", $ids_not);
    $post_args = array(
        'post_type' => 'post',
        'paged' => $paged,
        'posts_per_page'=> $posts_per_page,
        'post__not_in' => $ids_not,
        'orderby'=> $order_by,
        'order'=> $order,
        'post_status'       => 'publish',
    );
}else{
    $post_args = array(
        'post_type' => 'post',
        'paged' => $paged,
        'posts_per_page'=> $posts_per_page,
        'orderby'=> $order_by,
        'order'=> $order,
        'post_status'       => 'publish',
    );
}

if(!empty($cat_ids)){
    $post_args['tax_query'][] = array(
        'taxonomy'      => 'category',
        'field'         => 'term_id',
        'terms'         => explode(",", $cat_ids),
        // 'operator'      => 'NOT IN',
    );
}else if(!empty($cat_ids_not)){
    $post_args['tax_query'][] = array(
        'taxonomy'      => 'category',
        'field'         => 'term_id',
        'terms'         => explode(",", $cat_ids_not),
        'operator'      => 'NOT IN',
    );
}

?>

<div class="container">
    <div class="row">
        <?php 
        $posts_query = new \WP_Query($post_args);
        $count = 0;
        if($posts_query->have_posts()) : 
            while($posts_query->have_posts()) : $posts_query->the_post(); ?>
                <?php $count++; 
                    $content_align = $count%2 === 0 ? 'fadeInLeft' : 'fadeInRight';
                    $post_class = 'col-lg-12 wow '.$content_align.'';
                ?>
                <article id="post-<?php the_ID(); ?>" <?php post_class($post_class); ?> data-wow-duration="2s" data-wow-delay="0.2s">
                    <div class="blog-post blog-half blog-half1">
                        <?php if($count%2 === 0): ?>
                            <div class="post-info text-right">
                                <div class="dlab-post-title right">
                                    <h3 class="post-title font-weight-600"><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></h3>
                                </div>
                                <?php 
                                    function_exists('archia_post_meta') ?  archia_post_meta() : '';
                                ?>
                                <div class="dlab-post-text">
                                    <?php the_excerpt(); ?>
                                </div>
                                <div class="dlab-post-readmore blog-share"> 
                                    <a href="<?php the_permalink(); ?>" title="READ MORE" rel="bookmark" class="btn outline outline-2 button-lg black radius-xl btn-aware"><?php esc_html_e('Read More','archia-add-ons') ?><span></span></a>        
                                </div>
                            </div>
                        <?php endif; ?>
                        <?php function_exists('archia_blog_media') ? archia_blog_media() : '' ?>
                        <?php if($count%2!==0): ?>
                            <div class="post-info">
                                <div class="dlab-post-title left">
                                    <h3 class="post-title font-weight-600"><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></h3>
                                </div>
                                <?php 
                                    function_exists('archia_post_meta') ?  archia_post_meta() : '';
                                ?>
                                <?php function_exists('archia_blog_excerpt') ? archia_blog_excerpt() : '' ?>
                                <div class="dlab-post-readmore blog-share"> 
                                    <a href="<?php the_permalink(); ?>" title="READ MORE" rel="bookmark" class="btn outline outline-2 button-lg black radius-xl btn-aware"><?php esc_html_e('Read More','archia-add-ons') ?><span></span></a>        
                                </div>
                            </div>
                        <?php endif; ?>
                    </div>
                </article>
                <!-- article end -->
                
            <?php endwhile; ?>
        <?php endif; ?> 
    </div>
    <!-- end wrap-post-listing -->
    <nav class="navigation pagination" role="navigation">
        <div class="nav-links">
            <?php  
                $big = 999999999; // need an unlikely integer
                echo paginate_links( array(
                    'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
                    'format' => '?paged=%#%',
                    'current' => max( 1, get_query_var('paged') ),
                    'total' => $posts_query->max_num_pages,
                    'end_size'  => 0,
                    'mid_size'  => 4,
                    'prev_text'   => __('<i class="fa fa-arrow-left"></i> Previous', 'archia-add-ons'),
                    'next_text'   => __('Next <i class="fa fa-arrow-right"></i>', 'archia-add-ons'),
                ) );
            ?>
        </div>
        
    </nav>
    
    <?php wp_reset_postdata();?>
</div>