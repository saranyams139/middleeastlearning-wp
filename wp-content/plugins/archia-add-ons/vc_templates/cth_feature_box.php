<?php
/**
 * Shortcode attributes
 * @var $atts
 * @var $el_class
 * @var $iconname
 * @var $iconimg
 * @var $title
 * Shortcode class
 * @var $this WPBakeryShortCode_Cth_Feature_box
 */
$atts = vc_map_get_attributes( $this->getShortcode(), $atts );
extract( $atts );

$iconname = str_replace("fa-", "", $iconname);
$iconname = str_replace("fa ", "", $iconname);
?>
<div class="features-box <?php echo esc_attr($el_class );?>">
    <?php if(!empty($iconname)):?>
        <i class="fa fa-<?php echo esc_attr($iconname ); ?> feature-icon"></i>
    <?php endif;?>
    
    <?php 
    if(!empty($iconimg)) { 
        echo wp_get_attachment_image( $iconimg, 'full', false, array('class'=>'img-responsive feature-img')); 
    } ?>
    <?php if(!empty($title)):?>
        <h4><?php echo esc_html($title ); ?></h4>
    <?php endif;?>
    <?php echo wpb_js_remove_wpautop($content,true) ;?>
</div>
