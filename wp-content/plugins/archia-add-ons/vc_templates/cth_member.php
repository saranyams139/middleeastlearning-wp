<?php
/**
 * Shortcode attributes
 * @var $atts
 * @var $el_class
 * @var $photo
 * @var $name
 * @var $job
 * Shortcode class
 * @var $this WPBakeryShortCode_Cth_Member
 */
$atts = vc_map_get_attributes( $this->getShortcode(), $atts );
extract( $atts );
?>
<div class="team-wrapper <?php echo esc_attr($el_class );?>">
    <div class="team-contain">
        <?php if(!empty($job)):?>
            <span><?php echo esc_attr($job ); ?></span>
        <?php endif;?>
        <?php if(!empty($name)):?>
            <h3><?php echo esc_attr($name ); ?></h3>
        <?php endif;?>
        <?php echo wpb_js_remove_wpautop($content,true) ;?>
    </div>
    <?php 
    if(!empty($photo)) { ?>
    <div class="team-avatar">
        <?php echo wp_get_attachment_image( $photo, 'full', false, array('class'=>'img-responsive'));  ?>
    </div>
    <?php 
    } ?>
</div>