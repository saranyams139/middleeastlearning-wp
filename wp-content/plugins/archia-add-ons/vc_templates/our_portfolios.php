<?php
/**
 * Shortcode attributes
 * @var $atts
 * @var $count
 * @var $order
 * @var $order_by
 * @var $ids
 * @var $showfilter
 * @var $showlarge
 * @var $showdetail
 * @var $columns
 * @var $smcolumns
 * @var $xscolumns
 * @var $extra_class
 * Shortcode class
 * @var $this WPBakeryShortCode_Our_Portfolios
 */
$atts = vc_map_get_attributes( $this->getShortcode(), $atts );
extract( $atts );

if($showfilter === 'true') :
    // wp_enqueue_script("jquery-hover", get_template_directory_uri()."/js/hover/jquery-hover-effect.js",array(),false,true);
    // wp_enqueue_script("jquery-quicksand", get_template_directory_uri()."/js/filter/jquery.quicksand.js",array(),false,true);
    //wp_enqueue_script("filter-setting", get_template_directory_uri()."/js/filter/setting.js",array(),false,true);

    $term_args = array(
        'orderby'           => 'name', 
        'order'             => 'DESC',
    ); 
    $portfolio_skills = get_terms('skill',$term_args); 
    if(count($portfolio_skills)):
    ?>
    <div class="row">
        <div class="col-md-12">
            <ul class="filter tooltips">
                <li data-value="all" class="active"><a href="#" ><?php _e('All','archia-add-ons');?></a></li>
                <?php foreach($portfolio_skills as $portfolio_skill) { ?>
                    <li data-value="<?php echo esc_attr($portfolio_skill->slug ); ?>"><a href="#" ><?php echo esc_html($portfolio_skill->name ); ?></a></li>
                <?php } ?>
            </ul>
        </div>
    </div>
    <?php 
    endif;

endif;//end showfilter check
    ?>
    <div class="clearfix"></div>
    <div class="row">
        <div class="gallery-wrapper <?php echo esc_attr($extra_class );?>">
            
    <?php 
        if(is_front_page()) {
            $paged = (get_query_var('page')) ? get_query_var('page') : 1;
        } else {
            $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
        }
        if(!empty($ids)){
            $ids = explode(",", $ids);
            $args = array(
                'post_type' => 'portfolio',
                'post__in' => $ids,
                'paged' => $paged,
                //'posts_per_page' => $count,
                'order' => $order,
                'orderby' => $order_by,
            );
        }else{
            $args = array(
                'post_type' => 'portfolio',
                'paged' => $paged,
                'posts_per_page' => $count,
                'order' => $order,
                'orderby' => $order_by,
            );
        }

        
        $portfolio = new WP_Query($args);
        $pindex = 1;
        if($portfolio->have_posts()) : while($portfolio->have_posts()) : $portfolio->the_post();
            $item_classes = array();
            $item_cats = get_the_terms(get_the_ID(), 'skill');
            foreach((array)$item_cats as $item_cat){
                if(count($item_cat)>0){
                    $item_classes[] = $item_cat->slug ;
                }
            }
            $item_classes = implode(" ", $item_classes);
            //$viewtype = get_post_meta(get_the_ID(), '_cmb_viewtype', true);
            //$captionClass = '';
            //$viewlink = get_the_permalink();
            ?>

            <div data-type="<?php echo esc_attr($item_classes );?>" data-id="id-<?php echo esc_attr($pindex );?>" class="col-md-<?php echo esc_attr($columns );?> col-sm-<?php echo esc_attr($smcolumns );?> col-xs-<?php echo esc_attr($xscolumns );?> gallery-item">
                <div class="img-wrapper">
                <?php if($showlarge === 'true'||$showdetail === 'true') :?>
                    <div class="img-caption">
                    <?php if($showlarge === 'true') :?>
                        <a href="<?php echo archia_thumbnail_url('full');?>" class="img-zoom" data-pretty="prettyPhoto"><i class="fa fa-search"></i></a>
                    <?php endif;?>
                        <span></span>
                    <?php if($showdetail === 'true') :?>
                        <a href="<?php the_permalink();?>" class="img-link"><i class="fa fa-link"></i></a>
                    <?php endif;?>
                    </div>
                <?php endif;?>
                    <?php the_post_thumbnail('folio-thumb', array('class'=>'img-responsive' )); ?>
                </div>
            </div>

        <?php $pindex++; endwhile; endif; ?>
    </div>
</div>
