<?php
/* add_ons_php */
$css = $el_class = $icon_type = $title = $icon_pos = '';
$icon_fontawesome = $icon_themify = '';
$defaultIconClass = '';// 'fa fa-phone';
$atts = vc_map_get_attributes( $this->getShortcode(), $atts );
extract( $atts );
$css_classes = array(
    'folio-details-wrap',
    $el_class,
    $icon_pos,
    vc_shortcode_custom_css_class( $css ),
);
$css_class = preg_replace( '/\s+/', ' ', implode( ' ', array_filter( $css_classes ) ) );
$iconClass = isset( ${'icon_' . $icon_type} ) ? ${'icon_' . $icon_type} : $defaultIconClass;

?>
<div class="<?php echo esc_attr( $css_class );?>">
	<div class="pro-details">
		<?php if($iconClass != ''): ?><i class="<?php echo $iconClass; ?>"></i><?php endif; ?>
		<?php if($title != ''): ?>
		<h4 class="fodetls-tilte"><?php echo $title; ?></h4>
		<?php endif; ?>
		<?php echo wpb_js_remove_wpautop( $content, true ); ?>
	</div>
</div>
