<?php
/* add_ons_php */

$css = $el_class = $cat_ids = $cat_ids_not = $ids = $ids_not = $order = $order_by = $showposts = '';
$atts = vc_map_get_attributes( $this->getShortcode(), $atts );
extract( $atts );
// $columns =  $columns_grid.'-cols';
$css_classes = array(
    'project-view',
    $el_class,
    vc_shortcode_custom_css_class( $css ),
);
$css_class = preg_replace( '/\s+/', ' ', implode( ' ', array_filter( $css_classes ) ) );
$css_class = preg_replace( '/\s+/', ' ', implode( ' ', array_filter( $css_classes ) ) );
?>
<div class="<?php echo $css_class; ?>">
    <?php

    if(!empty($cat_ids)){
        $args['tax_query'][] = array(
            'hide_empty' => true,
            'taxonomy'      => 'project_cat',
            'field'         => 'term_id',
            'terms'         => explode(",", $cat_ids),
            // 'operator'      => 'NOT IN',
        );
    }else if(!empty($cat_ids_not)){
        $args['tax_query'][] = array(
            'hide_empty' => true,
            'taxonomy'      => 'project_cat',
            'field'         => 'term_id',
            'terms'         => explode(",", $cat_ids_not),
            'operator'      => 'NOT IN',
        );
    }else {
        $args = array( 
        'hide_empty' => true,
        'taxonomy' => 'project_cat',
    );
    }

    $terms = get_terms( $args );
    $course_terms = 0;
    ?>
    <div class="row">
        <?php
            
            $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;

            if(!empty($ids)){
                $ids = explode(",", $ids); 
                $args = array(
                    'post_type' => 'project',
                    'paged' => $paged,
                    'posts_per_page'=> $showposts, 
                    'showposts' => $showposts,
                    'post__in' => $ids,
                    'orderby'=> $order_by,
                    'order'=> $order,
                    'post_status'       => 'publish',
                );
            }elseif(!empty($ids_not)){
                $ids_not = explode(",", $ids_not);
                $args = array(
                    'post_type' => 'project',
                    'paged' => $paged,
                    'posts_per_page'=> $showposts,
                    'showposts' => $showposts,
                    'post__not_in' => $ids_not,
                    'orderby'=> $order_by,
                    'order'=> $order,
                    'post_status'       => 'publish',
                );
            }else{
                $args = array(
                    'post_type' => 'project',
                    'paged' => $paged,
                    'posts_per_page'=> $showposts,
                    'showposts' => $showposts,
                    'orderby'=> $order_by,
                    'order'=> $order,
                    'post_status'       => 'publish',
                );
            }
            
            $course = new \WP_Query($args);
            if ($course -> have_posts()) :
                while ($course -> have_posts()) : $course -> the_post(); 
                    $terms = get_the_terms( get_the_ID(), 'project_cat' );
                    
                    if ( $terms && ! is_wp_error( $terms ) ) : 
                        
                        foreach ( $terms as $term ) { 
                            ?>

                                <div class="col-lg-3 col-md-6 col-sm-6 wow fadeInUp " data-wow-delay="0.2s" data-wow-duration="1s">
                                    <div class="dlab-box portbox2">
                                        <div class="dlab-media"> 
                                             <?php the_post_thumbnail('archia-project',array('class'=>'resp-img') ); ?>
                                            <div class="dlab-info-has no-hover">
                                                <h4 class="title"><a href="<?php the_permalink(  ); ?>"><?php echo the_title(); ?></a></h4>
                                                <span><?php echo $term->name; ?></span>
                                                <?php the_excerpt(); ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                        <?php } 
                    endif;
                endwhile;
            endif;
            ?>

    </div>
</div>
    
    
