<?php
/* add_ons_php */
$css = $el_class = $icon_type = $title = $link = $btn_ali = $btn_color = $btn_size = $btn_radius = $btn_style = '';
$atts = vc_map_get_attributes( $this->getShortcode(), $atts );
extract( $atts );
$css_classes = array(
    'cth-btn-wrap',
    $btn_ali,
    $el_class,
    vc_shortcode_custom_css_class( $css ),
);
$css_class = preg_replace( '/\s+/', ' ', implode( ' ', array_filter( $css_classes ) ) );
$href = vc_build_link( $link );
// var_dump($href);
$url = (!empty($href['url'])) ? $href['url'] : '#';
$target = (!empty($href['target'])) ? $href['target'] : '';
$rel = (!empty($href['rel'])) ? $href['rel'] : '';
$title = ($title == '' && !empty($href['title'])) ? $href['title'] : $title;


// array(4) { ["url"]=> string(39) "http://localhost:8888/archia/contact-1/" ["title"]=> string(9) "Contact 1" ["target"]=> string(7) " _blank" ["rel"]=> string(8) "nofollow" }

$link_attrs = '';
if($target != '') $link_attrs .= ' target="'.$target.'"';
if($rel != '') $link_attrs .= ' rel="'.$rel.'"';

$a_cls = "btn $btn_color $btn_size $btn_radius $btn_style";
if($btn_color == 'bgblack'){
	$a_cls .= ' black';
}
?>
<div class="<?php echo esc_attr( $css_class );?>">
	<a href="<?php echo esc_url( $url ); ?>" <?php echo $link_attrs; ?> class="<?php echo esc_attr( $a_cls );?>"><?php echo $title; ?><span></span></a>
</div>
