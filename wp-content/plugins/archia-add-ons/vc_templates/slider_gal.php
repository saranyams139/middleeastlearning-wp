<?php
/* add_ons_php */

$el_class = $css = $galimgs = $columns = $target = $thumbnail_size = '';
// $show_navigation = $show_dots = $mousewheel = $keyboard = $autoplay = $loop = $direction = $speed = $effect = $responsive = $spacing = '';

$atts = vc_map_get_attributes( $this->getShortcode(), $atts );
extract( $atts );

$css_classes = array(
    'slider-gallery-wrap',
    $el_class,
    vc_shortcode_custom_css_class( $css ),
);
$css_class = preg_replace( '/\s+/', ' ', implode( ' ', array_filter( $css_classes ) ) );
if(!empty($galimgs)){
    $images = $links = array();
    $images = explode(",", $galimgs);
    if(!empty($content)){
        $seppos = strpos(strip_tags($content), "|");
        if($seppos !== false){
            $links = explode("|", strip_tags($content));
        }else{
            $links = preg_split( '/\r\n|\r|\n/', strip_tags($content) );
        }
    }

    if( strpos($thumbnail_size, "x") !== false){
        $thumbnail_size = explode("x", $thumbnail_size);
    }

    // $dataArr = array();
    // $dataArr['smartSpeed'] = (int)$speed;
    // if($autoplay == 'yes') $dataArr['autoplay'] = true;
    // if($loop == 'yes') 
    //     $dataArr['loop'] = true;
    // else 
    //     $dataArr['loop'] = false;
    // if($show_navigation == 'yes') 
    //     $dataArr['nav'] = true;
    // else 
    //     $dataArr['nav'] = false;
    // if($show_dots == 'yes') 
    //     $dataArr['dots'] = true;
    // else 
    //     $dataArr['dots'] = false;

    // if(!empty($responsive)){
    //     $css_class .=' resp-ena';
    //     $dataArr['responsive'] = $responsive;
    // }
    // if(is_numeric($spacing)) $dataArr['margin'] = (int)$spacing;
    ?>
    <div class="<?php echo esc_attr( $css_class ); ?>">
        <div class="footer-gallery owl-carousel owl-none mfp-gallery-with-owl">
            <?php foreach ($images as $key => $img) { ?>
                <div class="item">
                     <div class="dlab-box dlab-gallery-box overlay-gallery-bx1">
                        <div class="dlab-thum dlab-img-overlay1 primary"> 
                            <a href="javascript:void(0);"><?php echo wp_get_attachment_image( $img, $thumbnail_size, false, array('class'=>'slider-gal-thumbnail') );?></a>
                            <div class="overlay-bx">
                                <div class="overlay-icon"> 
                                    <?php if( isset($links[$key]) && !empty($links[$key]) ): ?>
                                    <a href="<?php echo esc_url( $links[$key] ); ?>" class="popup-youtube video"><i class="fa fa-play icon-bx-xs"></i></a> 
                                    <?php endif; ?>
                                    <a href="<?php echo wp_get_attachment_url( $img ); ?>" class="mfp-link" title="<?php echo esc_attr( get_the_title( $img ) ); ?>"> <i class="fa fa-search icon-bx-xs"></i> </a> 
                                </div>
                            </div>
                        </div>
                    </div>
                </div>  
            <?php } ?> 
        </div>
    </div>
<?php } ?>