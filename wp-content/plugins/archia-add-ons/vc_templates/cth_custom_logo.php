<?php
/* add_ons_php */
$css = $el_class = $logo = '';
$atts = vc_map_get_attributes( $this->getShortcode(), $atts );
extract( $atts );
$css_classes = array(
    'logo-header',
    $el_class,
    vc_shortcode_custom_css_class( $css ),
);
$css_class = preg_replace( '/\s+/', ' ', implode( ' ', array_filter( $css_classes ) ) );
?>
<div class="<?php echo esc_attr($css_class );?>">
    <?php echo '<a class="custom-logo-link" href="'.esc_url( home_url('/' ) ).'">'.wp_get_attachment_image($logo, 'full', false, array('class'=>'resp-img') ).'</a>';?>
</div>