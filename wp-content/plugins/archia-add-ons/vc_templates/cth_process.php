<?php
/* add_ons_php */
$css = $el_class = $img = $title = $col_title = $link = $sub_title = '';
$ani_type = $ani_time = $ani_delay = '';
$atts = vc_map_get_attributes( $this->getShortcode(), $atts );
extract( $atts );
$css_classes = array(
    'process-item-wraper',
    // $style,
    $el_class,
    vc_shortcode_custom_css_class( $css ),
);
$css_class = preg_replace( '/\s+/', ' ', implode( ' ', array_filter( $css_classes ) ) );

$ani_attrs = '';
if($ani_type != ''){
    $css_class .= ' wow ' . $ani_type;
    if($ani_time != '')
        $ani_attrs .= ' data-wow-duration="'.esc_attr( $ani_time ).'"';
    if($ani_delay != '')
        $ani_attrs .= ' data-wow-delay="'.esc_attr( $ani_delay ).'"';
    // else
//           $ani_attrs .= ' data-wow-delay="0.2s"';
}   

$href = vc_build_link( $link );
$url = (!empty($href['url'])) ? $href['url'] : '#';
$target = (!empty($href['target'])) ? $href['target'] : '';
$rel = (!empty($href['rel'])) ? $href['rel'] : '';
$link_attrs = '';
if($target != '') $link_attrs .= ' target="'.$target.'"';
if($rel != '') $link_attrs .= ' rel="'.$rel.'"';

?>
<div class="<?php echo esc_attr( $css_class ); ?>"<?php echo $ani_attrs; ?>>
	<div class="dlab-box portbox2">
		<div class="dlab-media"> 
			<?php echo wp_get_attachment_image( $img, 'archia-process', false, '' ); ?>
			<div class="dlab-info-has no-hover">
				<?php if($title != ''): ?><h4 class="title"><a href="<?php echo esc_url( $url ); ?>" <?php echo $link_attrs; ?>><?php echo $title; ?></a></h4><?php endif; ?>
				<?php if($sub_title != ''): ?><span><?php echo $sub_title; ?></span><?php endif; ?>
				<?php echo wpb_js_remove_wpautop( $content, true ); ?>
			</div>
		</div>
	</div>
</div>
