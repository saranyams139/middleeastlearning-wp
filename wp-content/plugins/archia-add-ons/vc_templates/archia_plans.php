<?php
/* add_ons_php */
$css = $el_class = $ids = $ids_not = $order_by = $order = $posts_per_page = $columns_grid = $style = $featured_plan = '';
$ani_type = $ani_time = $ani_delays = '';
$atts = vc_map_get_attributes( $this->getShortcode(), $atts );
extract( $atts );
// $bg_color = 'box-heading-wrapper-'.$color_picker_title;
$css_classes = array(
    'pricingtable-row',
    'archia-pricing',
    $el_class,
    vc_shortcode_custom_css_class( $css ),
);
$css_class = preg_replace( '/\s+/', ' ', implode( ' ', array_filter( $css_classes ) ) );
$ani_delays_arr = explode(',', $ani_delays);
?>
<div class="<?php echo esc_attr($css_class );?>">
    <div class="row">
        <?php
        if(!empty($ids)){
            $ids = explode(",", $ids);
            $post_args = array(
                'post_type' => 'lplan',
                'posts_per_page'=> $posts_per_page,
                'post__in' => $ids,
                'orderby'=> $order_by,
                'order'=> $order,
                'post_status' => 'publish'
            );
        }elseif(!empty($ids_not)){
            $ids_not = explode(",", $ids_not);
            $post_args = array(
                'post_type' => 'lplan',
                'posts_per_page'=> $posts_per_page,
                'post__not_in' => $ids_not,
                'orderby'=> $order_by,
                'order'=> $order,
                'post_status' => 'publish'
            );
        }else{
            $post_args = array(
                'post_type' => 'lplan',
                'posts_per_page'=> $posts_per_page,
                'orderby'=> $order_by,
                'order'=> $order,
                'post_status' => 'publish'
            );
        };

        $posts_query = new \WP_Query($post_args);
        if($posts_query->have_posts()): ?>
            <?php 
            $idx = 0;
            while($posts_query->have_posts()) : $posts_query->the_post(); 
            $_price = get_post_meta( get_the_ID(), BBT_META_PREFIX. 'price' , true );
            $odd_amount = get_post_meta( get_the_ID(), BBT_META_PREFIX. 'dd_amount' , true );
            $currency = get_post_meta( get_the_ID(), BBT_META_PREFIX. 'currency' , true );
            $url_plan = get_post_meta( get_the_ID(), BBT_META_PREFIX. '_url_plan' , true );


            $pricing_wrap = 'pricingtable-wrapper table-option m-b30';
            if( ($idx + 1) == $featured_plan) {
                if($style == ''){
                    $pricing_wrap .= ' active';
                    $class_btn = 'btn radius-xl button-md black';
                }elseif ($style == 'style-1'){
                    $pricing_wrap .= ' dark active';
                    $class_btn = 'btn radius-xl button-md white';
                }elseif ($style == 'style-2'){
                    $pricing_wrap .= ' ';
                    $class_btn = '';
                }     
            }else {
                if ($style == '') {
                    $pricing_wrap .= '';
                    $class_btn = 'btn outline outline-2 radius-xl button-md black btn-aware';
                }elseif ($style == 'style-1'){
                    $pricing_wrap .= ' dark';
                    $class_btn = 'btn outline outline-2 radius-xl button-md black';
                }elseif ($style == 'style-2'){
                    $pricing_wrap .= ' ';
                    $class_btn = '';
                }  
            }

            $css_pricing_item = "$columns_grid";
            $ani_attrs = '';
            if ( !empty($ani_type) ) {
                $css_pricing_item .= ' wow ' . $ani_type;
                if($ani_time != '')
                    $ani_attrs .= ' data-wow-duration="'.esc_attr( $ani_time ).'"';
                if( isset($ani_delays_arr[$idx]) && $ani_delays_arr[$idx] != '' )
                    $ani_attrs .= ' data-wow-delay="'.esc_attr( $ani_delays_arr[$idx] ).'"';
                else
                    $ani_attrs .= ' data-wow-delay="0.2s"';
            }

            ?>
            
            <div class="<?php echo esc_attr( $css_pricing_item ); ?>" <?php echo $ani_attrs; ?>>
                <div class="<?php echo $pricing_wrap; ?>">
                    <div class="pricingtable-inner">
                        <div class="pricingtable-title">
                            <h2><?php echo the_title(); ?></h2>
                        </div>
                        <div class="pricingtable-price">
                            <span class="pricingtable-bx"><sup><?php echo $currency;?></sup><?php echo $_price; ?><strong><?php echo '.'.$odd_amount ?></strong></span>
                        </div>
                        <?php the_content(); ?>
                        <div class="pricingtable-footer"> <a href="#" class="<?php echo $class_btn; ?>"><?php esc_html_e( 'View Plan', 'archia-add-ons' ) ?><span></span></a> </div>
                    </div>
                </div>
            </div>

        <?php
            $idx++;
            endwhile; ?>
        <?php endif; ?>
    </div>
</div>
<?php wp_reset_postdata();





