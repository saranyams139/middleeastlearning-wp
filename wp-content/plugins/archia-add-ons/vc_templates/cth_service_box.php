<?php
/**
 * Shortcode attributes
 * @var $atts
 * @var $el_class
 * @var $iconname
 * @var $iconimg
 * @var $title
 * @var $position
 * @var $color
 * Shortcode class
 * @var $this WPBakeryShortCode_Cth_Service_box
 */
$atts = vc_map_get_attributes( $this->getShortcode(), $atts );
extract( $atts );

$iconname = str_replace("fa-", "", $iconname);
$iconname = str_replace("fa ", "", $iconname);
?>
<div class="services-contain on-<?php echo esc_attr($position );?> <?php echo esc_attr($el_class );?>">
    <div class="icon-services icon-<?php echo esc_attr($color ); ?>">
        <?php if(!empty($iconname)):?>
            <i class="fa fa-<?php echo esc_attr($iconname ); ?>"></i>
        <?php endif;?>
        <?php 
        if(!empty($iconimg)) { 
            echo wp_get_attachment_image( $iconimg, 'full', false, array('class'=>'img-responsive service-img')); 
        } ?>
        <span class="linner"></span>
    </div>  
    <div class="services-desc">
        <?php if(!empty($title)):?>
            <h4><?php echo esc_html($title ); ?></h4>
        <?php endif;?>
        <?php echo wpb_js_remove_wpautop($content,true) ;?>
    </div>
</div>
<div class="clearfix"></div>