<?php
/* add_ons_php */
$css = $el_class = $icon = $vid_source = $btn_style = $btn_ali = '';
$ani_type = $ani_time = $ani_delay = '';
$atts = vc_map_get_attributes( $this->getShortcode(), $atts );
extract( $atts );
$css_classes = array(
    'video-popup-btn',
    $btn_style,
    $btn_ali,
    $el_class,
    vc_shortcode_custom_css_class( $css ),
);
$css_class = preg_replace( '/\s+/', ' ', implode( ' ', array_filter( $css_classes ) ) );
if($vid_source != ''):
	$a_cls = 'videoplay-bx';
	if($btn_style == 'popvid-small') 
		$a_cls = 'video btn outline outline-2 button-lg black radius-xl btn-aware btn-video';
	if($btn_style == 'popvid-medium'){
		$css_class .= ' video-content-box';
		$a_cls = 'video';
	} 
	if($btn_style == 'popvid-colored'){
		$css_class .= ' video-play';
		$a_cls = 'video';
	} 

	$ani_attrs = '';
	if($ani_type != ''){
		$css_class .= ' wow ' . $ani_type;
		if($ani_time != '')
			$ani_attrs .= ' data-wow-duration="'.esc_attr( $ani_time ).'"';
		if($ani_delay != '')
			$ani_attrs .= ' data-wow-delay="'.esc_attr( $ani_delay ).'"';
		// else
  //           $ani_attrs .= ' data-wow-delay="0.2s"';
	}	
?>
<div class="<?php echo esc_attr( $css_class ); ?>"<?php echo $ani_attrs; ?>>
	<div class="video-popup-inner clearfix">
		<a href="<?php echo $vid_source; ?>" class="popup-youtube <?php echo esc_attr( $a_cls ); ?>"><i class="popvid-icon <?php echo $icon; ?>"></i><span></span></a>
		<?php if($title != '') echo '<p class="m-b0 popup-video-title">'.$title.'</p>'; ?>
	</div>
	<?php echo wpb_js_remove_wpautop( $content, true ); ?>
</div>
<?php endif; 

