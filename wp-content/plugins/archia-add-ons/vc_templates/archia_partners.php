<?php
/* add_ons_php */

$el_class = $css = $partnerimgs = $columns = $target = $thumbnail_size = '';
// $show_navigation = $show_dots = $mousewheel = $keyboard = $autoplay = $loop = $direction = $speed = $effect = $responsive = $spacing = '';
$ani_type = $ani_time = $ani_delays = '';
$atts = vc_map_get_attributes( $this->getShortcode(), $atts );
extract( $atts );

$css_classes = array(
    'partners-slider-wrap',
    $el_class,
    vc_shortcode_custom_css_class( $css ),
);
$css_class = preg_replace( '/\s+/', ' ', implode( ' ', array_filter( $css_classes ) ) );
if(!empty($partnerimgs)){
    $partners = $partnerlinks = array();
    $partners = explode(",", $partnerimgs);
    if(!empty($content)){
        $seppos = strpos(strip_tags($content), "|");
        if($seppos !== false){
            $partnerlinks = explode("|", strip_tags($content));
        }else{
            $partnerlinks = preg_split( '/\r\n|\r|\n/', strip_tags($content) );
        }
    }

    if( strpos($thumbnail_size, "x") !== false){
        $thumbnail_size = explode("x", $thumbnail_size);
    }

    // $dataArr = array();
    // $dataArr['smartSpeed'] = (int)$speed;
    // if($autoplay == 'yes') $dataArr['autoplay'] = true;
    // if($loop == 'yes') 
    //     $dataArr['loop'] = true;
    // else 
    //     $dataArr['loop'] = false;
    // if($show_navigation == 'yes') 
    //     $dataArr['nav'] = true;
    // else 
    //     $dataArr['nav'] = false;
    // if($show_dots == 'yes') 
    //     $dataArr['dots'] = true;
    // else 
    //     $dataArr['dots'] = false;

    // if(!empty($responsive)){
    //     $css_class .=' resp-ena';
    //     $dataArr['responsive'] = $responsive;
    // }
    // if(is_numeric($spacing)) $dataArr['margin'] = (int)$spacing;

    $ani_delays_arr = explode(',', $ani_delays);
    ?>

    <div class="client-logo owl-carousel owl-none">
        <?php foreach ($partners as $key => $img) { 
            $link = '';
            if( isset($partnerlinks[$key]) ) $link = $partnerlinks[$key];

            $item_cls = 'client-logo-inner';
            $ani_attrs = '';
            if ( !empty($ani_type) ) {
                $item_cls .= ' wow ' . $ani_type;
                if($ani_time != '')
                    $ani_attrs .= ' data-wow-duration="'.esc_attr( $ani_time ).'"';
                if( isset($ani_delays_arr[$key]) && $ani_delays_arr[$key] != '' )
                    $ani_attrs .= ' data-wow-delay="'.esc_attr( $ani_delays_arr[$key] ).'"';
                else
                    $ani_attrs .= ' data-wow-delay="0.2s"';
            }
            
            ?>
            <div class="item">
                <div class="<?php echo esc_attr( $item_cls ); ?>"<?php echo $ani_attrs; ?>>
                    <a class="partner-link" href="<?php echo esc_url( $link );?>" target="<?php echo esc_attr($target );?>">
                        <?php echo wp_get_attachment_image( $img, $thumbnail_size, false, array('class'=>'partner-logo resp-img') );?>
                    </a>
                </div>
            </div>   
        <?php } ?> 
    </div>
<?php } ?>