<?php
/* add_ons_php */
$css = $el_class = '';
$atts = vc_map_get_attributes( $this->getShortcode(), $atts );
extract( $atts );
$css_classes = array(
    'header-contact contact-info',
    $el_class,
    vc_shortcode_custom_css_class( $css ),
);
if ($show_boder !== 'yes') {
	$css_classes[] = 'hidden-boder';
}
$css_class = preg_replace( '/\s+/', ' ', implode( ' ', array_filter( $css_classes ) ) );
?>
<div class="<?php echo esc_attr($css_class );?>" >
	<?php echo $content; ?>
</div>
