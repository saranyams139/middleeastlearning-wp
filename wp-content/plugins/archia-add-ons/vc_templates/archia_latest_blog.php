<?php
/* add_ons_php */

$css = $el_class = $posts_per_page = $order = $order_by = $first_side = $use_ani = $ani_dura = $ani_delay = $show_readmore = $posts_style = '';
$ani_type = $ani_time = $ani_delays = '';

$atts = vc_map_get_attributes( $this->getShortcode(), $atts );
extract( $atts );
$css_classes = array(
    'latest-blog',
    $el_class,
    $posts_style,
    vc_shortcode_custom_css_class( $css ),
);
$css_class = preg_replace( '/\s+/', ' ', implode( ' ', array_filter( $css_classes ) ) );
// $href = vc_build_link( $href_bt);
if(is_front_page()) {
    $paged = (get_query_var('page')) ? get_query_var('page') : 1;
} else {
    $paged = (get_query_var('paged')) ? get_query_var('paged') : 1; 
}
if(!empty($ids)){
    $ids = explode(",", $ids); 
    $post_args = array(
        'post_type' => 'post',
        'paged' => $paged,
        'posts_per_page'=> $posts_per_page, 
        'post__in' => $ids,
        'orderby'=> $order_by,
        'order'=> $order,
        'post_status'       => 'publish',
    );
}elseif(!empty($ids_not)){
    $ids_not = explode(",", $ids_not);
    $post_args = array(
        'post_type' => 'post',
        'paged' => $paged,
        'posts_per_page'=> $posts_per_page,
        'post__not_in' => $ids_not,
        'orderby'=> $order_by,
        'order'=> $order,
        'post_status'       => 'publish',
    );
}else{
    $post_args = array(
        'post_type' => 'post',
        'paged' => $paged,
        'posts_per_page'=> $posts_per_page,
        'orderby'=> $order_by,
        'order'=> $order,
        'post_status'       => 'publish',
    );
}

if(!empty($cat_ids)){
    $post_args['tax_query'][] = array(
        'taxonomy'      => 'category',
        'field'         => 'term_id',
        'terms'         => explode(",", $cat_ids),
        // 'operator'      => 'NOT IN',
    );
}else if(!empty($cat_ids_not)){
    $post_args['tax_query'][] = array(
        'taxonomy'      => 'category',
        'field'         => 'term_id',
        'terms'         => explode(",", $cat_ids_not),
        'operator'      => 'NOT IN',
    );
}
$ani_delays_arr = explode(',', $ani_delays);
?>

<div class="<?php echo esc_attr($css_class); ?>">
    <?php $posts_query = new \WP_Query($post_args);
        if($posts_query->have_posts()) : 

            if($posts_style == 'posts-style-slider'){
                $owl_cls = 'blog-carousel owl-carousel owl-none';
                $ani_attrs = '';
                if ( !empty($ani_type) ) {
                    $owl_cls .= ' wow ' . $ani_type;
                    if($ani_time != '')
                        $ani_attrs .= ' data-wow-duration="'.esc_attr( $ani_time ).'"';
                    if( isset($ani_delays_arr[0]) && $ani_delays_arr[0] != '' )
                        $ani_attrs .= ' data-wow-delay="'.esc_attr( $ani_delays_arr[0] ).'"';
                    
                }
                echo '<div class="'.$owl_cls.'"'.$ani_attrs.'>';
            } 

            $count = 0;
            while($posts_query->have_posts()) : $posts_query->the_post(); 

                if($posts_style != 'posts-style-slider'):

                    $wrap_cls = '';
                    if($first_side == 'left'){
                        $wrap_cls = 'post-left-side';
                        if( ($count+1) % 2 == 0)
                            $wrap_cls = 'post-right-side';
                    }else{
                        $wrap_cls = 'post-right-side';
                        if( ($count+1) % 2 == 0)
                            $wrap_cls = 'post-left-side';
                    }

                    $ani_attrs = '';
                    if ( !empty($ani_type) ) {
                        $wrap_cls .= ' wow ' . $ani_type;
                        if($ani_time != '')
                            $ani_attrs .= ' data-wow-duration="'.esc_attr( $ani_time ).'"';
                        if( isset($ani_delays_arr[$count]) && $ani_delays_arr[$count] != '' )
                            $ani_attrs .= ' data-wow-delay="'.esc_attr( $ani_delays_arr[$count] ).'"';
                        else
                            $ani_attrs .= ' data-wow-delay="0.2s"';
                    }

                    ?>

                    <div id="post-<?php the_ID(); ?>" class="blog-post blog-half <?php echo esc_attr( $wrap_cls ); ?>"<?php echo esc_attr( $ani_attrs ); ?>>
                        <div class="blog-media wow fadeIn" data-wow-delay="0.2s"  data-wow-duration="2s">
                            <?php the_post_thumbnail('archia-featured',array('class'=>'resp-img') ); ?>
                        </div>
                        <div class="post-info">

                            <div class="dlab-post-title ">
                                <h3 class="post-title"><a href="<?php the_permalink(  ); ?>"><?php the_title(); ?></a></h3>
                            </div>
                            <div class="dlab-post-meta">
                                <ul>
                                    <li class="post-date"><?php the_time(get_option('date_format'));?></li>
                                </ul>
                            </div>
                            <div class="dlab-post-text">
                                <?php the_excerpt(); ?>
                            </div>
                            <?php if($show_readmore != 'no'): ?>
                                <div class="dlab-post-readmore blog-share"> 
                                    <a href="<?php echo esc_url( get_permalink() ); ?>" rel="bookmark" class="btn outline outline-2 button-lg black radius-xl btn-aware"><?php esc_html_e( 'View details', 'archia-add-ons' ); ?><span></span></a>        
                                </div>
                            <?php endif; ?>
                        </div>
                    </div>
                    <?php else: // end posts list ?>
                    <div class="item" id="post-<?php the_ID(); ?>">
                            <div class="blog-post blog-grid style1">
                                <div class="blog-media">
                                    <?php the_post_thumbnail('archia-featured',array('class'=>'resp-img') ); ?>
                                </div>
                                <div class="dlab-post-text">
                                    <div class="dlab-post-title">
                                        <h4 class="post-title font-weight-600"><a href="<?php the_permalink(  ); ?>"><?php the_title(); ?></a></h4>
                                    </div>
                                    <?php the_excerpt(); ?>
                                    <div class="dlab-post-meta">
                                        <ul>
                                            <li class="post-date"><?php echo sprintf(__( '%1$s in %2$s', 'archia-add-ons' ), '<i class="fa fa-clock-o"></i>'.get_the_time(get_option('date_format')), get_the_category_list() ) ?></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
            <?php 
                    endif; // end posts layout check
                $count++; 
            endwhile; 
            if($posts_style == 'posts-style-slider') echo '</div>';
        endif; ?> 
    <?php wp_reset_postdata();?>
</div>