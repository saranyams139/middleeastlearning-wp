<?php
/* add_ons_php */
$css = $el_class = $logo = '';
$atts = vc_map_get_attributes( $this->getShortcode(), $atts );
extract( $atts );
$css_classes = array(
    'footer-logo align-self-center',
    $el_class,
    vc_shortcode_custom_css_class( $css ),
);
$css_class = preg_replace( '/\s+/', ' ', implode( ' ', array_filter( $css_classes ) ) );
?>
<div class="<?php echo esc_attr($css_class );?>">
    <?php
    	if (!empty($logo) && $logo != '') {
    		echo '<a class="custom-logo-link" href="'.esc_url( home_url('/' ) ).'">'.wp_get_attachment_image($logo, 'full', false, array('class'=>'resp-img') ).'</a>';
    	}else{
    		if(has_custom_logo()) the_custom_logo(); 
            else echo '<a class="custom-logo-link logo-text" href="'.esc_url( home_url('/' ) ).'"><h2>'.get_bloginfo( 'name' ).'</h2></a>'; 
    	}
    ?>
</div>