<?php
/* add_ons_php */

$css = $el_class = $cat_ids = $cat_order = $cat_order_by = $order = $order_by = $ids = $ids_not = $show_filter = $posts_per_page = $columns_grid = $spacing = $show_info = 
$show_excerpt = $show_view_project = $enable_gallery = $view_all_link = $show_overlay = $show_pagination = $show_loadmore = $hoz_style = $show_cat = $show_counter = $show_share = $filter_width = $sidebar_filter = $sidebar_title = $folio_item_style = $folio_content_width = $layout = '';

$items_width = '';

$loadmore_posts = '';

$atts = vc_map_get_attributes( $this->getShortcode(), $atts );
extract( $atts );
// $columns =  $columns_grid.'-cols';
$css_classes = array(
    'folios-slider-ele',
    'folios-slider-wrap',
    $el_class,
    vc_shortcode_custom_css_class( $css ),
);
$css_class = preg_replace( '/\s+/', ' ', implode( ' ', array_filter( $css_classes ) ) );
?>
<div class="<?php echo $css_class; ?>">
    <?php

    
    if(!empty($ids)){
        $ids = explode(",", $ids);
        $post_args = array(
            'post_type' => 'portfolio',
            // 'paged' => $paged,
            'posts_per_page'=> $posts_per_page,
            'post__in' => $ids,
            'orderby'=> $order_by,
            'order'=> $order,
        );
    }elseif(!empty($ids_not)){
        $ids_not = explode(",", $ids_not);
        $post_args = array(
            'post_type' => 'portfolio',
            // 'paged' => $paged,
            'posts_per_page'=> $posts_per_page,
            'post__not_in' => $ids_not,
            'orderby'=> $order_by,
            'order'=> $order,
        );
    }else{
        $post_args = array(
            'post_type' => 'portfolio',
            // 'paged' => $paged,
            'posts_per_page'=> $posts_per_page,
            'orderby'=> $order_by,
            'order'=> $order,
        );
    }
    
    

    if(!empty($cat_ids)){
        $post_args['tax_query'][] = array(
            'taxonomy' => 'portfolio_cat',
            'field' => 'term_id',
            'terms' => explode(",", $cat_ids),
            'operator' => 'IN',
        );
    }else if(!empty($cat_ids_not)){
        $post_args['tax_query'][] = array(
            'taxonomy'      => 'portfolio_cat',
            'field'         => 'term_id',
            'terms'         => explode(",", $cat_ids_not),
            'operator'      => 'NOT IN',
        );
    }

    $query_posts = new \WP_Query($post_args);
    if ($query_posts->have_posts()) :
    ?>
    <div class="portfolio-carousel full-vh mfp-gallery-with-owl owl-carousel owl-btn-center-lr">
        <?php

            while ($query_posts->have_posts()) : $query_posts->the_post();
                ?>
                <div class="item">
                    <div class="dlab-box portfolio-bx style2">
                        <div class="dlab-media dlab-img-overlay1 dlab-img-effect"> 
                            <a href="javascript:void(0);"><?php the_post_thumbnail( 'archia-folio-slider', array( 'class' => 'folio-slider-thumbnail' ) ); ?></a>
                            <div class="overlay-bx">
                                <a href="<?php echo wp_get_attachment_url( get_post_thumbnail_id() );?>" class="mfp-link" title="<?php the_title_attribute(); ?>"><i class="ti-zoom-in"></i></a>
                                <div class="overlay-icon align-b text-white"> 
                                    <div class="text-white text-left port-box">
                                        <h3 class="title"><a href="<?php the_permalink(  ); ?>"><?php the_title(); ?></a></h3>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <?php
            endwhile;
        ?>   
    </div>
    <?php endif; ?>
</div>
<?php wp_reset_postdata(); 
