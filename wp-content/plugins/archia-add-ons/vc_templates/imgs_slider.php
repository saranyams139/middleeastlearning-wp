<?php
/* add_ons_php */

$el_class = $css = $galimgs = $columns = $target = $thumbnail_size = '';
// $show_navigation = $show_dots = $mousewheel = $keyboard = $autoplay = $loop = $direction = $speed = $effect = $responsive = $spacing = '';

$atts = vc_map_get_attributes( $this->getShortcode(), $atts );
extract( $atts );

$css_classes = array(
    'imgs-slider-wrap',
    $el_class,
    vc_shortcode_custom_css_class( $css ),
);
$css_class = preg_replace( '/\s+/', ' ', implode( ' ', array_filter( $css_classes ) ) );
if(!empty($galimgs)){
    $images =  array();
    $images = explode(",", $galimgs);

    if( strpos($thumbnail_size, "x") !== false){
        $thumbnail_size = explode("x", $thumbnail_size);
    }
    ?>
    <div class="<?php echo esc_attr( $css_class ); ?>">
        <div class="exhibition-carousel owl-carousel owl-none m-b30">
            <?php foreach ($images as $key => $img) { ?>
                <div class="item">
                    <?php echo wp_get_attachment_image( $img, $thumbnail_size, false, array('class'=>'imgs-slider-thumbnail') );?>
                </div>
            <?php } ?> 
        </div>
    </div>
<?php } ?>