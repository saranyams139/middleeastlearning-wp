<?php
/**
 * Shortcode attributes
 * @var $atts
 * @var $el_class
 * @var $full_width
 * @var $full_height
 * @var $content_placement
 * @var $parallax
 * @var $parallax_image
 * @var $css
 * @var $el_id
 * @var $video_bg
 * @var $video_bg_url
 * @var $video_bg_parallax
 * @var $content - shortcode content
 *
 * @var $cth_layout 
 *
 * Shortcode class
 * @var $this WPBakeryShortCode_VC_Row 
 */
$el_class = $full_height = $parallax_speed_bg = $parallax_speed_video = $full_width = $equal_height = $flex_row = $columns_placement = $content_placement = $parallax = $parallax_image = $css = $el_id = $video_bg = $video_bg_url = $video_bg_parallax = '';
$disable_element = '';
$output = $after_output = '';

$cth_layout = $col_cen_pos = '';
$atts = vc_map_get_attributes( $this->getShortcode(), $atts );
extract( $atts );

if($cth_layout == 'home1_side_nav'){
    $css_classes = array(
        'archia-row ',
        'header-side-nav header-nav navbar-collapse collapse full-sidenav content-scroll',
        vc_shortcode_custom_css_class( $css ),
    );
    $css_class = preg_replace( '/\s+/', ' ', implode( ' ', array_filter( $css_classes ) ) );
    ?>
    <div <?php echo isset($el_id) && !empty($el_id) ? "id='" . esc_attr($el_id) . "'" : ""; ?> <?php
    echo !empty($css_class) ? "class='" . esc_attr( trim( $css_class ) ) . "'" : ""; ?>>
        <?php echo wpb_js_remove_wpautop( $content ); ?>
    </div>
    <div class="menu-close">
        <i class="ti-close"></i>
    </div>
    <?php
}elseif ($cth_layout == 'default_nav') { 
    $css_classes = array(
        'archia-row ',
        'sticky-header main-bar-wraper navbar-expand-lg',
        vc_shortcode_custom_css_class( $css ),
    );
    $css_class = preg_replace( '/\s+/', ' ', implode( ' ', array_filter( $css_classes ) ) );
    ?>
    <div <?php echo isset($el_id) && !empty($el_id) ? "id='" . esc_attr($el_id) . "'" : ""; ?> <?php
    echo !empty($css_class) ? "class='" . esc_attr( trim( $css_class ) ) . "'" : ""; ?>>
        <?php echo wpb_js_remove_wpautop( $content ); ?>
        <!-- nav toggle button -->
        <button class="navbar-bt-mobile navbar-toggler collapsed navicon justify-content-end" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
            <span></span>
            <span></span>
            <span></span>
        </button>
    </div>
    <div class="menu-close">
        <i class="ti-close"></i>
    </div>
<?php
}elseif ($cth_layout == 'footer_sec') { 
    if ($cth_pos_footer == 'top_footer') {
        $footer_class = 'bg footer-top';
    }else{
        $footer_class = 'footer-bottom';
    }

?>
    <div class="<?php echo esc_attr($footer_class);?>"  data-bg="<?php echo esc_url(wp_get_attachment_image_url( $cth_footer_bg, 'full',false ));?>">
            <div class="container">
                <div class="row">
                    <?php echo wpb_js_remove_wpautop( $content ); ?>       
                </div>
            </div>  
        </div>  
 <?php  
}elseif($cth_layout === 'page_sec'){
    $el_class = $this->getExtraClass( $el_class );
    $row_class = 'archia-row row';
    $css_classes = array(
        'archia-section',
        $el_class,
        vc_shortcode_custom_css_class( $css ),
        $cth_padding,
    );
    $wapcls = 'wrap_sec_classes';

    if ( 'yes' === $disable_element ) {
        if ( vc_is_page_editable() ) {
            $wapcls = 'vc_hidden-lg vc_hidden-xs vc_hidden-sm vc_hidden-md';
        } else {
            return '';
        }
    }
    
    $sec_id = '';
    if(!empty($el_id)){
        $sec_id = 'id="'.esc_attr($el_id ).'"';
    }
    if ( isset( $atts['gap'] ) ) {
        $css_classes[] = 'cth_column-gap-' . $atts['gap'];
    }
    $css_class = preg_replace( '/\s+/', ' ', implode( ' ', array_filter( $css_classes ) ) );
    $sec_class = 'class="' . esc_attr( trim( $css_class ) ) . '"';

    $output .= '<section '.$sec_id.' class="'.esc_attr($wapcls ).'">';
    $output .= '<div '.$sec_class.'>';
    if ( $cth_row_width === 'wide') {
        $output .= '<div class="container-fluid">';
    }elseif($cth_row_width === 'fix_width'){
        $output .= '<div class="container">';
    }else{
      $output .= '<div class="full-container archia-full-container">'; 
    }
    if($col_cen_pos == 'yes') $row_class .= ' align-items-center';
    $output .= '<div class="'.esc_attr($row_class ).'">';
    $output .= wpb_js_remove_wpautop( $content );
    $output .= '</div>';
    $output .= $after_output;
    $output .= $this->endBlockComment( $this->getShortcode() );
    $output .= '</div>';
    $output .= '</div>';
    $output .= '</section><div class="clearfix"></div>';

    echo $output;
}else{
    wp_enqueue_script( 'wpb_composer_front_js' );

    $el_class = $this->getExtraClass( $el_class );

    $css_classes = array(
        'vc_row',
        'wpb_row', //deprecated
        'vc_row-fluid',
        $el_class,
        vc_shortcode_custom_css_class( $css ),
    );

    if ( 'yes' === $disable_element ) {
        if ( vc_is_page_editable() ) {
            $css_classes[] = 'vc_hidden-lg vc_hidden-xs vc_hidden-sm vc_hidden-md';
        } else {
            return '';
        }
    }

    if (vc_shortcode_custom_css_has_property( $css, array('border', 'background') ) || $video_bg || $parallax) {
        $css_classes[]='vc_row-has-fill';
    }

    if (!empty($atts['gap'])) {
        $css_classes[] = 'vc_column-gap-'.$atts['gap'];
    }

    $wrapper_attributes = array();
    // build attributes for wrapper
    if ( ! empty( $el_id ) ) {
        $wrapper_attributes[] = 'id="' . esc_attr( $el_id ) . '"';
    }
    if ( ! empty( $full_width ) ) {
        $wrapper_attributes[] = 'data-vc-full-width="true"';
        $wrapper_attributes[] = 'data-vc-full-width-init="false"';
        if ( 'stretch_row_content' === $full_width ) {
            $wrapper_attributes[] = 'data-vc-stretch-content="true"';
        } elseif ( 'stretch_row_content_no_spaces' === $full_width ) {
            $wrapper_attributes[] = 'data-vc-stretch-content="true"';
            $css_classes[] = 'vc_row-no-padding';
        }
        $after_output .= '<div class="vc_row-full-width vc_clearfix"></div>';
    }

    if ( ! empty( $full_height ) ) {
        $css_classes[] = 'vc_row-o-full-height';
        if ( ! empty( $columns_placement ) ) {
            $flex_row = true;
            $css_classes[] = 'vc_row-o-columns-' . $columns_placement;
            if ( 'stretch' === $columns_placement ) {
                $css_classes[] = 'vc_row-o-equal-height';
            }
        }
    }

    if ( ! empty( $equal_height ) ) {
        $flex_row = true;
        $css_classes[] = 'vc_row-o-equal-height';
    }

    if ( ! empty( $content_placement ) ) {
        $flex_row = true;
        $css_classes[] = 'vc_row-o-content-' . $content_placement;
    }

    if ( ! empty( $flex_row ) ) {
        $css_classes[] = 'vc_row-flex';
    }

    $has_video_bg = ( ! empty( $video_bg ) && ! empty( $video_bg_url ) && vc_extract_youtube_id( $video_bg_url ) );

    $parallax_speed = $parallax_speed_bg;
    if ( $has_video_bg ) {
        $parallax = $video_bg_parallax;
        $parallax_speed = $parallax_speed_video;
        $parallax_image = $video_bg_url;
        $css_classes[] = 'vc_video-bg-container';
        wp_enqueue_script( 'vc_youtube_iframe_api_js' );
    }

    if ( ! empty( $parallax ) ) {
        wp_enqueue_script( 'vc_jquery_skrollr_js' );
        $wrapper_attributes[] = 'data-vc-parallax="' . esc_attr( $parallax_speed ) . '"'; // parallax speed
        $css_classes[] = 'vc_general vc_parallax vc_parallax-' . $parallax;
        if ( false !== strpos( $parallax, 'fade' ) ) {
            $css_classes[] = 'js-vc_parallax-o-fade';
            $wrapper_attributes[] = 'data-vc-parallax-o-fade="on"';
        } elseif ( false !== strpos( $parallax, 'fixed' ) ) {
            $css_classes[] = 'js-vc_parallax-o-fixed';
        }
    }

    if ( ! empty( $parallax_image ) ) {
        if ( $has_video_bg ) {
            $parallax_image_src = $parallax_image;
        } else {
            $parallax_image_id = preg_replace( '/[^\d]/', '', $parallax_image );
            $parallax_image_src = wp_get_attachment_image_src( $parallax_image_id, 'full' );
            if ( ! empty( $parallax_image_src[0] ) ) {
                $parallax_image_src = $parallax_image_src[0];
            }
        }
        $wrapper_attributes[] = 'data-vc-parallax-image="' . esc_attr( $parallax_image_src ) . '"';
    }
    if ( ! $parallax && $has_video_bg ) {
        $wrapper_attributes[] = 'data-vc-video-bg="' . esc_attr( $video_bg_url ) . '"';
    }
    $css_class = preg_replace( '/\s+/', ' ', apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, implode( ' ', array_filter( array_unique( $css_classes ) ) ), $this->settings['base'], $atts ) );
    $wrapper_attributes[] = 'class="' . esc_attr( trim( $css_class ) ) . '"';

    $output .= '<div ' . implode( ' ', $wrapper_attributes ) . '>';
    $output .= wpb_js_remove_wpautop( $content );
    $output .= '</div>';
    $output .= $after_output;

    echo $output;
}




