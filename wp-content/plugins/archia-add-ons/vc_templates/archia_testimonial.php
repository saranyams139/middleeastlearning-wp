 <?php
/* add_ons_php */

$el_class = $css = $posts_per_page = $order_by = $order = $ids = $show_avatar = $show_count = $show_title = $show_rating = $style = '';
// $show_navigation = $show_dots = $mousewheel = $keyboard = $autoplay = $loop = $direction = $speed = $effect = $responsive = $spacing = '';
$ani_type = $ani_time = $ani_delay = '';
$atts = vc_map_get_attributes( $this->getShortcode(), $atts );
extract( $atts );
$css_classes = array(
    'testimonials-slider',
    $el_class,
    vc_shortcode_custom_css_class( $css ),
);
$css_class = preg_replace( '/\s+/', ' ', implode( ' ', array_filter( $css_classes ) ) );

if(!empty($ids)){
    $ids = explode(",", $ids);
    $post_args = array(
        'post_type' => 'cth_testi',
        'posts_per_page'=> $posts_per_page,
        'post__in' => $ids,
        'orderby'=> $order_by,
        'order'=> $order,

        'post_status'       => 'publish',
    );
}elseif(!empty($ids_not)){
    $ids_not = explode(",", $ids_not);
    $post_args = array(
        'post_type' => 'cth_testi',
        'posts_per_page'=> $posts_per_page,
        'post__not_in' => $ids_not,
        'orderby'=> $order_by,
        'order'=> $order,

        'post_status'       => 'publish',
    );
}else{
    $post_args = array(
        'post_type' => 'cth_testi',
        'posts_per_page'=> $posts_per_page,
        'orderby'=> $order_by,
        'order'=> $order,

        'post_status'       => 'publish',
    );
}
$query_posts = new WP_Query($post_args); 

    // $dataArr = array();
    // $dataArr['smartSpeed'] = (int)$speed;
    // if($autoplay == 'yes') $dataArr['autoplay'] = true;
    // if($loop == 'yes') 
    //     $dataArr['loop'] = true;
    // else 
    //     $dataArr['loop'] = false;
    // if($show_navigation == 'yes') 
    //     $dataArr['nav'] = true;
    // else 
    //     $dataArr['nav'] = false;
    // if($show_dots == 'yes') 
    //     $dataArr['dots'] = true;
    // else 
    //     $dataArr['dots'] = false;
    // // $dataArr['effect'] = $effect;
    // if(!empty($responsive)){
    //     $css_class .=' resp-ena';
    //     $dataArr['responsive'] = $responsive;
    // }
    // if(is_numeric($spacing)) $dataArr['margin'] = (int)$spacing;
if ($style === '') {
    $wrap_open = '<div class="client-box">
                    <div class="testimonial-two-dots-bx owl-carousel owl-btn-center-lr owl-btn-3 owl-theme owl-dots-black-full btn-black owl-loaded owl-drag">';
    $wrap_closed = '</div>
                </div>'; 
    $class_client = 'client-says';
} elseif ($style === 'style-1') {
    $wrap_open = '<div class="client-box-full owl-carousel owl-btn-center-lr">';
    $wrap_closed = '</div>';
    $class_client = 'testimonial-1';
}  elseif ($style === 'style-2') {
    $wrap_open = '<div class="testimonial-two owl-carousel owl-none">';
    $wrap_closed = '</div>';
    $class_client = 'client-says';
}   

if($query_posts->have_posts()): 

    $ani_attrs = '';
    if($ani_type != ''){
        $css_class .= ' wow ' . $ani_type;
        if($ani_time != '')
            $ani_attrs .= ' data-wow-duration="'.esc_attr( $ani_time ).'"';
        if($ani_delay != '')
            $ani_attrs .= ' data-wow-delay="'.esc_attr( $ani_delay ).'"';
        // else
  //           $ani_attrs .= ' data-wow-delay="0.2s"';
    }   

    ?>
    
    <div class="<?php echo esc_attr( $css_class ); ?>"<?php echo $ani_attrs; ?>>

        <?php echo $wrap_open; ?>

                <?php        
                while($query_posts->have_posts()) : $query_posts->the_post();  
                ?>
                    <div class="item">
                        <div class="<?php echo $class_client; ?>">

                            <div class="testimonial-pic radius">
                                <?php the_post_thumbnail( 'thumbnail', array('class'=>'testi-thumb') ); ?>
                            </div>
                            <div class="testimonial-text">
                                <?php echo the_content(); ?>
                            </div>
                            <div class="testimonial-detail"> 
                                <strong class="testimonial-name"><?php echo the_title(); ?></strong> 
                                <span class="testimonial-position"><?php echo get_post_meta(get_the_ID(),'_cth_position',true ) ?></span> 
                            </div>

                        </div>
                    </div>
                <?php endwhile; ?>
                
        <?php echo $wrap_closed; ?>

    </div>

<?php endif;
/* Restore original Post Data */
// wp_reset_postdata();
