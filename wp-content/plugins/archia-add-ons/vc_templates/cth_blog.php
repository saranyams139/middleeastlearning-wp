<?php
/* add_ons_php */

$css = $el_class = $cat_ids = $cat_ids_not = $ids = $ids_not = $posts_per_page = $order = $order_by = '';
$layout = $cols = $pad = '';
$atts = vc_map_get_attributes( $this->getShortcode(), $atts );
extract( $atts );
$css_classes = array(
    'cth-blog-wrap',
    'blog-posts-'.$layout,
    $el_class,
    vc_shortcode_custom_css_class( $css ),
);
$css_class = preg_replace( '/\s+/', ' ', implode( ' ', array_filter( $css_classes ) ) );
// $href = vc_build_link( $href_bt);
if(is_front_page()) {
    $paged = (get_query_var('page')) ? get_query_var('page') : 1;
} else {
    $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
}
if(!empty($ids)){
    $ids = explode(",", $ids);
    $post_args = array(
        'post_type' => 'post',
        'paged' => $paged,
        'posts_per_page'=> $posts_per_page,
        'post__in' => $ids,
        'orderby'=> $order_by,
        'order'=> $order,

        'post_status'       => 'publish',
    );
}elseif(!empty($ids_not)){
    $ids_not = explode(",", $ids_not);
    $post_args = array(
        'post_type' => 'post',
        'paged' => $paged,
        'posts_per_page'=> $posts_per_page,
        'post__not_in' => $ids_not,
        'orderby'=> $order_by,
        'order'=> $order,
        'post_status'       => 'publish',
    );
}else{
    $post_args = array(
        'post_type' => 'post',
        'paged' => $paged,
        'posts_per_page'=> $posts_per_page,
        'orderby'=> $order_by,
        'order'=> $order,
        'post_status'       => 'publish',
    );
}

if(!empty($cat_ids)){
    $post_args['category__in'] = explode(",", $cat_ids);
    // $post_args['tax_query'][] = array(
    //     'taxonomy'      => 'category',
    //     'field'         => 'term_id',
    //     'terms'         => explode(",", $cat_ids),
    //     // 'operator'      => 'NOT IN',
    // );
}else if(!empty($cat_ids_not)){
    $post_args['category__not_in'] = explode(",", $cat_ids_not);
    // $post_args['tax_query'][] = array(
    //     'taxonomy'      => 'category',
    //     'field'         => 'term_id',
    //     'terms'         => explode(",", $cat_ids_not),
    //     'operator'      => 'NOT IN',
    // );
}
$posts_query = new \WP_Query($post_args);
// echo '<pre>';
// var_dump($posts_query);
    if($posts_query->have_posts()) : 

    $grid_cls = 'blog-posts-wrapper';
    if($layout == 'masonary'){
        $grid_cls .= ' blog-posts-masonry folio-items folio-isotope';
        $grid_cls .= ' folio-'.$cols.'-cols';
        $grid_cls .= ' folio-'.$pad.'-pad';
    }elseif($layout == 'grid'){
        $grid_cls .= ' blog-posts-grid folio-items folio-flex';
        $grid_cls .= ' folio-'.$cols.'-cols';
        $grid_cls .= ' folio-'.$pad.'-pad';
    }
?>
<div class="<?php echo esc_attr( $css_class ); ?>">
    <div class="<?php echo esc_attr( $grid_cls ); ?>">
        <?php 
        if($layout == 'masonary') echo '<div class="grid-sizer"></div>';
        $count = 0;
        while($posts_query->have_posts()) : $posts_query->the_post(); 

            if($layout == 'listing'){
                // set_query_var('count',$count);
                archia_get_template_part('template-parts/post/content','listing', array('loop_count'=>$count));
                // get_template_part('template-parts/post/content','listing');
            }else if($layout == 'masonary'){

                get_template_part('template-parts/post/content','masonry');

            }else if(archia_get_option('blog_show_format', true )){
                get_template_part( 'template-parts/post/content', ( post_type_supports( get_post_type(), 'post-formats' ) ? get_post_format() : get_post_type() ) );
            }else{
                get_template_part( 'template-parts/post/content' );
            }

            $count++;
        endwhile; ?>
        
    </div>
    <?php archia_custom_pagination($posts_query->max_num_pages, 2, $posts_query);  ?>

</div>
<?php endif; 
    wp_reset_postdata();
