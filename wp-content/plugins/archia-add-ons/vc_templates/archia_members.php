 <?php
/* add_ons_php */


$el_class = $css = $posts_per_page = $order_by = $order = $ids = $show_avatar = $show_count = $show_title = $show_rating = $show_excerpt = $link_single = $mems_style = '';
// $show_navigation = $show_dots = $mousewheel = $keyboard = $autoplay = $loop = $direction = $speed = $effect = $responsive = $spacing = '';
$ani_type = $ani_time = $ani_delay = '';

$atts = vc_map_get_attributes( $this->getShortcode(), $atts );
extract( $atts );

$css_classes = array(
    'chefs-slider-holder',
    $el_class,
    $mems_style,
    vc_shortcode_custom_css_class( $css ),
);
$css_class = preg_replace( '/\s+/', ' ', implode( ' ', array_filter( $css_classes ) ) );

if(is_front_page()) {
    $paged = (get_query_var('page')) ? get_query_var('page') : 1;
} else {
    $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
}
if(!empty($ids)){
    $ids = explode(",", $ids);
    $post_args = array(
        'post_type' => 'cth_member',
        'paged' => $paged,
        'posts_per_page'=> $posts_per_page,
        'post__in' => $ids,
        'orderby'=> $order_by,
        'order'=> $order,

        'post_status'       => 'publish',
    );
}elseif(!empty($ids_not)){
    $ids_not = explode(",", $ids_not);
    $post_args = array(
        'post_type' => 'cth_member',
        'paged' => $paged,
        'posts_per_page'=> $posts_per_page,
        'post__not_in' => $ids_not,
        'orderby'=> $order_by,
        'order'=> $order,

        'post_status'       => 'publish',
    );
}else{
    $post_args = array(
        'post_type' => 'cth_member',
        'paged' => $paged,
        'posts_per_page'=> $posts_per_page,
        'orderby'=> $order_by,
        'order'=> $order,

        'post_status'       => 'publish',
    );
}

$query_posts = new WP_Query($post_args); 

    

if($query_posts->have_posts()): 
    $ani_attrs = '';
    if($ani_type != ''){
        $css_class .= ' wow ' . $ani_type;
        if($ani_time != '')
            $ani_attrs .= ' data-wow-duration="'.esc_attr( $ani_time ).'"';
        if($ani_delay != '')
            $ani_attrs .= ' data-wow-delay="'.esc_attr( $ani_delay ).'"';
    }   

    $owl_cls = 'team-carousel owl-carousel owl-btn-center-lr';
    if($mems_style == 'mems-slider-style2') $owl_cls = 'team-slider owl-carousel owl-theme owl-none';
?>
<div class="<?php echo esc_attr( $css_class ); ?>"<?php echo $ani_attrs; ?>>
    <div class="<?php echo esc_attr( $owl_cls ); ?>">
        <?php        
        while($query_posts->have_posts()) : $query_posts->the_post();  
            $team_pic = get_post_meta( get_the_ID(), BBT_META_PREFIX.'pic', true );
                $id_team_pic = array_keys($team_pic);
            $team_sign = get_post_meta( get_the_ID(), BBT_META_PREFIX.'sign', true );
                $id_team_sign = array_keys($team_sign);

            $facebook = get_post_meta( get_the_ID(), BBT_META_PREFIX.'facebookurl', true );
            $twitter = get_post_meta( get_the_ID(), BBT_META_PREFIX.'twitterurl', true );
            $googleplus = get_post_meta( get_the_ID(), BBT_META_PREFIX.'googleplusurl', true );
            $instagram = get_post_meta( get_the_ID(), BBT_META_PREFIX.'instagramurl', true );
        ?>
            <div class="item">
                <?php 
                if($mems_style == 'mems-slider-style2'): ?>

                <div class="our-team team-style1">
                    <div class="dlab-media radius-sm">
                        <?php the_post_thumbnail( 'archia-member', array('class'=>'archia-member') );?>
                    </div>
                    <div class="team-title-bx text-center">
                        <h2 class="team-title"><?php the_title(); ?></h2>
                        <span class="clearfix"><?php echo get_post_meta( get_the_ID(), BBT_META_PREFIX.'field', true ); ?></span>
                        <?php if( $facebook != '' || $twitter != '' || $googleplus != '' || $instagram != '' ): ?>
                        <ul class="social-list">
                            <?php if(!empty($facebook)): ?>
                            <li><a href="<?php echo $facebook; ?>" class="btn-link white fa fa-facebook"></a></li>
                            <?php endif; ?>
                            <?php if(!empty($twitter)): ?>
                            <li><a href="<?php echo $twitter; ?>" class="btn-link white fa fa-twitter"></a></li>
                            <?php endif; ?>
                            <?php if(!empty($googleplus)): ?>
                            <li><a href="<?php echo $googleplus; ?>" class="btn-link white fa fa-google"></a></li>
                            <?php endif; ?>
                            <?php if(!empty($instagram)): ?>
                            <li><a href="<?php echo $instagram; ?>" class="btn-link white fa fa-instagram"></a></li>
                            <?php endif; ?>
                        </ul>
                        <?php endif; ?>
                    </div>
                </div>
                <?php else: ?>
                <div class="team-member">
                    <div class="team-media">
                        <?php the_post_thumbnail( 'archia-member', array('class'=>'archia-member') );?>
                    </div>
                    <div class="team-info">
                        <h5 class="team-job-title"><?php echo get_post_meta( get_the_ID(), BBT_META_PREFIX.'level_member', true ); ?></h5>
                        <h2 class="team-title"><?php the_title(); ?></h2>
                        <?php the_excerpt(); ?>
                        <div class="team-perinfo">
                            <div class="team-sign">

                                <?php echo wp_get_attachment_image( $id_team_sign[0], 'archia-sign', false, array('class'=>'img-responsive feature-img')) ?>
                            </div>
                            <div class="team-profile">
                                <div class="team-pic">

                                    <?php echo wp_get_attachment_image( $id_team_pic[0], 'archia-avatar', false, array('class'=>'img-responsive feature-img')) ?>

                                </div>
                                <div class="name">
                                    <h3><?php echo get_post_meta( get_the_ID(), BBT_META_PREFIX.'name', true ); ?></h3>
                                    <span><?php echo get_post_meta( get_the_ID(), BBT_META_PREFIX.'field', true ); ?></span>
                                    <?php if(!empty($facebook) || $twitter != '' || $googleplus != '' || $instagram != '' ): ?>
                                    <ul>
                                        <?php if(!empty($facebook)): ?>
                                        <li><a href="<?php echo $facebook; ?>"><i class="fa fa-facebook-f"></i></a></li>
                                        <?php endif; ?>
                                        <?php if(!empty($twitter)): ?>
                                        <li><a href="<?php echo $twitter; ?>"><i class="fa fa-twitter"></i></a></li>
                                        <?php endif; ?>
                                        <?php if(!empty($googleplus)): ?>
                                        <li><a href="<?php echo $googleplus; ?>"><i class="fa fa-google-plus"></i></a></li>
                                        <?php endif; ?>
                                        <?php if(!empty($instagram)): ?>
                                        <li><a href="<?php echo $instagram; ?>"><i class="fa fa-instagram"></i></a></li>
                                        <?php endif; ?>
                                    </ul>
                                    <?php endif; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <?php endif; ?>
            </div>

        <?php 
        endwhile; ?>
    </div>
</div>
<?php
endif;
/* Restore original Post Data */
wp_reset_postdata();

