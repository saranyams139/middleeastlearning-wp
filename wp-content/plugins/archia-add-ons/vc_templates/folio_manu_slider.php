<?php
/* add_ons_php */

$el_class = $css = $galimgs = $columns = $target = $thumbnail_size = $slides = $slider_style = $slider_type = '';
// $show_navigation = $show_dots = $mousewheel = $keyboard = $autoplay = $loop = $direction = $speed = $effect = $responsive = $spacing = '';
$ani_type = $ani_time = $ani_delay = '';
$atts = vc_map_get_attributes( $this->getShortcode(), $atts );
extract( $atts );

$css_classes = array(
    'folio-manu-slider-wrap',
    $el_class,
    vc_shortcode_custom_css_class( $css ),
);
$css_class = preg_replace( '/\s+/', ' ', implode( ' ', array_filter( $css_classes ) ) );

$slides = (array) vc_param_group_parse_atts( $slides );

if(!empty($slides)){
    // $images =  array();
    // $images = explode(",", $galimgs);

    if( strpos($thumbnail_size, "x") !== false){
        $thumbnail_size = explode("x", $thumbnail_size);
    }

    $ani_attrs = '';
    if($ani_type != ''){
        $css_class .= ' wow ' . $ani_type;
        if($ani_time != '')
            $ani_attrs .= ' data-wow-duration="'.esc_attr( $ani_time ).'"';
        if($ani_delay != '')
            $ani_attrs .= ' data-wow-delay="'.esc_attr( $ani_delay ).'"';
        // else
  //           $ani_attrs .= ' data-wow-delay="0.2s"';
    }   
    ?>
    <div class="<?php echo esc_attr( $css_class ); ?>"<?php echo $ani_attrs; ?>>
        <?php if($slider_type == 'sld-type-normal'): ?>
        <div class="project-area-info owl-carousel owl-btn-center-lr">
        <?php else: ?>
        <div class="project-carousel-1 <?php echo esc_attr( $slider_style ); ?> owl-carousel">
        <?php endif; ?>
            <?php foreach ( $slides as $data ) { 
                // var_dump($data);

                $href = vc_build_link( $data['link'] );
                // var_dump($href);
                $url = (!empty($href['url'])) ? $href['url'] : '';
                $target = (!empty($href['target'])) ? $href['target'] : '';
                $rel = (!empty($href['rel'])) ? $href['rel'] : '';
                $title = ( !empty($href['title'])) ? $href['title'] : __( 'View Portfolio', 'archia-add-ons' );


                // array(4) { ["url"]=> string(39) "http://localhost:8888/archia/contact-1/" ["title"]=> string(9) "Contact 1" ["target"]=> string(7) " _blank" ["rel"]=> string(8) "nofollow" }

                $link_attrs = '';
                if($target != '') $link_attrs .= ' target="'.$target.'"';
                if($rel != '') $link_attrs .= ' rel="'.$rel.'"';

                $details = (array) vc_param_group_parse_atts( $data['details'] );

                ?> 
                <?php if($slider_type == 'sld-type-normal'): ?>
                <div class="item project-area">
                    <div class="row">
                        <div class="col-xl-7 col-lg-6 col-md-6">
                            <?php echo wp_get_attachment_image( $data['img'], $thumbnail_size, false, array('class'=>'folio-manu-slider') ); ?>
                        </div>
                        <div class="col-xl-5 col-lg-6 col-md-6 project-content align-self-center">
                            <?php if(isset($data['sltitle'])): ?><h2 class="head-title m-b10"><?php echo $data['sltitle']; ?></h2><?php endif; ?>
                            <?php if(isset($data['excerpt'])) echo  $data['excerpt']; ?>
                            <ul class="list-details">
                                <?php 
                                foreach ($details as $idx => $dtl) {
                                    if(isset($dtl['title']) && isset($dtl['value']) ){
                                    ?>
                                    <li class="folio-slide-detls-<?php echo $idx + 1 ; ?>">
                                        <strong><?php echo $dtl['title']; ?></strong>
                                        <?php echo $dtl['value'] ?>
                                    </li>
                                <?php
                                    }
                                } ?>
                                <?php if($url != ''): ?>
                                <li>
                                    <a href="<?php echo esc_url( $url ); ?>" <?php echo $link_attrs; ?> class="btn outline outline-2 button-lg black m-t10 radius-xl btn-aware"><?php echo $title; ?><span></span></a>
                                </li>
                                <?php endif; ?>
                            </ul>
                        </div>
                    </div>
                </div>
                <?php else: ?>
                <div class="item">
                     <div class="project-info-box">
                        <?php if(isset($data['img'])): ?>
                        <div class="project-media">
                            <?php echo wp_get_attachment_image( $data['img'], $thumbnail_size, false, array('class'=>'folio-manu-slider') ); ?>
                        </div>
                        <?php endif; ?>
                        <div class="project-content">
                            <ul class="list-details">
                                <?php 
                                foreach ($details as $idx => $dtl) {
                                    if(isset($dtl['title']) && isset($dtl['value']) ){
                                    ?>
                                    <li class="folio-slide-detls-<?php echo $idx + 1 ; ?>">
                                        <strong><?php echo $dtl['title']; ?></strong>
                                        <?php echo $dtl['value'] ?>
                                    </li>
                                <?php
                                    }
                                } ?>
                                <?php if($url != ''): ?>
                                <li>
                                    <a href="<?php echo esc_url( $url ); ?>" <?php echo $link_attrs; ?> class="btn outline outline-2 button-lg black m-t10 radius-xl btn-aware"><?php echo $title; ?><span></span></a>
                                </li>
                                <?php endif; ?>
                            </ul>
                        </div>
                     </div>
                </div>
                <?php endif; ?>
            <?php } ?> 
        </div>
    </div>
<?php } ?>