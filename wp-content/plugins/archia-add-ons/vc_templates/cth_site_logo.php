<?php
/* add_ons_php */
$css = $el_class = $custom_logo = $fixed_logo = '';
$atts = vc_map_get_attributes( $this->getShortcode(), $atts );
extract( $atts );
$css_classes = array(
    'logo-header',
    $el_class,
    vc_shortcode_custom_css_class( $css ),
);
$css_class = preg_replace( '/\s+/', ' ', implode( ' ', array_filter( $css_classes ) ) );
?>
<div class="<?php echo esc_attr($css_class );?>">
    <?php   
    if($fixed_logo !== ''){
      echo '<a class="custom-logo-link custom-logo-fixed logo-image" href="'.esc_url( home_url('/' ) ).'">'.wp_get_attachment_image( $fixed_logo, 'archia-logo', false, array('class'=> 'custom-logo-image custom-logo-fixed-image') ).'</a>'; 
    }
   	if($custom_logo !== ''){
   		echo '<a class="custom-logo-link logo-image" href="'.esc_url( home_url('/' ) ).'">'.wp_get_attachment_image( $custom_logo, 'archia-logo', false, array('class'=> 'custom-logo-image') ).'</a>'; 
   	}elseif(has_custom_logo()) 
    	the_custom_logo(); 
    else 
    	echo '<a class="custom-logo-link logo-text" href="'.esc_url( home_url('/' ) ).'"><h2>'.get_bloginfo( 'name' ).'</h2></a>'; 
    ?>
</div>
