<?php
/* add_ons_php */
$css = $el_class = $icon_type = $title = $icon_pos = $box_style = $count_num = '';
$icon_fontawesome = $icon_themify = '';
$defaultIconClass = '';// 'fa fa-phone';
$atts = vc_map_get_attributes( $this->getShortcode(), $atts );
extract( $atts );
$css_classes = array(
    'cth-counter-wrap',
    'archia-counter',
    $el_class,
    // $box_style,
    $icon_pos,
    vc_shortcode_custom_css_class( $css ),
);
$css_class = preg_replace( '/\s+/', ' ', implode( ' ', array_filter( $css_classes ) ) );
// $href = vc_build_link( $link_video );
// $url = ($href['url']!='') ? $href['url'] : '#';

// if($box_style == 'icbox-simple') $css_class .= ' ' .$icon_pos;

$iconClass = isset( ${'icon_' . $icon_type} ) ? ${'icon_' . $icon_type} : $defaultIconClass;

?>
<div class="<?php echo esc_attr( $css_class );?>">
	<?php if($iconClass != ''): ?>
	<div class="icon-lg pull-left m-tb10"><i class="<?php echo $iconClass; ?>"></i></div>
	<?php endif; ?>
	<div class="clearfix m-l90">
		<?php if($count_num != ''): ?>
		<div class="counter m-b5"><?php echo $count_num; ?></div>
		<?php endif; ?>
		<?php if($title != ''): ?>
		<span class="font-16 text-black"><?php echo $title; ?></span>
		<?php endif; ?>
		<?php echo wpb_js_remove_wpautop( $content, true ); ?>
	</div>
</div>