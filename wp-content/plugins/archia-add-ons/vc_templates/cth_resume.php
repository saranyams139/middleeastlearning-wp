<?php
/* add_ons_php */
$css = $el_class = $image = $title = $year = $image_pos = '';
$ani_type = $ani_time = $ani_delay = '';
$atts = vc_map_get_attributes( $this->getShortcode(), $atts );
extract( $atts );
$css_classes = array(
	'company-history-wrap',
    $image_pos,
    $el_class,
    vc_shortcode_custom_css_class( $css ),
);
$css_class = preg_replace( '/\s+/', ' ', implode( ' ', array_filter( $css_classes ) ) );
$ani_attrs = '';
if($ani_type != ''){
	$css_class .= ' wow ' . $ani_type;
	if($ani_time != '')
		$ani_attrs .= ' data-wow-duration="'.esc_attr( $ani_time ).'"';
	if($ani_delay != '')
		$ani_attrs .= ' data-wow-delay="'.esc_attr( $ani_delay ).'"';
	// else
 //        $ani_attrs .= ' data-wow-delay="0.2s"';
}	
?>
<div class="<?php echo esc_attr($css_class);?>" <?php echo $ani_attrs; ?>>
	<div class="history-box">
		<div class="history-details">
			<div class="history-head">
				<?php if($year != ''): ?><h2 class="history-year"><?php echo $year; ?></h2><?php endif; ?>
				<?php if($title != ''): ?><h3 class="history-title"><?php echo $title; ?></h3><?php endif; ?>
			</div>
			<?php echo wpb_js_remove_wpautop($content, true);?>
		</div>
		<div class="history-media">
			<?php echo wp_get_attachment_image( $image, 'archia-history', false, array('class'=>'archia-history-thumb') ); ?>
		</div>
	</div>
</div>