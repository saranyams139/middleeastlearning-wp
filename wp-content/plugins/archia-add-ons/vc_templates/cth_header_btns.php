<?php
/* add_ons_php */
$css = $el_class = $show_search = '';
$atts = vc_map_get_attributes( $this->getShortcode(), $atts );
extract( $atts );
$css_classes = array(
    'cth-header-btns',
    $el_class,
    vc_shortcode_custom_css_class( $css ),
);
if ($show_search !== 'yes') {
	$css_classes[] = 'hide-search-btn';
}
$css_class = preg_replace( '/\s+/', ' ', implode( ' ', array_filter( $css_classes ) ) );
?>
<div class="<?php echo esc_attr($css_class );?>" >
	<button class="navbar-toggler collapsed navicon justify-content-end" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
		<span></span>
		<span></span>
		<span></span>
	</button>
	<div class="extra-nav">
		<div class="extra-cell">
			<ul>
				<?php if($content != ''): ?>
				<li class="infor-btn-li">
					<?php echo rawurldecode( base64_decode( wp_strip_all_tags( $content ) ) ) ?>
				</li>
				<?php endif; ?>
				<?php if($show_search == 'yes'): ?>
				<li class="search-btn-li"><a id="quik-search-btn" href="javascript:void(0);" class="search-btn"><i class="fa fa-search"></i></a></li>
				<?php endif; ?>
			</ul>
		</div>
	</div>
</div>