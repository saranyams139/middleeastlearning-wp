<?php
/* add_ons_php */

$el_class = $css = $galimgs = $columns = $target = $thumbnail_size = $slider_style = $no_overlay = '';
// $show_navigation = $show_dots = $mousewheel = $keyboard = $autoplay = $loop = $direction = $speed = $effect = $responsive = $spacing = '';
$ani_type = $ani_time = $ani_delay = '';
$atts = vc_map_get_attributes( $this->getShortcode(), $atts );
extract( $atts );

$css_classes = array(
    'cth-slider-wrap',
    $el_class,
    $slider_style,
    vc_shortcode_custom_css_class( $css ),
);
$css_class = preg_replace( '/\s+/', ' ', implode( ' ', array_filter( $css_classes ) ) );
if(!empty($galimgs)){
    $images =  array();
    $images = explode(",", $galimgs);

    if( strpos($thumbnail_size, "x") !== false){
        $thumbnail_size = explode("x", $thumbnail_size);
    }

    // $dataArr = array();
    // $dataArr['smartSpeed'] = (int)$speed;
    // if($autoplay == 'yes') $dataArr['autoplay'] = true;
    // if($loop == 'yes') 
    //     $dataArr['loop'] = true;
    // else 
    //     $dataArr['loop'] = false;
    // if($show_navigation == 'yes') 
    //     $dataArr['nav'] = true;
    // else 
    //     $dataArr['nav'] = false;
    // if($show_dots == 'yes') 
    //     $dataArr['dots'] = true;
    // else 
    //     $dataArr['dots'] = false;

    // if(!empty($responsive)){
    //     $css_class .=' resp-ena';
    //     $dataArr['responsive'] = $responsive;
    // }
    // if(is_numeric($spacing)) $dataArr['margin'] = (int)$spacing;
    $ani_attrs = '';
    if($ani_type != ''){
        $css_class .= ' wow ' . $ani_type;
        if($ani_time != '')
            $ani_attrs .= ' data-wow-duration="'.esc_attr( $ani_time ).'"';
        if($ani_delay != '')
            $ani_attrs .= ' data-wow-delay="'.esc_attr( $ani_delay ).'"';
        // else
  //           $ani_attrs .= ' data-wow-delay="0.2s"';
    }  

    $sld_cls = 'project-carousel owl-btn-center-lr owl-carousel mfp-gallery'; 
    if($slider_style == 'cthslider-freemode') $sld_cls = 'project-carousel-2 owl-carousel owl-btn-center-lr owl-btn-1 black'; 
    ?>
    <div class="<?php echo esc_attr( $css_class ); ?>"<?php echo $ani_attrs; ?>>
        <div class="<?php echo esc_attr( $sld_cls ); ?>">
            <?php foreach ($images as $key => $img) { ?>
                <div class="item">
                    <?php 
                    if($slider_style == 'cthslider-freemode'):
                    ?>
                    <div class="dlab-media<?php if($no_overlay != 'yes') echo ' dlab-img-overlay1';?>">
                        <?php echo wp_get_attachment_image( $img, $thumbnail_size, false, array('class'=>'cth-slider-thumbnail') );?>
                    </div>
                    <?php else: ?>            
                    <div class="dlab-box portfolio-bx style2 project-media">
                        <div class="dlab-media <?php if($no_overlay != 'yes') echo ' dlab-img-overlay1';?> dlab-img-effect"> 
                            <a href="javascript:void(0);"><?php echo wp_get_attachment_image( $img, $thumbnail_size, false, array('class'=>'cth-slider-thumbnail') );?></a>
                            <div class="overlay-bx">
                                <a href="<?php echo wp_get_attachment_url( $img ); ?>" class="mfp-link" title="<?php echo esc_attr( get_the_title( $img ) ); ?>"><i class="ti-zoom-in"></i></a>
                            </div>
                        </div>
                    </div>
                    <?php endif; ?>
                </div>
            <?php } ?> 
        </div>
    </div>
<?php } ?>