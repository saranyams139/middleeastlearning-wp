<?php
/* add_ons_php */
$css = $el_class = '';
$atts = vc_map_get_attributes( $this->getShortcode(), $atts );
extract( $atts );
$css_classes = array(
    $el_class,
    vc_shortcode_custom_css_class( $css ),
);
$css_class = preg_replace( '/\s+/', ' ', implode( ' ', array_filter( $css_classes ) ) );
$href = vc_build_link( $links );
$url = ($href['url'] != '') ? $href['url'] : '#';
?>
<li class="<?php echo esc_attr($css_class);?>"><a href="<?php echo $url; ?>"><i class="<?php echo $icon; ?>"></i></a></li>
