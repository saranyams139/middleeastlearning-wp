<?php
/* add_ons_php */

$el_class = $css = $bars = '';
// $show_navigation = $show_dots = $mousewheel = $keyboard = $autoplay = $loop = $direction = $speed = $effect = $responsive = $spacing = '';

$atts = vc_map_get_attributes( $this->getShortcode(), $atts );
extract( $atts );

$css_classes = array(
    'progress-section-wrap',
    $el_class,
    vc_shortcode_custom_css_class( $css ),
);
$css_class = preg_replace( '/\s+/', ' ', implode( ' ', array_filter( $css_classes ) ) );

$bars = (array) vc_param_group_parse_atts( $bars );

if(!empty($bars)){
?>
<div class="<?php echo esc_attr( $css_class ); ?>">
    <div class="progress-section">
        <?php 
        foreach ( $bars as $data ) { 
            $value = isset($data['value']) ? floatval($data['value']) : 50;
        ?> 
            <div class="progress-bx">
                <?php if( isset($data['label']) && $data['label'] != '' ): ?><h6 class="title"><?php echo $data['label']; ?></h6><?php endif; ?>
                <div class="count-box"><?php echo $data['value']; ?></div>
                <div class="progress">
                    <div class="progress-bar <?php echo $data['color']; ?>" style="width:<?php echo $value; ?>%" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>
                </div>
            </div>
        <?php 
        } ?> 
    </div>
</div>
<?php } ?>