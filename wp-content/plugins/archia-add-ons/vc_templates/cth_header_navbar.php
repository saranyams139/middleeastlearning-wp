<?php
/* add_ons_php */
$css = $el_class = $custom_logo = '';
$atts = vc_map_get_attributes( $this->getShortcode(), $atts );
extract( $atts );
$css_classes = array(
    'nav-holder header-nav navbar-collapse collapse justify-content-end mo-nav',
    $el_class,
    vc_shortcode_custom_css_class( $css ),
);
$css_class = preg_replace( '/\s+/', ' ', implode( ' ', array_filter( $css_classes ) ) );

?>
<div class="<?php echo esc_attr($css_class );?>" id="navbarNavDropdown">

	<div class="logo-header header-four-logo">
	    <?php   
	   	if($custom_logo !== ''){
	   		echo '<a class="custom-logo-link logo-image" href="'.esc_url( home_url('/' ) ).'">'.wp_get_attachment_image( $custom_logo, 'archia-logo', false, array('class'=> 'custom-logo-image') ).'</a>'; 
	   	}elseif(has_custom_logo()) 
	    	the_custom_logo(); 
	    else 
	    	echo '<a class="custom-logo-link logo-text" href="'.esc_url( home_url('/' ) ).'"><h2>'.get_bloginfo( 'name' ).'</h2></a>'; 
	    ?>
	</div>
	<?php if ( has_nav_menu( 'top' ) ) : ?>
        <?php get_template_part( 'template-parts/navigation/navigation', 'top' ); ?>
    <?php endif; ?>
</div>