<?php
/* add_ons_php */
$css = $el_class = $icon_type = $title = $icon_pos = $box_style = '';
$icon_fontawesome = $icon_themify = '';
$defaultIconClass = '';// 'fa fa-phone';
$atts = vc_map_get_attributes( $this->getShortcode(), $atts );
extract( $atts );
$css_classes = array(
    'icon-bx-wraper m-b30',
    $el_class,
    $box_style,
    // $icon_pos,
    vc_shortcode_custom_css_class( $css ),
);
$css_class = preg_replace( '/\s+/', ' ', implode( ' ', array_filter( $css_classes ) ) );
// $href = vc_build_link( $link_video );
// $url = ($href['url']!='') ? $href['url'] : '#';

if($box_style == 'icbox-simple') $css_class .= ' ' .$icon_pos;

$iconClass = isset( ${'icon_' . $icon_type} ) ? ${'icon_' . $icon_type} : $defaultIconClass;

?>
<div class="<?php echo esc_attr( $css_class );?>">
	<?php if($box_style == 'about-bx'): ?>
		<?php if($iconClass != ''): ?>
		<div class="icon-bx-sm radius bg-primary"> 
			<a href="javascript:void(0)" class="icon-cell">
				<i class="<?php echo $iconClass; ?>"></i>
			</a>
		</div>
		<?php endif; ?>
		<div class="icon-content">
			<?php if($title != ''): ?>
			<h5 class="dlab-tilte"><?php echo $title; ?></h5>
			<?php endif; ?>
			<?php echo wpb_js_remove_wpautop( $content, true ); ?>
		</div>
	<?php else: ?>
		<?php if($iconClass != ''): ?>
		<div class="icon-md m-b20 m-t5">
			<a href="javascript:void(0)" class="icon-cell text-primary">
				<i class="<?php echo $iconClass; ?>"></i>
			</a>
		</div>
		<?php endif; ?>
		<div class="icon-content">
			<?php if($title != ''): ?>
			<h4 class="dlab-tilte m-b5"><?php echo $title; ?></h4>
			<?php endif; ?>
			<?php echo wpb_js_remove_wpautop( $content, true ); ?>
		</div>
	<?php endif; ?>
</div>