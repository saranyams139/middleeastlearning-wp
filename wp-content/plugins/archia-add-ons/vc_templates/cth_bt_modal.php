<?php
/* add_ons_php */
$css = $el_class = '';
$atts = vc_map_get_attributes( $this->getShortcode(), $atts );
extract( $atts );
$css_classes = array(
    'extra-nav',
    $el_class,
    vc_shortcode_custom_css_class( $css ),
);
$css_class = preg_replace( '/\s+/', ' ', implode( ' ', array_filter( $css_classes ) ) );
?>
<div class="<?php echo esc_attr($css_class );?>">
   <div class="extra-cell">
		<ul>
			<li class="button-modal search-btn">
				<a href="javascript:void(0);" class="btn radius-xl white menu-icon">
					<div class="menu-icon-in">
						<span></span>
						<span></span>
						<span></span>
						<div class="menu-text">menu</div>
					</div>
				</a>
			</li>
		</ul>
	</div>
</div>
