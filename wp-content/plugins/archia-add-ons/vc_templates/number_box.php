<?php
/* add_ons_php */
$css = $el_class = $num = '';
$atts = vc_map_get_attributes( $this->getShortcode(), $atts );
extract( $atts );
$css_classes = array(
    'number-boxed-wrap',
    'about-year',
    $el_class,
    vc_shortcode_custom_css_class( $css ),
);
$css_class = preg_replace( '/\s+/', ' ', implode( ' ', array_filter( $css_classes ) ) );
?>
<div class="<?php echo esc_attr( $css_class );?>">
	<span><?php echo $num; ?></span>
	<?php echo wpb_js_remove_wpautop( $content, true ); ?>
</div>
