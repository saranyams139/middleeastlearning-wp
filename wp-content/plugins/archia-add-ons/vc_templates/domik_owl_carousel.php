<?php
/**
 * Shortcode attributes
 * @var $atts
 * @var $el_class
 * @var $el_id
 * @var $layout
 * @var $autoplay
 * @var $singleitem
 * @var $itemscustom
 * @var $slidespeed
 * Shortcode class
 * @var $this WPBakeryShortCode_Archia_Owl_Carousel
 */
$atts = vc_map_get_attributes( $this->getShortcode(), $atts );
extract( $atts );

$dataObj = new stdClass;
if($autoplay == 'true'){
	$dataObj->autoplay = true;
}elseif(is_numeric($autoplay)){
	$dataObj->autoplay = (int)$autoplay;
}
if($singleitem == 'true'){
	$dataObj->singleitem = true;
}else{
	$dataObj->singleitem = false;
}
$dataObj->slidespeed = (int)$slidespeed;
$dataObj = rawurlencode(json_encode($dataObj));
?>

<div<?php if(!empty($el_id)) echo ' id="'.esc_attr($el_id).'" ';?> class="owl-carousel team-carousel <?php echo esc_attr($el_class );?>" data-options="<?php echo esc_attr($dataObj );?>">
    <?php echo wpb_js_remove_wpautop($content);?>
</div>

