<?php
/* add_ons_php */

$el_class = $css = $vid_source = $title = $btn_color = $btn_size = '';
// $show_navigation = $show_dots = $mousewheel = $keyboard = $autoplay = $loop = $direction = $speed = $effect = $responsive = $spacing = '';

$atts = vc_map_get_attributes( $this->getShortcode(), $atts );
extract( $atts );

$css_classes = array(
    'popup-video-wrap',
    $el_class,
    vc_shortcode_custom_css_class( $css ),
);
$css_class = preg_replace( '/\s+/', ' ', implode( ' ', array_filter( $css_classes ) ) );
if(!empty($vid_source)){
    ?>
    <div class="<?php echo esc_attr( $css_class ); ?>">
        <div class="container popup-video-container">
            <div class="row">
                <div class="col-lg-12 video-bx">
                    <div class="video-play <?php echo esc_attr( $btn_color ); ?> <?php echo esc_attr( $btn_size ); ?>">
                        <a href="<?php echo esc_url( $vid_source ); ?>" class="popup-youtube video"><i class="fa fa-play"></i></a>
                        <?php if($title != '') echo '<p class="m-b0 popup-video-title">'.$title.'</p>'; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php } ?>