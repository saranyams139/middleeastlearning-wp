<?php
/* add_ons_php */
$css = $el_class = '';
$atts = vc_map_get_attributes( $this->getShortcode(), $atts );
extract( $atts );
$css_classes = array(
    'header_search',
    $el_class,
    vc_shortcode_custom_css_class( $css ),
);
$css_class = preg_replace( '/\s+/', ' ', implode( ' ', array_filter( $css_classes ) ) );
?>
<div class="<?php echo esc_attr($css_class );?>">
	<a id="quik-search-btn" href="javascript:void(0);" class="search-btn"><i class="fa fa-search"></i></a>
</div>
