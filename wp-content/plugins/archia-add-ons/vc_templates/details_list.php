<?php
/* add_ons_php */

$el_class = $css = $details = '';
// $show_navigation = $show_dots = $mousewheel = $keyboard = $autoplay = $loop = $direction = $speed = $effect = $responsive = $spacing = '';
$ani_type = $ani_time = $ani_delay = '';
$atts = vc_map_get_attributes( $this->getShortcode(), $atts );
extract( $atts );

$css_classes = array(
    'details-list-wrap',
    $el_class,
    vc_shortcode_custom_css_class( $css ),
);
$css_class = preg_replace( '/\s+/', ' ', implode( ' ', array_filter( $css_classes ) ) );

$details_arr = (array) vc_param_group_parse_atts( $details );

if(!empty($details_arr)){

    $ani_attrs = '';
    if($ani_type != ''){
        $css_class .= ' wow ' . $ani_type;
        if($ani_time != '')
            $ani_attrs .= ' data-wow-duration="'.esc_attr( $ani_time ).'"';
        if($ani_delay != '')
            $ani_attrs .= ' data-wow-delay="'.esc_attr( $ani_delay ).'"';
        // else
  //           $ani_attrs .= ' data-wow-delay="0.2s"';
    }   
    ?>
    <div class="<?php echo esc_attr( $css_class ); ?>"<?php echo $ani_attrs; ?>>
        <ul class="list-details">
            <?php 
            foreach ($details_arr as $idx => $dtl) {
                if(isset($dtl['title']) && isset($dtl['value']) ){
                ?>
                <li class="detls-list-<?php echo $idx + 1 ; ?>">
                    <strong><?php echo $dtl['title']; ?></strong>
                    <?php echo $dtl['value'] ?>
                </li>
            <?php
                }
            } ?>
        </ul>
    </div>
<?php } ?>