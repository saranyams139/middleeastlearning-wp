<?php
/* add_ons_php */
$css = $el_class = $section_loca = $title = $col_title = $pre_title = '';
$atts = vc_map_get_attributes( $this->getShortcode(), $atts );
extract( $atts );
$css_classes = array(
    'section-head',
    $section_loca,
    $el_class,
    vc_shortcode_custom_css_class( $css ),
);
$css_class = preg_replace( '/\s+/', ' ', implode( ' ', array_filter( $css_classes ) ) );
?>
<div class="<?php echo $css_class; ?>">
	<?php if($pre_title != ''): ?><p class="pre-title"><?php echo $pre_title; ?></p><?php endif; ?>
	<?php if($title != ''): ?><h2 class="head-title m-b10 <?php echo $col_title; ?>"><?php echo $title; ?></h2><?php endif; ?>
	<?php if($content != ''): ?><p><?php echo $content; ?></p><?php endif; ?>
</div>
