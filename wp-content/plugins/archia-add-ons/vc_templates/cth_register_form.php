<?php
/**
 * Shortcode attributes
 * @var $atts
 * @var $el_class
 * @var $redirect_link
 * @var $show_first_name
 * @var $show_last_name
 * @var $show_email_add
 * @var $show_url_link
 * Shortcode class
 * @var $this WPBakeryShortCode_Cth_Register_Form
 */
$atts = vc_map_get_attributes( $this->getShortcode(), $atts );
extract( $atts );
?>
<div class="form-wrapp-wrapp <?php echo esc_attr($el_class );?>">
    <div class="form-wrapp">
        <div class="form-horizontal">
            <?php echo wpb_js_remove_wpautop($content,true) ;?>
            <form name="advregisterform" method="post" id="advancedUserRegistration">
                <div id="advregmessage" class="alert-box"></div>

            <?php
            if($show_first_name === 'true'||$show_last_name === 'true') :?>
                <div class="form-group">
            <?php
            if($show_first_name === 'true') :?>
                    <div class="col-lg-6 col-md-6 col-sm-6">
                        <label for="advregfirst"><?php _e('First name','archia-add-ons');?></label>
                        <input type="text" autocomplete="off" class="form-control form-block" id="advregfirst" name="firstname" placeholder="<?php _e('First name','archia-add-ons');?>">
                    </div>
            <?php endif;?>
            <?php
            if($show_last_name === 'true') :?>
                    <div class="col-lg-6 col-md-6 col-sm-6">
                        <label for="advreglast"><?php _e('Last name','archia-add-ons');?></label>
                        <input type="text" autocomplete="off" class="form-control form-block" id="advreglast" name="lastname" placeholder="<?php _e('Last name','archia-add-ons');?>">
                    </div>
            <?php endif;?>
                </div>
            <?php endif;?>

                <div class="form-group">
                    <div class="col-lg-12">
                        <label for="advreglog"><?php _e('Username','archia-add-ons');?></label>
                        <input type="text" autocomplete="off" class="form-control form-block" id="advreglog" name="log" placeholder="<?php _e('Enter your username','archia-add-ons');?>">
                    </div>
                </div>
            <?php
            if($show_email_add === 'true') :?>
                <div class="form-group">
                    <div class="col-lg-12">
                        <label for="advregemail"><?php _e('Email','archia-add-ons');?></label>
                        <input type="email" autocomplete="off" class="form-control form-block" name="email" id="advregemail" placeholder="<?php _e('Enter your email','archia-add-ons');?>">
                    </div>
                </div>
            <?php endif;?>
            <?php
            if($show_url_link === 'true') :?>
                <div class="form-group">
                    <div class="col-lg-12">
                        <label for="advregurl"><?php _e('Url','archia-add-ons');?></label>
                        <input type="text" autocomplete="off" class="form-control form-block" name="url" id="advregurl" placeholder="<?php _e('Enter your web site','archia-add-ons');?>">
                    </div>
                </div>
            <?php endif;?>
                <div class="form-group">
                    <div class="col-lg-12">
                        <label for="advregpwd"><?php _e('Password','archia-add-ons');?></label>
                        <input type="password" autocomplete="off" class="form-control form-block" name="pwd" id="advregpwd" placeholder="<?php _e('Enter your password','archia-add-ons');?>">
                    </div>
                </div>
                <div class="form-group">

                    

                    <?php do_action( 'archia_advanced_reg_additional' ); ?>

                    <?php
                        $nonce = wp_create_nonce( 'archia-register' );
                    ?>
                    <input type="hidden" name="_wpnonce" value="<?php echo $nonce; ?>">

                </div>
                <?php if(!empty($redirect_link)) :?>
                    <input type="hidden" name="redirection_url" value="<?php echo esc_url($redirect_link ); ?>" />
                <?php else :?>
                    <input type="hidden" name="redirection_url" value="<?php echo home_url('/' ); ?>" />
                <?php endif;?>
                
                <div class="form-group">
                    <div class="col-lg-12">
                        <button type="submit" id="advregsubmit" class="btn btn-rounded btn-lg btn-primary btn-lg"><?php _e('Register now','archia-add-ons');?><i class="i-for-submit fa fa-spinner fa-pulse"></i></button>
                    </div>                                  
                </div>
                

            </form>
        </div>
    </div>

</div>