<?php
/* add_ons_php */

$css = $el_class = $cat_ids = $cat_order = $cat_order_by = $order = $order_by = $ids = $ids_not = $show_filter = $posts_per_page = $columns_grid = $spacing = $show_info = 
$show_excerpt = $show_view_project = $enable_gallery = $view_all_link = $show_overlay = $show_pagination = $show_loadmore = $hoz_style = $show_cat = $show_counter = $show_share = $filter_width = $sidebar_filter = $sidebar_title = $folio_item_style = $folio_content_width = $layout = '';

$items_width = '';

$loadmore_posts = '';

$ani_type = $ani_time = $ani_delays = '';

$atts = vc_map_get_attributes( $this->getShortcode(), $atts );
extract( $atts );
// $columns =  $columns_grid.'-cols';
$css_classes = array(
    'filter-portfolio',
    $el_class,
    vc_shortcode_custom_css_class( $css ),
);
$css_class = preg_replace( '/\s+/', ' ', implode( ' ', array_filter( $css_classes ) ) );
$css_class = preg_replace( '/\s+/', ' ', implode( ' ', array_filter( $css_classes ) ) );
?>
<div class="<?php echo $css_class; ?>">
    <?php

    if(is_front_page()) {
        $paged = (get_query_var('page')) ? get_query_var('page') : 1;
    } else {
        $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
    }
    if(!empty($ids)){
        $ids = explode(",", $ids);
        $post_args = array(
            'post_type' => 'portfolio',
            'paged' => $paged,
            'posts_per_page'=> $posts_per_page,
            'post__in' => $ids,
            'orderby'=> $order_by,
            'order'=> $order,
        );
    }elseif(!empty($ids_not)){
        $ids_not = explode(",", $ids_not);
        $post_args = array(
            'post_type' => 'portfolio',
            'paged' => $paged,
            'posts_per_page'=> $posts_per_page,
            'post__not_in' => $ids_not,
            'orderby'=> $order_by,
            'order'=> $order,
        );
    }else{
        $post_args = array(
            'post_type' => 'portfolio',
            'paged' => $paged,
            'posts_per_page'=> $posts_per_page,
            'orderby'=> $order_by,
            'order'=> $order,
        );
    }

    if(!empty($cat_ids)){
        $term_args = array(
            'orderby'           => $cat_order_by, 
            'order'             => $cat_order,
            'include'           => $cat_ids,
            'parent'            => 0,
            
        ); 
        $post_args['tax_query'][] = array(
            'taxonomy' => 'portfolio_cat',
            'field' => 'term_id',
            'terms' => explode(",", $cat_ids),
            'operator' => 'IN',
        );
    }elseif(!empty($cat_ids_not)){
        $term_args = array(
            'orderby'           => $cat_order_by, 
            'order'             => $cat_order,
            'exclude'           => $cat_ids_not,
            'parent'            => 0,
            
        ); 
        $post_args['tax_query'][] = array(
            'taxonomy'      => 'portfolio_cat',
            'field'         => 'term_id',
            'terms'         => explode(",", $cat_ids_not),
            'operator'      => 'NOT IN',
        );
    }else{
        $term_args = array(
            'orderby'           => $cat_order_by, 
            'order'             => $cat_order,
            'parent' => 0,
            
        ); 
    }

    $query_posts = new \WP_Query($post_args);
    if ($query_posts->have_posts()) :

        if( $show_filter == 'yes' ){
            $filter_style = 'style1';
            if($folio_item_style == 'home2') $filter_style = 'style2';
            archia_addons_get_template_part( 
                'template-parts/portfolio/filter', 
                '1' , 
                array(
                    'term_args'=>$term_args,
                    'filter_style'=>$filter_style,
                ) 
            );
        } 
        $grid_cls = 'folio-items folio-isotope';
        $grid_cls .= ' folio-'.$columns_grid.'-cols';
        $grid_cls .= ' folio-style-'.$folio_item_style;
        $grid_cls .= ' folio-'.$spacing.'-pad';



    ?>
    <div class="folio-items-wrap clearfix">
        <?php
        archia_addons_get_template_part(
            'template-parts/portfolio/grid', 
            $folio_item_style, 
            array( 
                'grid_cls' => $grid_cls, 
                'query_posts' => $query_posts, 
                'items_width'  => $items_width ,
                'ani_type'  => $ani_type ,
                'ani_time'  => $ani_time ,
                'ani_delays'  => $ani_delays ,
            )
        );
        ?> 
    </div>
    <?php endif; ?>
</div>
<?php wp_reset_postdata(); 
