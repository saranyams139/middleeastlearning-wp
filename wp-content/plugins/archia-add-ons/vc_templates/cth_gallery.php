<?php
/* add_ons_php */

$el_class = $css = $galimgs = $columns = $target = $columns_grid = $spacing = $items_width = $no_overlay = $gal_style = '';
// $show_navigation = $show_dots = $mousewheel = $keyboard = $autoplay = $loop = $direction = $speed = $effect = $responsive = $spacing = '';
$ani_type = $ani_time = $ani_delays = '';
$atts = vc_map_get_attributes( $this->getShortcode(), $atts );
extract( $atts );

$css_classes = array(
    'cth-gallery-wrap',
    $gal_style,
    $el_class,
    vc_shortcode_custom_css_class( $css ),
);
$css_class = preg_replace( '/\s+/', ' ', implode( ' ', array_filter( $css_classes ) ) );
if(!empty($galimgs)){
    $images = $links = array();
    $images = explode(",", $galimgs);
    if(!empty($content)){
        $seppos = strpos(strip_tags($content), "|");
        if($seppos !== false){
            $links = explode("|", strip_tags($content));
        }else{
            $links = preg_split( '/\r\n|\r|\n/', strip_tags($content) );
        }
    }

    

    // $dataArr = array();
    // $dataArr['smartSpeed'] = (int)$speed;
    // if($autoplay == 'yes') $dataArr['autoplay'] = true;
    // if($loop == 'yes') 
    //     $dataArr['loop'] = true;
    // else 
    //     $dataArr['loop'] = false;
    // if($show_navigation == 'yes') 
    //     $dataArr['nav'] = true;
    // else 
    //     $dataArr['nav'] = false;
    // if($show_dots == 'yes') 
    //     $dataArr['dots'] = true;
    // else 
    //     $dataArr['dots'] = false;

    // if(!empty($responsive)){
    //     $css_class .=' resp-ena';
    //     $dataArr['responsive'] = $responsive;
    // }
    // if(is_numeric($spacing)) $dataArr['margin'] = (int)$spacing;

    $grid_cls = 'cth-gal-items folio-items folio-isotope mfp-gallery';
    $grid_cls .= ' folio-'.$columns_grid.'-cols';
    $grid_cls .= ' folio-'.$spacing.'-pad';
    ?>
    <div class="<?php echo esc_attr( $css_class ); ?>">
        <div class="<?php echo esc_attr( $grid_cls ); ?>">
            <div class="grid-sizer"></div>
            <?php 
            $items_widths = explode(',',$items_width);
            $ani_delays_arr = explode(',', $ani_delays);
            foreach ($images as $key => $img) { 
                $item_wid = 'x1';
                if(isset($items_widths[$key])) $item_wid = $items_widths[$key];
                $item_cls = 'folio-item';
                $tnsize = 'archia-folio';
                switch ($item_wid) {
                    case 'x1':
                        // $item_cls .= ' col-one col-lg-4 col-md-4 col-sm-4';
                        $tnsize = 'archia-folio';
                        break;
                    case 'x2':
                        $item_cls = 'folio-item folio-item-two';
                        $tnsize = 'archia-folio-two';
                        break;
                    case 'x3':
                        $item_cls = 'folio-item folio-item-three';
                        $tnsize = 'archia-folio-three';
                        break;
                }


                $ani_attrs = '';
                if ( !empty($ani_type) ) {
                    $item_cls .= ' wow ' . $ani_type;
                    if($ani_time != '')
                        $ani_attrs .= ' data-wow-duration="'.esc_attr( $ani_time ).'"';
                    if( isset($ani_delays_arr[$key]) && $ani_delays_arr[$key] != '' )
                        $ani_attrs .= ' data-wow-delay="'.esc_attr( $ani_delays_arr[$key] ).'"';
                    else
                        $ani_attrs .= ' data-wow-delay="0.2s"';
                }

                if($gal_style == 'stygal-simple') $item_cls .= ' portfolio-1';
                ?>
                <div class="cth-gal-item <?php echo esc_attr( $item_cls ); ?>"<?php echo $ani_attrs; ?>>
                <?php 
                if($gal_style == 'stygal-simple'): ?>
                    <div class="dlab-box dlab-gallery-box">
                        <div class="dlab-media<?php if($no_overlay != 'yes') echo ' dlab-img-overlay1';?> dlab-img-effect"> 
                        <?php 
                            if( isset($links[$key]) && !empty($links[$key]) ): ?>
                            <a href="<?php echo esc_url( $links[$key] ); ?>" class="cth-gal-img-wrap">
                            <?php else: ?>
                            <a href="javascript:void(0);" class="cth-gal-img-wrap">
                            <?php endif; ?><?php echo wp_get_attachment_image( $img, $tnsize, false, array('class'=>$tnsize.'-thumbnail') );?></a>
                            <?php $img_post = get_post($img); ?>
                            <div class="overlay-bx">
                                <div class="overlay-icon align-b text-white"> 
                                    <div class="text-white text-left port-box">
                                        <h5 class="title"><?php echo $img_post->post_title; ?></h5>
                                        <?php echo $img_post->post_excerpt; ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php else: ?>
                    <div class="dlab-box portfolio-bx style2 project-media">
                        <div class="dlab-media<?php if($no_overlay != 'yes') echo ' dlab-img-overlay1';?> dlab-img-effect"> 
                            <?php 
                            if( isset($links[$key]) && !empty($links[$key]) ): ?>
                            <a href="<?php echo esc_url( $links[$key] ); ?>" class="cth-gal-img-wrap">
                            <?php else: ?>
                            <a href="javascript:void(0);" class="cth-gal-img-wrap">
                            <?php endif; ?><?php echo wp_get_attachment_image( $img, $tnsize, false, array('class'=>$tnsize.'-thumbnail') );?></a>
                            <div class="overlay-bx">
                                <a href="<?php echo wp_get_attachment_url( $img ); ?>" class="mfp-link" title="<?php echo esc_attr( get_the_title( $img ) ); ?>"><i class="ti-zoom-in"></i></a>
                            </div>
                        </div>
                    </div>
                <?php endif; ?>
                </div> 
            <?php } ?> 
        </div>
    </div>
<?php } ?>