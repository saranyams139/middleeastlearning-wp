<?php
/* add_ons_php */
if(!function_exists('archia_subscribe_callback')) {

    function archia_subscribe_callback( $atts, $content="" ) {
        
        extract(shortcode_atts(array(
           'class'=>'',
           // 'title'=>'Newsletter',
           'message'=>__( '<p>Want to be notified when we go live?</p>', 'archia-add-ons' ),
           'placeholder'=>__( 'Your Email', 'archia-add-ons' ),
           'button'=>__( 'Subscribe', 'archia-add-ons' ),
           'list_id' => '',
        ), $atts));

        $return = '';

        ob_start();
        ?>
        
        <div class="subscribe-form <?php echo esc_attr( $class ); ?>">
            <?php echo $message; ?>
            <form class="archia_mailchimp-form">
                <div class="input-group">
                	<input class="form-control radius-no bg-black" id="subscribe-email" name="email" placeholder="<?php echo esc_attr( $placeholder ); ?>" type="email" required="required">
                	

                	<span class="input-group-btn">
						<button type="submit" class="btn radius-xl"><?php echo esc_html( $button ); ?></button>
					</span> 

                </div>
                <label class="subscribe-agree-label" for="subscribe-agree-checkbox">
                    <input id="subscribe-agree-checkbox" type="checkbox" name="sub-agree-terms" required="required" value="1"><?php _e( 'I agree with the <a href="#" target="_blank">Privacy Policy</a>', 'archia-add-ons' ); ?>
                </label>
                <label for="subscribe-email" class="subscribe-message"></label>
                <?php if ( function_exists( 'wp_create_nonce' ) ) { ?>
                <input type="hidden" name="_nonce" value="<?php echo wp_create_nonce( 'archia_mailchimp' ) ?>">
                <?php } 
                if($list_id !=''){ ?>
                <input type="hidden" name="_list_id" value="<?php echo esc_attr( $list_id ); ?>">
                <?php } ?>
            </form>
        </div>

        <?php  
        return ob_get_clean();
            
    }
        
    add_shortcode( 'archia_subscribe', 'archia_subscribe_callback' ); //Mailchimp

}