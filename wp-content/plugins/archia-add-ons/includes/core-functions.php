<?php
/** 
 * get template part file related to plugin folder
 *
 */
if(!function_exists('archia_addons_get_template_part')){
    /**
     * Load a template part into a template
     *
     * Makes it easy for a theme to reuse sections of code in a easy to overload way
     * for child themes.
     *
     * Includes the named template part for a theme or if a name is specified then a
     * specialised part will be included. If the theme contains no {slug}.php file
     * then no template will be included.
     *
     * The template is included using require, not require_once, so you may include the
     * same template part multiple times.
     *
     * For the $name parameter, if the file is called "{slug}-special.php" then specify
     * "special".
      * For the var parameter, simple create an array of variables you want to access in the template
     * and then access them e.g. 
     * 
     * array("var1=>"Something","var2"=>"Another One","var3"=>"heres a third";
     * 
     * becomes
     * 
     * $var1, $var2, $var3 within the template file.
     *
     * @since 1.0.3
     *
     * @param string $slug The slug name for the generic template.
     * @param string $name The name of the specialised template.
     * @param array $vars The list of variables to carry over to the template
     * @author CTHthemes 
     * @ref http://www.zmastaa.com/2015/02/06/php-2/wordpress-passing-variables-get_template_part
     * @ref http://keithdevon.com/passing-variables-to-get_template_part-in-wordpress/
     */
    function archia_addons_get_template_part( $slug, $name = null, $vars = null, $include = true ) {

        $template = "{$slug}.php";
        $name = (string) $name;
        if ( '' !== $name ) {
            $template = "{$slug}-{$name}.php";
        }

        if(isset($vars)) extract($vars);
        if($located = locate_template( 'archia-add-ons/'.$template )){
            if($include) 
                include $located;
            else 
                return $located;
        }else{
            if($include) 
                include BBT_ABSPATH.$template;
            else 
                return BBT_ABSPATH.$template;
            
        }
        // include(archia_addons_locate_template($template));
        
    }

 //    function archia_addons_locate_template($template_names, $load = false, $require_once = true ) {
    //  $located = '';
    //  foreach ( (array) $template_names as $template_name ) {
    //      if ( !$template_name )
    //          continue;
    //      if ( file_exists(BBT_ABSPATH . '/' . $template_name)) {
    //          $located = BBT_ABSPATH . '/' . $template_name;
    //          break;
    //      } elseif ( file_exists(BBT_ABSPATH . '/' . $template_name) ) {
    //          $located = BBT_ABSPATH . '/' . $template_name;
    //          break;
    //      } elseif ( file_exists( ABSPATH . WPINC . '/theme-compat/' . $template_name ) ) {
    //          $located = ABSPATH . WPINC . '/theme-compat/' . $template_name;
    //          break;
    //      }
    //  }

    //  if ( $load && '' != $located )
    //      load_template( $located, $require_once );

    //  return $located;
    // }
}


function archia_addons_get_option( $setting, $default = null ) { 
    global $archia_addons_options;

    $default_options = array(
        
        'maintenance_mode'                      => 'disable', 
    );
    $value = false;
    if ( isset( $archia_addons_options[ $setting ] ) ) { 
        $value = $archia_addons_options[ $setting ];
    }else {
        if(isset($default)){
            $value = $default;
        }else if( isset( $default_options[ $setting ] ) ){
            $value = $default_options[ $setting ];
        }
    }
    return $value;
}

add_action( 'archia_login_additional', function(){
    archia_addons_display_recaptcha('loginCaptcha');
} );
add_action( 'archia_reg_additional', function(){
    archia_addons_display_recaptcha('regCaptcha');
} );
add_action( 'archia_advanced_reg_additional', function(){
    archia_addons_display_recaptcha('adRegCaptcha');
} );


function archia_addons_display_recaptcha($ele_id){
    if( archia_addons_get_option('enable_g_recaptcah') == 'yes' && archia_addons_get_option('g_recaptcha_site_key') != '' ) 
        echo '<div id="'.$ele_id.'" class="cth-recaptcha mt-20"></div>';
}

function archia_addons_verify_recaptcha(){
        
    if( archia_addons_get_option('enable_g_recaptcah') == 'yes' && archia_addons_get_option('g_recaptcha_secret_key') != '' ){
        if( empty( $_POST['g-recaptcha-response'] ) ) return false;
        $response = wp_remote_post( 
            'https://www.google.com/recaptcha/api/siteverify' ,
            array(
                'body' =>   array(
                                'secret'   => archia_addons_get_option('g_recaptcha_secret_key'),
                                'response' => $_POST['g-recaptcha-response'],
                                'remoteip' => isset($_SERVER['HTTP_X_FORWARDED_FOR']) ? $_SERVER['HTTP_X_FORWARDED_FOR'] : $_SERVER['REMOTE_ADDR']
                            )
            )
        );

        // return json_decode( $response['body'] ); // captcha: {success: true, challenge_ts: "2019-03-19T09:58:27Z", hostname: "localhost"}

        if( is_wp_error( $response ) || empty($response['body']) || ! ($json = json_decode( $response['body'] )) || ! $json->success ) {
            //return new WP_Error( 'validation-error',  __('reCAPTCHA validation failed. Please try again.' ) );
            return false;
        }

        return true;
    }

    return true;
}


// https://stackoverflow.com/questions/10808109/script-tag-async-defer
function archia_addons_add_async_forscript($url)
{
    if(is_admin()){
        return str_replace(array('#cthasync','#cthdefer'), '', $url);
    }else{
        if(strpos($url, '#cthasync') !== false) $url = str_replace('#cthasync', '', $url)."' async='async"; 
        if(strpos($url, '#cthdefer') !== false) $url = str_replace('#cthdefer', '', $url)."' defer='defer"; 
    }

    return $url;
}
add_filter('clean_url', 'archia_addons_add_async_forscript', 11, 1);
function archia_addons_get_header_type_options(){
    $options = array(
        'use_global'      => __( 'Global', 'archia-add-ons' ),
        'none'      => __( 'None', 'archia-add-ons' ),
        
    );
    $posts = get_posts( array(
        'fields'            => 'ids',
        'post_type'         => 'cth_header',
        'posts_per_page'    => -1,
        'post_status'       => 'publish',

    ) );
    if($posts){
        foreach ($posts as $ltid) {
            $options[$ltid] = get_the_title( $ltid );
        }
    }
    return $options;
}
function archia_addons_get_footer_type_options(){
    $options = array(
        'use_global'      => __( 'Global', 'archia-add-ons' ),
        'none'      => __( 'None', 'archia-add-ons' ),
        
    );
    $posts = get_posts( array(
        'fields'            => 'ids',
        'post_type'         => 'cth_footer',
        'posts_per_page'    => -1,
        'post_status'       => 'publish',

    ) );
    if($posts){
        foreach ($posts as $ltid) {
            $options[$ltid] = get_the_title( $ltid );
        }
    }
    return $options;
}

add_filter('archia_plugin_header_style', function(){
    return true;
});

add_action('archia_header_style', function(){
    $header_type = archia_addons_get_option('header_type');
    $hide = false;
    if(is_singular()){
        $post_header = get_post_meta( get_the_ID(), '_cth_header_type', true );
        if($post_header == 'none'){
            $hide = true;
        }elseif( $post_header != '' && $post_header != 'use_global') 
            $header_type = $post_header;
    }
    if ($hide === false && !empty($header_type)) {
        $header_post = get_post($header_type);
        echo wpb_js_remove_wpautop(  $header_post->post_content );
    }
});
add_filter('archia_plugin_footer_style', function(){
    return true;
});

add_action('archia_footer_style', function(){
    $footer_type = archia_addons_get_option('footer_type');
    $hide = false;
    if(is_singular()){
        $post_footer = get_post_meta( get_the_ID(), '_cth_footer_type', true );
        if($post_footer == 'none'){
            $hide = true;
        }elseif( $post_footer != '' && $post_footer != 'use_global') 
            $footer_type = $post_footer;
    }
    if ($hide === false && !empty($footer_type)) {
        $footer_post = get_post($footer_type);
        echo wpb_js_remove_wpautop(  $footer_post->post_content );
    }
});

add_filter( 'body_class', function($classes){
    $classes[] = 'archia-has-addons';
    if ( is_singular() ) {
        if( get_post_meta( get_the_ID(), '_cth_footer_type', true ) == 'none') 
            $classes[] = 'archia-hide-footer';
    }
    return $classes;
} );

function archia_addons_portfolio_taxs($post_id = 0, $tax = 'portfolio_cat'){
    $terms = get_the_terms( $post_id, $tax );             
    if ( $terms && ! is_wp_error( $terms ) ){
     
        $tax_names = array();
     
        foreach ( $terms as $term ) {
            $tax_names[] = $term->name;
        }
                             
        $tax_names_text = join( ", ", $tax_names );

        return $tax_names_text;
    }
    return '';
}
function archia_addons_color_skins(){
    $skins = array(
        'use_global'      => __( 'Global', 'archia-add-ons' ),
        'skin-1' => __( 'Skin 1', 'archia-add-ons' ),
        'color/skin-1' => __( 'Skin 1 - Color', 'archia-add-ons' ),
        'skin-2' => __( 'Skin 2', 'archia-add-ons' ),
        'color/skin-2' => __( 'Skin 2 - Color', 'archia-add-ons' ),
        'skin-3' => __( 'Skin 3', 'archia-add-ons' ),
        'color/skin-3' => __( 'Skin 3 - Color', 'archia-add-ons' ),
        'skin-4' => __( 'Skin 4', 'archia-add-ons' ),
        'color/skin-4' => __( 'Skin 4 - Color', 'archia-add-ons' ),
    );
    return $skins;
}
add_filter( 'archia_color_skin', function($skin){
    if ( is_singular() ) {
        $single_skin = get_post_meta( get_the_ID(), '_cth_skin', true );
        if( $single_skin != '' && $single_skin !== 'use_global') 
            return $single_skin;
    }
    return $skin;
}, 20 );

function archia_addons_extract_themify(){
    $content =file_get_contents(BBT_ABSPATH.'assets/css/archia-icons.css');
    $re = '/\.(ti-([\w-]+))/m';
    preg_match_all($re, $content, $matches, PREG_SET_ORDER, 0);
    // var_dump($matches);
    $icons = array();
    if($matches){
        foreach ($matches as $match) {
            $icons[] = array($match[1] => str_replace(array('-'), array(' '), $match[2]) );
        }
    }
    return $icons;
}

function archia_addons_get_icon_themify() {
    $icons = archia_addons_extract_themify();
    $icon_options = array();
    foreach ($icons as $icon) {
        // php 7.3
        // $icon_options['im '.array_key_first($icon)] = reset( $icon );
        $icotitle = reset( $icon );
        $icon_options[key($icon)] = $icotitle;
    }
    return $icon_options;
}

function archia_addons_get_socials_list(){

    $socials = array(
        'facebook' => __( 'Facebook',  'archia-add-ons' ),
        'twitter' => __( 'Twitter',  'archia-add-ons' ),
        'youtube' => __( 'Youtube',  'archia-add-ons' ),
        'vimeo' => __( 'Vimeo',  'archia-add-ons' ),
        'instagram' => __( 'Instagram',  'archia-add-ons' ),
        'vk' => __( 'Vkontakte',  'archia-add-ons' ),
        'reddit' => __( 'Reddit',  'archia-add-ons' ),
        'pinterest' => __( 'Pinterest',  'archia-add-ons' ),
        'vine' => __( 'Vine Camera',  'archia-add-ons' ),
        'tumblr' => __( 'Tumblr',  'archia-add-ons' ),
        'flickr' => __( 'Flickr',  'archia-add-ons' ),
        'google-plus-g' => __( 'Google+',  'archia-add-ons' ),
        'linkedin' => __( 'LinkedIn',  'archia-add-ons' ),
        'whatsapp' => __( 'Whatsapp',  'archia-add-ons' ),
        'meetup' => __( 'Meetup',  'archia-add-ons' ),
        'custom_icon' => __( 'Custom',  'archia-add-ons' ),
    );

    return $socials ;

}  
// echo socials share content
function archia_addons_echo_socials_share(){
    $widgets_share_names = archia_addons_get_option('widgets_share_names',' facebook , twitter , pinterest ');
    if($widgets_share_names !=''):
    ?>
    <div class="share-holder share-post ml-auto">
        <div class="share-container" data-share="<?php echo esc_attr( $widgets_share_names ); ?>">
            <span class="share-lbl"><?php esc_html_e( 'SHARE: ', 'archia-add-ons' ) ?></span>
        </div>
    </div>
    <?php
    endif;  
}

// for subscription plan
function archia_add_ons_get_subscription_duration_units($unit = ''){
    $duration_units = array(
        'day'           => esc_html__( 'Days', 'archia-add-ons' ),
        'week'          => esc_html__( 'Weeks', 'archia-add-ons' ),
        'month'         => esc_html__( 'Months', 'archia-add-ons' ),
        'year'          => esc_html__( 'Years', 'archia-add-ons' ),
    );
    if( !empty($unit) && isset( $duration_units[$unit] ) ) return $duration_units[$unit];

    return $duration_units;
}
function archia_addons_get_price_formated($price = 0, $show_currency = true){
    if($price == '') $price = 0;
    $return = number_format( $price, archia_addons_get_option('decimals','2'), archia_addons_get_option('decimal_sep','.'), archia_addons_get_option('thousand_sep',',') );
    if($show_currency){
        $currency = archia_add_ons_get_currency_symbol(archia_addons_get_option('currency','USD'));
        $currency_pos = archia_addons_get_option('currency_pos','left_space');
        switch ($currency_pos) {
            case 'left':
                $return = $currency .$return;
                break;
            case 'right':
                $return .= $currency;
                break;
            case 'right_space':
                $return .= '&nbsp;'. $currency;
                break;
            default:
                $return = $currency . '&nbsp;'. $return;
                break;
        }
        
    }

    return $return;
}

function archia_add_ons_get_currency_symbol($currency = 'USD'){

    $world_curr = array (
        'ALL' => _x('Albania Lek', 'currency symbol', 'archia-add-ons' ), 
        'AFN' => _x('Afghanistan Afghani', 'currency symbol', 'archia-add-ons' ), 
        'ARS' => _x('Argentina Peso', 'currency symbol', 'archia-add-ons' ), 
        'AWG' => _x('Aruba Guilder', 'currency symbol', 'archia-add-ons' ), 
        'AUD' => _x('Australia Dollar', 'currency symbol', 'archia-add-ons' ), 
        'AZN' => _x('Azerbaijan New Manat', 'currency symbol', 'archia-add-ons' ), 
        'BSD' => _x('Bahamas Dollar', 'currency symbol', 'archia-add-ons' ), 
        'BBD' => _x('Barbados Dollar', 'currency symbol', 'archia-add-ons' ), 
        'BDT' => _x('Bangladeshi taka', 'currency symbol', 'archia-add-ons' ), 
        'BYR' => _x('Belarus Ruble', 'currency symbol', 'archia-add-ons' ), 
        'BZD' => _x('Belize Dollar', 'currency symbol', 'archia-add-ons' ), 
        'BMD' => _x('Bermuda Dollar', 'currency symbol', 'archia-add-ons' ), 
        'BOB' => _x('Bolivia Boliviano', 'currency symbol', 'archia-add-ons' ), 
        'BAM' => _x('Bosnia and Herzegovina Convertible Marka', 'currency symbol', 'archia-add-ons' ), 
        'BWP' => _x('Botswana Pula', 'currency symbol', 'archia-add-ons' ), 
        'BGN' => _x('Bulgaria Lev', 'currency symbol', 'archia-add-ons' ), 
        'BRL' => _x('Brazil Real', 'currency symbol', 'archia-add-ons' ), 
        'BND' => _x('Brunei Darussalam Dollar', 'currency symbol', 'archia-add-ons' ), 
        'KHR' => _x('Cambodia Riel', 'currency symbol', 'archia-add-ons' ), 
        'CAD' => _x('Canada Dollar', 'currency symbol', 'archia-add-ons' ), 
        'KYD' => _x('Cayman Islands Dollar', 'currency symbol', 'archia-add-ons' ), 
        'CLP' => _x('Chile Peso', 'currency symbol', 'archia-add-ons' ), 
        'CNY' => _x('China Yuan Renminbi', 'currency symbol', 'archia-add-ons' ), 
        'COP' => _x('Colombia Peso', 'currency symbol', 'archia-add-ons' ), 
        'CRC' => _x('Costa Rica Colon', 'currency symbol', 'archia-add-ons' ), 
        'HRK' => _x('Croatia Kuna', 'currency symbol', 'archia-add-ons' ), 
        'CUP' => _x('Cuba Peso', 'currency symbol', 'archia-add-ons' ), 
        'CZK' => _x('Czech Republic Koruna', 'currency symbol', 'archia-add-ons' ), 
        'DKK' => _x('Denmark Krone', 'currency symbol', 'archia-add-ons' ), 
        'DOP' => _x('Dominican Republic Peso', 'currency symbol', 'archia-add-ons' ), 
        'XCD' => _x('East Caribbean Dollar', 'currency symbol', 'archia-add-ons' ), 
        'EGP' => _x('Egypt Pound', 'currency symbol', 'archia-add-ons' ), 
        'SVC' => _x('El Salvador Colon', 'currency symbol', 'archia-add-ons' ), 
        'EEK' => _x('Estonia Kroon', 'currency symbol', 'archia-add-ons' ), 
        'EUR' => _x('Euro Member Countries', 'currency symbol', 'archia-add-ons' ), 
        'FKP' => _x('Falkland Islands (Malvinas) Pound', 'currency symbol', 'archia-add-ons' ), 
        'FJD' => _x('Fiji Dollar', 'currency symbol', 'archia-add-ons' ), 
        'GHC' => _x('Ghana Cedis', 'currency symbol', 'archia-add-ons' ), 
        'GIP' => _x('Gibraltar Pound', 'currency symbol', 'archia-add-ons' ), 
        'GTQ' => _x('Guatemala Quetzal', 'currency symbol', 'archia-add-ons' ), 
        'GGP' => _x('Guernsey Pound', 'currency symbol', 'archia-add-ons' ), 
        'GYD' => _x('Guyana Dollar', 'currency symbol', 'archia-add-ons' ), 
        'HNL' => _x('Honduras Lempira', 'currency symbol', 'archia-add-ons' ), 
        'HKD' => _x('Hong Kong Dollar', 'currency symbol', 'archia-add-ons' ), 
        'HUF' => _x('Hungary Forint', 'currency symbol', 'archia-add-ons' ), 
        'ISK' => _x('Iceland Krona', 'currency symbol', 'archia-add-ons' ), 
        'INR' => _x('India Rupee', 'currency symbol', 'archia-add-ons' ), 
        'IDR' => _x('Indonesia Rupiah', 'currency symbol', 'archia-add-ons' ), 
        'IRR' => _x('Iran Rial', 'currency symbol', 'archia-add-ons' ), 
        'IMP' => _x('Isle of Man Pound', 'currency symbol', 'archia-add-ons' ), 
        'ILS' => _x('Israel Shekel', 'currency symbol', 'archia-add-ons' ), 
        'JMD' => _x('Jamaica Dollar', 'currency symbol', 'archia-add-ons' ), 
        'JPY' => _x('Japan Yen', 'currency symbol', 'archia-add-ons' ), 
        'JEP' => _x('Jersey Pound', 'currency symbol', 'archia-add-ons' ), 
        'KZT' => _x('Kazakhstan Tenge', 'currency symbol', 'archia-add-ons' ), 
        'KPW' => _x('Korea (North) Won', 'currency symbol', 'archia-add-ons' ), 
        'KRW' => _x('Korea (South) Won', 'currency symbol', 'archia-add-ons' ), 
        'KGS' => _x('Kyrgyzstan Som', 'currency symbol', 'archia-add-ons' ), 
        'LAK' => _x('Laos Kip', 'currency symbol', 'archia-add-ons' ), 
        'LVL' => _x('Latvia Lat', 'currency symbol', 'archia-add-ons' ), 
        'LBP' => _x('Lebanon Pound', 'currency symbol', 'archia-add-ons' ), 
        'LRD' => _x('Liberia Dollar', 'currency symbol', 'archia-add-ons' ), 
        'LTL' => _x('Lithuania Litas', 'currency symbol', 'archia-add-ons' ), 
        'MKD' => _x('Macedonia Denar', 'currency symbol', 'archia-add-ons' ), 
        'MYR' => _x('Malaysia Ringgit', 'currency symbol', 'archia-add-ons' ), 
        'MUR' => _x('Mauritius Rupee', 'currency symbol', 'archia-add-ons' ), 
        'MXN' => _x('Mexico Peso', 'currency symbol', 'archia-add-ons' ), 
        'MNT' => _x('Mongolia Tughrik', 'currency symbol', 'archia-add-ons' ), 
        'MZN' => _x('Mozambique Metical', 'currency symbol', 'archia-add-ons' ), 
        'NAD' => _x('Namibia Dollar', 'currency symbol', 'archia-add-ons' ), 
        'NPR' => _x('Nepal Rupee', 'currency symbol', 'archia-add-ons' ), 
        'ANG' => _x('Netherlands Antilles Guilder', 'currency symbol', 'archia-add-ons' ), 
        'NZD' => _x('New Zealand Dollar', 'currency symbol', 'archia-add-ons' ), 
        'NIO' => _x('Nicaragua Cordoba', 'currency symbol', 'archia-add-ons' ), 
        'NGN' => _x('Nigeria Naira', 'currency symbol', 'archia-add-ons' ), 
        'NOK' => _x('Norway Krone', 'currency symbol', 'archia-add-ons' ), 
        'OMR' => _x('Oman Rial', 'currency symbol', 'archia-add-ons' ), 
        'PKR' => _x('Pakistan Rupee', 'currency symbol', 'archia-add-ons' ), 
        'PAB' => _x('Panama Balboa', 'currency symbol', 'archia-add-ons' ), 
        'PYG' => _x('Paraguay Guarani', 'currency symbol', 'archia-add-ons' ), 
        'PEN' => _x('Peru Nuevo Sol', 'currency symbol', 'archia-add-ons' ), 
        'PHP' => _x('Philippines Peso', 'currency symbol', 'archia-add-ons' ), 
        'PLN' => _x('Poland Zloty', 'currency symbol', 'archia-add-ons' ), 
        'QAR' => _x('Qatar Riyal', 'currency symbol', 'archia-add-ons' ), 
        'RON' => _x('Romania New Leu', 'currency symbol', 'archia-add-ons' ), 
        'RUB' => _x('Russia Ruble', 'currency symbol', 'archia-add-ons' ), 
        'SHP' => _x('Saint Helena Pound', 'currency symbol', 'archia-add-ons' ), 
        'SAR' => _x('Saudi Arabia Riyal', 'currency symbol', 'archia-add-ons' ), 
        'RSD' => _x('Serbia Dinar', 'currency symbol', 'archia-add-ons' ), 
        'SCR' => _x('Seychelles Rupee', 'currency symbol', 'archia-add-ons' ), 
        'SGD' => _x('Singapore Dollar', 'currency symbol', 'archia-add-ons' ), 
        'SBD' => _x('Solomon Islands Dollar', 'currency symbol', 'archia-add-ons' ), 
        'SOS' => _x('Somalia Shilling', 'currency symbol', 'archia-add-ons' ), 
        'ZAR' => _x('South Africa Rand', 'currency symbol', 'archia-add-ons' ), 
        'LKR' => _x('Sri Lanka Rupee', 'currency symbol', 'archia-add-ons' ), 
        'SEK' => _x('Sweden Krona', 'currency symbol', 'archia-add-ons' ), 
        'CHF' => _x('Switzerland Franc', 'currency symbol', 'archia-add-ons' ), 
        'SRD' => _x('Suriname Dollar', 'currency symbol', 'archia-add-ons' ), 
        'SYP' => _x('Syria Pound', 'currency symbol', 'archia-add-ons' ), 
        'TWD' => _x('Taiwan New Dollar', 'currency symbol', 'archia-add-ons' ), 
        'THB' => _x('Thailand Baht', 'currency symbol', 'archia-add-ons' ), 
        'TTD' => _x('Trinidad and Tobago Dollar', 'currency symbol', 'archia-add-ons' ), 
        'TRY' => _x('Turkey Lira', 'currency symbol', 'archia-add-ons' ), 
        'TRL' => _x('Turkey Lira', 'currency symbol', 'archia-add-ons' ), 
        'TVD' => _x('Tuvalu Dollar', 'currency symbol', 'archia-add-ons' ), 
        'UAH' => _x('Ukraine Hryvna', 'currency symbol', 'archia-add-ons' ), 
        'GBP' => _x('United Kingdom Pound', 'currency symbol', 'archia-add-ons' ), 
        'UGX' => _x('Uganda Shilling', 'currency symbol', 'archia-add-ons' ), 
        'USD' => _x('United States Dollar', 'currency symbol', 'archia-add-ons' ), 
        'UYU' => _x('Uruguay Peso', 'currency symbol', 'archia-add-ons' ), 
        'UZS' => _x('Uzbekistan Som', 'currency symbol', 'archia-add-ons' ), 
        'VEF' => _x('Venezuela Bolivar', 'currency symbol', 'archia-add-ons' ), 
        'VND' => _x('Viet Nam Dong', 'currency symbol', 'archia-add-ons' ), 
        'YER' => _x('Yemen Rial', 'currency symbol', 'archia-add-ons' ), 
        'ZWD' => _x('Zimbabwe Dollar', 'currency symbol', 'archia-add-ons' ), 
    );
    $currency_symbols = array (
        'USD'          => esc_html__( '$', 'archia-add-ons' ),
        'EUR'          => esc_html__( '€', 'archia-add-ons' ),
        'GBP'          => esc_html__( '£', 'archia-add-ons' ),
        'AUD'          => esc_html__( '$', 'archia-add-ons' ),
        'BRL'          => esc_html__( 'R$', 'archia-add-ons' ),
        'CAD'          => esc_html__( '$', 'archia-add-ons' ),
        'HKD'          => esc_html__( '$', 'archia-add-ons' ),
        'ILS'          => esc_html__( '₪', 'archia-add-ons' ),
        'JPY'          => esc_html__( '¥', 'archia-add-ons' ),
        'MXN'          => esc_html__( '$', 'archia-add-ons' ),
        'NZD'          => esc_html__( '$', 'archia-add-ons' ),
        'SGD'          => esc_html__( '$', 'archia-add-ons' ),
        'THB'          => esc_html__( '฿', 'archia-add-ons' ),
        'INR'          => esc_html__( '₹', 'archia-add-ons' ),
        'TRY'           => esc_html__( '₺', 'archia-add-ons' ),
        'RIAL'          => esc_html__( '﷼', 'archia-add-ons' ),
        'VND'           => esc_html__( '₫', 'archia-add-ons' ),
        'RUB'           => esc_html__( '&#x20bd;', 'archia-add-ons' ),
        'ZAR'           => esc_html__( 'R', 'archia-add-ons' ),
    );

    $currency_symbols = array_merge($world_curr, $currency_symbols);
    if(isset($currency_symbols[$currency])) return $currency_symbols[$currency];

    return $currency;
    
}

