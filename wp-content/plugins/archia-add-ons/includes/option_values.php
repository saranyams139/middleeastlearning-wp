<?php 
/* add_ons_php */
defined( 'ABSPATH' ) || exit;
function archia_addons_get_plugin_options(){  
    return array(
        'general' => array(

            // array(
            //     "type" => "section",
            //     'id' => 'submit_captcha_sec',
            //     "title" => __( 'Google reCAPTCHA', 'archia-add-ons' ),
            //     'callback' => function(){
            //         echo sprintf(__( '<p>Get <a href="%s" target="_blank">reCAPTCHA Keys</a></p>', 'archia-add-ons' ), esc_url('https://www.google.com/recaptcha'));
                    
            //     }
            // ),
            // array(
            //     "type" => "field",
            //     "field_type" => "checkbox",
            //     'id' => 'enable_g_recaptcah',
            //     'args'=> array(
            //         'default' => 'no',
            //         'value' => 'yes',
            //     ),
            //     "title" => __('Enable reCAPTCHA', 'archia-add-ons'),
            // ),

            // array(
            //     "type" => "field",
            //     "field_type" => "text",
            //     'id' => 'g_recaptcha_site_key',
            //     "title" => __('Site Key', 'archia-add-ons'),
            //     'desc'  => '',
            //     'args' => array(
            //         'default' => '',
            //     )
            // ),

            // array(
            //     "type" => "field",
            //     "field_type" => "text",
            //     'id' => 'g_recaptcha_secret_key',
            //     "title" => __('Secret key', 'archia-add-ons'),
            //     'desc'  => '',
            //     'args' => array(
            //         'default' => '',
            //     )
            // ),

            array(
                "type" => "section",
                'id' => 'general_style_opts',
                "title" => __( 'Style Options', 'archia-add-ons' ),    
            ),



            // array(
            //     "type" => "section",
            //     'id' => 'general_design_opts',
            //     "title" => __( 'General Options', 'archia-add-on' ),    
            // ),
            array(
                "type" => "field",
                "field_type" => "select",
                'id' => 'header_type',
                "title" => __('Default Header Style', 'archia-add-ons'),
                'args'=> array(
                    'options'=> archia_addons_get_header_type_options(),
                )
            ),
            array(
                "type" => "field",
                "field_type" => "select",
                'id' => 'footer_type',
                "title" => __('Default Footer Style', 'archia-add-ons'),
                'args'=> array(
                    'options'=> archia_addons_get_footer_type_options(),
                )
            ),
            

            // array(
            //     "type" => "section",
            //     'id' => 'general_tax_sec',
            //     "title" => __( 'Taxes', 'archia-add-ons' ),
            // ),

            // array(
            //     "type" => "field",
            //     "field_type" => "text",
            //     'id' => 'vat_tax',
            //     "title" => __('VAT Tax', 'archia-add-ons'),
            //     'desc'  => __( 'VAT tax percent. Default: 10%', 'archia-add-ons' ),
            //     'args' => array(
            //         'default' => '10',
            //     )
            // ),

            
            // array(
            //     "type" => "section",
            //     'id' => 'general_section_6',
            //     "title" => __( 'Listing Pages - Important', 'archia-add-ons' ),
            // ),

            // // array(
            // //     "type" => "field",
            // //     "field_type" => "page_select",
            // //     'id' => 'submit_page',
            // //     "title" => __('Submit Listing Page', 'archia-add-ons'),
            // //     'desc'  => __('The page will be used to display listing submission. The page content should contain <b>[listing_submit_page]</b> shortcode', 'archia-add-ons'),
            // //     'args' => array(
            // //         'default_title' => "Submit Listing",
            // //     )
            // // ),

            // // array(
            // //     "type" => "field",
            // //     "field_type" => "page_select",
            // //     'id' => 'edit_page',
            // //     "title" => __('Edit Listing Page', 'archia-add-ons'),
            // //     'desc'  => __('The page will be used to edit listing. The page content should contain <b>[listing_edit_page]</b> shortcode', 'archia-add-ons'),
            // //     'args' => array(
            // //         'default_title' => "Edit Listing",
            // //     )
            // // ),

            // array(
            //     "type" => "field",
            //     "field_type" => "page_select",
            //     'id' => 'dashboard_page',
            //     "title" => __('Listing Author Dashboard Page', 'archia-add-ons'),
            //     'desc'  => __('The page will be used for listing author dashboard. The page content should contain <b>[listing_dashboard_page]</b> shortcode', 'archia-add-ons'),
            //     'args' => array(
            //         'default_title' => "Dashboard",
            //     )
            // ),

            // // array(
            // //     "type" => "field",
            // //     "field_type" => "page_select",
            // //     'id' => 'payment_page',
            // //     "title" => __('Listing Payment Page', 'archia-add-ons'),
            // //     'desc'  => __('The page will be used for listing/booking checkout', 'archia-add-ons'),
            // //     'args' => array(
            // //         'default_title' => "Listing Payment",
            // //     )
            // // ),

            // array(
            //     "type" => "field",
            //     "field_type" => "page_select",
            //     'id' => 'checkout_page',
            //     "title" => __('Listing Checkout Page', 'archia-add-ons'),
            //     'desc'  => __('The page will be used for Membership/Listing checkout. The page content should contain <b>[listing_checkout_page]</b> shortcode', 'archia-add-ons'),
            //     'args' => array(
            //         'default_title' => "Listing Checkout",
            //     )
            // ),
            

        ),
        // end tab array
//         'register'      => array(

//             array(
//                 "type" => "section",
//                 'id' => 'register_general_sec',
//                 "title" => __( 'User Registration', 'archia-add-ons' ),
//             ),

//             array(
//                 "type" => "field",
//                 "field_type" => "select",
//                 'id' => 'new_user_email',
//                 "title" => __('Send new user registration email to', 'archia-add-ons'),
//                 'args'=> array(
//                     'default'=> 'both',
//                     'options'=> array(
//                         'user' => __( 'User only', 'archia-add-ons' ),
//                         'admin' => __( 'Admin only', 'archia-add-ons' ),
//                         'both' => __( 'Admin and user', 'archia-add-ons' ),
//                         'none' => __( 'None', 'archia-add-ons' ),
                        
//                     ),
//                 )
//             ),

//             array(
//                 "type" => "field",
//                 "field_type" => "checkbox",
//                 'id' => 'register_password',
//                 'args'=> array(
//                     'default' => 'no',
//                     'value' => 'yes',
//                 ),
//                 "title" => __('Show Password field', 'archia-add-ons'),
//                 'desc'  => '',
//             ),

//             array(
//                 "type" => "field",
//                 "field_type" => "checkbox",
//                 'id' => 'register_auto_login',
//                 'args'=> array(
//                     'default' => 'no',
//                     'value' => 'yes',
//                 ),
//                 "title" => __('Login user after registered?', 'archia-add-ons'),
//                 'desc'  => '',
//             ),

//             array(
//                 "type" => "field",
//                 "field_type" => "checkbox",
//                 'id' => 'register_no_redirect',
//                 'args'=> array(
//                     'default' => 'yes',
//                     'value' => 'yes',
//                 ),
//                 "title" => __('Disable redirect after registered?', 'archia-add-ons'),
//                 'desc'  => '',
//             ),

//             array(
//                 "type" => "field",
//                 "field_type" => "checkbox",
//                 'id' => 'register_role',
//                 'args'=> array(
//                     'default' => 'no',
//                     'value' => 'yes',
//                 ),
//                 "title" => __('Allow register as author?', 'archia-add-ons'),
//                 'desc'  => '',
//             ),

//             array(
//                 "type" => "field",
//                 "field_type" => "textarea",
//                 'id' => 'register_term_text',
//                 "title" => __('Terms Text', 'archia-add-ons'),
//                 'desc'  => __( 'Accept terms text on user register form.', 'archia-add-ons' ),
//                 'args' => array(
//                     'default' => 'By using the website, you accept the terms and conditions',
//                 )
//             ),

//             array(
//                 "type" => "field",
//                 "field_type" => "textarea",
//                 'id' => 'register_consent_data_text',
//                 "title" => __('Consent Personal Data Text', 'archia-add-ons'),
//                 'desc'  => '',
//                 'args' => array(
//                     'default' => 'Consent to processing of personal data',
//                 )
//             ),
//             array(
//                 "type" => "field",
//                 "field_type" => "textarea",
//                 'id' => 'logreg_form_after',
//                 "title" => __('Log/Reg Footer Content', 'archia-add-ons'),
//                 'desc'  => __( 'Content showing up bellow user login - register form. You can add shortcode for social login.', 'archia-add-ons' ),
//                 'args' => array(
//                     'default' => '<p>For faster login or register use your social account.</p>
// [fbl_login_button redirect="" hide_if_logged="" size="large" type="continue_with" show_face="true"]',
//                 )
//             ),
            

//             array(
//                 "type" => "section",
//                 'id' => 'register_login_sec',
//                 "title" => __( 'User Login', 'archia-add-ons' ),
//             ),

//             array(
//                 "type" => "field",
//                 "field_type" => "page_select",
//                 'id' => 'login_redirect_page',
//                 "title" => __('After Login Redirect', 'archia-add-ons'),
//                 'desc'  => __('The page user redirect to after login.', 'archia-add-ons'),
//                 'args' => array(
//                     'default'   => 'cth_current_page',
//                     // 'default_title' => "Pricing Tables",
//                     'options' => array(
//                         array(
//                             'cth_current_page',
//                             __( 'Current Page', 'archia-add-ons' ),
//                         ),
//                     )
//                 )
//             ),
            


//         ),
        
        // end tab array
        // 'gmap' => array(
        //     array(
        //         "type" => "section",
        //         'id' => 'gmap_osm_sec',
        //         "title" => __( 'OpenarchiaMap', 'archia-add-ons' ),
        //     ),

        //     array(
        //         "type" => "field",
        //         "field_type" => "checkbox",
        //         'id' => 'use_osm_map',
        //         'args'=> array(
        //             'default' => 'no',
        //             'value' => 'yes',
        //         ),
        //         "title" => __('Use Free OpenarchiaMap Instead', 'archia-add-ons'),
        //         'desc'  => '',
        //     ),
        //     array(
        //         "type" => "section",
        //         'id' => 'gmap_section_1',
        //         "title" => __( 'Google API', 'archia-add-ons' ),
        //     ),

        //     array(
        //         "type" => "field",
        //         "field_type" => "text",
        //         'id' => 'gmap_api_key',
        //         "title" => __('Google Map API Key', 'archia-add-ons'),
        //         'desc'  => sprintf( __( 'You have to enter your API key to use google map feature. Required services: <b>Google Places API Web Service</b>, <b>Google Maps JavaScript API</b> and <b>Google Maps Geocoding API</b>.<br /><a href="%s" target="_blank">Get Your Key</a>', 'archia-add-ons' ), esc_url('https://developers.google.com/maps/documentation/javascript/get-api-key' ) ),
        //     ),

        //     array(
        //         "type" => "field",
        //         "field_type" => "select",
        //         'id' => 'gmap_type',
        //         "title" => __('Google Map Type', 'archia-add-ons'),
        //         'args'=> array(
        //             'default'=> 'ROADMAP',
        //             'options'=> array(
        //                 "ROADMAP" => __('ROADMAP - default 2D map','archia-add-ons'), 
        //                 "SATELLITE" => __('SATELLITE - photographic map','archia-add-ons'), 
        //                 "HYBRID" => __('HYBRID - photographic map + roads and city names','archia-add-ons'), 
        //                 "TERRAIN" => __('TERRAIN - map with mountains, rivers, etc','archia-add-ons'), 
                        
        //             ),
        //         )
        //     ),
            
        //     array(
        //         "type" => "field",
        //         "field_type" => "text",
        //         'id' => 'google_map_language',
        //         "title" => __('Google Map Language Code', 'archia-add-ons'),
        //         'args'=> array(
        //             'default'=> '',
        //         ),
        //         'desc'  => sprintf( __( 'Leave this empty for user location or browser settings value. Available value at <a href="%s" target="_blank">Google Supported Languages</a>', 'archia-add-ons' ), 'https://developers.google.com/maps/faq#languagesupport'),
        //     ),

            


        //     array(
        //         "type" => "section",
        //         'id' => 'gmap_section_geolocation',
        //         "title" => __( 'Place Autocomplete', 'archia-add-ons' ),
        //     ),
        //     // https://developers.google.com/places/web-service/supported_types#table2
        //     array(
        //         "type" => "field",
        //         "field_type" => "select",
        //         'id' => 'listing_location_result_type',
        //         "title" => __('Listing Location Type', 'archia-add-ons'),
        //         'args'=> array(
        //             'default'=> 'administrative_area_level_1',
        //             'options'=> array(
        //                 "locality" => __('District','archia-add-ons'), 
        //                 "sublocality" => __('Road','archia-add-ons'), 
        //                 "postal_code" => __('Postal Code','archia-add-ons'), 
        //                 "country" => __('Country','archia-add-ons'), 
        //                 "administrative_area_level_1" => __('State','archia-add-ons'), 
        //                 "administrative_area_level_2" => __('City','archia-add-ons'),
        //             ),
        //         )
        //     ),

        //     array(
        //         "type" => "field",
        //         "field_type" => "text",
        //         'id' => 'listing_address_format',
        //         "title" => __('Or Define Your Address Format', 'archia-add-ons'),
        //         'args'=> array(
        //             'default'=> 'formatted_address',
        //         ),
        //         'desc'  => sprintf( __( 'Define address format will received when user using google autocomplete place service. Address types separated by comma. Available types at <a href="%s" target="_blank">Google Address Types</a>', 'archia-add-ons' ), 'https://developers.google.com/maps/documentation/geocoding/intro#Types'). '<br />'.__( 'Using <strong>formatted_address</strong> for Google formated address.', 'archia-add-ons' ),
        //     ),



            
        //     array(
        //         "type" => "section",
        //         'id' => 'gmap_section_listings',
        //         "title" => __( 'Listings Map', 'archia-add-ons' ),
        //     ),

        //     array(
        //         "type" => "field",
        //         "field_type" => "text",
        //         'id' => 'gmap_default_lat',
        //         'args'=> array(
        //             'default'=> '40.7',
        //         ),
        //         "title" => __('Default Latitude', 'archia-add-ons'),
        //         'desc'  => sprintf( __( 'Enter your address latitude - default: 40.7. You can get value from this <a href="%s" target="_blank">website</a>', 'archia-add-ons' ), esc_url('http://www.gps-coordinates.net/' ) ),
        //     ),

        //     array(
        //         "type" => "field",
        //         "field_type" => "text",
        //         'id' => 'gmap_default_long',
        //         'args'=> array(
        //             'default'=> '-73.87',
        //         ),
        //         "title" => __('Default Longtitude', 'archia-add-ons'),
        //         'desc'  => sprintf( __( 'Enter your address longtitude - default: -73.87. You can get value from this <a href="%s" target="_blank">website</a>', 'archia-add-ons' ), esc_url('http://www.gps-coordinates.net/' ) ),
        //     ),

        //     array(
        //         "type" => "field",
        //         "field_type" => "text",
        //         'id' => 'gmap_default_zoom',
        //         'args'=> array(
        //             'default'=> '10',
        //         ),
        //         "title" => __('Default Zoom', 'archia-add-ons'),
        //         'desc'  => __('Default map zoom level, max: 18', 'archia-add-ons'),
        //     ),


        //     array(
        //         "type" => "field",
        //         "field_type" => "image",
        //         'id' => 'gmap_marker',
        //         "title" => __('Map Marker', 'archia-add-ons'),
        //         // 'args'=> array(
        //         //     'default'=> array(
        //         //         'url' => ESB_DIR_URL ."assets/images/marker.png"
        //         //     )
        //         // ),
                
        //         'desc'  => ''
        //     ),

        // ),
        


        


        

        'widgets' => array(


            array(
                "type" => "section",
                'id' => 'mailchimp_sub_section',
                "title" => __( 'Mailchimp Section', 'archia-add-ons' ),
            ),

            array(
                "type" => "field",
                "field_type" => "text",
                'id' => 'mailchimp_api',
                "title" => __('Mailchimp API key', 'archia-add-ons'),
                'desc'  => '<a href="'.esc_url('http://kb.mailchimp.com/accounts/management/about-api-keys#Finding-or-generating-your-API-key').'" target="_blank">'.esc_html__('Find your API key','archia-add-ons' ).'</a>'
            ),
            array(
                "type" => "field",
                "field_type" => "text",
                'id' => 'mailchimp_list_id',
                "title" => __('Mailchimp List ID', 'archia-add-ons'),
                'desc'  => '<a href="'.esc_url('http://kb.mailchimp.com/lists/managing-subscribers/find-your-list-id').'" target="_blank">'.esc_html__('Find your list ID','archia-add-ons' ).'</a>',
            ),
        
            array(
                "type" => "field",
                "field_type" => "info",
                'id' => 'mailchimp_shortcode',
                "title" => __('Subscribe Shortcode', 'archia-add-ons'),
                'desc'  => wp_kses_post( __('Use the <code><strong>[archia_subscribe]</strong></code> shortcode  to display subscribe form inside a post, page or text widget.
<br />Available Variables:<br />
<code><strong>message</strong></code> (Optional) - The message above subscription form.<br />
<code><strong>placeholder</strong></code> (Optional) - The form placeholder text.<br />
<code><strong>button</strong></code> (Optional) - The submit button text.<br />
<code><strong>list_id</strong></code> (Optional) - List ID. If you want user subscribe to a different list from the option above.<br />
<code><strong>class</strong></code> (Optional) - Your extraclass used to style the form.<br /><br />
Example: <code><strong>[archia_subscribe list_id="b02fb5f96f" class="your_class_here"]</strong></code>', 'archia-add-ons') )
                
            ),

            array(
                "type" => "section",
                'id' => 'tweets_section',
                "title" => __( 'Twitter Feeds Section', 'archia-add-ons' ),
                'callback' => function($arg){ 
                    echo '<p>'.esc_html__('Visit ','archia-add-ons' ).
                        '<a href="'.esc_url('https://apps.twitter.com' ).'" target="_blank">'.esc_html__("Twitter's Application Management",'archia-add-ons' ).'</a> '
                        .__('page, sign in with your account, click on Create a new application and create your own keys if you haven\'t one.<br /> Fill all the fields bellow with those keys.','archia-add-ons' ).
                        '</p>';  
                }
            ),

            array(
                "type" => "field",
                "field_type" => "text",
                'id' => 'consumer_key',
                "title" => __('Consumer Key', 'archia-add-ons'),
                'desc'  => ''
            ),
            array(
                "type" => "field",
                "field_type" => "text",
                'id' => 'consumer_secret',
                "title" => __('Consumer Secret', 'archia-add-ons'),
                'desc'  => ''
            ),
            array(
                "type" => "field",
                "field_type" => "text",
                'id' => 'access_token',
                "title" => __('Access Token', 'archia-add-ons'),
                'desc'  => ''
            ),
            array(
                "type" => "field",
                "field_type" => "text",
                'id' => 'access_token_secret',
                "title" => __('Access Token Secret', 'archia-add-ons'),
                'desc'  => ''
            ),
            array(
                "type" => "field",
                "field_type" => "info",
                'id' => 'tweets_shortcode',
                "title" => __('Access Token Secret', 'archia-add-ons'),
                'desc'  => wp_kses_post( __('You can use <code><strong>archia Twitter Feed</strong></code> widget or  <code><strong>[archia_tweets]</strong></code> shortcode  to display tweets inside a post, page or text widget.
<br />Available Variables:<br />
<code><strong>username</strong></code> (Optional) - Option to load tweets from another account. Leave this empty to load from your own.<br />
<code><strong>list</strong></code> (Optional) - List name to load tweets from. If you define list name you also must define the <strong>username</strong> of the list owner.<br />
<code><strong>hashtag</strong></code> (Optional) - Option to load tweets with a specific hashtag.<br />
<code><strong>count</strong></code> (Required) - Number of tweets you want to display.<br />
<code><strong>list_ticker</strong></code> (Optional) - Display tweets as a list ticker?. Values: <strong>yes</strong> or <strong>no</strong><br />
<code><strong>follow_url</strong></code> (Optional) - Follow us link.<br />
<code><strong>extraclass</strong></code> (Optional) - Your extraclass used to style the form.<br /><br />
Example: <code><strong>[archia_tweets count="3" username="CTHthemes" list_ticker="no" extraclass="your_class_here"]</strong></code>', 'archia-add-ons') )
                
            ),
            // api weather
            array(
                "type" => "section",
                'id' => 'weather_api_section',
                "title" => __( 'Weather Section', 'archia-add-ons' ),
            ),

            array(
                "type" => "field",
                "field_type" => "text",
                'id' => 'weather_api',
                "title" => __('Weather API key', 'archia-add-ons'),
                'desc'  => '<a href="'.esc_url('https://openweathermap.org/api').'" target="_blank">'.esc_html__('Find your API key','archia-add-ons' ).'</a>'
            ),
            // socials share
            array(
                "type" => "section",
                'id' => 'widgets_section_3',
                "title" => __( 'Socials Share', 'archia-add-ons' ),
            ),
            array(
                "type" => "field",
                "field_type" => "text",
                'id' => 'widgets_share_names',
                "title" => __('Socials Share', 'archia-add-ons'),
                'desc'  => __('Enter your social share names separated by a comma.<br /> List bellow are available names:<strong><br /> facebook,twitter,linkedin,in1,tumblr,digg,googleplus,reddit,pinterest,stumbleupon,email,vk</strong>', 'archia-add-ons'),
                'args'=> array(
                    'default' => 'facebook, pinterest, googleplus, twitter, linkedin'
                ),
            ),


        ),
        // end tab array

        // end tab array
        'maintenance' => array(
            array(
                "type" => "section",
                'id' => 'maintenance_section_1',
                "title" => __( 'Status', 'archia-add-ons' ),
            ),

            array(
                "type" => "field",
                "field_type" => "radio",
                'id' => 'maintenance_mode',
                "title" => __('Mode', 'archia-add-ons'),
                'args'=> array(
                    'default'=> 'disable',
                    'options'=> array(
                        'disable' => __( 'Disable', 'archia-add-ons' ),
                        // 'maintenance' => __( 'Maintenance', 'archia-add-ons' ),
                        'coming_soon' => __( 'Coming Soon', 'archia-add-ons' ),
                    ),
                    'options_block' => true
                )
            ),
//             array(
//                 "type" => "section",
//                 'id' => 'maintenance_section_2',
//                 "title" => __( 'Maintenance Options', 'archia-add-ons' ),
//             ),

//             array(
//                 "type" => "field",
//                 "field_type" => "textarea",
//                 'id' => 'maintenance_msg',
//                 "title" => __('Message', 'archia-add-ons'),
//                 'args' => array(
//                     'default'  => '<h3 class="soon-title">We\'ll be right back!</h3>
// <p>We are currently performing some quick updates. Leave us your email and we\'ll let you know as soon as we are back up again.</p>
// [archia_subscribe message=""]
// <div class="cs-social fl-wrap">
// <ul>
// <li><a href="#" target="_blank" ><i class="fa fa-facebook-official"></i></a></li>
// <li><a href="#" target="_blank"><i class="fa fa-twitter"></i></a></li>
// <li><a href="#" target="_blank" ><i class="fa fa-chrome"></i></a></li>
// <li><a href="#" target="_blank" ><i class="fa fa-vk"></i></a></li>
// <li><a href="#" target="_blank" ><i class="fa fa-whatsapp"></i></a></li>
// </ul>
// </div>',
//                 ),
//                 'desc'  => ''
//             ),

            array(
                "type" => "section",
                'id' => 'maintenance_section_3',
                "title" => __( 'Coming Soon Options', 'archia-add-ons' ),
            ),
            array(
                "type" => "field",
                "field_type" => "textarea",
                'id' => 'coming_soon_msg',
                "title" => __('Message', 'archia-add-ons'),
                'args' => array(
                    'default'  => '<h3 class="soon-title">Our website is coming soon!</h3>',
                ),
                'desc'  => ''
            ),

            array(
                "type" => "field",
                "field_type" => "text",
                'id' => 'coming_soon_email',
                "title" => __('Owner Email', 'archia-add-ons'),
                'args' => array(
                    'default'  => 'support@domain.com',
                ),
                'desc'  => '',
            ),

            array(
                "type" => "field",
                "field_type" => "text",
                'id' => 'coming_soon_date',
                "title" => __('Coming Soon Date', 'archia-add-ons'),
                'args' => array(
                    'default'  => '09/12/2021',
                ),
                'desc'  => __('The date should be DD/MM/YYYY format. Ex: 09/12/2021', 'archia-add-ons'),
            ),
            array(
                "type" => "field",
                "field_type" => "text",
                'id' => 'coming_soon_time',
                "title" => __('Coming Soon Time', 'archia-add-ons'),
                'args' => array(
                    'default'  => '10:30:00',
                ),
                'desc'  => __('The time should be hh:mm:ss format. Ex: 10:30:00', 'archia-add-ons'),
            ),

            array(
                "type" => "field",
                "field_type" => "number",
                'id' => 'coming_soon_tz',
                "title" => __('Timezone Offset', 'archia-add-ons'),
                'args' => array(
                    'default'  => '0',
                    'min'  => '-12',
                    'max'  => '14',
                    'step'  => '1',
                ),
                'desc'  => __('Timezone offset value from UTC', 'archia-add-ons'),
            ),
            array(
                "type" => "field",
                "field_type" => "textarea",
                'id' => 'coming_soon_msg_after',
                "title" => __('Message After', 'archia-add-ons'),
                'args' => array(
                    'default'  => '<h2><span>Coming soon</span><strong>days</strong></h2>
[archia_subscribe]',
                ),
                'desc'  => ''
            ),

            array(
                "type" => "field",
                "field_type" => "image",
                'id' => 'coming_soon_bg',
                "title" => __('Background', 'archia-add-ons'),
                'desc'  => ''
            ),


        ),
        // end tab array



    );
}