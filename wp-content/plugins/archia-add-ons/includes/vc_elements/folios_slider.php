<?php
vc_map(array(
    "name"        => __("Portfolios Slider", 'archia-add-ons'),
    // "description" => __("Building portfolio element", 'archia-add-on'),
    "base"        => "folios_slider",
    "class"       => "",
    "icon"        => BBT_DIR_URL . "assets/cththemes-logo.png",
    "category"    => __('Archia Theme', 'archia-add-ons'),
    "params"      => array(
        array(
            "type"        => "textfield",
            "heading"     => esc_html__("Portfolio Category IDs to include", 'archia-add-ons'),
            "param_name"  => "cat_ids",
            "description" => esc_html__("Enter portfolio category ids to include, separated by a comma. Leave empty to display all categories.", 'archia-add-ons'),
            "value"       => '',
        ),
        array(
            "type"          => "textfield", 
            "admin_label"   => true,
            "heading"       => esc_html__("Project Category IDs to exclude", 'archia-add-ons'), 
            "param_name"    => "cat_ids_not", 
            "description"   => esc_html__("Enter Project category ids to exclude, separated by a comma. Leave empty to display all categories. Will be ignore if include para above not empty.", 'archia-add-ons'),
            "value"         => '',
        ), 
        array(
            "type"       => "dropdown",
            "class"      => "",
            "heading"    => esc_html__('Order Portfolio Categories by', 'archia-add-ons'),
            "param_name" => "cat_order_by",
            "value"      => array(
                esc_html__('Name', 'archia-add-ons')  => 'name',
                esc_html__('ID', 'archia-add-ons')    => 'id',
                esc_html__('Count', 'archia-add-ons') => 'count',
                esc_html__('Slug', 'archia-add-ons')  => 'slug',
                esc_html__('None', 'archia-add-ons')  => 'none',
            ),
            "std"        => 'name',
        ),
        array(
            "type"       => "dropdown",
            "class"      => "",
            "heading"    => esc_html__('Sort Order', 'archia-add-ons'),
            "param_name" => "cat_order",
            "value"      => array(
                esc_html__('Ascending', 'archia-add-ons')  => 'ASC',
                esc_html__('Descending', 'archia-add-ons') => 'DESC',

            ),
            "std"        => 'ASC',
        ),
        array(
            "type"       => "dropdown",
            "class"      => "",
            "heading"    => esc_html__('Show Filter', 'archia-add-ons'),
            "param_name" => "show_filter",
            "value"      => array(
                esc_html__('Yes', 'archia-add-ons') => 'yes',
                esc_html__('No', 'archia-add-ons')  => 'no',
            ),
            "std"        => 'yes',
        ),

        array(
            "type"        => "textfield",
            'admin_label'       => true,
            "heading"     => esc_html__("Enter Portfolio IDs", 'archia-add-ons'),
            "param_name"  => "ids",
            "description" => esc_html__("Enter portfolio ids to show, separated by a comma. Leave empty to get all.", 'archia-add-ons'),
        ),
        array(
            "type"        => "textfield",
            // "holder" => "div",
            "heading"     => esc_html__("Portfolio IDs to Exclude", 'archia-add-ons'),
            "param_name"  => "ids_not",
            "description" => esc_html__("Enter portfolio ids to exclude, separated by a comma. Use if the field above is empty. Leave empty to get all.", 'archia-add-ons'),
        ),
        array(
            "type"        => "dropdown",
            "class"       => "",
            "heading"     => esc_html__('Order Portfolios by', 'archia-add-ons'),
            "param_name"  => "order_by",
            "value"       => array(
                esc_html__('Date', 'archia-add-ons') => 'date' , 
                esc_html__('ID', 'archia-add-ons')  =>'ID' , 
                esc_html__('Author', 'archia-add-ons') =>'author', 
                esc_html__('Title', 'archia-add-ons') =>'title', 
                esc_html__('Modified', 'archia-add-ons') =>'modified',
                esc_html__('Random', 'archia-add-ons')=> 'rand' ,
                esc_html__('Comment Count', 'archia-add-ons') =>'comment_count',
                esc_html__('Menu Order', 'archia-add-ons')=>'menu_order' ,
                esc_html__('ID order given (post__in)', 'archia-add-ons') => 'post__in',
            ),
            "description" => esc_html__("Order Portfolios by", 'archia-add-ons'),
            "std"         => 'date',
        ),
        array(
            "type"        => "dropdown",
            "class"       => "",
            "heading"     => esc_html__('Order Portfolios', 'archia-add-ons'),
            "param_name"  => "order",
            "value"       => array(
                esc_html__('Ascending', 'archia-add-ons')  => 'ASC',
                esc_html__('Descending', 'archia-add-ons') => 'DESC',

            ),
            "description" => esc_html__("Order Portfolios", 'archia-add-ons'),
            "std"         => 'DESC',
        ),
        array(
            "type"        => "textfield",
            'admin_label'       => true,
            "heading"     => esc_html__("Post to show", 'archia-add-ons'),
            "param_name"  => "posts_per_page",
            "description" => esc_html__("Number of portfolio items to show (-1 for all).", 'archia-add-ons'),
            "value"       => '12',
        ),

        array(
            "type"       => "dropdown",
            "class"      => "",
            "heading"    => esc_html__('Content Width', 'archia-add-ons'),
            "param_name" => "folio_content_width",
            "value"      => array(
                esc_html__('Small Boxed', 'archia-add-ons') => 'boxed-container',
                esc_html__('Wide Boxed', 'archia-add-ons')  => 'big-container',
                esc_html__('Fullwidth', 'archia-add-ons')   => 'full-container',

            ),

            "std"        => 'full-container',
        ),
        array(
            "type"        => "textfield",
            "heading"     => __("Extra class name", 'archia-add-ons'),
            "param_name"  => "el_class",
            "description" => __("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", 'archia-add-ons'),
        ),
    ),
));

if (class_exists('WPBakeryShortCode')) {
    class WPBakeryShortCode_Folios_Slider extends WPBakeryShortCode{}
}
