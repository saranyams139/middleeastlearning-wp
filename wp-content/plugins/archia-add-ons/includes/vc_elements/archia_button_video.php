<?php
vc_map( array(
    "name"      => __("Popup Video", 'archia-add-ons'),
    "description" => __("Popup video button",'archia-add-ons'),
    "base"      => "archia_button_video",
    "class"     => "",
    "icon"      => BBT_DIR_URL . "/assets/cththemes-logo.png",
    "category"  => __('Archia Theme', 'archia-add-ons'),
    "params"    => array(
        array(
            "type"      => "dropdown",
            "class"     =>"",
            "heading"   => __('Button Style', 'archia-add-ons'),
            "param_name" => "btn_style",
            "value"     => array(   
                            __('Default', 'archia-add-ons') => 'popvid-default',  
                            __('Colored', 'archia-add-ons') => 'popvid-colored',  
                            __('Medium Button', 'archia-add-ons') => 'popvid-medium',
                            __('Small Button', 'archia-add-ons') => 'popvid-small',
                            __('Bordered Box', 'archia-add-ons') => 'popvid-bordered-box',
                        ),
            "std" => 'popvid-default',
            // "description" => __("Choose section title location", 'archia-add-ons'), 
        ), 
        array(
            'type' => 'iconpicker',
            'heading' => esc_html__( 'Icon', 'archia-add-ons' ),
            'param_name' => 'icon',
            'settings' => array(
                'emptyIcon' => false, // default true, display an "EMPTY" icon?
                'type' => 'fontawesome',
                'iconsPerPage' => 200, // default 100, how many icons per/page to display
            ),
            "description"   => "Choose icon",
            "std"         => "fa fa-play",
        ),

        array(
            "type"      => "textfield",
            // "holder"    => "div",
            "admin_label"   => true,
            "heading"   => esc_html__("Video Source", 'archia-add-ons'),
            "param_name"=> "vid_source",
            "value"     => 'https://www.youtube.com/watch?v=Dj6CKxQue7U',
            "description" => esc_html__("Enter your video source.", 'archia-add-ons')
        ),
        
        array(
            "type"      => "textfield",
            'admin_label'       => true,
            "heading"   => __("Title", 'archia-add-ons'),
            "param_name"=> "title",
            "value"     => "",
            // "description" => __("Enter section title (Note: you can leave it empty).", 'archia-add-ons')
        ),

        array(
            "type"        => "textarea",
            "heading"     => __("Content", 'archia-add-ons'),
            "param_name"  => "content",
            "value"       => "",
            // "description" => __("Description", 'archia-add-ons'),
        ),

        array(
            "type"      => "dropdown",
            "class"     =>"",
            "heading"   => __('Button Alignment', 'archia-add-ons'),
            "param_name" => "btn_ali",
            "value"     => array(   
                            __('Left', 'archia-add-ons') => 'text-left',  
                            __('Center', 'archia-add-ons') => 'text-center',
                            __('Right', 'archia-add-ons') => 'text-right',
                        ),
            "std" => 'text-center',
            // "description" => __("Choose section title location", 'archia-add-ons'), 
        ), 
        array(
            "type" => "dropdown",  
            "admin_label"   => true,
            "heading" => __('CSS Animation', 'archia-add-ons'), 
            "param_name" => "ani_type", 
            "value" => archia_addons_vc_animation(),
            "description" => __("Select type of animation for element to be animated when it \"enters\" the browsers viewport (Note: works only in modern browsers).", 'archia-add-ons'), 
            "std" => '',
        ),
        array(
            "type" => "textfield",
            "admin_label"   => true,
            "heading" => esc_html__("Animation Duration", 'archia-add-ons'),
            "param_name" => "ani_time",
            "description" => esc_html__("Ex: 0.2s for 200 miliseconds, 2s for 2 second", 'archia-add-ons'),
            "value" => "2s",
            'dependency' => array(
                'element' => 'ani_type',
                'not_empty' => true,
            ),
        ),
        array(
            "type" => "textfield",
            "admin_label"   => true,
            "heading" => esc_html__("Animation delay", 'archia-add-ons'),
            "param_name" => "ani_delay",
            "description" => esc_html__("Ex: 0.2s for 200 miliseconds, 2s for 2 second", 'archia-add-ons'),
            "value" => "0.2s",
            'dependency' => array(
                'element' => 'ani_type',
                'not_empty' => true,
            ),
        ),
        array(
            "type"      => "textfield",
            "heading"   => __("Extra class name", 'archia-add-ons'),
            "param_name" => "el_class",
            "description" => __("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", 'archia-add-ons')
        ),
        array(
            'type'          => 'css_editor',
            'heading'       => esc_html__( 'Css', 'archia-add-ons' ),
            'param_name'    => 'css',
            'group'         => esc_html__( 'Design options', 'archia-add-ons' ),
        ),
    )
));

if ( class_exists( 'WPBakeryShortCode' ) ) {
    class WPBakeryShortCode_Archia_Button_Video extends WPBakeryShortCode {}
}