<?php
vc_map(array(
    "name"        => __("Navigation Bar", 'archia-add-ons'),
    // "description" => __("Logo Theme", 'archia-add-on'),
    "base"        => "cth_header_navbar",
    "class"       => "",
    "icon"        => BBT_DIR_URL . "assets/cththemes-logo.png",
    "category"    => 'Archia Header',
    "params"      => array(
        array(
            "type"        => "attach_image",
            "class"       => "",
            "heading"     => __("Custom Logo", 'archia-add-ons'),
            "param_name"  => "custom_logo",
            "value"       => "",
            "description" => __("For custom logo instead of option from customize.", 'archia-add-ons'),
        ),
        array(
            "type"        => "textfield",
            "heading"     => __("Extra class name", 'archia-add-ons'),
            "param_name"  => "el_class",
            "description" => __("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", 'archia-add-ons'),
        ),
    ),
));
if (class_exists('WPBakeryShortCode')) {
    class WPBakeryShortCode_Cth_Header_Navbar extends WPBakeryShortCode
    {}
}