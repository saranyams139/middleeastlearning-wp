<?php
/* add_ons_php */
vc_map( array(
    "name"                      => esc_html__("Folio Images Slider", 'archia-add-ons'),
    "base"                      => "folio_manu_slider",
    "icon"                      => BBT_DIR_URL . "/assets/cththemes-logo.png",
    "category"                  => __( 'Archia Theme', 'archia-add-ons' ),
    "content_element"           => true,
    "show_settings_on_create"   => true,
    "params"    => array(

        array(
            'type' => 'dropdown',
            'heading' => __( 'Slider Type', 'archia-add-ons' ),
            'value' => array(
                __( 'Background Image', 'archia-add-ons' ) => 'sld-type-bg',
                __( 'Normal Type', 'archia-add-ons' ) => 'sld-type-normal',
            ),
            'param_name' => 'slider_type',
            "std"         => "sld-type-bg",
            // 'description' => __( 'Select icon library.', 'js_composer' ),
        ),

        array(
            'type' => 'dropdown',
            'heading' => __( 'Layout Style', 'archia-add-ons' ),
            'value' => array(
                __( 'Style 1', 'archia-add-ons' ) => 'project-style-1',
                __( 'Style 2', 'archia-add-ons' ) => 'project-style-2',
            ),
            'param_name' => 'slider_style',
            "std"         => "project-style-2",
            // 'description' => __( 'Select icon library.', 'js_composer' ),

            'dependency' => array(
                'element' => 'slider_type',
                'value' => array('sld-type-bg'),
            ),
        ),

        array(
            'type' => 'param_group',
            'heading' => esc_html__( 'Slides', 'archia-add-ons' ),
            'param_name' => 'slides',
            // 'description' => esc_html__( 'Enter values for graph - value, title and color.', 'inshot' ),
            'value' => urlencode( json_encode( array(
                array(
                    'sltitle' => '',
                    'excerpt' => '',
                    'img' => '297',
                    'link' => 'url:http%3A%2F%2Flocalhost%3A8888%2Farchia%2Fportfolios%2Fportfolio-masonary%2F|title:View%20Portfolio||',
                    'details' => '%5B%7B%22title%22%3A%22Clients%3A%22%2C%22value%22%3A%22%3Cspan%3EEthan%20Hunt%3C%2Fspan%3E%22%7D%2C%7B%22title%22%3A%22Completion%3A%22%2C%22value%22%3A%22%3Cspan%3EFebruary%205th%2C%202019%3C%2Fspan%3E%22%7D%2C%7B%22title%22%3A%22Project%20Type%3A%22%2C%22value%22%3A%22%3Cspan%3EVilla%2C%20Residence%3C%2Fspan%3E%22%7D%2C%7B%22title%22%3A%22Architects%3A%22%2C%22value%22%3A%22%3Cspan%3ELogan%20Cee%3C%2Fspan%3E%22%7D%5D',
                ),
                array(
                    'sltitle' => '',
                    'excerpt' => '',
                    'img' => '323',
                    'link' => 'url:http%3A%2F%2Flocalhost%3A8888%2Farchia%2Fportfolios%2Fportfolio-tiles-filter%2F|title:View%20Portfolio||',
                    'details' => '%5B%7B%22title%22%3A%22Clients%3A%22%2C%22value%22%3A%22%3Cspan%3EEthan%20Hunt%3C%2Fspan%3E%22%7D%2C%7B%22title%22%3A%22Completion%3A%22%2C%22value%22%3A%22%3Cspan%3EFebruary%205th%2C%202019%3C%2Fspan%3E%22%7D%2C%7B%22title%22%3A%22Project%20Type%3A%22%2C%22value%22%3A%22%3Cspan%3EVilla%2C%20Residence%3C%2Fspan%3E%22%7D%2C%7B%22title%22%3A%22Architects%3A%22%2C%22value%22%3A%22%3Cspan%3ELogan%20Cee%3C%2Fspan%3E%22%7D%5D',
                ),
            ) ) ),
            'params' => array(

                array(
                    "type"          => "attach_image",
                    "heading"       => __("Slide Image", 'archia-add-ons'),
                    "param_name"    => "img",
                    // "description"   => __("Choose a Image", 'archia-add-ons'),
                ), 
                array(
                    'type' => 'textfield',
                    'heading' => esc_html__( 'Title', 'archia-add-ons' ),
                    'param_name' => 'sltitle',
                    // 'description' => esc_html__( 'Enter text used as title of bar.', 'archia-add-ons' ),
                    'admin_label' => true,
                ),
                array(
                    'type' => 'textarea',
                    'heading' => esc_html__( 'Description', 'archia-add-ons' ),
                    'param_name' => 'excerpt',
                    // 'description' => esc_html__( 'Enter text used as title of bar.', 'archia-add-ons' ),
                    // 'admin_label' => true,
                ),
                
                array(
                    'type' => 'param_group',
                    'heading' => esc_html__( 'Details', 'archia-add-ons' ),
                    'param_name' => 'details',
                    // 'description' => esc_html__( 'Enter values for graph - value, title and color.', 'inshot' ),
                    'value' => urlencode( json_encode( array(
                        array(
                            'title' => 'Clients:',
                            'value' => '<span>Ethan Hunt</span>',
                        ),
                        array(
                            'title' => 'Completion:',
                            'value' => '<span>February 5th, 2019</span>',
                        ),
                        array(
                            'title' => 'Project Type:',
                            'value' => '<span>Villa, Residence</span>',
                        ),
                        array(
                            'title' => 'Architects:',
                            'value' => '<span>Logan Cee</span>',
                        ),
                    ) ) ),
                    'params' => array(
                        array(
                            'type' => 'textfield',
                            'heading' => esc_html__( 'Title', 'archia-add-ons' ),
                            'param_name' => 'title',
                            'description' => esc_html__( 'Enter text used as title of bar.', 'archia-add-ons' ),
                            'admin_label' => true,
                        ),
                        array(
                            'type' => 'textarea',
                            'heading' => esc_html__( 'Value', 'archia-add-ons' ),
                            'param_name' => 'value',
                            'description' => esc_html__( 'Enter value of bar.', 'archia-add-ons' ),
                            'admin_label' => true,
                        ),
                    ),
                ),
                // end child param group
                array(
                    "type"        => "vc_link",
                    "heading"     => __("Link", 'archia-add-ons'),
                    "param_name"  => "link",
                    "value"       => "#",
                    // "description"   => esc_html__( 'Enter links for each partner (Note: divide links with linebreaks (Enter) or | and no spaces)', 'archia-add-ons' ). get_post_type_archive_link( 'listing' ),
                ),
            ),
        ),

    

        array(
            "type" => "textfield",
            // "holder"    => "div",
            // "admin_label"   => true,
            "heading" => esc_html__("Thumbnail size", 'archia-add-ons'),
            "param_name" => "thumbnail_size",
            "description" => esc_html__('Enter thumbnail size. Example: thumbnail, medium, large, full or other sizes defined by current theme. Alternatively enter image size in pixels: 200x100 (Width x Height).','archia-add-ons' ),
            "value"=> 'archia-slider',
        ),

        array(
            "type" => "dropdown",  
            "admin_label"   => true,
            "heading" => __('CSS Animation', 'archia-add-ons'), 
            "param_name" => "ani_type", 
            "value" => archia_addons_vc_animation(),
            "description" => __("Select type of animation for element to be animated when it \"enters\" the browsers viewport (Note: works only in modern browsers).", 'archia-add-ons'), 
            "std" => '',
        ),
        array(
            "type" => "textfield",
            "admin_label"   => true,
            "heading" => esc_html__("Animation Duration", 'archia-add-ons'),
            "param_name" => "ani_time",
            "description" => esc_html__("Ex: 0.2s for 200 miliseconds, 2s for 2 second", 'archia-add-ons'),
            "value" => "2s",
            'dependency' => array(
                'element' => 'ani_type',
                'not_empty' => true,
            ),
        ),
        array(
            "type" => "textfield",
            "admin_label"   => true,
            "heading" => esc_html__("Animation delay", 'archia-add-ons'),
            "param_name" => "ani_delay",
            "description" => esc_html__("Ex: 0.2s for 200 miliseconds, 2s for 2 second", 'archia-add-ons'),
            "value" => "0.2s",
            'dependency' => array(
                'element' => 'ani_type',
                'not_empty' => true,
            ),
        ),

        array(
            "type" => "textfield",
            "heading" => esc_html__("Extra class name", 'archia-add-ons'),
            "param_name" => "el_class",
            "description" => esc_html__("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", 'archia-add-ons')
        ),
        array(
            'type' => 'css_editor',
            'heading' => esc_html__( 'Css', 'archia-add-ons' ),
            'param_name' => 'css',
            'group' => esc_html__( 'Design options', 'archia-add-ons' ),
        ),
    ),
    // 'admin_enqueue_js'      => TYPHOON_ADD_ONS_DIR_URL . "assets/js/wpbakerry-eles.js", // need "holder"    => "div" and "class"     => "cth-vc-images" option for attach_image, attach_images type
    // 'js_view'=> 'CTHVCImages',
));

if ( class_exists( 'WPBakeryShortCode' ) ) {
    class WPBakeryShortCode_Folio_Manu_Slider extends WPBakeryShortCode {}
}