<?php
/* add_ons_php */
vc_map( array(
    "name"                      => esc_html__("Testimonials Slider", 'archia-add-ons'),
    // "description"               => esc_html__("Special Menu",'typhoon-add-ons'),
    "base"                      => "archia_testimonial",
    "icon"                      => BBT_DIR_URL . "/assets/cththemes-logo.png",
    "category"                  => __( 'Archia Theme', 'archia-add-ons' ),
    "content_element"           => true,
    "show_settings_on_create"   => true,
    "params"                    => array(
 
        array(
              "type"        => "dropdown",
              "heading"     => __("Choose Style", 'archia-add-ons'),
              "param_name"  => "style",
              "admin_label" => true,
              "value"       => array(
                    esc_html__('Default', 'archia-add-ons') => '' ,
                    esc_html__('Style 1', 'archia-add-ons') =>'style-1' , 
                    esc_html__('Style 2', 'archia-add-ons') =>'style-2', 
                ), //value
              "std"         => "",
        ),
        array(
            "type"          => "textfield", 
            "admin_label"   => true,
            // "holder"        => "div",
            "heading"       => esc_html__("Enter Testimonial IDs", 'archia-add-ons'), 
            "param_name"    => "ids", 
            "description"   => esc_html__("Enter testimonial ids to show, separated by a comma.", 'archia-add-ons')
        ), 
        array(
            "type"          => "textfield", 
            "admin_label"   => true,
            // "holder"        => "div",
            "heading"       => esc_html__("Testimonial IDs to Exclude", 'archia-add-ons'), 
            "param_name"    => "ids_not", 
            "description"   => esc_html__("Enter testimonial ids to exclude, separated by a comma. Use if the field above is empty. Leave empty to get all.", 'archia-add-ons')
        ), 
        array(
              "type"        => "dropdown",
              "heading"     => __("Choose the dominant color", 'archia-add-ons'),
              "param_name"  => "col_content",
              "admin_label" => true,
              "value"       => array(
                    esc_html__('Theme Color', 'archia-add-ons') => 'theme' ,
                    esc_html__('White', 'archia-add-ons')  =>'white' , 
                    esc_html__('Dark', 'archia-add-ons') =>'dark', 
                ), //value
              "std"         => "theme",
              'description' => 'Choose the dominant color to be displayed',
        ),
        array(
            "type" => "dropdown", 
            "heading" => __('Order Testimonials by', 'archia-add-ons'), 
            "param_name" => "order_by", 
            "value" => array(
                esc_html__('Date', 'archia-add-ons') => 'date' , 
                esc_html__('ID', 'archia-add-ons')  =>'ID' , 
                esc_html__('Author', 'archia-add-ons') =>'author', 
                esc_html__('Title', 'archia-add-ons') =>'title', 
                esc_html__('Modified', 'archia-add-ons') =>'modified',
                esc_html__('Random', 'archia-add-ons')=> 'rand' ,
                esc_html__('Comment Count', 'archia-add-ons') =>'comment_count',
                esc_html__('Menu Order', 'archia-add-ons')=>'menu_order' ,
                esc_html__('ID order given (post__in)', 'archia-add-ons') => 'post__in',


            ), 
            // "description" => __("Order Speakers by", 'cth-gather-plugins'), 
            "std" => 'date',
        ), 
        array(
            "type" => "dropdown", 
            "heading" => __('Sort Order', 'archia-add-ons'), 
            "param_name" => "order", 
            "value" => array(
                
                __('Descending', 'archia-add-ons') => 'DESC', 
                __('Ascending', 'archia-add-ons') => 'ASC',
                
            ), 
            // "description" => __("Order Speakers", 'cth-gather-plugins'),
            "std" => 'DESC',
        ), 
        array(
            "type"          => "textfield",
            "admin_label"   => true,
            "heading"       => esc_html__("Posts per page", 'archia-add-ons'),
            "param_name"    => "posts_per_page",
            "description"   => esc_html__("Number of testimonaial items to show (-1 for all).", 'archia-add-ons'),
            "value"         => '3',
        ),

        array(
            "type" => "textfield",
            "admin_label"   => true,
            "heading" => esc_html__("responsive", 'archia-add-ons'),
            "param_name" => "responsive",
            "description" => esc_html__("The format is: screen-size:number-items-display,larger-screen-size:number-items-display. Ex: 320:1,768:1,992:3,1200:3", 'archia-add-ons'),
            "value" => "320:1,768:1,992:3,1200:3"
        ),

        array(
            "type" => "dropdown",  
            "admin_label"   => true,
            "heading" => __('Spacing', 'archia-add-ons'), 
            "param_name" => "spacing", 
            "value" => array(
                esc_html__('None', 'archia-add-ons') => '0' , 
                esc_html__('1px', 'archia-add-ons')  =>'1' , 
                esc_html__('2px', 'archia-add-ons') =>'2', 
                esc_html__('3px', 'archia-add-ons') =>'3', 
                esc_html__('4px', 'archia-add-ons') =>'4',
                esc_html__('5px', 'archia-add-ons')=> '5' ,
                esc_html__('10px', 'archia-add-ons') =>'10',
                esc_html__('15px', 'archia-add-ons')=>'15' ,
                esc_html__('20px', 'archia-add-ons') => '20',
                esc_html__('25px', 'archia-add-ons') => '25',
                esc_html__('30px', 'archia-add-ons') => '30',


            ), 
            "description" => __("Spacing between slide items", 'archia-add-ons'), 
            "std" => '0',
        ), 


        
        array(
            "type"          => "checkbox",
            "admin_label"   => true,
            // "holder"        => "div",
            "heading"       => esc_html__("Show Name", 'archia-add-ons'),
            "param_name"    => "show_title",
            // "description"   => esc_html__("", 'typhoon-add-ons'),
            "value"         => array(
                __('Yes', 'archia-add-ons') => 'yes', 
            ),
            'std'           => 'yes'
        ),

        array(
            "type"          => "checkbox",
            "admin_label"   => true,
            "heading"       => esc_html__("Show Avatar", 'archia-add-ons'),
            "param_name"    => "show_avatar",
            "value"         => array(
                __('Yes', 'archia-add-ons') => 'yes', 
            ),
            'std'           => 'yes'
        ),

        array(
            "type"          => "checkbox",
            "admin_label"   => true,
            // "holder"        => "div",
            "heading"       => esc_html__("Show Rating", 'archia-add-ons'),
            "param_name"    => "show_rating",
            // "description"   => esc_html__("", 'typhoon-add-ons'),
            "value"         => array(
                __('Yes', 'archia-add-ons') => 'yes', 
            ),
            'std'           => 'yes'
        ),


        array(
            "type" => "textfield",
            "admin_label"   => true,
            "heading" => esc_html__("Speed", 'archia-add-ons'),
            "param_name" => "speed",
            "value"=>'1300',
            "description" => esc_html__("Duration of transition between slides (in ms). Default: 1300", 'archia-add-ons')
        ),

        array(
            "type" => "checkbox",
            "admin_label"   => true,
            "heading" => esc_html__("Auto Play", 'archia-add-ons'),
            "param_name" => "autoplay",
     
            "value" => array( 
                esc_html__('Yes', 'archia-add-ons') => 'yes',   
            ),
            "std"=>'', 
        ),

        array(
            "type" => "checkbox",
            "admin_label"   => true,
            "heading" => esc_html__('Loop', 'archia-add-ons'),
            "param_name" => "loop",
            "value" => array(    
                            esc_html__('Yes', 'archia-add-ons') => 'yes',                                                                                
                        ),
            "description" => esc_html__("Set this to Yes to enable continuous loop mode", 'archia-add-ons'), 
            'std'=>'yes'
        ),

        array(
            "type" => "checkbox",
            "admin_label"   => true,
            "heading" => esc_html__("Show Navigation", 'archia-add-ons'),
            "param_name" => "show_navigation",
     
            "value" => array( 
                esc_html__('Yes', 'archia-add-ons') => 'yes',   
            ),
            "std"=>'yes', 
        ),
        array(
            "type" => "checkbox",
            "admin_label"   => true,
            "heading" => esc_html__("Show Dots", 'archia-add-ons'),
            "param_name" => "show_dots",
     
            "value" => array( 
                esc_html__('Yes', 'archia-add-ons') => 'yes',   
            ),
            "std"=>'', 
        ),

        array(
            "type" => "dropdown",  
            "admin_label"   => true,
            "heading" => __('CSS Animation', 'archia-add-ons'), 
            "param_name" => "ani_type", 
            "value" => archia_addons_vc_animation(),
            "description" => __("Select type of animation for element to be animated when it \"enters\" the browsers viewport (Note: works only in modern browsers).", 'archia-add-ons'), 
            "std" => '',
        ),
        array(
            "type" => "textfield",
            "admin_label"   => true,
            "heading" => esc_html__("Animation Duration", 'archia-add-ons'),
            "param_name" => "ani_time",
            "description" => esc_html__("Ex: 0.2s for 200 miliseconds, 2s for 2 second", 'archia-add-ons'),
            "value" => "2s",
            'dependency' => array(
                'element' => 'ani_type',
                'not_empty' => true,
            ),
        ),
        array(
            "type" => "textfield",
            "admin_label"   => true,
            "heading" => esc_html__("Animation delay", 'archia-add-ons'),
            "param_name" => "ani_delay",
            "description" => esc_html__("Ex: 0.2s for 200 miliseconds, 2s for 2 second", 'archia-add-ons'),
            "value" => "0.2s",
            'dependency' => array(
                'element' => 'ani_type',
                'not_empty' => true,
            ),
        ),


        array(
                "type"          => "textfield",
                "heading"       => esc_html__("Extra class name", 'archia-add-ons'),
                "param_name"    => "el_class",
                "description"   => esc_html__("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", 'archia-add-ons'),
                "value"         => "",
            ),

        array(
                'type'          => 'css_editor',
                'heading'       => esc_html__( 'Css', 'archia-add-ons' ),
                'param_name'    => 'css',
                'group'         => esc_html__( 'Design options', 'archia-add-ons' ),
             ),
        )
));
if ( class_exists( 'WPBakeryShortCode' ) ) {
    class WPBakeryShortCode_Archia_Testimonial extends WPBakeryShortCode {}
}