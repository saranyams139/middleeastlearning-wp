<?php
vc_map(array(
    "name"                    => __("Flexslider", 'archia-add-ons'),
    "description"             => __("Slider using flexslider plugin", 'archia-add-ons'),
    "base"                    => "cth_flexslider",
    "category"                => __('Archia Theme', 'archia-add-ons'),
    "as_parent"               => array('only' => 'cth_flexslider_item'), // Use only|except attributes to limit child shortcodes (separate multiple values with comma)
    "content_element"         => true,
    "show_settings_on_create" => false,
    "class"                   => '',
    "icon"                    => BBT_ABSPATH . "assets/cththemes-logo.png",
    "params"                  => array(
        array(
            "type"        => "dropdown",
            "heading"     => __("Auto Play", 'archia-add-ons'),
            "param_name"  => "slideshow",
            "value"       => array(
                __('Yes', 'archia-add-ons') => 'true',
                __('No', 'archia-add-ons')  => 'false',
            ),
            "description" => __("Animate slider automatically?", 'archia-add-ons'),
        ),
        array(
            "type"        => "dropdown",
            // "class"=>"",
            "heading"     => __('Animation Type', 'archia-add-ons'),
            "param_name"  => "animation",
            "value"       => array(
                __('Fade', 'archia-add-ons')  => 'fade',
                __('Slide', 'archia-add-ons') => 'slide',
            ),
            "description" => __("Select your animation type", 'archia-add-ons'),
        ),
        array(
            "type"        => "dropdown",
            //"class"=>"",
            "heading"     => __('Direction', 'archia-add-ons'),
            "param_name"  => "direction",
            "value"       => array(
                __('Horizontal', 'archia-add-ons') => 'horizontal',
                __('Vertical', 'archia-add-ons')   => 'vertical',
            ),
            "description" => __("Select the sliding direction", 'archia-add-ons'),
        ),
        array(
            "type"        => "dropdown",
            "heading"     => __("SmoothHeight", 'archia-add-ons'),
            "param_name"  => "smoothheight",
            "value"       => array(
                __('No', 'archia-add-ons')  => 'false',
                __('Yes', 'archia-add-ons') => 'true',

            ),
            "description" => __("Allow height of the slider to animate smoothly in horizontal mode", 'archia-add-ons'),
        ),
        array(
            "type"        => "textfield",
            "heading"     => __("Slide Speed", 'archia-add-ons'),
            "param_name"  => "slideshowspeed",
            "description" => __("Set the speed of the slideshow cycling, in milliseconds", 'archia-add-ons'),
            "value"       => "7000",
        ),
        array(
            "type"        => "dropdown",
            "heading"     => __("controlNav", 'archia-add-ons'),
            "param_name"  => "controlnav",
            "value"       => array(

                __('Yes', 'archia-add-ons') => 'true',
                __('No', 'archia-add-ons')  => 'false',

            ),
            "description" => __("Create navigation for paging control of each slide? Note: Leave true for manualControls usage", 'archia-add-ons'),
        ),
        array(
            "type"        => "dropdown",
            "heading"     => __("directionNav", 'archia-add-ons'),
            "param_name"  => "directionnav",
            "value"       => array(

                __('Yes', 'archia-add-ons') => 'true',
                __('No', 'archia-add-ons')  => 'false',

            ),
            "description" => __("Boolean: Create navigation for previous/next navigation?", 'archia-add-ons'),
        ),
        array(
            "type"        => "dropdown",
            //"class"=>"",
            "heading"     => __('Skin', 'archia-add-ons'),
            "param_name"  => "sliderskin",
            "value"       => array(
                __('Default', 'archia-add-ons') => 'default',
                __('Iphone', 'archia-add-ons')  => 'iphone',
                __('Macbook', 'archia-add-ons') => 'macbook',
            ),
            //"default"=>'testimonial',
            "description" => __("Slider skin", 'archia-add-ons'),
        ),
        array(
            "type"        => "textfield",
            "heading"     => __("ID", 'archia-add-ons'),
            "param_name"  => "el_id",
            "description" => __("Slider id", 'archia-add-ons'),
        ),

        array(
            "type"        => "textfield",
            "heading"     => __("Extra class name", 'archia-add-ons'),
            "param_name"  => "el_class",
            "description" => __("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", 'archia-add-ons'),
        ),

    ),
    "js_view"                 => 'VcColumnView',
));

vc_map(array(
    "name"            => __("Slide Item", 'archia-add-ons'),
    "base"            => "cth_flexslider_item",
    "content_element" => true,
    "icon"            => get_template_directory_uri() . "/vc_extend/cth-icon.png",
    "as_child"        => array('only' => 'cth_flexslider'),
    "params"          => array(
        array(
            "type"        => "textfield",
            "heading"     => __("Extra class name", 'archia-add-ons'),
            "param_name"  => "el_class",
            "description" => __("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", 'archia-add-ons'),
        ),
        array(
            "type"        => "attach_image",
            "holder"      => "div",
            "class"       => "",
            "heading"     => __("Slide Image", 'archia-add-ons'),
            "param_name"  => "slideimg",
            "description" => __("Slide Image", 'archia-add-ons'),
        ),

        array(
            "type"       => "textarea_raw_html",
            "heading"    => __('Content', 'archia-add-ons'),
            //"holder"=>'div',
            "param_name" => "content",
            "ace_mode"   => "html",
            "ace_style"  => "min-height:100px;border:1px solid #bbb;",
            "value"      => "",
        ),

    ),
));

//Your "container" content element should extend WPBakeryShortCodesContainer class to inherit all required functionality
if (class_exists('WPBakeryShortCodesContainer')) {
    class WPBakeryShortCode_Cth_Flexslider extends WPBakeryShortCodesContainer
    {}
}
if (class_exists('WPBakeryShortCode')) {
    class WPBakeryShortCode_Cth_Flexslider_Item extends WPBakeryShortCode
    {

    }
}
