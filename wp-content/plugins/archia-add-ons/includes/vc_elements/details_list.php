<?php
/* add_ons_php */
vc_map( array(
    "name"                      => esc_html__("Details List", 'archia-add-ons'),
    "base"                      => "details_list",
    "icon"                      => BBT_DIR_URL . "/assets/cththemes-logo.png",
    "category"                  => __( 'Archia Theme', 'archia-add-ons' ),
    "content_element"           => true,
    "show_settings_on_create"   => true,
    "params"    => array(

        

        array(
            'type' => 'param_group',
            'heading' => esc_html__( 'Details', 'archia-add-ons' ),
            'param_name' => 'details',
            // 'description' => esc_html__( 'Enter values for graph - value, title and color.', 'inshot' ),
            'value' => urlencode( json_encode( array(
                array(
                    'title' => 'Clients:',
                    'value' => '<span>Ethan Hunt</span>',
                ),
                array(
                    'title' => 'Completion:',
                    'value' => '<span>February 5th, 2019</span>',
                ),
                array(
                    'title' => 'Project Type:',
                    'value' => '<span>Villa, Residence</span>',
                ),
                
            ) ) ),
            'params' => array(
                array(
                    'type' => 'textfield',
                    'heading' => esc_html__( 'Title', 'archia-add-ons' ),
                    'param_name' => 'title',
                    'description' => esc_html__( 'Enter text used as title of bar.', 'archia-add-ons' ),
                    'admin_label' => true,
                ),
                array(
                    'type' => 'textarea',
                    'heading' => esc_html__( 'Value', 'archia-add-ons' ),
                    'param_name' => 'value',
                    'description' => esc_html__( 'Enter value of bar.', 'archia-add-ons' ),
                    'admin_label' => true,
                ),
            ),
        ),
        // end child param group

        array(
            "type" => "dropdown",  
            "admin_label"   => true,
            "heading" => __('CSS Animation', 'archia-add-ons'), 
            "param_name" => "ani_type", 
            "value" => archia_addons_vc_animation(),
            "description" => __("Select type of animation for element to be animated when it \"enters\" the browsers viewport (Note: works only in modern browsers).", 'archia-add-ons'), 
            "std" => '',
        ),
        array(
            "type" => "textfield",
            "admin_label"   => true,
            "heading" => esc_html__("Animation Duration", 'archia-add-ons'),
            "param_name" => "ani_time",
            "description" => esc_html__("Ex: 0.2s for 200 miliseconds, 2s for 2 second", 'archia-add-ons'),
            "value" => "2s",
            'dependency' => array(
                'element' => 'ani_type',
                'not_empty' => true,
            ),
        ),
        array(
            "type" => "textfield",
            "admin_label"   => true,
            "heading" => esc_html__("Animation delay", 'archia-add-ons'),
            "param_name" => "ani_delay",
            "description" => esc_html__("Ex: 0.2s for 200 miliseconds, 2s for 2 second", 'archia-add-ons'),
            "value" => "0.2s",
            'dependency' => array(
                'element' => 'ani_type',
                'not_empty' => true,
            ),
        ),

        array(
            "type" => "textfield",
            "heading" => esc_html__("Extra class name", 'archia-add-ons'),
            "param_name" => "el_class",
            "description" => esc_html__("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", 'archia-add-ons')
        ),
        array(
            'type' => 'css_editor',
            'heading' => esc_html__( 'Css', 'archia-add-ons' ),
            'param_name' => 'css',
            'group' => esc_html__( 'Design options', 'archia-add-ons' ),
        ),
    ),
    // 'admin_enqueue_js'      => TYPHOON_ADD_ONS_DIR_URL . "assets/js/wpbakerry-eles.js", // need "holder"    => "div" and "class"     => "cth-vc-images" option for attach_image, attach_images type
    // 'js_view'=> 'CTHVCImages',
));

if ( class_exists( 'WPBakeryShortCode' ) ) {
    class WPBakeryShortCode_Details_List extends WPBakeryShortCode {}
}