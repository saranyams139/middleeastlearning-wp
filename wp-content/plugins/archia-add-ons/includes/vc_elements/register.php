<?php
vc_map(array(
    "name"        => __("User Register Form", 'archia-add-ons'),
    "description" => __("User Register Form", 'archia-add-ons'),
    "base"        => "cth_register_form",
    "class"       => "",
    "icon"        => BBT_ABSPATH . "assets/cththemes-logo.png",
    "category"    => __('Archia Theme', 'archia-add-ons'),
    "params"      => array(
        array(
            "type"        => "textarea",
            "class"       => "",
            "holder"      => 'div',
            "heading"     => __("Form Title", 'archia-add-ons'),
            "param_name"  => "content",
            "value"       => '<h4>Join with us and get <span>more premium design</span></h4>',
            "description" => __("Form Title", 'archia-add-ons'),
        ),
        array(
            "type"        => "dropdown",
            "heading"     => __("Show First Name Field", 'archia-add-ons'),
            "param_name"  => "show_first_name",
            "value"       => array(
                __('No', 'archia-add-ons')  => 'false',
                __('Yes', 'archia-add-ons') => 'true',

            ),
            "description" => __("Allow user enter his first name on registration form.", 'archia-add-ons'),
        ),
        array(
            "type"        => "dropdown",
            "heading"     => __("Show Last Name Field", 'archia-add-ons'),
            "param_name"  => "show_last_name",
            "value"       => array(

                __('No', 'archia-add-ons')  => 'false',
                __('Yes', 'archia-add-ons') => 'true',

            ),
            "description" => __("Allow user enter his last name on registration form.", 'archia-add-ons'),
        ),
        array(
            "type"        => "dropdown",
            "heading"     => __("Show Email Field", 'archia-add-ons'),
            "param_name"  => "show_email_add",
            "value"       => array(
                __('No', 'archia-add-ons')  => 'false',
                __('Yes', 'archia-add-ons') => 'true',

            ),
            "description" => __("Allow user enter his email address on registration form.", 'archia-add-ons'),
        ),
        array(
            "type"        => "dropdown",
            "heading"     => __("Show Url Field", 'archia-add-ons'),
            "param_name"  => "show_url_link",
            "value"       => array(
                __('No', 'archia-add-ons')  => 'false',
                __('Yes', 'archia-add-ons') => 'true',

            ),
            "description" => __("Allow user enter his url for web site on registration form.", 'archia-add-ons'),
        ),
        array(
            "type"        => "textfield",
            "heading"     => __("Redirection Link", 'archia-add-ons'),
            "holder"      => "div",
            "value"       => "",
            "param_name"  => "redirect_link",
            "description" => __("Link that will redirect to when user registration success. Empty string for home link.", 'archia-add-ons'),
        ),
        array(
            "type"        => "textfield",
            "heading"     => __("Extra class name", 'archia-add-ons'),
            "param_name"  => "el_class",
            "description" => __("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", 'archia-add-ons'),
        ),
    ),
));
if (class_exists('WPBakeryShortCode')) {
    class WPBakeryShortCode_Cth_Register_Form extends WPBakeryShortCode
    {}
}
