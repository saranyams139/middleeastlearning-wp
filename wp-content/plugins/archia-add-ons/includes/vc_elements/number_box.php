<?php
vc_map( array(
    "name"      => __("Number Box", 'archia-add-ons'),
    // "description" => __("Button Video",'archia-add-ons'),
    "base"      => "number_box",
    "class"     => "",
    "icon"      => BBT_DIR_URL . "/assets/cththemes-logo.png",
    "category"  => __('Archia Theme', 'archia-add-ons'),
    "params"    => array(

        array(
            "type"      => "textfield",
            'admin_label'       => true,
            "heading"   => __("Number", 'archia-add-ons'),
            "param_name"=> "num",
            "value"     => "26",
            // "description" => __("Enter section title (Note: you can leave it empty).", 'archia-add-ons')
        ),
        
        array(
            "type"      => "textarea_html",
            'admin_label'       => true,
            "heading"   => __("Additional Info", 'archia-add-ons'),
            "param_name"=> "content",
            "value"     => "<p>Years<br/>Experience<br/>Working</p>",
            // "description" => __("Description", 'archia-add-ons'),
        ),
        array(
            "type"      => "textfield",
            "heading"   => __("Extra class name", 'archia-add-ons'),
            "param_name" => "el_class",
            "description" => __("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", 'archia-add-ons')
        ),
        array(
            'type'          => 'css_editor',
            'heading'       => esc_html__( 'Css', 'archia-add-ons' ),
            'param_name'    => 'css',
            'group'         => esc_html__( 'Design options', 'archia-add-ons' ),
        ),
    )
));

if ( class_exists( 'WPBakeryShortCode' ) ) {
    class WPBakeryShortCode_Number_Box extends WPBakeryShortCode {}
}