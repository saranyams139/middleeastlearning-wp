<?php
vc_map(array(
    "name"            => __("Company History", 'archia-add-ons'),
    "base"            => "cth_resume",
    "icon"                      => BBT_DIR_URL . "/assets/cththemes-logo.png",
    "category"                  => __( 'Archia Theme', 'archia-add-ons' ),
    "content_element"           => true,
    "show_settings_on_create"   => true,

    "params"          => array(
        array(
            "type"       => "attach_image",
            "admin_label"   => true,
            "class"      => "",
            "heading"    => __("Image", 'archia-add-ons'),
            "param_name" => "image",
            //"description" => __("Background Image, will display in mobile device", 'archia')
        ),

        array(
            "type"        => "textfield",
            "class"       => "",
            "admin_label"   => true,
            "heading"     => __("Title", 'archia-add-ons'),
            "param_name"  => "title",
            "value"       => "Online Network Connection Monitor",
            // "description" => __("Title", 'archia-add-ons'),
        ),

        array(
            "type"        => "textfield",
            "class"       => "",
            "admin_label"   => true,
            "heading"     => __("Year", 'archia-add-ons'),
            "param_name"  => "year",
            "value"       => "2019",
            // "description" => __("Title", 'archia-add-ons'),
        ),

        array(
            "type"        => "textarea_html",
            "class"       => "",
            "holder"      => 'div',
            "heading"     => __("Description", 'archia-add-ons'),
            "param_name"  => "content",
            "value"       => "Lorem ipsum dolor sit amet, conse dipisicing elit. Ea ratione optio null ius dolor maiores nulla illum,",
            // "description" => __("Description", 'archia-add-ons'),
        ),

        array(
            "type"      => "dropdown",
            "class"     =>"",
            "heading"   => __('Image Position', 'archia-add-ons'),
            "param_name" => "image_pos",
            "value"     => array(   
                            __('Left', 'archia-add-ons') => 'thumb-left',  
                            __('Center', 'archia-add-ons') => 'thumb-center',
                            __('Right', 'archia-add-ons') => 'thumb-right',
                        ),
            "std" => 'thumb-left',
            // "description" => __("Choose section title location", 'archia-add-ons'), 
        ), 

        array(
            "type" => "dropdown",  
            "admin_label"   => true,
            "heading" => __('CSS Animation', 'archia-add-ons'), 
            "param_name" => "ani_type", 
            "value" => archia_addons_vc_animation(),
            "description" => __("Select type of animation for element to be animated when it \"enters\" the browsers viewport (Note: works only in modern browsers).", 'archia-add-ons'), 
            "std" => '',
        ),
        array(
            "type" => "textfield",
            "admin_label"   => true,
            "heading" => esc_html__("Animation Duration", 'archia-add-ons'),
            "param_name" => "ani_time",
            "description" => esc_html__("Ex: 0.2s for 200 miliseconds, 2s for 2 second", 'archia-add-ons'),
            "value" => "2s",
            'dependency' => array(
                'element' => 'ani_type',
                'not_empty' => true,
            ),
        ),
        array(
            "type" => "textfield",
            "admin_label"   => true,
            "heading" => esc_html__("Animation delay", 'archia-add-ons'),
            "param_name" => "ani_delay",
            "description" => esc_html__("Ex: 0.2s for 200 miliseconds, 2s for 2 second", 'archia-add-ons'),
            "value" => "0.2s",
            'dependency' => array(
                'element' => 'ani_type',
                'not_empty' => true,
            ),
        ),

        array(
            "type"          => "textfield",
            "heading"       => esc_html__("Extra class name", 'archia-add-ons'),
            "param_name"    => "el_class",
            "value"         =>'',
            "description"   => esc_html__("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", 'archia-add-ons')
        ),
        array(
            'type'          => 'css_editor',
            'heading'       => esc_html__( 'Css', 'archia-add-ons' ),
            'param_name'    => 'css',
            'group'         => esc_html__( 'Design options', 'archia-add-ons' ),
        ),

    ),
));

if (class_exists('WPBakeryShortCode')) {
    class WPBakeryShortCode_CTH_Resume extends WPBakeryShortCode
    {
    }
}
