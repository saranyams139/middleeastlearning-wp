<?php
vc_map( array(
    "name"        => __("Social", 'archia-add-ons'),
    "base"        => "cth_header_social",
    "icon"        => BBT_DIR_URL . "assets/cththemes-logo.png",
    "category"    => 'Archia Header',
    "as_parent"                 => array('only' => 'cth_social_item'), 
    "content_element"           => true,
    "show_settings_on_create"   => true,
    "params"                    => array(
        array(
            "type"          => "textfield",
            "heading"       => esc_html__("Extra class name", 'archia-add-ons'),
            "param_name"    => "el_class",
            "description"   => esc_html__("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", 'archia-add-ons'),
            "value"         => "",
        ),    
    ),
    "js_view"               => 'VcColumnView'
));
vc_map( array(
    "name"                      => esc_html__("Social Item", 'archia-add-ons'),
    "base"                      => "cth_social_item",
    "content_element"           => true,
    "show_settings_on_create"   => true,
    "as_child"                  => array('only' => 'cth_header_social'),
    "icon"                      => BBT_DIR_URL . "assets/cththemes-logo.png",
    "params"                    => array(
        array(
            'type' => 'iconpicker',
            'heading' => esc_html__( 'Icon', 'archia-add-ons' ),
            'param_name' => 'icon',
            'settings' => array(
                'emptyIcon' => false, // default true, display an "EMPTY" icon?
                'type' => 'fontawesome',
                'iconsPerPage' => 200, // default 100, how many icons per/page to display
            ),
            "description"   => "Choose icon",
            "std"         => "",
        ),
        array(
            "type"        => "vc_link",
            "heading"     => __("Links", 'archia-add-ons'),
            "param_name"  => "links",
            "value"       => "#",
            "description"   => esc_html__( 'Enter links for each partner (Note: divide links with linebreaks (Enter) or | and no spaces)', 'archia-add-ons' ). get_post_type_archive_link( 'listing' ),
        ),
        array(
            "type"          => "textfield",
            "heading"       => esc_html__("Extra class name", 'archia-add-ons'),
            "param_name"    => "el_class",
            "description"   => esc_html__("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", 'archia-add-ons')
        ),
    ),
    'js_view'               =>'InshotImagesView'
));
if ( class_exists( 'WPBakeryShortCodesContainer' ) ) {
        class WPBakeryShortCode_CTH_Header_Social extends WPBakeryShortCodesContainer {}
    }
if ( class_exists( 'WPBakeryShortCode' ) ) {
    class WPBakeryShortCode_CTH_Social_Item extends WPBakeryShortCode {}
}