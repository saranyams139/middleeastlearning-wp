<?php
vc_map(array(
    "name"     => __("ACE Raw HTML", 'archia-add-ons'),
    "base"     => "cthace",
    "class"    => "",
    "icon"     => BBT_ABSPATH . "assets/cththemes-logo.png",
    "category" => __('Archia Theme', 'archia-add-ons'),
    "params"   => array(
        array(
            "type"       => "textarea_raw_html",
            "heading"    => __('HTML Code', 'archia-add-ons'),
            "param_name" => "content",
            // "ace_mode"=>"html",
            // "ace_style"=>"min-height:500px;border:1px solid #bbb;",
            "value"      => "",
        ),

    )));

if (class_exists('WPBakeryShortCode')) {
    class WPBakeryShortCode_Cthace extends WPBakeryShortCode
    {}
}
