<?php
vc_map( array(
    "name"      => __("Feature Box", 'archia-add-ons'),
    "description" => __("Feature Box",'archia-add-ons'),
    "base"      => "archia_feature_box",
    "class"     => "",
    "icon"      => BBT_DIR_URL . "/assets/cththemes-logo.png",
    "category"  => __('Archia Theme', 'archia-add-ons'),
    "params"    => array(
        array(
            "type" => "dropdown", 
            "class" => "", 
            "heading" => __('Icon', 'archia-add-ons'), 
            "param_name" => "style", 
            "value" => array(
                
                __('Image', 'archia-add-ons') => 'sr-iconbox', 
                __('Number', 'archia-add-ons') => 'sr-numbox',
                
            ), 
            "std" => 'sr-iconbox',
        ),
        array(
            "type"              => "attach_image",
            "admin_label"       => true,
            "class"             => "",
            "heading"           => __("Image", 'archia-add-ons'),
            "param_name"        => "img",
            "description"       => __("Choose a Image", 'archia-add-ons'),
            "dependency"        => array(
                'element'           => 'style',
                'value'             => 'sr-iconbox',
            ),
        ), 
        array(
            "type"      => "textfield",
            "class"     => "",
            "heading"   => __("Number", 'archia-add-ons'),
            "param_name"=> "number",
            "value"     => "01",
            "description" => __("Add a zero first if the number is less than 10).", 'archia-add-ons'),
            "dependency"  => array(
                'element' => 'style',
                'value'   => 'sr-numbox',
            ),
        ),
        array(
            "type"      => "textfield",
            "admin_label"       => true,
            "heading"   => __("Title", 'archia-add-ons'),
            "param_name"=> "title",
            "value"     => "Title",
            "description" => __("Enter section title (Note: you can leave it empty).", 'archia-add-ons')
        ),
        array(
            "type"      => "textarea_html",
            "class"     => "",
            "heading"   => __("Text", 'archia-add-ons'),
            "param_name"=> "content",
            "value"     => "",
            "description" => __("Description", 'archia-add-ons'),
        ),
        array(
            "type"      => "textfield",
            "heading"   => __("Link", 'archia-add-ons'),
            "param_name"=> "link",
            "value"     => "",
            "dependency"  => array(
                'element' => 'style',
                'value'   => 'sr-iconbox',
            ),
        ),

        array(
            "type" => "dropdown",  
            "admin_label"   => true,
            "heading" => __('CSS Animation', 'archia-add-ons'), 
            "param_name" => "ani_type", 
            "value" => archia_addons_vc_animation(),
            "description" => __("Select type of animation for element to be animated when it \"enters\" the browsers viewport (Note: works only in modern browsers).", 'archia-add-ons'), 
            "std" => '',
        ),
        array(
            "type" => "textfield",
            "admin_label"   => true,
            "heading" => esc_html__("Animation Duration", 'archia-add-ons'),
            "param_name" => "ani_time",
            "description" => esc_html__("Ex: 0.2s for 200 miliseconds, 2s for 2 second", 'archia-add-ons'),
            "value" => "2s",
            'dependency' => array(
                'element' => 'ani_type',
                'not_empty' => true,
            ),
        ),
        array(
            "type" => "textfield",
            "admin_label"   => true,
            "heading" => esc_html__("Animation delay", 'archia-add-ons'),
            "param_name" => "ani_delay",
            "description" => esc_html__("Ex: 0.2s for 200 miliseconds, 2s for 2 second", 'archia-add-ons'),
            "value" => "0.2s",
            'dependency' => array(
                'element' => 'ani_type',
                'not_empty' => true,
            ),
        ),
        array(
            "type"      => "textfield",
            "heading"   => __("Extra class name", 'archia-add-ons'),
            "param_name" => "el_class",
            "description" => __("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", 'archia-add-ons')
        ),
        array(
            'type'          => 'css_editor',
            'heading'       => esc_html__( 'Css', 'archia-add-ons' ),
            'param_name'    => 'css',
            'group'         => esc_html__( 'Design options', 'archia-add-ons' ),
        ),
    )
));

if ( class_exists( 'WPBakeryShortCode' ) ) {
    class WPBakeryShortCode_Archia_Feature_Box extends WPBakeryShortCode {}
}