<?php
vc_map(array(
    "name"        => __("Custom Logo", 'archia-add-ons'),
    // "description" => __("Site Logo Theme", 'archia-add-ons'),
    "base"        => "cth_site_logo",
    "class"       => "",
    "icon"        => BBT_DIR_URL . "assets/cththemes-logo.png",
    "category"    => 'Archia Header',
    "params"      => array(
        array(
            "type"        => "attach_image",
            "class"       => "",
            "heading"     => __("Custom Logo", 'archia-add-ons'),
            "param_name"  => "custom_logo",
            "value"       => "",
            "description" => __("For custom logo instead of option from customize.", 'archia-add-ons'),
        ),
        array(
            "type"        => "attach_image",
            "class"       => "",
            "heading"     => __("Custom Fixed Logo", 'archia-add-ons'),
            "param_name"  => "fixed_logo",
            "value"       => "",
            "description" => __("Logo used when header got fixed while scrolling down page.", 'archia-add-ons'),
        ),
        array(
            "type"        => "textfield",
            "heading"     => __("Extra class name", 'archia-add-ons'),
            "param_name"  => "el_class",
            "description" => __("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", 'archia-add-ons'),
        ),
    ),
));
if (class_exists('WPBakeryShortCode')) {
    class WPBakeryShortCode_Cth_Site_Logo extends WPBakeryShortCode
    {}
}