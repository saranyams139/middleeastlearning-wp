<?php
vc_map( array(
    "name"      => __("Icon Box", 'archia-add-ons'),
    // "description" => __("Button Video",'archia-add-ons'),
    "base"      => "icon_box",
    "class"     => "",
    "icon"      => BBT_DIR_URL . "/assets/cththemes-logo.png",
    "category"  => __('Archia Theme', 'archia-add-ons'),
    "params"    => array(
        
        array(
            'type' => 'dropdown',
            'heading' => __( 'Box Style', 'archia-add-ons' ),
            'value' => array(
                __( 'Simple', 'archia-add-ons' ) => 'icbox-simple',
                __( 'About Style', 'archia-add-ons' ) => 'about-bx',
            ),
            'param_name' => 'box_style',
            "std"         => "icbox-simple",
            // 'description' => __( 'Select icon library.', 'js_composer' ),
        ),
        
        array(
            "type"      => "textfield",
            'admin_label'       => true,
            "heading"   => __("Title", 'archia-add-ons'),
            "param_name"=> "title",
            "value"     => "Phone",
            // "description" => __("Enter section title (Note: you can leave it empty).", 'archia-add-ons')
        ),
        array(
            "type"      => "textarea_html",
            "class"     => "",
            "heading"   => __("Text", 'archia-add-ons'),
            "param_name"=> "content",
            "value"     => "<p>Phone 01:(+032) 3456 7890 <br> Phone 02: (+032) 3427 7670</p>",
            "description" => __("Description", 'archia-add-ons'),
        ),

        array(
            'type' => 'dropdown',
            'heading' => __( 'Icon library', 'archia-add-ons' ),
            'value' => array(
                __( 'Font Awesome', 'archia-add-ons' ) => 'fontawesome',
                // __( 'Open Iconic', 'js_composer' ) => 'openiconic',
                // __( 'Typicons', 'js_composer' ) => 'typicons',
                // __( 'Entypo', 'js_composer' ) => 'entypo',
                // __( 'Linecons', 'js_composer' ) => 'linecons',
                // __( 'Pixel', 'js_composer' ) => 'pixelicons',
                // __( 'Mono Social', 'js_composer' ) => 'monosocial',
                // defaule WPBakery
                __( 'Themify Icons', 'archia-add-ons' ) => 'themify',
            ),
            "std"         => "themify",
            'param_name' => 'icon_type',
            'description' => __( 'Select icon library.', 'archia-add-ons' ),
        ),
        array(
            'type' => 'iconpicker',
            'heading' => esc_html__( 'Icon', 'archia-add-ons' ),
            'param_name' => 'icon_fontawesome',
            'settings' => array(
                'emptyIcon' => true, // default true, display an "EMPTY" icon?
                'type' => 'fontawesome',
                'iconsPerPage' => 200, // default 100, how many icons per/page to display
            ),
            // "description"   => "Choose icon",
            "std"         => "fa fa-phone",
            'dependency' => array(
                'element' => 'icon_type',
                'value' => 'fontawesome',
            ),
        ),
        array(
            'type' => 'iconpicker',
            'heading' => esc_html__( 'Icon', 'archia-add-ons' ),
            'param_name' => 'icon_themify',
            'settings' => array(
                'emptyIcon' => true, // default true, display an "EMPTY" icon?
                'type' => 'themify',
                'iconsPerPage' => 200, // default 100, how many icons per/page to display
            ),
            // "description"   => "Choose icon",
            "std"         => "ti-headphone-alt",
            'dependency' => array(
                'element' => 'icon_type',
                'value' => 'themify',
            ),
        ),

        

        array(
            'type' => 'dropdown',
            'heading' => __( 'Icon Position', 'archia-add-ons' ),
            'value' => array(
                __( 'Left', 'archia-add-ons' ) => 'left',
                __( 'Right', 'archia-add-ons' ) => 'right',
                __( 'Center', 'archia-add-ons' ) => 'center',
            ),
            'param_name' => 'icon_pos',
            "std"         => "left",
            // 'description' => __( 'Select icon library.', 'js_composer' ),
            'dependency' => array(
                'element' => 'box_style',
                'value' => 'icbox-simple',
            ),
        ),


        array(
            "type"      => "textfield",
            "heading"   => __("Extra class name", 'archia-add-ons'),
            "param_name" => "el_class",
            "description" => __("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", 'archia-add-ons')
        ),
        array(
            'type'          => 'css_editor',
            'heading'       => esc_html__( 'Css', 'archia-add-ons' ),
            'param_name'    => 'css',
            'group'         => esc_html__( 'Design options', 'archia-add-ons' ),
        ),
    )
));

if ( class_exists( 'WPBakeryShortCode' ) ) {
    class WPBakeryShortCode_Icon_Box extends WPBakeryShortCode {}
}