<?php
/* add_ons_php */
vc_map( array(
    "name"                      => esc_html__("Popup Video", 'archia-add-ons'),
    "base"                      => "popup_video",
    "icon"                      => BBT_DIR_URL . "/assets/cththemes-logo.png",
    "category"                  => __( 'Archia Portfolio', 'archia-add-ons' ),
    "content_element"           => true,
    "show_settings_on_create"   => true,
    "params"    => array(

        
        array(
            "type"      => "textfield",
            
            "admin_label"   => true,
            "heading"   => esc_html__("Video Source", 'archia-add-ons'),
            "param_name"=> "vid_source",
            "value"     => 'https://www.youtube.com/watch?v=Dj6CKxQue7U',
            "description" => esc_html__("Enter your video source.", 'archia-add-ons')
        ),

        array(
            "type"      => "textfield",
            'admin_label'       => true,
            "heading"   => __("Title", 'archia-add-ons'),
            "param_name"=> "title",
            "value"     => "",
            // "description" => __("Enter section title (Note: you can leave it empty).", 'archia-add-ons')
        ),

        array(
            "type" => "dropdown",
            "heading" => esc_html__("Play Button Size", 'archia-add-ons'),
            "param_name" => "btn_size",
            "value" => array( 
                esc_html__('Small', 'archia-add-ons') => 'play-size-small',  
                esc_html__('Large', 'archia-add-ons') => 'play-size-large',   
                 
            ),
            "std"=>'play-size-large',    
            // "description" => esc_html__("Show image caption. (If image have on  title, caption or description ).Default: not displayed.", 'archia-add-ons'),
        ),

        array(
            "type" => "dropdown",
            "heading" => esc_html__("Play Button Color", 'archia-add-ons'),
            "param_name" => "btn_color",
            "value" => array( 
                esc_html__('Colored', 'archia-add-ons') => 'play-bg-primary',   
                esc_html__('Transparent', 'archia-add-ons') => 'play-bg-transparent',   
            ),
            "std"=>'play-bg-primary',    
            // "description" => esc_html__("Show image caption. (If image have on  title, caption or description ).Default: not displayed.", 'archia-add-ons'),
        ),

        array(
            "type" => "textfield",
            "heading" => esc_html__("Extra class name", 'archia-add-ons'),
            "param_name" => "el_class",
            "description" => esc_html__("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", 'archia-add-ons')
        ),
        array(
            'type' => 'css_editor',
            'heading' => esc_html__( 'Css', 'archia-add-ons' ),
            'param_name' => 'css',
            'group' => esc_html__( 'Design options', 'archia-add-ons' ),
        ),
    ),
    // 'admin_enqueue_js'      => TYPHOON_ADD_ONS_DIR_URL . "assets/js/wpbakerry-eles.js", // need "holder"    => "div" and "class"     => "cth-vc-images" option for attach_image, attach_images type
    // 'js_view'=> 'CTHVCImages',
));

if ( class_exists( 'WPBakeryShortCode' ) ) {
    class WPBakeryShortCode_Popup_Video extends WPBakeryShortCode {}
}