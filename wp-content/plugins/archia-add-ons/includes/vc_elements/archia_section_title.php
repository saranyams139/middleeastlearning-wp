<?php
vc_map( array(
    "name"      => __("Section Title", 'archia-add-ons'),
    "description" => __("Section Title",'archia-add-ons'),
    "base"      => "archia_section_title",
    "class"     => "",
    "icon"      => BBT_DIR_URL . "/assets/cththemes-logo.png",
    "category"  => __('Archia Theme', 'archia-add-ons'),
    "params"    => array(
        
        array(
            "type"      => "textfield",
            "class"     => "",
            "admin_label"   => true,
            "heading"   => __("Pre Title", 'archia-add-ons'),
            "param_name"=> "pre_title",
            "value"     => "",
            "description" => ''
        ),
        array(
            "type"      => "textfield",
            "class"     => "",
            "admin_label"   => true,
            "heading"   => __("Title", 'archia-add-ons'),
            "param_name"=> "title",
            "value"     => "Section Title",
            "description" => __("Enter section title (Note: you can leave it empty).", 'archia-add-ons')
        ),
        
        array(
            "type"      => "textarea_html",
            "class"     => "",
            "heading"   => __("Text", 'archia-add-ons'),
            "param_name"=> "content",
            "value"     => "I am text block. Click edit button to change this text. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut elit tellus, luctus nec ullamcorper mattis, pulvinar dapibus leo.",
            "description" => __("Description", 'archia-add-ons'),
        ),
        array(
            "type"      => "dropdown",
            "class"     =>"",
            "heading"   => __('Title Style', 'archia-add-ons'),
            "param_name" => "title_style",
            "value"     => array(   
                            __('Default', 'archia-add-ons') => 'default',  
                            __('Medium Title', 'archia-add-ons') => 'medium',
                            __('Small Title', 'archia-add-ons') => 'small',
                        ),
            "std" => 'default',
            // "description" => __("Choose section title location", 'archia-add-ons'), 
        ), 
        array(
            "type"      => "dropdown",
            "class"     =>"",
            "heading"   => __('Title Alignment', 'archia-add-ons'),
            "param_name" => "section_loca",
            "value"     => array(   
                            __('Left', 'archia-add-ons') => '',  
                            __('Center', 'archia-add-ons') => 'text-center',
                            __('Right', 'archia-add-ons') => 'text-right',
                        ),
            "std" => 'text-center',
            // "description" => __("Choose section title location", 'archia-add-ons'), 
        ), 
        array(
            "type"      => "dropdown",
            "class"     =>"",
            "heading"   => __('Color Title', 'archia-add-ons'),
            "param_name" => "col_title",
            "value"     => array(   
                            __('Default', 'archia-add-ons') => '',  
                            __('Dark', 'archia-add-ons') => 'text-black',
                            __('Blue Dark', 'archia-add-ons') => 'text-blue-dark',
                            __('White', 'archia-add-ons') => 'text-white',
                        ),
            "description" => __("Choose section title color title ", 'archia-add-ons'), 
            "default"     => '',     
        ),
        array(
            "type"      => "textfield",
            "heading"   => __("Extra class name", 'archia-add-ons'),
            "param_name" => "el_class",
            "description" => __("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", 'archia-add-ons')
        ),
        array(
            'type'          => 'css_editor',
            'heading'       => esc_html__( 'Css', 'archia-add-ons' ),
            'param_name'    => 'css',
            'group'         => esc_html__( 'Design options', 'archia-add-ons' ),
        ),
    )
));

if ( class_exists( 'WPBakeryShortCode' ) ) {
    class WPBakeryShortCode_Archia_Section_Title extends WPBakeryShortCode {}
}