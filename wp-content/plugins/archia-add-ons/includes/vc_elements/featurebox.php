<?php
vc_map(array(
    "name"            => __("Feature Box", 'archia-add-ons'),
    "base"            => "cth_feature_box",
    "content_element" => true,
    "icon"            => BBT_ABSPATH . "assets/cththemes-logo.png",
    "category"        => __('Archia Theme', 'archia-add-ons'),
    "params"          => array(
        array(
            "type"        => "textfield",
            "class"       => "",
            "heading"     => __("Awesome Icon Name", 'archia-add-ons'),
            "param_name"  => "iconname",
            "value"       => "",
            "description" => __("Awesome icon name. You can find the name from <a href='http://fortawesome.github.io/Font-Awesome/icons/' target='_blank'>Font Awesome Icons</a>", 'archia-add-ons'),
        ),
        array(
            "type"       => "attach_image",
            //"holder"    => "div",
            "class"      => "",
            "heading"    => __("Icon Image", 'archia-add-ons'),
            "param_name" => "iconimg",
            //"description" => __("Background Image, will display in mobile device", 'archia')
        ),

        array(
            "type"        => "textfield",
            "class"       => "",
            "holder"      => 'div',
            "heading"     => __("Title", 'archia-add-ons'),
            "param_name"  => "title",
            "value"       => "",
            "description" => __("Title", 'archia-add-ons'),
        ),

        array(
            "type"        => "textarea_html",
            "class"       => "",
            "holder"      => 'div',
            "heading"     => __("Description", 'archia-add-ons'),
            "param_name"  => "content",
            "value"       => "",
            "description" => __("Description", 'archia-add-ons'),
        ),

        array(
            "type" => "dropdown",  
            "admin_label"   => true,
            "heading" => __('CSS Animation', 'archia-add-ons'), 
            "param_name" => "ani_type", 
            "value" => archia_addons_vc_animation(),
            "description" => __("Select type of animation for element to be animated when it \"enters\" the browsers viewport (Note: works only in modern browsers).", 'archia-add-ons'), 
            "std" => '',
        ),
        array(
            "type" => "textfield",
            "admin_label"   => true,
            "heading" => esc_html__("Animation Duration", 'archia-add-ons'),
            "param_name" => "ani_time",
            "description" => esc_html__("Ex: 0.2s for 200 miliseconds, 2s for 2 second", 'archia-add-ons'),
            "value" => "2s",
            'dependency' => array(
                'element' => 'ani_type',
                'not_empty' => true,
            ),
        ),
        array(
            "type" => "textfield",
            "admin_label"   => true,
            "heading" => esc_html__("Animation delay", 'archia-add-ons'),
            "param_name" => "ani_delay",
            "description" => esc_html__("Ex: 0.2s for 200 miliseconds, 2s for 2 second", 'archia-add-ons'),
            "value" => "0.2s",
            'dependency' => array(
                'element' => 'ani_type',
                'not_empty' => true,
            ),
        ),

        array(
            "type" => "textfield",
            "heading" => esc_html__("Extra class name", 'archia-add-ons'),
            "param_name" => "el_class",
            "description" => esc_html__("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", 'archia-add-ons')
        ),
        array(
            'type' => 'css_editor',
            'heading' => esc_html__( 'Css', 'archia-add-ons' ),
            'param_name' => 'css',
            'group' => esc_html__( 'Design options', 'archia-add-ons' ),
        ),

    ),
));

if (class_exists('WPBakeryShortCode')) {
    class WPBakeryShortCode_Cth_Feature_Box extends WPBakeryShortCode
    {
    }
}
