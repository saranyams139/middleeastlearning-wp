<?php
vc_map( array(
    "name"      => esc_html__("Gallery", 'archia-add-ons'),
    "description" => esc_html__("with images selected",'archia-add-ons'),
    "base"      => "cth_gallery",
    "icon"      =>                BBT_DIR_URL . "assets/cththemes-logo.png",
    "category"  => __( 'Archia Portfolio', 'archia-add-ons' ),
    "params"    => array(
        array(
            'type' => 'dropdown',
            'heading' => __( 'Gallery Style', 'archia-add-ons' ),
            'value' => array(
                __( 'Button Popup', 'archia-add-ons' ) => 'stygal-btn-popup',
                __( 'Simple', 'archia-add-ons' ) => 'stygal-simple',
                
            ),
            'param_name' => 'gal_style',
            "std"         => "stygal-btn-popup",
            // 'description' => __( 'Select icon library.', 'js_composer' ),
        ),
        array(
            "type"      => "attach_images",
            "admin_label"   => true,
            // "class"     => "cth-vc-images",
            "heading"   => esc_html__("Gallery Images", 'archia-add-ons'),
            "param_name"=> "galimgs",
            "value"     => '',
        ),
        array(
            "type"      => "textarea",
            // "holder"    => "span",
            "admin_label"   => true,
            "heading"   => esc_html__("Links", 'archia-add-ons'),
            "param_name"=> "content",
            "value"     => '',
            "description" => esc_html__("Enter links for each partner (Note: divide links with | or linebreaks (Enter) and no spaces).", 'archia-add-ons')
        ),
        // array(
        //     "type" => "dropdown",
        //     // "holder"    => "div",
        //     // "admin_label"   => true,
        //     "heading" => esc_html__('Target', 'archia-add-ons'),
        //     "param_name" => "target",
        //     "value" => array(   
        //         esc_html__('Opens Partner link in new window', 'archia-add-ons') => '_blank',  
        //         esc_html__('Opens Partner link in the same window', 'archia-add-ons') => '_self',                                                                               
        //                 ),
        //     "std" => '_blank', 
        // ),
        // array(
        //     "type" => "textfield",
        //     // "holder"    => "div",
        //     // "admin_label"   => true,
        //     "heading" => esc_html__("Thumbnail size", 'archia-add-ons'),
        //     "param_name" => "thumbnail_size",
        //     "description" => esc_html__('Enter thumbnail size. Example: thumbnail, medium, large, full or other sizes defined by current theme. Alternatively enter image size in pixels: 200x100 (Width x Height).','archia-add-ons' ),
        //     "value"=> 'archia-gal',
        // ),

        array(
            "type"       => "dropdown",
            "class"      => "",
            "heading"    => esc_html__('Grid Columns', 'archia-add-ons'),
            "param_name" => "columns_grid",
            "value"      => array(
                esc_html__('One Column', 'archia-add-ons')    => 'one',
                esc_html__('Two Columns', 'archia-add-ons')   => 'two',
                esc_html__('Three Columns', 'archia-add-ons') => 'three',
                esc_html__('Four Columns', 'archia-add-ons')  => 'four',
                esc_html__('Five Columns', 'archia-add-ons')  => 'five',
                esc_html__('Six Columns', 'archia-add-ons')   => 'six',
                esc_html__('Seven Columns', 'archia-add-ons') => 'seven',
                esc_html__('Eight Columns', 'archia-add-ons') => 'eight',
                esc_html__('Nine Columns', 'archia-add-ons')  => 'nine',
                esc_html__('Ten Columns', 'archia-add-ons')   => 'ten',

            ),

            "std"        => 'two',
        ),

        array(
            "type"        => "textfield",
            "admin_label"   => true,
            "heading"     => esc_html__("Items widths", 'archia-add-ons'),
            "param_name"  => "items_width",
            "description" => esc_html__("Defined gallery item width. Available values are x1(default),x2(x2 width),x3(x3 width), and separated by comma. Ex: x2,x1,x1,x2,x2,x1", 'archia-add-ons'),
            "value"       => 'x2,x1,x1',
        ),

        array(
            "type"       => "dropdown",
            "class"      => "",
            "heading"    => esc_html__('Spacing', 'archia-add-ons'),
            "param_name" => "spacing",
            "value"      => array(
                esc_html__('Extra Small', 'archia-add-ons') => 'extrasmall',
                esc_html__('Small', 'archia-add-ons')       => 'small',
                esc_html__('Medium', 'archia-add-ons')      => 'medium',
                esc_html__('Big', 'archia-add-ons')         => 'big',
                esc_html__('None', 'archia-add-ons')        => 'no',

            ),
            "std"        => 'big',
        ),

        array(
            "type"          => "checkbox",
            // "admin_label"   => true,
            // "holder"        => "div",
            "heading"       => esc_html__("No Overlay?", 'archia-add-ons'),
            "param_name"    => "no_overlay",
            // "description"   => esc_html__("Show collapsed in first position.", 'archia-add-on'),
            "value"         => array(
                __('Yes', 'archia-add-ons') => 'yes', 
            ),
            'std'           => '',
        ),

        array(
            "type" => "dropdown",  
            "admin_label"   => true,
            "heading" => __('CSS Animation', 'archia-add-ons'), 
            "param_name" => "ani_type", 
            "value" => archia_addons_vc_animation(),
            "description" => __("Select type of animation for element to be animated when it \"enters\" the browsers viewport (Note: works only in modern browsers).", 'archia-add-ons'), 
            "std" => '',
        ),
        array(
            "type" => "textfield",
            "admin_label"   => true,
            "heading" => esc_html__("Animation Duration", 'archia-add-ons'),
            "param_name" => "ani_time",
            "description" => esc_html__("Ex: 0.2s for 200 miliseconds, 2s for 2 second", 'archia-add-ons'),
            "value" => "2s",
            'dependency' => array(
                'element' => 'ani_type',
                'not_empty' => true,
            ),
        ),
        array(
            "type" => "textfield",
            "admin_label"   => true,
            "heading" => esc_html__("Animation delays", 'archia-add-ons'),
            "param_name" => "ani_delays",
            "description" => esc_html__("Delay for each items. Ex: 0.2s,0.4s,0.6s,0.8s (0.2s for 200 miliseconds, 2s for 2 second)", 'archia-add-ons'),
            "value" => "0.2s,0.4s,0.6s,0.8s",
            'dependency' => array(
                'element' => 'ani_type',
                'not_empty' => true,
            ),
        ),

        
        array(
            "type"          => "textfield",
            "heading"       => esc_html__("Extra class name", 'archia-add-ons'),
            "param_name"    => "el_class",
            "value"         =>'',
            "description"   => esc_html__("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", 'archia-add-ons')
        ),
        array(
            'type'          => 'css_editor',
            'heading'       => esc_html__( 'Css', 'archia-add-ons' ),
            'param_name'    => 'css',
            'group'         => esc_html__( 'Design options', 'archia-add-ons' ),
        ),
    ),
    // 'admin_enqueue_js'      => CTH_INSHOT_DIR_URL . "inc/assets/inshot-elements.js",
    // 'js_view'=> 'InshotImagesView',
));

if ( class_exists( 'WPBakeryShortCode' ) ) {
    class WPBakeryShortCode_CTH_Gallery extends WPBakeryShortCode {}
}
