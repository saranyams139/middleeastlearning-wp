<?php
/* add_ons_php */
vc_map( array(
    "name"                      => esc_html__("Our Partners", 'archia-add-ons'),
    "base"                      => "archia_partners",
    "icon"                      => BBT_DIR_URL . "/assets/cththemes-logo.png",
    "category"                  => __( 'Archia Theme', 'archia-add-ons' ),
    "content_element"           => true,
    "show_settings_on_create"   => true,
    "params"    => array(

        array(
            "type"      => "attach_images",
            "holder"    => "div",
            // "class"     => "cth-vc-images",
            "heading"   => esc_html__("Partner Images", 'archia-add-ons'),
            "param_name"=> "partnerimgs",
            "value"     => '',
        ),
        array(
            "type"      => "textarea",
            // "holder"    => "span",
            "admin_label"   => true,
            "heading"   => esc_html__("Partner Links", 'archia-add-ons'),
            "param_name"=> "content",
            "value"     => '#|#|#|#|#|#',
            "description" => esc_html__("Enter links for each partner (Note: divide links with | or linebreaks (Enter) and no spaces).", 'archia-add-ons')
        ),
        array(
            "type" => "dropdown",
            // "holder"    => "div",
            // "admin_label"   => true,
            "heading" => esc_html__('Target', 'archia-add-ons'),
            "param_name" => "target",
            "value" => array(   
                esc_html__('Opens Partner link in new window', 'archia-add-ons') => '_blank',  
                esc_html__('Opens Partner link in the same window', 'archia-add-ons') => '_self',                                                                               
                        ),
            "std" => '_blank', 
        ),
        array(
            "type" => "textfield",
            // "holder"    => "div",
            // "admin_label"   => true,
            "heading" => esc_html__("Thumbnail size", 'archia-add-ons'),
            "param_name" => "thumbnail_size",
            "description" => esc_html__('Enter thumbnail size. Example: thumbnail, medium, large, full or other sizes defined by current theme. Alternatively enter image size in pixels: 200x100 (Width x Height).','archia-add-ons' ),
            "value"=> 'archia-partner',
        ),

        array(
            "type" => "textfield",
            "admin_label"   => true,
            "heading" => esc_html__("responsive", 'archia-add-ons'),
            "param_name" => "responsive",
            "description" => esc_html__("The format is: screen-size:number-items-display,larger-screen-size:number-items-display. Ex: 320:2,768:2,992:4,1200:5", 'archia-add-ons'),
            "value" => "320:1,425;1,768:2,992:4,1200:5"
        ),

        array(
            "type" => "dropdown",  
            // "holder"    => "div",
            // "admin_label"   => true,
            "heading" => __('Spacing', 'archia-add-ons'), 
            "param_name" => "spacing", 
            "value" => array(
                esc_html__('None', 'archia-add-ons') => '0' , 
                esc_html__('1px', 'archia-add-ons')  =>'1' , 
                esc_html__('2px', 'archia-add-ons') =>'2', 
                esc_html__('3px', 'archia-add-ons') =>'3', 
                esc_html__('4px', 'archia-add-ons') =>'4',
                esc_html__('5px', 'archia-add-ons')=> '5' ,
                esc_html__('10px', 'archia-add-ons') =>'10',
                esc_html__('15px', 'archia-add-ons')=>'15' ,
                esc_html__('20px', 'archia-add-ons') => '20',
                esc_html__('25px', 'archia-add-ons') => '25',
                esc_html__('30px', 'archia-add-ons') => '30',
            ), 
            "description" => __("Spacing between slide items", 'archia-add-ons'), 
            "std" => '10',
        ),
        array(
            "type" => "textfield",
            "admin_label"   => true,
            "heading" => esc_html__("Speed", 'archia-add-ons'),
            "param_name" => "speed",
            "value"=>'1300',
            "description" => esc_html__("Duration of transition between slides (in ms). Default: 1300", 'archia-add-ons')
        ),
        array(
            "type" => "checkbox",
            // "holder"    => "div",
            // "admin_label"   => true,
            "heading" => esc_html__("Show Title/Caption ", 'archia-add-ons'),
            "param_name" => "show_title",
            "value" => array( 
                esc_html__('Yes', 'archia-add-ons') => 'yes',   
            ),
            "std"=>'',    
            "description" => esc_html__("Show image caption. (If image have on  title, caption or description ).Default: not displayed.", 'archia-add-ons'),
        ),
        array(
            "type" => "checkbox",
            "admin_label"   => true,
            "heading" => esc_html__("Auto Play", 'archia-add-ons'),
            "param_name" => "autoplay",
            "value" => array( 
                esc_html__('Yes', 'archia-add-ons') => 'yes',   
            ),
            "std"=>'', 
        ),

        array(
            "type" => "checkbox",
            "admin_label"   => true,
            "heading" => esc_html__('Loop', 'archia-add-ons'),
            "param_name" => "loop",
            "value" => array(    
                            esc_html__('Yes', 'archia-add-ons') => 'yes',                                                                                
                        ),
            "description" => esc_html__("Set this to Yes to enable continuous loop mode", 'archia-add-ons'), 
            'std'=>'yes'
        ),

        array(
            "type" => "checkbox",
            "admin_label"   => true,
            "heading" => esc_html__("Show Navigation", 'archia-add-ons'),
            "param_name" => "show_navigation",
     
            "value" => array( 
                esc_html__('Yes', 'archia-add-ons') => 'yes',   
            ),
            "std"=>'yes', 
        ),

        array(
            "type" => "checkbox",
            "admin_label"   => true,
            "heading" => esc_html__("Show Dots", 'archia-add-ons'),
            "param_name" => "show_dots",
     
            "value" => array( 
                esc_html__('Yes', 'archia-add-ons') => 'yes',   
            ),
            "std"=>'', 
        ),
        

        array(
            "type" => "dropdown",  
            "admin_label"   => true,
            "heading" => __('CSS Animation', 'archia-add-ons'), 
            "param_name" => "ani_type", 
            "value" => archia_addons_vc_animation(),
            "description" => __("Select type of animation for element to be animated when it \"enters\" the browsers viewport (Note: works only in modern browsers).", 'archia-add-ons'), 
            "std" => '',
        ),
        array(
            "type" => "textfield",
            "admin_label"   => true,
            "heading" => esc_html__("Animation Duration", 'archia-add-ons'),
            "param_name" => "ani_time",
            "description" => esc_html__("Ex: 0.2s for 200 miliseconds, 2s for 2 second", 'archia-add-ons'),
            "value" => "1s",
            'dependency' => array(
                'element' => 'ani_type',
                'not_empty' => true,
            ),
        ),
        array(
            "type" => "textfield",
            "admin_label"   => true,
            "heading" => esc_html__("Animation delays", 'archia-add-ons'),
            "param_name" => "ani_delays",
            "description" => esc_html__("Delay for each items. Ex: 0.2s,0.4s,0.6s,0.8s (0.2s for 200 miliseconds, 2s for 2 second)", 'archia-add-ons'),
            "value" => "0.2s,0.4s,0.6s,0.8s,1s,1.2s",
            'dependency' => array(
                'element' => 'ani_type',
                'not_empty' => true,
            ),
        ),

        array(
            "type" => "textfield",
            "heading" => esc_html__("Extra class name", 'archia-add-ons'),
            "param_name" => "el_class",
            "description" => esc_html__("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", 'archia-add-ons')
        ),
        array(
            'type' => 'css_editor',
            'heading' => esc_html__( 'Css', 'archia-add-ons' ),
            'param_name' => 'css',
            'group' => esc_html__( 'Design options', 'archia-add-ons' ),
        ),
    ),
    // 'admin_enqueue_js'      => TYPHOON_ADD_ONS_DIR_URL . "assets/js/wpbakerry-eles.js", // need "holder"    => "div" and "class"     => "cth-vc-images" option for attach_image, attach_images type
    // 'js_view'=> 'CTHVCImages',
));

if ( class_exists( 'WPBakeryShortCode' ) ) {
    class WPBakeryShortCode_Archia_Partners extends WPBakeryShortCode {}
}