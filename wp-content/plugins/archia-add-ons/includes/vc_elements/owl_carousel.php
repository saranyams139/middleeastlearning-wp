<?php
vc_map(array(
    "name"                    => __("Owl Carousel", 'archia-add-ons'),
    "description"             => __("Slider using owl carousel plugin", 'archia-add-ons'),
    "base"                    => "archia_owl_carousel",
    "category"                => __('Archia Theme', 'archia-add-ons'),
    "as_parent"               => array('only' => 'archia_owl_carousel_item'), // Use only|except attributes to limit child shortcodes (separate multiple values with comma)
    "content_element"         => true,
    "show_settings_on_create" => false,
    "class"                   => 'archia_owl_carousel',
    "icon"                    => BBT_ABSPATH . "assets/cththemes-logo.png",
    "params"                  => array(
        array(
            "type"        => "textfield",
            "heading"     => __("Auto Play", 'archia-add-ons'),
            "param_name"  => "autoplay",
            "description" => __("Boolen or number in mili-second (5000)", 'archia-add-ons'),
        ),
        array(
            'type'        => 'dropdown',
            'heading'     => __('Single Item', 'archia-add-ons'),
            'param_name'  => 'singleitem',
            'value'       => array(
                __('Yes', 'archia-add-ons') => 'true',
                __('No', 'archia-add-ons')  => 'false',
            ),
            'description' => __('Set this to Yes if you want to display single item only', 'archia-add-ons'),
        ),

        array(
            "type"        => "textfield",
            "heading"     => __("Slide Speed", 'archia-add-ons'),
            "param_name"  => "slidespeed",
            "description" => __("Slide speed in milliseconds", 'archia-add-ons'),
            "value"       => "200",
        ),
        array(
            "type"        => "textfield",
            "heading"     => __("ID", 'archia-add-ons'),
            "param_name"  => "el_id",
            "description" => __("Slider id", 'archia-add-ons'),
        ),

        array(
            "type"        => "textfield",
            "heading"     => __("Extra class name", 'archia-add-ons'),
            "param_name"  => "el_class",
            "description" => __("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", 'archia-add-ons'),
        ),

    ),
    "js_view"                 => 'VcColumnView',
));

vc_map(array(
    "name"            => __("Slide Item", 'archia-add-ons'),
    "base"            => "archia_owl_carousel_item",
    "content_element" => true,
    "icon"            => get_template_directory_uri() . "/vc_extend/cth-icon.png",
    "as_child"        => array('only' => 'archia_owl_carousel'),
    "params"          => array(
        array(
            "type"        => "textfield",
            "heading"     => __("Extra class name", 'archia-add-ons'),
            "param_name"  => "el_class",
            "description" => __("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", 'archia-add-ons'),
        ),
        array(
            "type"        => "attach_image",
            "holder"      => "div",
            "class"       => "",
            "heading"     => __("Slide Image", 'archia-add-ons'),
            "param_name"  => "slideimg",
            "description" => __("Slide Image", 'archia-add-ons'),
        ),

        array(
            "type"       => "textarea_raw_html",
            "heading"    => __('Content', 'archia-add-ons'),
            //"holder"=>'div',
            "param_name" => "content",
            "ace_mode"   => "html",
            "ace_style"  => "min-height:200px;border:1px solid #bbb;",
            "value"      => "",
        ),

    ),
));

//Your "container" content element should extend WPBakeryShortCodesContainer class to inherit all required functionality
if (class_exists('WPBakeryShortCodesContainer')) {
    class WPBakeryShortCode_Archia_Owl_Carousel extends WPBakeryShortCodesContainer
    {}
}
if (class_exists('WPBakeryShortCode')) {
    class WPBakeryShortCode_Archia_Owl_Carousel_Item extends WPBakeryShortCode
    {
    }
}
