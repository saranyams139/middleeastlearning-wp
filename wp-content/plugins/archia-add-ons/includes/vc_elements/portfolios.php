<?php
vc_map(array(
    "name"        => __("Portfolios", 'archia-add-ons'),
    "description" => __("Building portfolio element", 'archia-add-ons'),
    "base"        => "our_portfolios",
    "class"       => "",
    "icon"        => BBT_ABSPATH . "assets/cththemes-logo.png",
    "category"    => __('Archia Theme', 'archia-add-ons'),
    "params"      => array(
        array(
            "type"        => "textfield",
            "holder"      => "div",
            "class"       => "",
            "heading"     => __("Count", 'archia-add-ons'),
            "param_name"  => "count",
            "value"       => "9",
            "description" => __("Number of portfolios to show", 'archia-add-ons'),
        ),
        array(
            "type"        => "dropdown",
            "class"       => "",
            "heading"     => __('Order by', 'archia-add-ons'),
            "param_name"  => "order_by",
            "value"       => array(
                __('Date', 'archia-add-ons')     => 'date',
                __('ID', 'archia-add-ons')       => 'ID',
                __('Author', 'archia-add-ons')   => 'author',
                __('Title', 'archia-add-ons')    => 'title',
                __('Modified', 'archia-add-ons') => 'modified',
            ),
            "description" => __("Order by", 'archia-add-ons'),
            "default"     => 'date',
        ),
        array(
            "type"        => "dropdown",
            "class"       => "",
            "heading"     => __('Order', 'archia-add-ons'),
            "param_name"  => "order",
            "value"       => array(
                __('Descending', 'archia-add-ons') => 'DESC',
                __('Ascending', 'archia-add-ons')  => 'ASC',

            ),
            "description" => __("Order", 'archia-add-ons'),
        ),
        array(
            "type"        => "textfield",
            "heading"     => __("Or Enter Story IDs", 'archia-add-ons'),
            "param_name"  => "ids",
            "description" => __("Enter story ids to show, separated by a comma.", 'archia-add-ons'),
        ),
        array(
            "type"        => "dropdown",
            "class"       => "",
            "heading"     => __('Columns Grid', 'archia-add-ons'),
            "param_name"  => "columns",
            "value"       => array(
                __('One Column', 'archia-add-ons')    => '12',
                __('Two Columns', 'archia-add-ons')   => '6',
                __('Three Columns', 'archia-add-ons') => '4',
                __('Four Columns', 'archia-add-ons')  => '3',
                //__('Five Columns', 'archia') => '5',
                __('Six Columns', 'archia-add-ons')   => '2',
            ),
            "description" => __("Columns Grid", 'archia-add-ons'),
            "default"     => '4',
        ),
        array(
            "type"        => "dropdown",
            "class"       => "",
            "heading"     => __('Tablet Columns Grid', 'archia-add-ons'),
            "param_name"  => "smcolumns",
            "value"       => array(
                __('One Column', 'archia-add-ons')    => '12',
                __('Two Columns', 'archia-add-ons')   => '6',
                __('Three Columns', 'archia-add-ons') => '4',
                __('Four Columns', 'archia-add-ons')  => '3',
                //__('Five Columns', 'archia') => '5',
                __('Six Columns', 'archia-add-ons')   => '2',
            ),
            "description" => __("Tablet Columns Grid", 'archia-add-ons'),
            "default"     => '6',
        ),
        array(
            "type"        => "dropdown",
            "class"       => "",
            "heading"     => __('Mobile Columns Grid', 'archia-add-ons'),
            "param_name"  => "xscolumns",
            "value"       => array(
                __('One Column', 'archia-add-ons')    => '12',
                __('Two Columns', 'archia-add-ons')   => '6',
                __('Three Columns', 'archia-add-ons') => '4',
                __('Four Columns', 'archia-add-ons')  => '3',
                //__('Five Columns', 'archia') => '5',
                __('Six Columns', 'archia-add-ons')   => '2',
            ),
            "description" => __("Mobile Columns Grid", 'archia-add-ons'),
            "default"     => '6',
        ),

        array(
            "type"        => "dropdown",
            "class"       => "",
            "heading"     => __('Show Filter', 'archia-add-ons'),
            "param_name"  => "showfilter",
            "value"       => array(
                __('Yes', 'archia-add-ons') => 'true',
                __('No', 'archia-add-ons')  => 'false',
            ),
            "description" => __("Set this to No to hide filter buttons bar.", 'archia-add-ons'),
        ),
        array(
            "type"        => "dropdown",
            "class"       => "",
            "heading"     => __('Show Item Large', 'archia-add-ons'),
            "param_name"  => "showlarge",
            "value"       => array(
                __('Yes', 'archia-add-ons') => 'true',
                __('No', 'archia-add-ons')  => 'false',
            ),
            "description" => __("Set this to No to hide View Large button.", 'archia-add-ons'),
        ),
        array(
            "type"        => "dropdown",
            "class"       => "",
            "heading"     => __('Show Item Detail', 'archia-add-ons'),
            "param_name"  => "showdetail",
            "value"       => array(
                __('Yes', 'archia-add-ons') => 'true',
                __('No', 'archia-add-ons')  => 'false',
            ),
            "description" => __("Set this to No to hide View Detail button.", 'archia-add-ons'),
        ),
        array(
            "type"        => "textfield",
            "heading"     => __("Extra class name", 'archia-add-ons'),
            "param_name"  => "el_class",
            "description" => __("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", 'archia-add-ons'),
        ),
    ),
));

if (class_exists('WPBakeryShortCode')) {
    class WPBakeryShortCode_Our_Portfolios extends WPBakeryShortCode
    {}
}
