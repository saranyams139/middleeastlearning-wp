<?php
/* add_ons_php */
vc_map( array(
    "name"                      => esc_html__("Progress Bars", 'archia-add-ons'),
    "base"                      => "cth_progress",
    "icon"                      => BBT_DIR_URL . "/assets/cththemes-logo.png",
    "category"                  => __( 'Archia Theme', 'archia-add-ons' ),
    "content_element"           => true,
    "show_settings_on_create"   => true,
    "params"    => array(

        array(
            'type' => 'param_group',
            'heading' => esc_html__( 'Values', 'archia-add-ons' ),
            'param_name' => 'bars',
            // 'description' => esc_html__( 'Enter values for graph - value, title and color.', 'inshot' ),
            'value' => urlencode( json_encode( array(
                array(
                    'label' => '2D Drawings',
                    'value' => '93%',
                    'color' => 'bg-pink',
                ),
                array(
                    'label' => '3D Modeling',
                    'value' => '70%',
                    'color' => 'bg-red',
                ),
                array(
                    'label' => 'Moodboard',
                    'value' => '48%',
                    'color' => 'bg-green',
                ),
            ) ) ),
            'params' => array(
                array(
                    'type' => 'textfield',
                    'heading' => esc_html__( 'Label', 'archia-add-ons' ),
                    'param_name' => 'label',
                    // 'description' => esc_html__( 'Enter text used as title of bar.', 'archia-add-ons' ),
                    'admin_label' => true,
                ),
                array(
                    'type' => 'textfield',
                    'heading' => esc_html__( 'Value', 'archia-add-ons' ),
                    'param_name' => 'value',
                    // 'description' => esc_html__( 'Enter text used as title of bar.', 'archia-add-ons' ),
                    'admin_label' => true,
                ),
                array(
                    "type" => "dropdown",
                    "heading" => esc_html__('Color', 'archia-add-ons'),
                    "param_name" => "color",
                    "value" => array(   
                        esc_html__('Default', 'archia-add-ons') => 'bg-default',  
                        esc_html__('Pink', 'archia-add-ons') => 'bg-pink',  
                        esc_html__('Red', 'archia-add-ons') => 'bg-red',  
                        esc_html__('Green', 'archia-add-ons') => 'bg-green',                                                                             
                    ),
                    "std" => 'bg-default', 
                ),
            ),
        ),

        
        array(
            "type" => "textfield",
            "heading" => esc_html__("Extra class name", 'archia-add-ons'),
            "param_name" => "el_class",
            "description" => esc_html__("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", 'archia-add-ons')
        ),
        array(
            'type' => 'css_editor',
            'heading' => esc_html__( 'Css', 'archia-add-ons' ),
            'param_name' => 'css',
            'group' => esc_html__( 'Design options', 'archia-add-ons' ),
        ),
    ),
    // 'admin_enqueue_js'      => TYPHOON_ADD_ONS_DIR_URL . "assets/js/wpbakerry-eles.js", // need "holder"    => "div" and "class"     => "cth-vc-images" option for attach_image, attach_images type
    // 'js_view'=> 'CTHVCImages',
));

if ( class_exists( 'WPBakeryShortCode' ) ) {
    class WPBakeryShortCode_CTH_Progress extends WPBakeryShortCode {}
}