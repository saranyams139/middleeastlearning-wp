<?php
vc_map( array(
    "name"      => __("Process Item", 'archia-add-ons'),
    // "description" => __("Feature Box",'archia-add-ons'),
    "base"      => "cth_process",
    "class"     => "",
    "icon"      => BBT_DIR_URL . "/assets/cththemes-logo.png",
    "category"  => __('Archia Theme', 'archia-add-ons'),
    "params"    => array(
        
        array(
            "type"              => "attach_image",
            "admin_label"       => true,
            "heading"           => __("Image", 'archia-add-ons'),
            "param_name"        => "img",
            // "description"       => __("Choose a Image", 'archia-add-ons'),
            
        ), 
        
        array(
            "type"      => "textfield",
            "admin_label"       => true,
            "heading"   => __("Title", 'archia-add-ons'),
            "param_name"=> "title",
            "value"     => "Architecture",
            // "description" => __("Enter section title (Note: you can leave it empty).", 'archia-add-ons')
        ),
        array(
            "type"      => "textfield",
            "admin_label"       => true,
            "heading"   => __("Sub-Title", 'archia-add-ons'),
            "param_name"=> "sub_title",
            "value"     => "Commercial Work Design",
        ),
        array(
            "type"        => "vc_link",
            "heading"     => __("Link", 'archia-add-ons'),
            "param_name"  => "link",
            "value"       => "#",
            // "description"   => esc_html__( 'Enter links for each partner (Note: divide links with linebreaks (Enter) or | and no spaces)', 'archia-add-ons' ). get_post_type_archive_link( 'listing' ),
        ),
        array(
            "type"      => "textarea_html",
            // "class"     => "",
            "heading"   => __("Text", 'archia-add-ons'),
            "param_name"=> "content",
            "value"     => "<p>We have the expertise to create just the right web presence for you.</p>",
            "description" => __("Description", 'archia-add-ons'),
        ),
        array(
            "type" => "dropdown",  
            "admin_label"   => true,
            "heading" => __('CSS Animation', 'archia-add-ons'), 
            "param_name" => "ani_type", 
            "value" => archia_addons_vc_animation(),
            "description" => __("Select type of animation for element to be animated when it \"enters\" the browsers viewport (Note: works only in modern browsers).", 'archia-add-ons'), 
            "std" => '',
        ),
        array(
            "type" => "textfield",
            "admin_label"   => true,
            "heading" => esc_html__("Animation Duration", 'archia-add-ons'),
            "param_name" => "ani_time",
            "description" => esc_html__("Ex: 0.2s for 200 miliseconds, 2s for 2 second", 'archia-add-ons'),
            "value" => "2s",
            'dependency' => array(
                'element' => 'ani_type',
                'not_empty' => true,
            ),
        ),
        array(
            "type" => "textfield",
            "admin_label"   => true,
            "heading" => esc_html__("Animation delay", 'archia-add-ons'),
            "param_name" => "ani_delay",
            "description" => esc_html__("Ex: 0.2s for 200 miliseconds, 2s for 2 second", 'archia-add-ons'),
            "value" => "0.2s",
            'dependency' => array(
                'element' => 'ani_type',
                'not_empty' => true,
            ),
        ),
        array(
            "type"      => "textfield",
            "heading"   => __("Extra class name", 'archia-add-ons'),
            "param_name" => "el_class",
            "description" => __("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", 'archia-add-ons')
        ),
        array(
            'type'          => 'css_editor',
            'heading'       => esc_html__( 'Css', 'archia-add-ons' ),
            'param_name'    => 'css',
            'group'         => esc_html__( 'Design options', 'archia-add-ons' ),
        ),
    )
));

if ( class_exists( 'WPBakeryShortCode' ) ) {
    class WPBakeryShortCode_CTH_Process extends WPBakeryShortCode {}
}