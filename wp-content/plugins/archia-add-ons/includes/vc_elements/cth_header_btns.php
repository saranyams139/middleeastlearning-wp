<?php
vc_map(array(
    "name"        => __("Header Buttons", 'archia-add-ons'),
    // "description" => __("Logo Theme", 'archia-add-on'),
    "base"        => "cth_header_btns",
    "class"       => "",
    "icon"        => BBT_DIR_URL . "assets/cththemes-logo.png",
    "category"    => 'Archia Header',
    "params"      => array(
        array(
            "type"          => "textarea_raw_html",
            // "holder"      => "div",
            "heading"       => esc_html__("Contact", 'archia-add-ons'),
            "param_name"    => "content",
            'value'         => base64_encode( '<div  class="contact-info">
    <i class="fa fa-phone"></i>
    <span>Phone</span>
    <h4 class="m-b0">003 746 87 92</h4>
</div>'),

        ),
        array(
            "type" => "checkbox",
            "admin_label"   => true,
            "heading" => esc_html__("Show search button?", 'archia-add-ons'),
            "param_name" => "show_search",
            "value" => array( 
                esc_html__('Yes', 'archia-add-ons') => 'yes',   
            ),
            "std"=> 'yes', 
        ),
        array(
            "type"        => "textfield",
            "heading"     => __("Extra class name", 'archia-add-ons'),
            "param_name"  => "el_class",
            "description" => __("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", 'archia-add-ons'),
        ),
    ),
));
if (class_exists('WPBakeryShortCode')) {
    class WPBakeryShortCode_Cth_Header_Btns extends WPBakeryShortCode{}
}