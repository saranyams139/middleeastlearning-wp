<?php
/* add_ons_php */

vc_map(array(
    "name"                    => esc_html__("Portfolios Grid", 'archia-add-ons'),
    // "description"             => esc_html__("Archia Portfolio", 'archia-add-ons'),
    "base"                    => "archia_portfolio",
    "content_element"         => true,
    "icon"                    => BBT_DIR_URL . "/assets/cththemes-logo.png",
    // "html_template"             => TYPHOON_ADD_ONS_DIR.'/vc_templates/cth_section_title.php',
    "category"                => __('Archia Theme', 'archia-add-ons'),
    "show_settings_on_create" => true,
    "params"                  => array(
        // array(
        //     "type"       => "dropdown",
        //     "heading"    => esc_html__('Layout', 'archia-add-ons'),
        //     "param_name" => "layout",
        //     "value"      => array(
        //         esc_html__('Normal', 'archia-add-ons')         => 'masonry',
        //         esc_html__('Sidebar Filter', 'archia-add-ons') => 'sidebar-filter',
        //     ),
        //     "std"        => 'masonry',
        // ),

        array(
            "type"       => "dropdown",
            "class"      => "",
            "heading"    => esc_html__('Grid style', 'archia-add-ons'),
            "param_name" => "folio_item_style",
            "value"      => array(
                // esc_html__('Show on Image - not', 'archia-add-ons')    => 'default-thumb-info',
                // esc_html__('Show on hover - not', 'archia-add-ons')    => 'hid-det-items',
                // esc_html__('Bellow Thumbnail - not', 'archia-add-ons') => 'vis-thumb-info',
                esc_html__('Default - Home 1', 'archia-add-ons') => 'default',
                esc_html__('Tiles - Portfolio 2', 'archia-add-ons') => 'tile',
                esc_html__('Masonry - Portfolio 1', 'archia-add-ons') => 'masonry',
                esc_html__('Home 2 Style', 'archia-add-ons') => 'home2',
                esc_html__('Home 4 Style', 'archia-add-ons') => 'home4',
                esc_html__('Home 5 Style', 'archia-add-ons') => 'home5',
            ),

            'std'        => 'default',
        ),

        array(
            "type"        => "textfield",
            "heading"     => esc_html__("Portfolio Category IDs to include", 'archia-add-ons'),
            "param_name"  => "cat_ids",
            "description" => esc_html__("Enter portfolio category ids to include, separated by a comma. Leave empty to display all categories.", 'archia-add-ons'),
            "value"       => '',
        ),
        array(
            "type"          => "textfield", 
            "admin_label"   => true,
            "heading"       => esc_html__("Project Category IDs to exclude", 'archia-add-ons'), 
            "param_name"    => "cat_ids_not", 
            "description"   => esc_html__("Enter Project category ids to exclude, separated by a comma. Leave empty to display all categories. Will be ignore if include para above not empty.", 'archia-add-ons'),
            "value"         => '',
        ), 
        array(
            "type"       => "dropdown",
            "class"      => "",
            "heading"    => esc_html__('Order Portfolio Categories by', 'archia-add-ons'),
            "param_name" => "cat_order_by",
            "value"      => array(
                esc_html__('Name', 'archia-add-ons')  => 'name',
                esc_html__('ID', 'archia-add-ons')    => 'id',
                esc_html__('Count', 'archia-add-ons') => 'count',
                esc_html__('Slug', 'archia-add-ons')  => 'slug',
                esc_html__('None', 'archia-add-ons')  => 'none',
            ),
            "std"        => 'name',
        ),
        array(
            "type"       => "dropdown",
            "class"      => "",
            "heading"    => esc_html__('Sort Order', 'archia-add-ons'),
            "param_name" => "cat_order",
            "value"      => array(
                esc_html__('Ascending', 'archia-add-ons')  => 'ASC',
                esc_html__('Descending', 'archia-add-ons') => 'DESC',

            ),
            "std"        => 'ASC',
        ),
        array(
            "type"       => "dropdown",
            "class"      => "",
            "heading"    => esc_html__('Show Filter', 'archia-add-ons'),
            "param_name" => "show_filter",
            "value"      => array(
                esc_html__('Yes', 'archia-add-ons') => 'yes',
                esc_html__('No', 'archia-add-ons')  => 'no',
            ),
            "std"        => 'yes',
        ),

        // array(
        //     "type"          => "dropdown",
        //     "heading"       => esc_html__('Is Sidebar Filter', 'inshot-add-ons'),
        //     "param_name"    => "sidebar_filter",
        //     "value"         => array(
        //         esc_html__('Yes', 'inshot-add-ons')     => 'yes',
        //         esc_html__('No', 'inshot-add-ons')      => 'no',
        //     ),
        //     "std"           => 'no',
        //     'dependency'        => array(
        //         'element'   => 'show_filter',
        //         'value'     => array( 'yes' ),
        //         'not_empty' => false,
        //     ),
        // ),
        // array(
        //     "type" => "textfield",
        //     "holder" => "div",
        //     "heading" => esc_html__("Sidebar Title", 'inshot-add-ons'),
        //     "param_name" => "sidebar_title",
        //     'value'         => 'Our Portfolios',
        //     'dependency'        => array(
        //         'element'   => 'sidebar_filter',
        //         'value'     => array( 'yes' ),
        //         'not_empty' => false,
        //     ),
        // ),
        // array(
        //     "type" => "dropdown",
        //     "class" => "",
        //     "heading" => esc_html__('Filter Width', 'inshot-add-ons'),
        //     "param_name" => "filter_width",
        //     "value" => array(
        //         esc_html__('Fixed Width', 'inshot-add-ons') => 'container',
        //         esc_html__('Fullwidth', 'inshot-add-ons') => 'full-container',
        //     ),

        //     "std" => 'full-container',
        //     'dependency'        => array(
        //         'element'   => 'show_filter',
        //         'value'     => array( 'yes' ),
        //         'not_empty' => false,
        //     ),
        // ),

        // array(
        //     "type" => "dropdown",
        //     "class" => "",
        //     "heading" => esc_html__('Show Counter', 'inshot-add-ons'),
        //     "param_name" => "show_counter",
        //     "value" => array(
        //         esc_html__('Yes', 'inshot-add-ons') => 'yes',
        //         esc_html__('No', 'inshot-add-ons') => 'no',
        //     ),

        //     "std" => 'yes',
        //     'dependency'        => array(
        //         'element'   => 'show_filter',
        //         'value'     => array( 'yes' ),
        //         'not_empty' => false,
        //     ),
        // ),

        array(
            "type"        => "textfield",
            "admin_label"   => true,
            "heading"     => esc_html__("Enter Portfolio IDs", 'archia-add-ons'),
            "param_name"  => "ids",
            "description" => esc_html__("Enter portfolio ids to show, separated by a comma. Leave empty to get all.", 'archia-add-ons'),
        ),
        array(
            "type"        => "textfield",
            // "holder" => "div",
            "heading"     => esc_html__("Portfolio IDs to Exclude", 'archia-add-ons'),
            "param_name"  => "ids_not",
            "description" => esc_html__("Enter portfolio ids to exclude, separated by a comma. Use if the field above is empty. Leave empty to get all.", 'archia-add-ons'),
        ),
        array(
            "type"        => "dropdown",
            "class"       => "",
            "heading"     => esc_html__('Order Portfolios by', 'archia-add-ons'),
            "param_name"  => "order_by",
            "value"       => array(
                esc_html__('Date', 'archia-add-ons') => 'date' , 
                esc_html__('ID', 'archia-add-ons')  =>'ID' , 
                esc_html__('Author', 'archia-add-ons') =>'author', 
                esc_html__('Title', 'archia-add-ons') =>'title', 
                esc_html__('Modified', 'archia-add-ons') =>'modified',
                esc_html__('Random', 'archia-add-ons')=> 'rand' ,
                esc_html__('Comment Count', 'archia-add-ons') =>'comment_count',
                esc_html__('Menu Order', 'archia-add-ons')=>'menu_order' ,
                esc_html__('ID order given (post__in)', 'archia-add-ons') => 'post__in',
            ),
            "description" => esc_html__("Order Portfolios by", 'archia-add-ons'),
            "std"         => 'date',
        ),
        array(
            "type"        => "dropdown",
            "class"       => "",
            "heading"     => esc_html__('Order Portfolios', 'archia-add-ons'),
            "param_name"  => "order",
            "value"       => array(
                esc_html__('Ascending', 'archia-add-ons')  => 'ASC',
                esc_html__('Descending', 'archia-add-ons') => 'DESC',

            ),
            "description" => esc_html__("Order Portfolios", 'archia-add-ons'),
            "std"         => 'DESC',
        ),
        array(
            "type"        => "textfield",
            "admin_label"   => true,
            "heading"     => esc_html__("Post to show", 'archia-add-ons'),
            "param_name"  => "posts_per_page",
            "description" => esc_html__("Number of portfolio items to show (-1 for all).", 'archia-add-ons'),
            "value"       => '12',
        ),

        // array(
        //     "type"       => "dropdown",
        //     "class"      => "",
        //     "heading"    => esc_html__('Content Width', 'archia-add-ons'),
        //     "param_name" => "folio_content_width",
        //     "value"      => array(
        //         esc_html__('Small Boxed', 'archia-add-ons') => 'boxed-container',
        //         esc_html__('Wide Boxed', 'archia-add-ons')  => 'big-container',
        //         esc_html__('Fullwidth', 'archia-add-ons')   => 'full-container',

        //     ),

        //     "std"        => 'full-container',
        // ),

        array(
            "type"       => "dropdown",
            "class"      => "",
            "heading"    => esc_html__('Grid Columns', 'archia-add-ons'),
            "param_name" => "columns_grid",
            "value"      => array(
                esc_html__('One Column', 'archia-add-ons')    => 'one',
                esc_html__('Two Columns', 'archia-add-ons')   => 'two',
                esc_html__('Three Columns', 'archia-add-ons') => 'three',
                esc_html__('Four Columns', 'archia-add-ons')  => 'four',
                esc_html__('Five Columns', 'archia-add-ons')  => 'five',
                esc_html__('Six Columns', 'archia-add-ons')   => 'six',
                esc_html__('Seven Columns', 'archia-add-ons') => 'seven',
                esc_html__('Eight Columns', 'archia-add-ons') => 'eight',
                esc_html__('Nine Columns', 'archia-add-ons')  => 'nine',
                esc_html__('Ten Columns', 'archia-add-ons')   => 'ten',

            ),

            "std"        => 'three',
        ),

        array(
            "type"        => "textfield",
            "admin_label"   => true,
            "heading"     => esc_html__("Portfolio Items width", 'archia-add-ons'),
            "param_name"  => "items_width",
            "description" => esc_html__("Defined portfolio width. Available values are x1(default),x2(x2 width),x3(x3 width), and separated by comma. Ex: x2,x1,x1,x2,x2,x1", 'archia-add-ons'),
            "value"       => 'x2,x1,x1,x2,x2,x1',
        ),

        array(
            "type"       => "dropdown",
            "class"      => "",
            "heading"    => esc_html__('Spacing', 'archia-add-ons'),
            "param_name" => "spacing",
            "value"      => array(
                esc_html__('Extra Small', 'archia-add-ons') => 'extrasmall',
                esc_html__('Small', 'archia-add-ons')       => 'small',
                esc_html__('Medium', 'archia-add-ons')      => 'medium',
                esc_html__('Big', 'archia-add-ons')         => 'big',
                esc_html__('None', 'archia-add-ons')        => 'no',

            ),
            "std"        => 'big',
        ),

        // array(
        //     "type"       => "dropdown",
        //     "class"      => "",
        //     "heading"    => esc_html__('Show Categories', 'archia-add-ons'),
        //     "param_name" => "show_cat",
        //     "value"      => array(
        //         esc_html__('Yes', 'archia-add-ons') => 'yes',
        //         esc_html__('No', 'archia-add-ons')  => 'no',
        //     ),

        //     "std"        => 'yes',
        // ),

        // array(
        //     "type"       => "dropdown",
        //     "class"      => "",
        //     "heading"    => esc_html__('Show Excerpt', 'archia-add-ons'),
        //     "param_name" => "show_excerpt",
        //     "value"      => array(
        //         esc_html__('No', 'archia-add-ons')  => 'no',
        //         esc_html__('Yes', 'archia-add-ons') => 'yes',

        //     ),

        //     "std"        => 'no',
        // ),
        // array(
        //     "type"       => "dropdown",
        //     "class"      => "",
        //     "heading"    => esc_html__('Show View Project', 'archia-add-ons'),
        //     "param_name" => "show_view_project",
        //     "value"      => array(
        //         esc_html__('Yes', 'archia-add-ons') => 'yes',
        //         esc_html__('No', 'archia-add-ons')  => 'no',
        //     ),

        //     "std"        => 'yes',
        // ),

        // array(
        //     "type"       => "dropdown",
        //     "class"      => "",
        //     "heading"    => esc_html__('Enable Gallery', 'archia-add-ons'),
        //     "param_name" => "enable_gallery",
        //     "value"      => array(
        //         esc_html__('Yes', 'archia-add-ons') => 'yes',
        //         esc_html__('No', 'archia-add-ons')  => 'no',
        //     ),
        //     "std"        => 'yes',

        // ),

        // array(
        //     "type"       => "dropdown",
        //     "class"      => "",
        //     "heading"    => esc_html__('Use INFINITE scroll to load more items?', 'archia-add-ons'),
        //     "param_name" => "show_loadmore",
        //     "value"      => array(
        //         esc_html__('Yes', 'archia-add-ons') => 'yes',
        //         esc_html__('No', 'archia-add-ons')  => 'no',
        //     ),
        //     "std"        => 'yes',
        // ),

        // array(
        //     "type"        => "textfield",
        //     "admin_label"   => true,
        //     "heading"     => esc_html__("Load more items", 'archia-add-ons'),
        //     "param_name"  => "loadmore_posts",
        //     "description" => esc_html__("Number of items to get on additional load.", 'archia-add-ons'),
        //     "value"       => '3',
        //     'dependency'  => array(
        //         'element'   => 'show_loadmore',
        //         'value'     => array('yes'),
        //         'not_empty' => false,
        //     ),
        // ),

        // array(
        //     "type"       => "dropdown",
        //     "class"      => "",
        //     "heading"    => esc_html__('Show Pagination', 'archia-add-ons'),
        //     "param_name" => "show_pagination",
        //     "value"      => array(
        //         esc_html__('Yes', 'archia-add-ons') => 'yes',
        //         esc_html__('No', 'archia-add-ons')  => 'no',
        //     ),
        //     "std"        => 'no',
        //     'dependency' => array(
        //         'element'   => 'show_loadmore',
        //         'value'     => array('no'),
        //         'not_empty' => false,
        //     ),
        // ),

        array(
            "type" => "dropdown",  
            "admin_label"   => true,
            "heading" => __('CSS Animation', 'archia-add-ons'), 
            "param_name" => "ani_type", 
            "value" => archia_addons_vc_animation(),
            "description" => __("Select type of animation for element to be animated when it \"enters\" the browsers viewport (Note: works only in modern browsers).", 'archia-add-ons'), 
            "std" => '',
        ),
        array(
            "type" => "textfield",
            "admin_label"   => true,
            "heading" => esc_html__("Animation Duration", 'archia-add-ons'),
            "param_name" => "ani_time",
            "description" => esc_html__("Ex: 0.2s for 200 miliseconds, 2s for 2 second", 'archia-add-ons'),
            "value" => "2s",
            'dependency' => array(
                'element' => 'ani_type',
                'not_empty' => true,
            ),
        ),
        array(
            "type" => "textfield",
            "admin_label"   => true,
            "heading" => esc_html__("Animation delays", 'archia-add-ons'),
            "param_name" => "ani_delays",
            "description" => esc_html__("Delay for each items. Ex: 0.2s,0.4s,0.6s,0.8s (0.2s for 200 miliseconds, 2s for 2 second)", 'archia-add-ons'),
            "value" => "0.2s,0.4s,0.6s,0.8s,1s,1.2s",
            'dependency' => array(
                'element' => 'ani_type',
                'not_empty' => true,
            ),
        ),


        array(
            "type"        => "textfield",
            "heading"     => esc_html__("Extra class name", 'archia-add-ons'),
            "param_name"  => "el_class",
            "description" => esc_html__("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", 'archia-add-ons'),
        ),
        array(
            'type'       => 'css_editor',
            'heading'    => esc_html__('Css', 'archia-add-ons'),
            'param_name' => 'css',
            'group'      => esc_html__('Design options', 'archia-add-ons'),
        ),
    ),
));
if (class_exists('WPBakeryShortCode')) {
    class WPBakeryShortCode_Archia_Portfolio extends WPBakeryShortCode
    {}
}
// end Our Gallery
