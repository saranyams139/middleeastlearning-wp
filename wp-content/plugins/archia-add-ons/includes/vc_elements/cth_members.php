<?php
/* add_ons_php */
vc_map( array(
    "name"                      => esc_html__("Members Grid", 'archia-add-ons'),
    // "description"               => esc_html__("Special Menu",'archia-add-on'),
    "base"                      => "cth_members",
    "icon"                      => BBT_DIR_URL . "/assets/cththemes-logo.png",
    "category"                  => __( 'Archia Theme', 'archia-add-ons' ),
    "content_element"           => true,
    "show_settings_on_create"   => true,
    "params"                    => array(

        array(
            'type' => 'dropdown',
            'heading' => __( 'Box Style', 'archia-add-ons' ),
            'value' => array(
                __( 'Simple Style', 'archia-add-ons' ) => 'team-simple',
                __( 'Hover Style', 'archia-add-ons' ) => 'team-style1',
            ),
            'param_name' => 'mem_style',
            "std"         => "team-style1",
            // 'description' => __( 'Select icon library.', 'js_composer' ),
        ),

        array(
            "type"          => "textfield", 
            "admin_label"   => true,
            // "holder"        => "div",
            "heading"       => esc_html__("Enter Member IDs", 'archia-add-ons'), 
            "param_name"    => "ids", 
            "description"   => esc_html__("Enter post ids to show, separated by a comma.", 'archia-add-ons')
        ), 
        array(
            "type"          => "textfield", 
            "admin_label"   => true,
            // "holder"        => "div",
            "heading"       => esc_html__("Member IDs to Exclude", 'archia-add-ons'), 
            "param_name"    => "ids_not", 
            "description"   => esc_html__("Enter post ids to exclude, separated by a comma. Use if the field above is empty. Leave empty to get all.", 'archia-add-ons')
        ), 

        array(
            "type" => "dropdown", 
            "class" => "", 
            "heading" => __('Order Members by', 'archia-add-ons'), 
            "param_name" => "order_by", 
            "value" => array(
                esc_html__('Date', 'archia-add-ons') => 'date' , 
                esc_html__('ID', 'archia-add-ons')  =>'ID' , 
                esc_html__('Author', 'archia-add-ons') =>'author', 
                esc_html__('Title', 'archia-add-ons') =>'title', 
                esc_html__('Modified', 'archia-add-ons') =>'modified',
                esc_html__('Random', 'archia-add-ons')=> 'rand' ,
                esc_html__('Comment Count', 'archia-add-ons') =>'comment_count',
                esc_html__('Menu Order', 'archia-add-ons')=>'menu_order' ,
                esc_html__('ID order given (post__in)', 'archia-add-ons') => 'post__in',


            ), 
            // "description" => __("Order Speakers by", 'cth-gather-plugins'), 
            "std" => 'date',
        ), 
        array(
            "type" => "dropdown", 
            "class" => "", 
            "heading" => __('Sort Order', 'archia-add-ons'), 
            "param_name" => "order", 
            "value" => array(
                
                __('Descending', 'archia-add-ons') => 'DESC', 
                __('Ascending', 'archia-add-ons') => 'ASC',
                
            ), 
            // "description" => __("Order Speakers", 'cth-gather-plugins'),
            "std" => 'DESC',
        ), 
        array(
            "type"          => "textfield",
            "admin_label"   => true,
            "heading"       => esc_html__("Posts per page", 'archia-add-ons'),
            "param_name"    => "posts_per_page",
            "description"   => esc_html__("Number of menu items to show (-1 for all).", 'archia-add-ons'),
            "value"         => '4',
        ),

        array(
            "type"       => "dropdown",
            "class"      => "",
            "heading"    => esc_html__('Grid Columns', 'archia-add-ons'),
            "param_name" => "columns_grid",
            "value"      => array(
                esc_html__('One Column', 'archia-add-ons')    => 'one',
                esc_html__('Two Columns', 'archia-add-ons')   => 'two',
                esc_html__('Three Columns', 'archia-add-ons') => 'three',
                esc_html__('Four Columns', 'archia-add-ons')  => 'four',
                esc_html__('Five Columns', 'archia-add-ons')  => 'five',
                esc_html__('Six Columns', 'archia-add-ons')   => 'six',
                esc_html__('Seven Columns', 'archia-add-ons') => 'seven',
                esc_html__('Eight Columns', 'archia-add-ons') => 'eight',
                esc_html__('Nine Columns', 'archia-add-ons')  => 'nine',
                esc_html__('Ten Columns', 'archia-add-ons')   => 'ten',

            ),

            "std"        => 'four',
        ),


        array(
            "type"       => "dropdown",
            "class"      => "",
            "heading"    => esc_html__('Spacing', 'archia-add-ons'),
            "param_name" => "spacing",
            "value"      => array(
                esc_html__('Extra Small', 'archia-add-ons') => 'extrasmall',
                esc_html__('Small', 'archia-add-ons')       => 'small',
                esc_html__('Medium', 'archia-add-ons')      => 'medium',
                esc_html__('Big', 'archia-add-ons')         => 'big',
                esc_html__('None', 'archia-add-ons')        => 'no',

            ),
            "std"        => 'big',
        ),

        array(
            "type" => "checkbox", 
            "heading" => __('Show Socials?', 'archia-add-ons'), 
            "param_name" => "mem_socials", 
            "value" => array(
                
                __('Yes', 'archia-add-ons') => 'yes', 
               
                
            ), 
            // "description" => __("Order Speakers", 'cth-gather-plugins'),
            "std" => 'yes',
        ), 

        array(
            "type" => "dropdown",  
            "admin_label"   => true,
            "heading" => __('CSS Animation', 'archia-add-ons'), 
            "param_name" => "ani_type", 
            "value" => archia_addons_vc_animation(),
            "description" => __("Select type of animation for element to be animated when it \"enters\" the browsers viewport (Note: works only in modern browsers).", 'archia-add-ons'), 
            "std" => '',
        ),
        array(
            "type" => "textfield",
            "admin_label"   => true,
            "heading" => esc_html__("Animation Duration", 'archia-add-ons'),
            "param_name" => "ani_time",
            "description" => esc_html__("Ex: 0.2s for 200 miliseconds, 2s for 2 second", 'archia-add-ons'),
            "value" => "2s",
            'dependency' => array(
                'element' => 'ani_type',
                'not_empty' => true,
            ),
        ),
        array(
            "type" => "textfield",
            "admin_label"   => true,
            "heading" => esc_html__("Animation delays", 'archia-add-ons'),
            "param_name" => "ani_delays",
            "description" => esc_html__("Delay for each items. Ex: 0.2s,0.4s,0.6s,0.8s (0.2s for 200 miliseconds, 2s for 2 second)", 'archia-add-ons'),
            "value" => "0.2s,0.4s,0.6s,0.8s",
            'dependency' => array(
                'element' => 'ani_type',
                'not_empty' => true,
            ),
        ),


        
        array(
            "type"          => "textfield",
            "heading"       => esc_html__("Extra class name", 'archia-add-ons'),
            "param_name"    => "el_class",
            "description"   => esc_html__("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", 'archia-add-ons'),
            "value"         => "",
        ),

        array(
            'type'          => 'css_editor',
            'heading'       => esc_html__( 'Css', 'archia-add-ons' ),
            'param_name'    => 'css',
            'group'         => esc_html__( 'Design options', 'archia-add-ons' ),
         ),
    )
));
if ( class_exists( 'WPBakeryShortCode' ) ) {
    class WPBakeryShortCode_CTH_Members extends WPBakeryShortCode {}
}