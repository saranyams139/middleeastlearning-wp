<?php
vc_map(array(
    "name"        => __("Team Member", 'archia-add-ons'),
    "description" => __("Team member", 'archia-add-ons'),
    "base"        => "cth_member",
    "class"       => "",
    "icon"        => BBT_ABSPATH . "assets/cththemes-logo.png",
    "category"    => __('Archia Theme', 'archia-add-ons'),
    "params"      => array(
        array(
            "type"        => "textfield",
            "heading"     => __("Name", 'archia-add-ons'),
            "holder"      => "div",
            "value"       => 'Asep koswara',
            "param_name"  => "name",
            "description" => __("Member name", 'archia-add-ons'),
        ),
        array(
            "type"        => "textfield",
            "heading"     => __("Job", 'archia-add-ons'),
            //"holder"    => "div",
            "param_name"  => "job",
            "value"       => 'CEO',
            "description" => __("Member Job", 'archia-add-ons'),
        ),
        array(
            "type"        => "attach_image",
            "class"       => "",
            "heading"     => __("Photo", 'archia-add-ons'),
            "param_name"  => "photo",
            "value"       => "",
            "description" => __("Upload avatar photo of the member", 'archia-add-ons'),
        ),
        array(
            "type"        => "textarea",
            "class"       => "",
            "heading"     => __("Member Introduction", 'archia-add-ons'),
            "param_name"  => "content",
            "value"       => '<blockquote>
Ullum scripserit eum ad in salutandi ocurreret nec, his alii consequat in. essent mea. Ei pro corrumpit elit
</blockquote>
<div class="follow-team"><a href="#" class="follow-link" target="_blank">[faicon name="linkedin"]</a><a href="#" class="follow-link" target="_blank">[faicon name="facebook"]</a><a href="#" class="follow-link" target="_blank">[faicon name="twitter"]</a>
</div>',
            "description" => __("Introduction", 'archia-add-ons'),
        ),

        array(
            "type"        => "textfield",
            "heading"     => __("Extra class name", 'archia-add-ons'),
            "param_name"  => "el_class",
            "description" => __("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", 'archia-add-ons'),
        ),
    ),
));
if (class_exists('WPBakeryShortCode')) {
    class WPBakeryShortCode_Cth_Member extends WPBakeryShortCode
    {}
}
