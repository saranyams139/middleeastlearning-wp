<?php
vc_map( array(
    "name"      => __("Section Head", 'archia-add-ons'),
    // "description" => __("Section Title",'archia-add-ons'),
    "base"      => "sec_head",
    "class"     => "",
    "icon"      => BBT_DIR_URL . "/assets/cththemes-logo.png",
    "category"  => __('Archia Theme', 'archia-add-ons'),
    "params"    => array(
        array(
            "type"      => "dropdown",
            "class"     =>"",
            "heading"   => __('Title Alignment', 'archia-add-ons'),
            "param_name" => "section_loca",
            "value"     => array(   
                            __('Left', 'archia-add-ons') => '',  
                            __('Center', 'archia-add-ons') => 'text-center',
                            __('Right', 'archia-add-ons') => 'text-right',
                        ),
            "std" => 'text-center',
            // "description" => __("Choose section title location", 'archia-add-ons'), 
        ), 
        array(
            "type"      => "textfield",
            "class"     => "",
            "heading"   => __("Pre Title", 'archia-add-ons'),
            "param_name"=> "pre_title",
            "value"     => "",
            "description" => ''
        ),
        array(
            "type"      => "textfield",
            "class"     => "",
            "heading"   => __("Title", 'archia-add-ons'),
            "param_name"=> "title",
            "value"     => "Section Head",
            "description" => __("Enter section title (Note: you can leave it empty).", 'archia-add-ons')
        ),
        array(
            "type"      => "dropdown",
            "class"     =>"",
            "heading"   => __('Color Title', 'archia-add-ons'),
            "param_name" => "col_title",
            "value"     => array(   
                            __('Grey', 'archia-add-ons') => '',  
                            __('Blue Dark', 'archia-add-ons') => 'text-blue-dark',
                        ),
            "description" => __("Choose section title color title ", 'archia-add-ons'), 
            "default"     => '',     
        ),
        array(
            "type"      => "textarea_html",
            "class"     => "",
            "heading"   => __("Text", 'archia-add-ons'),
            "param_name"=> "content",
            "value"     => "",
            "description" => __("Description", 'archia-add-ons'),
        ),
        array(
            "type"      => "textfield",
            "heading"   => __("Extra class name", 'archia-add-ons'),
            "param_name" => "el_class",
            "description" => __("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", 'archia-add-ons')
        ),
        array(
            'type'          => 'css_editor',
            'heading'       => esc_html__( 'Css', 'archia-add-ons' ),
            'param_name'    => 'css',
            'group'         => esc_html__( 'Design options', 'archia-add-ons' ),
        ),
    )
));

if ( class_exists( 'WPBakeryShortCode' ) ) {
    class WPBakeryShortCode_Sec_Head extends WPBakeryShortCode {}
}