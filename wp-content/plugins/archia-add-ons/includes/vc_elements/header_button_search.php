<?php
vc_map(array(
    "name"        => __("Button Search", 'archia-add-ons'),
    // "description" => __("Logo Theme", 'archia-add-on'),
    "base"        => "cth_header_search",
    "class"       => "",
    "icon"        => BBT_DIR_URL . "assets/cththemes-logo.png",
    "category"    => 'Archia Header',
    "params"      => array(
        // array(
        //     "type"        => "textfield",
        //     "heading"     => __("", 'archia-add-on'),
        //     "param_name"  => "",
        //     "description" => __("", 'archia-add-on'),
        // ),
        array(
            "type"        => "textfield",
            "heading"     => __("Extra class name", 'archia-add-ons'),
            "param_name"  => "el_class",
            "description" => __("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", 'archia-add-ons'),
        ),
    ),
));
if (class_exists('WPBakeryShortCode')) {
    class WPBakeryShortCode_Cth_Header_Search extends WPBakeryShortCode
    {}
}