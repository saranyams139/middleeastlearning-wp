<?php
vc_map(array(
    "name"        => __("Footer Logo", 'archia-add-ons'),
    "description" => __("Footer Logo Theme", 'archia-add-ons'),
    "base"        => "cth_footer_logo",
    "class"       => "",
    "icon"        => BBT_DIR_URL . "assets/cththemes-logo.png",
    "category"    => 'Archia Header',
    "params"      => array(
        array(
            "type"        => "attach_image",
            "class"       => "",
            "heading"     => __("Logo", 'archia-add-ons'),
            "param_name"  => "logo",
            "value"       => "",
            "description" => __("Upload logo of the theme", 'archia-add-ons'),
        ),
        array(
            "type"        => "textfield",
            "heading"     => __("Extra class name", 'archia-add-ons'),
            "param_name"  => "el_class",
            "description" => __("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", 'archia-add-ons'),
        ),
    ),
));
if (class_exists('WPBakeryShortCode')) {
    class WPBakeryShortCode_Cth_Footer_Logo extends WPBakeryShortCode
    {}
}