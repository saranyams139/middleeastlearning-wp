<?php
vc_map(array(
    "name"            => __("Service Box", 'archia-add-ons'),
    "base"            => "cth_service_box",
    "content_element" => true,
    "icon"            => BBT_ABSPATH . "assets/cththemes-logo.png",
    "category"        => __('Archia Theme', 'archia-add-ons'),
    "params"          => array(
        array(
            "type"        => "textfield",
            "class"       => "",
            "heading"     => __("Awesome Icon Name", 'archia-add-ons'),
            "param_name"  => "iconname",
            "value"       => "",
            "description" => __("Awesome icon name. You can find the name from <a href='http://fortawesome.github.io/Font-Awesome/icons/' target='_blank'>Font Awesome Icons</a>", 'archia-add-ons'),
        ),
        array(
            "type"       => "attach_image",
            //"holder"    => "div",
            "class"      => "",
            "heading"    => __("Icon Image", 'archia-add-ons'),
            "param_name" => "iconimg",
            //"description" => __("Background Image, will display in mobile device", 'archia')
        ),

        array(
            "type"        => "textfield",
            "class"       => "",
            "holder"      => 'div',
            "heading"     => __("Title", 'archia-add-ons'),
            "param_name"  => "title",
            "value"       => "",
            "description" => __("Title", 'archia-add-ons'),
        ),

        array(
            "type"        => "textarea_html",
            "class"       => "",
            "holder"      => 'div',
            "heading"     => __("Description", 'archia-add-ons'),
            "param_name"  => "content",
            "value"       => "",
            "description" => __("Description", 'archia-add-ons'),
        ),

        array(
            "type"       => "dropdown",
            "class"      => "",
            "heading"    => __('Position', 'archia-add-ons'),
            "param_name" => "position",
            "value"      => array(
                __('Left', 'archia-add-ons')  => 'left',
                __('Right', 'archia-add-ons') => 'right',
            ),
            //"description" => __("Set this to No to hide View Detail button.", "archia"),
        ),

        array(
            "type"       => "dropdown",
            "class"      => "",
            "heading"    => __('Color', 'archia-add-ons'),
            "param_name" => "color",
            "value"      => array(
                __('Green', 'archia-add-ons')  => 'green',
                __('Yellow', 'archia-add-ons') => 'yellow',
                __('Red', 'archia-add-ons')    => 'red',
            ),
            //"description" => __("Set this to No to hide View Detail button.", "archia"),
        ),

        array(
            "type"        => "textfield",
            "heading"     => __("Extra class name", 'archia-add-ons'),
            "param_name"  => "el_class",
            "description" => __("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", 'archia-add-ons'),
        ),

    ),
));

if (class_exists('WPBakeryShortCode')) {
    class WPBakeryShortCode_Cth_Service_Box extends WPBakeryShortCode
    {
    }
}
