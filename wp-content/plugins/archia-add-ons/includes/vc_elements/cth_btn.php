<?php

vc_map( array(
    "name"                      => esc_html__("Button", 'archia-add-ons'),
    "base"                      => "cth_btn",
    "content_element"           => true,
    "show_settings_on_create"   => true,
    "icon"                      => BBT_DIR_URL . "assets/cththemes-logo.png",
    "category"                  => __( 'Archia Theme', 'archia-add-ons' ),
    "params"                    => array(
        array(
            "type"      => "textfield",
            "class"     => "",
            "admin_label"   => true,
            "heading"   => __("Title", 'archia-add-ons'),
            "param_name"=> "title",
            "value"     => "Contact us",
            // "description" => __("Enter section title (Note: you can leave it empty).", 'archia-add-ons')
        ),
        array(
            "type"        => "vc_link",
            "heading"     => __("Link", 'archia-add-ons'),
            "param_name"  => "link",
            "value"       => "#",
            // "description"   => esc_html__( 'Enter links for each partner (Note: divide links with linebreaks (Enter) or | and no spaces)', 'archia-add-ons' ). get_post_type_archive_link( 'listing' ),
        ),
        array(
            "type"      => "dropdown",
            "class"     =>"",
            "heading"   => __('Button Color', 'archia-add-ons'),
            "param_name" => "btn_color",
            "value"     => array(   
                            __('White', 'archia-add-ons') => 'white',  
                            __('Black', 'archia-add-ons') => 'black',
                            __('Background Black', 'archia-add-ons') => 'bgblack',
                            __('Colored', 'archia-add-ons') => 'primary',
                            __('Gray', 'archia-add-ons') => 'gray',
                        ),
            "std" => 'black',
            // "description" => __("Choose section title location", 'archia-add-ons'), 
        ), 
        array(
            "type"      => "dropdown",
            "class"     =>"",
            "heading"   => __('Button Style', 'archia-add-ons'),
            "param_name" => "btn_style",
            "value"     => array(   
                            __('Bordered - Animated', 'archia-add-ons') => 'btn-aware outline outline-2',  
                            __('Bordered', 'archia-add-ons') => 'outline outline-2',  
                            __('Animated', 'archia-add-ons') => 'btn-aware',  
                            __('None', 'archia-add-ons') => 'btn-none-style',  
                            
                        ),
            "std" => 'btn-aware outline outline-2',
            // "description" => __("Choose section title location", 'archia-add-ons'), 
        ), 
        array(
            "type"      => "dropdown",
            "class"     =>"",
            "heading"   => __('Button Size', 'archia-add-ons'),
            "param_name" => "btn_size",
            "value"     => array(   
                            __('Small', 'archia-add-ons') => 'btn-sm',  
                            __('Medium', 'archia-add-ons') => 'btn-md',  
                            __('Large', 'archia-add-ons') => 'btn-lg',  
                            __('XLarge', 'archia-add-ons') => 'btn-xl',  
                            
                        ),
            "std" => 'btn-md',
            // "description" => __("Choose section title location", 'archia-add-ons'), 
        ), 
        array(
            "type"      => "dropdown",
            "class"     =>"",
            "heading"   => __('Border Radius', 'archia-add-ons'),
            "param_name" => "btn_radius",
            "value"     => array(   
                            __('None', 'archia-add-ons') => 'radius-no',  
                            __('Small', 'archia-add-ons') => 'radius-sm',  
                            __('Medium', 'archia-add-ons') => 'radius-md',  
                            __('Large - Rounded', 'archia-add-ons') => 'radius-lg',  
                            __('XLarge - Rounded', 'archia-add-ons') => 'radius-xl',  
                            
                        ),
            "std" => 'radius-lg',
            // "description" => __("Choose section title location", 'archia-add-ons'), 
        ), 
        array(
            "type"      => "dropdown",
            "class"     =>"",
            "heading"   => __('Button Alignment', 'archia-add-ons'),
            "param_name" => "btn_ali",
            "value"     => array(   
                            __('Left', 'archia-add-ons') => 'text-left',  
                            __('Center', 'archia-add-ons') => 'text-center',
                            __('Right', 'archia-add-ons') => 'text-right',
                        ),
            "std" => 'text-left',
            // "description" => __("Choose section title location", 'archia-add-ons'), 
        ), 

        array(
            "type"          => "textfield",
            "heading"       => esc_html__("Extra class name", 'archia-add-ons'),
            "param_name"    => "el_class",
            "description"   => esc_html__("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", 'archia-add-ons'),
            "value"         => "",
        ),

        array(
            'type'          => 'css_editor',
            'heading'       => esc_html__( 'Css', 'archia-add-ons' ),
            'param_name'    => 'css',
            'group'         => esc_html__( 'Design options', 'archia-add-ons' ),
         ),
    )
));
if ( class_exists( 'WPBakeryShortCode' ) ) {
    class WPBakeryShortCode_CTH_Btn extends WPBakeryShortCode {}
}