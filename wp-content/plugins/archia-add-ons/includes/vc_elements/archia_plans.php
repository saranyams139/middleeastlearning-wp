<?php
/* add_ons_php */ 
vc_map( array(
    "name"                      => esc_html__("Plans", 'archia-add-ons'),
    "description"               => esc_html__("Plans for Archia",'archia-add-ons'),
    "base"                      => "archia_plans",
    "content_element"           => true,
    "icon"                      => BBT_DIR_URL . "/assets/cththemes-logo.png",
    // "html_template"             => TYPHOON_ADD_ONS_DIR.'/vc_templates/cth_section_title.php',
    "category"                  => __( 'Archia Theme', 'archia-add-ons' ),
    "show_settings_on_create"   => true,
    "params"                    => array(
        array(
            "type" => "dropdown", 
            "class" => "", 
            "heading" => __('Style', 'archia-add-ons'), 
            "param_name" => "style", 
            "value" => array(
                esc_html__('Home 01 style', 'archia-add-ons') => '' , 
                esc_html__('Dark Style', 'archia-add-ons') =>'style-1' , 
                esc_html__('Style 2', 'archia-add-ons') =>'style-2', 
            ), 
            "std" => '',
        ),
        array(
            "type"          => "textfield", 
            "admin_label"   => true,
            // "holder"        => "div",
            "heading"       => esc_html__("Enter Post IDs", 'archia-add-ons'), 
            "param_name"    => "ids", 
            "description"   => esc_html__("Enter post ids to show, separated by a comma.", 'archia-add-ons')
        ), 
        array(
            "type"          => "textfield", 
            "admin_label"   => true,
            // "holder"        => "div",
            "heading"       => esc_html__("Post IDs to Exclude", 'archia-add-ons'), 
            "param_name"    => "ids_not", 
            "description"   => esc_html__("Enter post ids to exclude, separated by a comma. Use if the field above is empty. Leave empty to get all.", 'archia-add-ons')
        ),
        array(
            "type" => "dropdown", 
            "class" => "", 
            "heading" => __('Order List Post by', 'archia-add-ons'), 
            "param_name" => "order_by", 
            "value" => array(
                esc_html__('Date', 'archia-add-ons') => 'date' , 
                esc_html__('ID', 'archia-add-ons')  =>'ID' , 
                esc_html__('Author', 'archia-add-ons') =>'author', 
                esc_html__('Title', 'archia-add-ons') =>'title', 
                esc_html__('Modified', 'archia-add-ons') =>'modified',
                esc_html__('Random', 'archia-add-ons')=> 'rand' ,
                esc_html__('Comment Count', 'archia-add-ons') =>'comment_count',
                esc_html__('Menu Order', 'archia-add-ons')=>'menu_order' ,
                esc_html__('ID order given (post__in)', 'archia-add-ons') => 'post__in',
            ), 
            // "description" => __("Order Speakers by", 'cth-gather-plugins'), 
            "std" => 'date',
        ),

        array(
            "type" => "dropdown", 
            "class" => "", 
            "heading" => __('Sort Order', 'archia-add-ons'), 
            "param_name" => "order", 
            "value" => array(
                
                __('Descending', 'archia-add-ons') => 'DESC', 
                __('Ascending', 'archia-add-ons') => 'ASC',
                
            ), 
            // "description" => __("Order Speakers", 'cth-gather-plugins'),
            "std" => 'DESC',
        ), 
        array(
            "type"          => "textfield",
            "admin_label"   => true,
            "heading"       => esc_html__("Number of posts displayed", 'archia-add-ons'),
            "param_name"    => "posts_per_page",
            "description"   => esc_html__("Number of menu items to show (-1 for all).", 'archia-add-ons'),
            "value"         => '3',
        ),
        array(
            "type"          => "textfield",
            "admin_label"   => true,
            "heading"       => esc_html__("Featured plan", 'archia-add-ons'),
            "param_name"    => "featured_plan",
            "description"   => esc_html__("Featured plan item, 1 for first item.", 'archia-add-ons'),
            "value"         => '2',
        ),
        array(
              "type"        => "dropdown",
              "heading"     => __("Select Column", 'archia-add-ons'),
              "param_name"  => "columns_grid",
              "admin_label" => true,
              "value"       => array(
                        esc_html__('One Column', 'archia-add-ons') =>'col-sm-12 col-md-12 col-lg-12', 
                        esc_html__('Two Columns', 'archia-add-ons')=>'col-sm-12 col-md-6 col-lg-6' , 
                        esc_html__('Three Columns', 'archia-add-ons') => 'col-sm-12 col-md-4 col-lg-4',
                        esc_html__('Four Columns', 'archia-add-ons') => 'col-sm-12 col-md-3 col-lg-3', 
                ), //value
              "std"         => "col-sm-12 col-md-4 col-lg-4",
              'description' => 'Select Colums Show Plans',
        ),
        array(
            "type" => "dropdown",  
            "admin_label"   => true,
            "heading" => __('CSS Animation', 'archia-add-ons'), 
            "param_name" => "ani_type", 
            "value" => archia_addons_vc_animation(),
            "description" => __("Select type of animation for element to be animated when it \"enters\" the browsers viewport (Note: works only in modern browsers).", 'archia-add-ons'), 
            "std" => '',
        ),
        array(
            "type" => "textfield",
            "admin_label"   => true,
            "heading" => esc_html__("Animation Duration", 'archia-add-ons'),
            "param_name" => "ani_time",
            "description" => esc_html__("Ex: 0.2s for 200 miliseconds, 2s for 2 second", 'archia-add-ons'),
            "value" => "2s",
            'dependency' => array(
                'element' => 'ani_type',
                'not_empty' => true,
            ),
        ),
        array(
            "type" => "textfield",
            "admin_label"   => true,
            "heading" => esc_html__("Animation delays", 'archia-add-ons'),
            "param_name" => "ani_delays",
            "description" => esc_html__("Delay for each items. Ex: 0.2s,0.4s,0.6s (0.2s for 200 miliseconds, 2s for 2 second)", 'archia-add-ons'),
            "value" => "0.2s,0.4s,0.6s",
            'dependency' => array(
                'element' => 'ani_type',
                'not_empty' => true,
            ),
        ),
        array(
            "type"          => "textfield",
            "heading"       => esc_html__("Extra class name", 'archia-add-ons'),
            "param_name"    => "el_class",
            "description"   => esc_html__("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", 'archia-add-ons'),
            "value"         => "",
        ),

        array(
            'type'          => 'css_editor',
            'heading'       => esc_html__( 'Css', 'archia-add-ons' ),
            'param_name'    => 'css',
            'group'         => esc_html__( 'Design options', 'archia-add-ons' ),
        ),
    )
));
if ( class_exists( 'WPBakeryShortCode' ) ) {
    class WPBakeryShortCode_Archia_Plans extends WPBakeryShortCode {}
}