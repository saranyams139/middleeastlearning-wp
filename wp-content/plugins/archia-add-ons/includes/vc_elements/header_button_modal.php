<?php
vc_map(array(
    "name"        => __("Button Modal", 'archia-add-ons'),
    // "description" => __("Logo Theme", 'archia-add-on'),
    "base"        => "cth_bt_modal",
    "class"       => "",
    "icon"        => BBT_DIR_URL . "assets/cththemes-logo.png",
    "category"    => 'Archia Header',
    "params"      => array(
        // array(
        //     "type"        => "attach_image",
        //     "class"       => "",
        //     "heading"     => __("Logo Modal", 'archia-add-on'),
        //     "param_name"  => "logo_modal",
        //     "value"       => "",
        //     "description" => __("Upload logo of the theme", 'archia-add-on'),
        // ),
        // array(
        //     "type"      => "textarea_html",
        //     "holder"    => "div",
        //     "class"     => "",
        //     "heading"   => __("Social Modal", 'archia-add-on'),
        //     "param_name"=> "social",
        //      "value"     => __( '<div class="social-menu">
        //         <ul>
        //             <li><a href="javascript:void(0);"><i class="fa fa-facebook"></i></a></li>
        //             <li><a href="javascript:void(0);"><i class="fa fa-twitter"></i></a></li>
        //             <li><a href="javascript:void(0);"><i class="fa fa-google-plus"></i></a></li>
        //             <li><a href="javascript:void(0);"><i class="fa fa-facebook"></i></a></li>
        //         </ul>
        //         <p class="copyright-head">© 2019 Archia</p>
        //     </div>', "archia-add-on" ), 
            
        // ),
        array(
            "type"        => "textfield",
            "heading"     => __("Extra class name", 'archia-add-ons'),
            "param_name"  => "el_class",
            "description" => __("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", 'archia-add-ons'),
        ),
    ),
));
if (class_exists('WPBakeryShortCode')) {
    class WPBakeryShortCode_Cth_Bt_Modal extends WPBakeryShortCode
    {}
}