<?php

add_action( 'cmb2_admin_init', 'archia_cmb2_sample_metaboxes' );  
/**
 * Define the metabox and field configurations.
 */
function archia_cmb2_sample_metaboxes() {

    // Start with an underscore to hide fields from custom fields list
    $prefix = '_cth_';

    /**
     * Initiate Post metabox
     */
    $post_cmb = new_cmb2_box( array(
        'id'            => 'post_options',
        'title'         => esc_html__( 'Post Format Options', 'archia-add-ons' ),
        'object_types'  => array( 'post'), // Post type
        'context'       => 'normal',// normal, side and advanced
        'priority'      => 'high',// default, high and low - core
        'show_names'    => true, // Show field names on the left
    ) );

    $post_cmb->add_field( array(
        'name' => esc_html__('Post Slider and Gallery Images', 'archia-add-ons' ),
        'id'   => $prefix . 'post_slider_images',
        'type' => 'file_list',
        'preview_size' => array( 150, 150 ), // Default: array( 50, 50 )
    ) );

    $post_cmb->add_field( array(
        'name'       => esc_html__('oEmbed for Post Format', 'archia-add-ons' ),
        'desc'       => wp_kses(__('Enter a youtube, twitter, or instagram URL. Supports services listed at <a href="http://codex.wordpress.org/Embeds">http://codex.wordpress.org/Embeds</a>.', 'archia-add-ons' ),array('a'=>array('href'=>array()))),
        'id'   => $prefix . 'embed_video',
        'type' => 'oembed',
    ) );

    

    $post_cmb->add_field( array(
        'name' => esc_html__('Single Layout', 'archia-add-ons' ),
        'desc' => esc_html__('Choose display layout for this post','archia-add-ons'),
        'id'   => $prefix . 'single_layout',
        'type'    => 'select',
        'default'=>'right_sidebar',
        'options' => array(
            'fullwidth' => __( 'Fullwidth', 'archia-add-ons' ),
                  'right_sidebar' => __( 'Right Sidebar', 'archia-add-ons' ),
                  'left_sidebar' => __( 'Left Sidebar', 'archia-add-ons' ),
        ),
    ) );


    /**
     * Initiate portfolio metabox
     */
    $portfolio_cmb = new_cmb2_box( array(
        'id'            => 'portfolio_single_fields',
        'title'         => esc_html__('Portfolio Single Page Options', 'archia-add-ons' ),
        'object_types'  => array( 'portfolio'), // Post type
        'context'       => 'normal',// normal, side and advanced
        'priority'      => 'high',// default, high and low - core
        'show_names'    => true, // Show field names on the left
    ) );

    $portfolio_cmb->add_field( array(
        'name' => esc_html__( 'User Field', 'archia-add-ons' ),
        // 'desc' => esc_html__( 'field description (optional)', 'archia' ),
        'id'   => $prefix . 'user_text_field',
        'default' => '',
        'type'    => 'text',
    ) );

    $portfolio_cmb->add_field( array(
        'name'       => esc_html__('Portfolio Video or Audio Link', 'archia-add-ons' ),
        'desc'       => wp_kses(__('Enter a youtube, twitter, or instagram URL. Supports services listed at <a href="http://codex.wordpress.org/Embeds">http://codex.wordpress.org/Embeds</a>.<br>
                            This link will be used in Portfolio single style 1 and style 5 as main video or in portfolio single style 4 as video background. Note: for style 4 just enter Youtube video link only.', 'archia-add-ons' ),array('a'=>array('href'=>array()))),
        'id'   => $prefix . 'embed_video',
        'type' => 'oembed',
    ) );
    

    $portfolio_cmb->add_field( array(
        'name'          => esc_html__('Portfolio Detail', 'archia-add-ons' ),
        'desc'          => '',
        'default'       => '',
        'id'            => $prefix . 'folio_detail',
        'type'          => 'wysiwyg',
    ) );

    /**
     * Initiate Member metabox
     */
    $member_cmb = new_cmb2_box( array(
        'id'            => 'member_fields',
        'title'         => esc_html__('Member Options', 'archia-add-ons' ),
        'object_types'  => array( 'member'), // Post type
        'context'       => 'normal',// normal, side and advanced
        'priority'      => 'high',// default, high and low - core
        'show_names'    => true, // Show field names on the left
    ) );

    $member_cmb->add_field( array(
        'name'          => esc_html__('Job Description', 'archia-add-ons' ),
        'desc'          => '',
        'default'       => '',
        'id'            => $prefix . 'mem_job',
        'type'          => 'textarea',
    ) );


    /**
     * Initiate Single Icon metabox
     */
    $single_icon_cmb = new_cmb2_box( array(
        'id'            => 'layout_header_fields',
        'title'         => esc_html__('Layout Header', 'archia-add-ons' ),
        'object_types'  => array('post','page', 'portfolio', 'services', 'construction_service'), // Post type
        'context'       => 'normal',// normal, side and advanced
        'priority'      => 'high',// default, high and low - core
        'show_names'    => true, // Show field names on the left
    ) );

    $single_icon_cmb->add_field( array(
        'name'    => esc_html__('Header Type', 'archia-add-ons' ),
        'desc'    => __('', 'archia-add-ons' ),
        'id'      => $prefix . 'header_type',
        'type'    => 'select',
        'options' => archia_addons_get_header_type_options(),
    ) );
    $single_icon_cmb->add_field( array(
        'name'    => esc_html__('Footer Type', 'archia-add-ons' ),
        'desc'    => __('', 'archia-add-ons' ),
        'id'      => $prefix . 'footer_type',
        'type'    => 'select',
        'options' => archia_addons_get_footer_type_options(),
    ) );
    $single_icon_cmb->add_field( array(
        'name'    => esc_html__('Skin Color - Demo only', 'archia-add-ons' ),
        'desc'    => __('Select skin for this page instead of default', 'archia-add-ons' ),
        'id'      => $prefix . 'skin',
        'type'    => 'select',
        'options' => archia_addons_color_skins(),
    ) );

    //Page cmb2 ----------
    $page_cmb = new_cmb2_box(array(
        'id'            => 'page_options',
        'title'         => esc_html__( 'Page Options Header', 'archia-add-ons' ),
        'object_types'  => array( 'post','page', 'portfolio', 'services', 'construction_service' ), // Post type
        'context'       => 'normal',// normal, side and advanced
        'priority'      => 'high',// default, high and low - core
        'show_names'    => true, // Show field names on the left
    ));
    $page_cmb->add_field(array(
        'name' => 'Show header',
        'desc' => 'Show header content' ,
        'id'   => $prefix.'show_header',
        'type' => 'checkbox',
    ));
    $page_cmb->add_field(array(
        'name' => 'Show Title',
        // 'desc' => 'Show header content' ,
        'id'   => $prefix.'show_title',
        'type' => 'checkbox',
    ));
    $page_cmb->add_field(array(
        'name' => 'Show breadcrumb',
        // 'desc' => 'Show header content' ,
        'id'   => $prefix.'show_breadcrumb',
        'type' => 'checkbox',
    ));
    $page_cmb->add_field(array(
        'name'    => 'Background Image Header',
        'desc'    => 'Upload an image or enter an URL.',
        'id'      => $prefix.'header_bg',
        'type'    => 'file',
        // Optional:
        'options' => array(
            'url' => false, // Hide the text input for the url
        ),
        'text'    => array(
            'add_upload_file_text' => 'Add File' // Change upload button text. Default: "Add or Upload File"
        ),
        // query_args are passed to wp.media's library query.
        'query_args' => array(
            // 'type' => 'application/pdf', // Make library only display PDFs.
            // Or only allow gif, jpg, or png images
            // 'type' => array(
            //  'image/gif',
            //  'image/jpeg',
            //  'image/png',
            // ),
        ),
        'preview_size' => 'thumbnail', // Image size to use when previewing in the admin.
    ));
    $page_cmb->add_field(array(
        'name' => 'Header title description',
        // 'desc' => 'field description (optional)',
        'default' =>esc_html__( 'Stay informed on our news or events!', 'archia-add-ons' ) ,
        'id' => $prefix.'header_intro',
        'type' => 'textarea_small'
    ));

    
    
    
    /**
     * Initiate User Profile metabox
     */
    $user_cmb = new_cmb2_box( array(
        'id'         => 'user_edit',
        'title'      => esc_html__( 'User Profile Metabox', 'archia-add-ons' ),
        'object_types'  => array( 'user' ), // Tells CMB to use user_meta vs post_meta
        'context'       => 'normal',// normal, side and advanced
        'priority'      => 'core',// default, high and low - core
        'show_names'    => true, // Show field names on the left
        // 'cmb_styles' => false, // false to disable the CMB stylesheet
        // 'closed'     => true, // Keep the metabox closed by default
    ) );

    $user_cmb->add_field( array(
        'name'     => esc_html__( 'Archia Theme Infos', 'archia-add-ons' ),
        // 'desc'     => esc_html__( 'field description (optional)', 'archia' ),
        'id'       => $prefix . 'exta_info',
        'type' => 'title',
        'on_front' => false,
    ) );

    // $user_cmb->add_field( array(
    //     'name'     => esc_html__( 'Avatar', 'archia-add-ons' ),
    //     // 'desc'     => esc_html__( 'field description (optional)', 'archia' ),
    //     'id'       => $prefix . 'avatar',
    //     'type' => 'file',
    //     'save_id' => true,
    // ) );

    $user_cmb->add_field( array(
        'name' => esc_html__( 'Facebook URL', 'archia-add-ons' ),
        // 'desc' => esc_html__( 'field description (optional)', 'archia' ),
        'id'   => $prefix . 'facebookurl',
        'type' => 'text_url',
        // 'protocols' => array( 'http', 'https', 'ftp', 'ftps', 'mailto', 'news', 'irc', 'gopher', 'nntp', 'feed', 'telnet' ), // Array of allowed protocols
    ) );

    $user_cmb->add_field( array(
        'name' => esc_html__( 'Twitter URL', 'archia-add-ons' ),
        // 'desc' => esc_html__( 'field description (optional)', 'archia' ),
        'id'   => $prefix . 'twitterurl',
        'type' => 'text_url',
        // 'protocols' => array( 'http', 'https', 'ftp', 'ftps', 'mailto', 'news', 'irc', 'gopher', 'nntp', 'feed', 'telnet' ), // Array of allowed protocols
    ) );
    $user_cmb->add_field( array(
        'name' => esc_html__( 'Google+ URL', 'archia-add-ons' ),
        // 'desc' => esc_html__( 'field description (optional)', 'archia' ),
        'id'   => $prefix . 'googleplusurl',
        'type' => 'text_url',
        // 'protocols' => array( 'http', 'https', 'ftp', 'ftps', 'mailto', 'news', 'irc', 'gopher', 'nntp', 'feed', 'telnet' ), // Array of allowed protocols
    ) );
    $user_cmb->add_field( array(
       'name' => esc_html__( 'Linkedin URL', 'archia-add-ons' ),
        // 'desc' => esc_html__( 'field description (optional)', 'archia' ),
        'id'   => $prefix . 'linkedinurl',
        'type' => 'text_url',
        // 'protocols' => array( 'http', 'https', 'ftp', 'ftps', 'mailto', 'news', 'irc', 'gopher', 'nntp', 'feed', 'telnet' ), // Array of allowed protocols
    ) );

    $user_cmb->add_field( array(
       'name' => esc_html__( 'Instagram URL', 'archia-add-ons' ),
        // 'desc' => esc_html__( 'field description (optional)', 'archia' ),
        'id'   => $prefix . 'instagramurl',
        'type' => 'text_url',
        // 'protocols' => array( 'http', 'https', 'ftp', 'ftps', 'mailto', 'news', 'irc', 'gopher', 'nntp', 'feed', 'telnet' ), // Array of allowed protocols
    ) );

    $user_cmb->add_field( array(
       'name' => esc_html__( 'Tumblr URL', 'archia-add-ons' ),
        // 'desc' => esc_html__( 'field description (optional)', 'archia' ),
        'id'   => $prefix . 'tumblrurl',
        'type' => 'text_url',
        // 'protocols' => array( 'http', 'https', 'ftp', 'ftps', 'mailto', 'news', 'irc', 'gopher', 'nntp', 'feed', 'telnet' ), // Array of allowed protocols
    ) );



    // $user_cmb->add_field( array(
    //     'name' => esc_html__( 'User Field', 'archia-add-ons' ),
    //     // 'desc' => esc_html__( 'field description (optional)', 'archia' ),
    //     'id'   => $prefix . 'user_text_field',
    //     'default' => '',
    //     'type'    => 'text',
    // ) );


    /**
     * Testimonials - dev3
     */
    $testim_cmb = new_cmb2_box( array(
        'id'            => 'testimonials',
        'title'         => esc_html__( 'Testimonials Options', 'archia-add-ons' ),
        'object_types'  => array( 'cth_testi'), // Post type
        'context'       => 'normal',// normal, side and advanced
        'priority'      => 'high',// default, high and low - core
        'show_names'    => true, // Show field names on the left
    ) );

    $testim_cmb->add_field( array(
        'name' => esc_html__( 'Position', 'archia-add-ons' ),
        'id' => $prefix . 'position',
        'type'             => 'text',
        'default'          => 'Client',
        
    ) );


    /**
     * Initiate Plan metabox - dev3
     */
    $plan_cmb = new_cmb2_box( array(
        'id'            => 'lplan_fields',
        'title'         => esc_html__('Plan Options', 'archia-add-ons' ),
        'object_types'  => array( 'lplan'), // Post type
        'context'       => 'normal',// normal, side and advanced
        'priority'      => 'high',// default, high and low - core
        'show_names'    => true, // Show field names on the left
    ) );
    // $plan_cmb->add_field( array(
    //     'name'          => esc_html__( 'Period', 'archia-add-on' ),
    //     'desc'          => esc_html__( 'Expired period', 'archia-add-on' ),
    //     'id'            => $prefix . 'period',
    //     'type'             => 'select',
    //     'show_option_none' => false,
    //     'default'          => 'month',
    //     'options'       => archia_add_ons_get_subscription_duration_units(),  
    // ) );
    $plan_cmb->add_field( array(
        'name'          => esc_html__('Price', 'archia-add-ons' ),
        'desc'          => esc_html__('Value 0 for free.', 'archia-add-ons' ),
        'default'       => '29',
        'id'            => $prefix .'price',
        'type'          => 'text_small',
        // 'before_field'  => '$',
    ) );

    $plan_cmb->add_field( array(
        'name'          => esc_html__('Decimal Value', 'archia-add-ons' ),
        // 'desc'          => esc_html__('The odd amount is separated by a dot.', 'archia-add-ons' ),
        'default'       => '00',
        'id'            => $prefix .'odd_amount',
        'type'          => 'text_small',
        // 'before_field'  => '$',
    ) );
    $plan_cmb->add_field( array(
        'name'          => esc_html__('Currency', 'archia-add-ons' ),
        // 'desc'          => esc_html__('Value 0 for free.', 'archia-add-on' ),
        'default'       => '$',
        'id'            => $prefix .'currency',
        'type'          => 'text',
    ) );

    $plan_cmb->add_field( array(
        'name'          => esc_html__('URL (Link)', 'archia-add-ons' ),
        // 'desc'          => esc_html__('', 'archia-add-on' ),
        'default'       => '',
        'id'            => $prefix . 'url_plan',
        'type'          => 'text'
    ) );


    /**
     * Members - dev3
     */
    $member_cmb2 = new_cmb2_box( array(
        'id'            => 'member_additional_mtb',
        'title'         => esc_html__('Social Links', 'archia-add-ons' ),
        'object_types'  => array( 'cth_member'), // Post type
        'context'       => 'normal',// normal, side and advanced
        'priority'      => 'high',// default, high and low - core
        'show_names'    => true, // Show field names on the left
    ) );
    $member_cmb2->add_field( array(
        'name' => esc_html__( 'Member Level', 'archia-add-ons' ),
        'id'   => $prefix . 'level_member',
        'type' => 'text',
        'default'=>'Partners',
    ) );

    $member_cmb2->add_field( array(
        'name' => esc_html__( 'Member Sign', 'archia-add-ons' ),
        'id'   => $prefix . 'sign',
        'type' => 'file_list',
        'default' =>'',
    ) );

    $member_cmb2->add_field( array(
        'name' => esc_html__( 'Member Avatar', 'archia-add-ons' ),
        'id'   => $prefix . 'pic',
        'type' => 'file_list',
        'default' =>'',
    ) );

    $member_cmb2->add_field( array(
        'name' => esc_html__( 'Member Name', 'archia-add-ons' ),
        'id'   => $prefix . 'name',
        'type' => 'text',
        'default'=>'Envato',
    ) );
    $member_cmb2->add_field( array(
        'name' => esc_html__( 'Member Job position', 'archia-add-ons' ),
        'id'   => $prefix . 'field',
        'type' => 'text',
        'default'=>'Architectural engineer',
    ) );

    $member_cmb2->add_field( array(
        'name' => esc_html__( 'Facebook URL', 'archia-add-ons' ),
        'id'   => $prefix . 'facebookurl',
        'type' => 'text_url',
        'default'=>'',
    ) );

    $member_cmb2->add_field( array(
        'name' => esc_html__( 'Twitter URL', 'archia-add-ons' ),
        'id'   => $prefix . 'twitterurl',
        'type' => 'text_url',
        'default'=>'',
    ) );
    $member_cmb2->add_field( array(
        'name' => esc_html__( 'Google+ URL', 'archia-add-ons' ),
        'id'   => $prefix . 'googleplusurl',
        'type' => 'text_url',
        'default'=>'',
    ) );
    $member_cmb2->add_field( array(
       'name' => esc_html__( 'Instagram URL', 'archia-add-ons' ),
        'id'   => $prefix . 'instagramurl',
        'type' => 'text_url',
        'default'=>'',
    ) );



}