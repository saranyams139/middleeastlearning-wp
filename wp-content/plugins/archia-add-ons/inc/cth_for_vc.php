<?php
/* add_ons_php */

function archia_addons_vc_animation() {
    $animation = array(
        esc_html__('None', 'archia-add-ons') => '', 
        esc_html__('BounceIn', 'archia-add-ons') =>'bounceIn',
        esc_html__('BounceInUp', 'archia-add-ons') =>'bounceInUp',
        esc_html__('BounceInDown', 'archia-add-ons') =>'bounceInDown', 
        esc_html__('BounceInLeft', 'archia-add-ons') =>'bounceInLeft',
        esc_html__('BounceInRight', 'archia-add-ons') =>'bounceInRight',
        
        esc_html__('FadeIn', 'archia-add-ons') =>'fadeIn',
        esc_html__('FadeInUp', 'archia-add-ons') =>'fadeInUp', 
        esc_html__('FadeInDown', 'archia-add-ons') =>'fadeInDown',
        esc_html__('FadeInLeft', 'archia-add-ons') =>'fadeInLeft',
        esc_html__('FadeInRight', 'archia-add-ons') =>'fadeInRight',
        
        esc_html__('ZoomIn', 'archia-add-ons')  =>'zoomIn' ,
        esc_html__('ZoomInUp', 'archia-add-ons')  =>'zoomInUp' , 
        esc_html__('ZoomInDown', 'archia-add-ons')  =>'zoomInDown' , 
        esc_html__('ZoomInLeft', 'archia-add-ons')  =>'zoomInLeft' , 
        esc_html__('ZoomInRight', 'archia-add-ons')  =>'zoomInRight' ,

        esc_html__('RotateIn', 'archia-add-ons')  =>'rotateIn' ,
        esc_html__('RotateInDownLeft', 'archia-add-ons')  =>'rotateInDownLeft' ,
        esc_html__('RotateInDownRight', 'archia-add-ons')  =>'rotateInDownRight' ,
        esc_html__('RotateInUpLeft', 'archia-add-ons')  =>'rotateInUpLeft' ,
        esc_html__('RotateInUpRight', 'archia-add-ons')  =>'rotateInUpRight' ,
    ); 
    return $animation;
}


function archia_addons_register_wpbakerry_elements()
{
    //header
    require_once BBT_ABSPATH . 'includes/vc_elements/cth_site_logo.php';
    // require_once BBT_ABSPATH . 'includes/vc_elements/custom_logo.php';
    require_once BBT_ABSPATH . 'includes/vc_elements/header_button_modal.php'; 
    require_once BBT_ABSPATH . 'includes/vc_elements/header_social.php';
    require_once BBT_ABSPATH . 'includes/vc_elements/header_contact.php';
    require_once BBT_ABSPATH . 'includes/vc_elements/header_navbar.php';
    require_once BBT_ABSPATH . 'includes/vc_elements/header_button_search.php';
    require_once BBT_ABSPATH . 'includes/vc_elements/header_side_navbar.php';
    require_once BBT_ABSPATH . 'includes/vc_elements/cth_header_btns.php';

    
    // require_once BBT_ABSPATH . 'includes/vc_elements/post_listing.php';
    // require_once BBT_ABSPATH . 'includes/vc_elements/posts_masonary.php';
    require_once BBT_ABSPATH . 'includes/vc_elements/slider_gal.php';
    require_once BBT_ABSPATH . 'includes/vc_elements/cth_gallery.php';
    require_once BBT_ABSPATH . 'includes/vc_elements/cth_slider.php';
    require_once BBT_ABSPATH . 'includes/vc_elements/popup_video.php';
    require_once BBT_ABSPATH . 'includes/vc_elements/folio_details.php';
    //footer
    // require_once BBT_ABSPATH . 'includes/vc_elements/footer_logo.php';
    // Home-page
    require_once BBT_ABSPATH . 'includes/vc_elements/archia_section_title.php';
    // require_once BBT_ABSPATH . 'includes/vc_elements/sec_head.php';

    
    require_once BBT_ABSPATH . 'includes/vc_elements/archia_feature_box.php';
    require_once BBT_ABSPATH . 'includes/vc_elements/cth_process.php';
    require_once BBT_ABSPATH . 'includes/vc_elements/archia_partners.php';
    require_once BBT_ABSPATH . 'includes/vc_elements/archia_latest_blog.php';
    require_once BBT_ABSPATH . 'includes/vc_elements/archia_testimonial.php';
    require_once BBT_ABSPATH . 'includes/vc_elements/archia_plans.php';
    require_once BBT_ABSPATH . 'includes/vc_elements/archia_members.php';
    require_once BBT_ABSPATH . 'includes/vc_elements/cth_members.php';
    require_once BBT_ABSPATH . 'includes/vc_elements/archia_portfolio.php';
    require_once BBT_ABSPATH . 'includes/vc_elements/folios_slider.php';
    require_once BBT_ABSPATH . 'includes/vc_elements/film_strip.php';
    require_once BBT_ABSPATH . 'includes/vc_elements/split_slider.php';
    // require_once BBT_ABSPATH . 'includes/vc_elements/archia_project.php';
    require_once BBT_ABSPATH . 'includes/vc_elements/cth_btn.php';
    require_once BBT_ABSPATH . 'includes/vc_elements/archia_button_video.php';
    require_once BBT_ABSPATH . 'includes/vc_elements/icon_box.php';
    require_once BBT_ABSPATH . 'includes/vc_elements/cth_resume.php';
    require_once BBT_ABSPATH . 'includes/vc_elements/imgs_slider.php';
    require_once BBT_ABSPATH . 'includes/vc_elements/folio_manu_slider.php';
    require_once BBT_ABSPATH . 'includes/vc_elements/cth_progress.php';
    require_once BBT_ABSPATH . 'includes/vc_elements/cth_counter.php';
    require_once BBT_ABSPATH . 'includes/vc_elements/number_box.php';
    require_once BBT_ABSPATH . 'includes/vc_elements/details_list.php';
    require_once BBT_ABSPATH . 'includes/vc_elements/cth_blog.php';






    
    
    // require_once BBT_ABSPATH . 'includes/vc_elements/owl_carousel.php';
    // require_once BBT_ABSPATH . 'includes/vc_elements/flexslider.php';
    // require_once BBT_ABSPATH . 'includes/vc_elements/member.php';
    // require_once BBT_ABSPATH . 'includes/vc_elements/register.php';
    // require_once BBT_ABSPATH . 'includes/vc_elements/portfolios.php';
    // require_once BBT_ABSPATH . 'includes/vc_elements/featurebox.php';
    // require_once BBT_ABSPATH . 'includes/vc_elements/servicebox.php';
    // require_once BBT_ABSPATH . 'includes/vc_elements/ace.php';

}
add_action('vc_before_init', 'archia_addons_register_wpbakerry_elements'); 



// Add new Param in Row
function archia_add_ons_add_vc_param()
{
    if (function_exists('vc_set_shortcodes_templates_dir')) {
        vc_set_shortcodes_templates_dir(BBT_ABSPATH . '/vc_templates/');
    }

    $new_row_params = array(
        array(
            "type"        => "dropdown",
            "heading"     => __('Section Layout', 'archia-add-ons'),
            "param_name"  => "cth_layout",
            "value"       => array(
                __('Default', 'archia-add-ons')                    => 'default',
                __('Home 1 Side Nav', 'archia-add-ons')            => 'home1_side_nav',
                __('Default Nav', 'archia-add-ons')                => 'default_nav',
                __('Footer Section', 'archia-add-ons')             => 'footer_sec', 
                __('Page Section', 'archia-add-ons')               => 'page_sec',     
            ),
            "description" => __("Select one of the pre made page sections or using default", 'archia-add-ons'),
            "group"       => "Archia Theme",
        ),
        array(
            "type"        => "dropdown",
            "heading"     => __('Content Width', 'archia-add-ons'),
            "param_name"  => "cth_row_width",
            "value"       => array(
                __('Full Width', 'archia-add-ons')                => 'full_width',
                __('Wide Boxed', 'archia-add-ons')               => 'wide',
                __('Small Boxed', 'archia-add-ons')              => 'fix_width',

            ),
            'std'   => 'fix_width',
            'dependency' => array(
                'element' => 'cth_layout',
                'value' => array( 'page_sec'),
                'not_empty' => false,
            ),
            "group"       => "Archia Theme",
        ),
        array(
            "type" => "dropdown",
            "heading" => esc_html__('Top/Bottom Padding', 'archia-add-ons'),
            "param_name" => "cth_padding",
            "value" => array(   
                            esc_html__('No Padding', 'archia-add-ons') => 'no-padding',                                                                               
                            esc_html__('Small Padding', 'archia-add-ons') => 'small-padding',                                                                               
                            esc_html__('Medium Padding', 'archia-add-ons') => 'medium-padding',                                                                               
                            esc_html__('Large Padding', 'archia-add-ons') => 'large-padding',                                                                               
                            esc_html__('XLarge Padding', 'archia-add-ons') => 'xlarge-padding',                                                                               
                            esc_html__('Top-30-Bottom-0', 'archia-add-ons') => 'top30-bot0',                                                                               
                            esc_html__('Top-50-Bottom-0', 'archia-add-ons') => 'top50-bot0',                                                                               
                            esc_html__('Top-80-Bottom-0', 'archia-add-ons') => 'top80-bot0',                                                                               
                            esc_html__('Top-0-Bottom-30', 'archia-add-ons') => 'top0-bot30',                                                                               
                            esc_html__('Top-0-Bottom-50', 'archia-add-ons') => 'top0-bot50',                                                                               
                            esc_html__('Top-0-Bottom-80', 'archia-add-ons') => 'top0-bot80',                                                                               
                        ),
            "std" => 'medium-padding',
            'dependency' => array(
                'element' => 'cth_layout',
                'value' => array( 'page_sec'),
                'not_empty' => false,
            ),
           "group" => __( 'Archia Theme', 'archia-add-ons' ),
        ),
        array(
            "type"        => "checkbox",
            "heading"     => __('Center Content', 'archia-add-ons'),
            "param_name"  => "col_cen_pos",
            "value"       => array(
                __('Yes', 'archia-add-ons')                => 'yes',
            ),
            "description" => __("Make content center in each column.", 'archia-add-ons'),
            'dependency' => array(
                'element' => 'cth_layout',
                'value' => array( 'page_sec'),
                'not_empty' => false,
            ),
            "std"           => 'no',
            "group"         => "Archia Theme",
        ),
        array(
            "type"        => "dropdown",
            "heading"     => __('Position Footer', 'archia-add-ons'),
            "param_name"  => "cth_pos_footer",
            "value"       => array(
                __('Top Footer', 'archia-add-ons')                => 'top_footer',
                __('Bottom Footer', 'archia-add-ons')               => 'bottom_footer',
            ),
            'dependency' => array(
                'element' => 'cth_layout',
                'value' => array( 'footer_sec'),
                'not_empty' => false,
            ),
            "group"       => "Archia Theme",
        ),
        array(
            "type" => "attach_image",
            "heading" => esc_html__('Footer Background Image', 'archia-add-ons'),
            "param_name" => "cth_footer_bg",
            'dependency' => array(
                'element' => 'cth_layout',
                'value' => array( 'footer_sec'),
                'not_empty' => false,
            ),
            "group" => __( 'Archia Theme', 'archia-add-ons' ),
        ),

    );
    if (function_exists('vc_add_params')) {
        vc_add_params('vc_row', $new_row_params);
    }
    $new_sec_params = array(
        array(
            "type"        => "dropdown",
            "heading"     => __('Section Layout', 'archia-add-ons'),
            "param_name"  => "cth_sec_layout",
            "value"       => array(
                __('Default', 'archia-add-ons')                            => 'default',
                __('Default Header', 'archia-add-ons')                     => 'default_header',
                __('Home 1 Header', 'archia-add-ons')                      => 'home1_header',
                __('Home 3 Header', 'archia-add-ons')                      => 'home3_header',
                __('Home 4 Header', 'archia-add-ons')                      => 'home4_header',
                __('Home 5 Header', 'archia-add-ons')                      => 'home5_header',
                __('Footer Section', 'archia-add-ons')                     => 'footer_sec', 
                __('Page Section', 'archia-add-ons')                       => 'page_sec', 

            ),
            "description" => __("Select one of the pre made page sections or using default", 'archia-add-ons'),
            "group"       => "Archia Theme",
        ),
        array(
            "type"        => "dropdown",
            "heading"     => __('Content Width', 'archia-add-ons'),
            "param_name"  => "cth_content_width",
            "value"       => array(
                __('Fullwidth', 'archia-add-ons')                => 'full_width',
                __('Wide Boxed', 'archia-add-ons')               => 'wide',
                __('Small Boxed', 'archia-add-ons')              => 'fix_width',

            ),
            'dependency' => array(
                'element' => 'cth_sec_layout',
                'value' => array( 'page_sec'),
                'not_empty' => false,
            ),
            "group"       => "Archia Theme",
        ),
       
        array(
            "type" => "attach_image",
            "heading" => esc_html__('Background Image', 'archia-add-ons'),
            "param_name" => "cth_bg",
            'dependency' => array(
                'element' => 'cth_sec_layout',
                'value' => array( 'home5_header'),
                'not_empty' => false,
            ),
            "group" => __( 'Archia Theme', 'archia-add-ons' ),
        ),
    );
    if (function_exists('vc_add_params')) {
        vc_add_params('vc_section', $new_sec_params);
    }

    $animation_params = array(
        array(
            "type" => "dropdown",  
            "admin_label"   => true,
            "heading" => __('CSS Animation', 'archia-add-ons'), 
            "param_name" => "ani_type", 
            "value" => archia_addons_vc_animation(),
            "description" => __("Select type of animation for element to be animated when it \"enters\" the browsers viewport (Note: works only in modern browsers).", 'archia-add-ons'), 
            "std" => '',
            "group" => __( 'Archia Theme', 'archia-add-ons' ),
        ),
        array(
            "type" => "textfield",
            "admin_label"   => true,
            "heading" => esc_html__("Animation Duration", 'archia-add-ons'),
            "param_name" => "ani_time",
            "description" => esc_html__("Ex: 0.2s for 200 miliseconds, 2s for 2 second", 'archia-add-ons'),
            "value" => "2s",
            'dependency' => array(
                'element' => 'ani_type',
                'not_empty' => true,
            ),
            "group" => __( 'Archia Theme', 'archia-add-ons' ),
        ),
        array(
            "type" => "textfield",
            "admin_label"   => true,
            "heading" => esc_html__("Animation delay", 'archia-add-ons'),
            "param_name" => "ani_delay",
            "description" => esc_html__("Ex: 0.2s for 200 miliseconds, 2s for 2 second", 'archia-add-ons'),
            "value" => "0.2s",
            'dependency' => array(
                'element' => 'ani_type',
                'not_empty' => true,
            ),
            "group" => __( 'Archia Theme', 'archia-add-ons' ),
        ),
    );

    // new columns animation
    if (function_exists('vc_add_params')) {
        vc_add_params('vc_column', $animation_params);
        vc_add_params('vc_column_inner', $animation_params);
        vc_add_params('vc_single_image', $animation_params);
    }

    if( function_exists('vc_remove_param') ){ 
        // vc_remove_param( 'vc_column', 'css_animation' ); 
        // vc_remove_param( 'vc_single_image', 'css_animation' ); 
    }

    // new themify icon
    add_filter( 'vc_iconpicker-type-themify', 'archia_addons_vc_iconpicker_type_themify' );

}

add_action('init', 'archia_add_ons_add_vc_param');

function archia_addons_vc_iconpicker_type_themify($icons){
    $icons = archia_addons_extract_themify();
    $icon_options = array();
    foreach ($icons as $icon) {
        // php 7.3
        // $icon_options['im '.array_key_first($icon)] = reset( $icon );
        $icotitle = reset( $icon );
        $icon_options[] = array( key($icon) => $icotitle ) ;
    }
    return $icon_options;

    // return archia_addons_get_icon_themify();
}

