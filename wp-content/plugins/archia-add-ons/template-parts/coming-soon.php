<?php

?>
<!DOCTYPE html>
<html class="no-js no-svg" itemscope>
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link rel="profile" href="http://gmpg.org/xfn/11">
    <title><?php echo wp_get_document_title() ?></title>
    <link href="https://fonts.googleapis.com/css?family=Poppins:400,600,700,900" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo BBT_DIR_URL.'assets/css/font-awesome.min.css'; ?>" type="text/css" media="all">
    <link rel="stylesheet" href="<?php echo BBT_DIR_URL.'assets/css/maintenance.min.css'; ?>" type="text/css" media="all">
</head>
<body id="bg">
	<div class="page-wraper">
		<div id="loading-area" class="loading-1"><h1 class="loader1"><?php _e( 'Loading', 'archia-add-ons' ); ?></h1></div>
		<!-- Content -->
		<?php $bgimg = archia_addons_get_option('coming_soon_bg'); ?>
		<div class="content-block">
			<div class="coming-soon bg" data-bg="<?php echo esc_url( wp_get_attachment_url( $bgimg['id'] ) ); ?>">
				<div class="countdown-box">
					<?php 
					$email = archia_addons_get_option('coming_soon_email'); 
					if($email != ''): ?>
					<div class="button-home">
						<a href="mailto:<?php echo esc_attr( $email ); ?>" class="btn btn-lg radius-xl"><i class="fa fa-home"></i> <?php echo  $email ; ?></a>
					</div>
					<?php endif; ?>
					<?php if(archia_addons_get_option('coming_soon_date') != ''){ ?>
	                <!-- countdown -->
	                <div class="countdown fl-wrap countdown-widget" data-countdate="<?php echo esc_attr( archia_addons_get_option('coming_soon_date') ); ?>" data-counttime="<?php echo esc_attr( archia_addons_get_option('coming_soon_time') ); ?>" data-tz="<?php echo esc_attr( archia_addons_get_option('coming_soon_tz') ); ?>">
	                    <div class="date">
							<span class="time days text-primary"></span>
							<div><strong><?php _e( 'Days', 'archia-add-ons' ); ?></strong></div>
						</div>
						<div class="date">
							<span class="time hours text-primary"></span>
							<div><strong><?php _e( 'Hours', 'archia-add-ons' ); ?></strong></div>
						</div>
						<div class="date">
							<span class="time mins minutes text-primary"></span>
							<div><strong><?php _e( 'Minutes', 'archia-add-ons' ); ?></strong></div>
						</div>
						<div class="date">
							<span class="time secs seconds text-primary"></span>
							<div><strong><?php _e( 'Seconds', 'archia-add-ons' ); ?></strong></div>
						</div>
	                </div>
	                <!-- countdown end -->
					<?php } ?>

				</div>
				<div class="coming-head">
					
					<?php echo do_shortcode( archia_addons_get_option('coming_soon_msg_after') ); ?>
					
				</div>
			</div>
		</div>
	    <!-- Content END-->
	</div>
    <!-- Main end -->
    <script type='text/javascript' src='<?php echo BBT_DIR_URL.'assets/js/jquery.min.js'; ?>'></script>
    <script type='text/javascript'>
    /* <![CDATA[ */
    var _archia_add_ons = {"pl_w":"Please wait...","url": "<?php echo esc_url(admin_url( 'admin-ajax.php' ) ); ?>","disable_bubble":"<?php _ex( 'no', 'Disable bubbles on coming soon page: yes or no', 'archia-add-ons' ) ?>"};
    /* ]]> */
    </script>
    <script type='text/javascript' src='<?php echo BBT_DIR_URL.'assets/js/maintenance.min.js'; ?>'></script>
</body>
</html>
