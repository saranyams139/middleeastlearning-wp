<?php
if(!isset($filter_style)) $filter_style = 'style1';
if(!isset($term_args)){
    $term_args = array(
        'orderby'           => archia_get_option('folio_filter_orderby','name'),
        'order'             => archia_get_option('folio_filter_order', 'ASC'),
  
    ); 
}

$term_args['taxonomy'] = 'portfolio_cat';

$terms = get_terms($term_args); 

if ( ! empty( $terms ) && ! is_wp_error( $terms ) ){
?>
<div class="row">
    <div class="col-lg-12 text-center">
        <div class="site-filters filter-<?php echo esc_attr( $filter_style );?> clearfix m-b20">
            <ul class="filters folio-filters" data-toggle="buttons">
                <li data-filter="*" class="btn folio-filter active"><a href="javascript:void(0)"><span><?php  esc_html_e('All' , 'archia-add-ons' ); ?></span></a></li>
                <?php foreach ($terms as $term) { ?>
                    <li class="btn folio-filter" data-filter=".portfolio_cat-<?php echo esc_attr($term->slug ); ?>"><a href="javascript:void(0)"><span><?php echo esc_html($term->name);?></span></a></li>
                <?php } ?>
            </ul>
        </div>
    </div>
</div>
<?php 
} ?>



