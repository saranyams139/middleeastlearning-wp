<?php

get_header(); 
get_template_part('template-parts/header/singular');
//get_breadcrumb();
while(have_posts()) : the_post();
    the_content(); 
    wp_link_pages();
endwhile; 

get_footer( );