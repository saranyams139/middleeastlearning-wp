<?php 
// $ani_type = $ani_time = $ani_delays = '';
if(!isset($ani_type)) $ani_type = '';
if(!isset($ani_time)) $ani_time = '';
if(!isset($ani_delays)) $ani_delays = '';
$ani_delays_arr = explode(',', $ani_delays);
?>
<div class="<?php echo esc_attr($grid_cls );?> portfolio-bx">
    <div class="grid-sizer"></div>
<?php
    $key = 0;
    $items_widths = explode(',',$items_width);
    while ($query_posts->have_posts()) : $query_posts->the_post();
        $item_wid = 'x1';
        if(isset($items_widths[$key])) $item_wid = $items_widths[$key];
        $item_cls = 'folio-item';
        $tnsize = 'archia-folio';
        switch ($item_wid) {
            case 'x1':
                // $item_cls .= ' col-one col-lg-4 col-md-4 col-sm-4';
                $tnsize = 'archia-folio';
                break;
            case 'x2':
                $item_cls = 'folio-item folio-item-two';
                $tnsize = 'archia-folio-two';
                break;
            case 'x3':
                $item_cls = 'folio-item folio-item-three';
                $tnsize = 'archia-folio-three';
                break;
        }

        $ani_attrs = '';
        if ( !empty($ani_type) ) {
            $item_cls .= ' wow ' . $ani_type;
            if($ani_time != '')
                $ani_attrs .= ' data-wow-duration="'.esc_attr( $ani_time ).'"';
            if( isset($ani_delays_arr[$key]) && $ani_delays_arr[$key] != '' )
                $ani_attrs .= ' data-wow-delay="'.esc_attr( $ani_delays_arr[$key] ).'"';
            else
                $ani_attrs .= ' data-wow-delay="0.2s"';
        }

        ?>
        <div <?php post_class('card-containeraa p-lr0 '.$item_cls ); ?><?php echo $ani_attrs; ?>>
			<div class="dlab-media dlab-img-overlay1 dlab-img-effect portbox1">
				<?php the_post_thumbnail( $tnsize, array( 'class' => $tnsize.'-thumbnail' ) ); ?>
				<div class="overlay-bx">
					<div class="portinner">
						<span><?php echo sprintf( __( '%s in %s', 'archia-add-ons' ), the_time(get_option('date_format')), archia_addons_portfolio_taxs( get_the_ID() ) ) ; ?></span>
						<h3 class="port-title"><a href="<?php the_permalink(  ); ?>"><?php the_title(); ?></a></h3>
						<a href="<?php the_permalink(  ); ?>" class="btn outline outline-2 radius-xl"><?php _e( 'View Project', 'archia-add-ons' ); ?></a>
					</div>
				</div>
			</div>
		</div>
        <?php
        $key++;
    endwhile; ?>
</div>
