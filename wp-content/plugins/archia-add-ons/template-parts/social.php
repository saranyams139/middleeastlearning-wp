<?php
/* add_ons_php */
$socials = archia_addons_get_socials_list();
if(!isset($index)) $index = false;
if(!isset($url)) $url = '#';
if(!isset($name)) $name = 'facebook';
?>
<div class="entry">
    <select class="custom-select" name="socials[<?php echo $index === false ? '{{data.index}}':$index;?>][name]" required>
        <?php
        foreach ($socials as $val => $lbl) {
            echo '<option value="'.$val.'" '.selected( $name, $val, false ).'>'.$lbl.'</option>';
        }
        ?>
    </select>
    <input type="text" name="socials[<?php echo $index === false ? '{{data.index}}':$index;?>][url]" placeholder="<?php esc_attr_e( 'Social URL',  'archia-add-ons' );?>" value="<?php echo $url;?>" required>
    <button class="btn rmfield" type="button" ><?php _e( 'Remove', 'archia-add-ons' ); ?><i class="fa fa-trash"></i></button>
</div>
<!-- end entry -->

