<?php
/* add_ons_php */
get_header(); 


get_template_part('template-parts/header/portfolio'); ?>
    <!-- blog grid -->
    <div class="content-block">
        <!-- blog grid -->
        <div class="section-full content-inner">
            <div class="container">

                <div class="row">
					
					<div class="col-md-12">
						<?php
						global $wp_query;

						$folio_item_style = archia_get_option('folio_style');  // default - tile - masonry - home2 - home4 - home5
						if( archia_get_option('folio_show_filter') ){
				            $filter_style = 'style1';
				            if($folio_item_style == 'home2') $filter_style = 'style2';
				            archia_addons_get_template_part( 
				                'template-parts/portfolio/filter', 
				                '1' , 
				                array(
				                    // 'term_args'=>$term_args,
				                    'filter_style'=>$filter_style,
				                ) 
				            );
				        } 

        
						
						$grid_cls = 'folio-items folio-isotope';
				        $grid_cls .= ' folio-'.archia_get_option('folio-cols').'-cols';
				        $grid_cls .= ' folio-style-'.$folio_item_style;
				        $grid_cls .= ' folio-'.archia_get_option('folio-pad').'-pad';

					    ?>
					    <div class="folio-items-wrap clearfix">
					        <?php
					        archia_addons_get_template_part(
					            'template-parts/portfolio/grid', 
					            $folio_item_style, 
					            array( 
					                'grid_cls' => $grid_cls, 
					                'query_posts' => $wp_query, 
					                'items_width'  => '' ,
					                'ani_type'  => '' ,
					                'ani_time'  => '' ,
					                'ani_delays'  => '' ,
					            )
					        );
					        ?> 
					    </div>
						
						<?php archia_pagination() ?>

					</div>
                
                    
                </div>
                
            </div>

        </div>
        <!-- blog grid END -->
    </div>
    <!-- blog grid END -->
<?php 
get_footer();
