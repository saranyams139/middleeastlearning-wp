<?php
/* add_ons_php */

class Esb_Class_Portfolio_CPT extends Esb_Class_CPT {
    protected $name = 'portfolio';
    protected function init(){
        parent::init();
        add_action( 'init', array($this, 'taxonomies'), 0 ); 
        add_filter('manage_edit-portfolio_cat_columns', array($this, 'tax_cat_columns_head') );
        add_filter('manage_portfolio_cat_custom_column', array($this, 'tax_cat_columns_content'), 10, 3); 
        add_filter('single_template', array($this, 'single_template')); 
        add_filter('pre_get_posts', array($this, 'pre_get_posts')); 


        // add_filter('use_block_editor_for_post_type', array($this, 'enable_gutenberg'), 10, 2 );
    }
    public function enable_gutenberg( $current_status, $post_type ){
        if ($post_type === 'portfolio') 
            return true;

        return $current_status;
    }

    public function single_template($single_template){
        global $post;

        if ($post->post_type == 'portfolio') {
            $single_template = archia_addons_get_template_part('template-parts/portfolio/single', '', null, false);
        }
        return $single_template;
    }

    public function pre_get_posts($query){
        if ( ! is_admin() && $query->is_main_query() ) {
            if( is_post_type_archive('portfolio') || is_tax('portfolio_cat') ){
                $query->set('posts_per_page', archia_get_option('folio_posts'));
                $query->set('orderby', archia_get_option('folio_orderby'));
                $query->set('order', archia_get_option('folio_order'));

            }
        }
    }

    public function register(){

        $labels = array( 
            'name' => __( 'Portfolio', 'archia-add-ons' ),
            'singular_name' => __( 'Portfolio', 'archia-add-ons' ),
            'add_new' => __( 'Add New Portfolio', 'archia-add-ons' ),
            'add_new_item' => __( 'Add New Portfolio', 'archia-add-ons' ),
            'edit_item' => __( 'Edit Portfolio', 'archia-add-ons' ),
            'new_item' => __( 'New Portfolio', 'archia-add-ons' ),
            'view_item' => __( 'View Portfolio', 'archia-add-ons' ),
            'search_items' => __( 'Search Portfolios', 'archia-add-ons' ),
            'not_found' => __( 'No Portfolios found', 'archia-add-ons' ),
            'not_found_in_trash' => __( 'No Portfolios found in Trash', 'archia-add-ons' ),
            'parent_item_colon' => __( 'Parent Portfolio:', 'archia-add-ons' ),
            'menu_name' => __( 'Portfolios', 'archia-add-ons' ),
        );

        $args = array( 
            'labels' => $labels,
            'hierarchical' => true,
            'description' => 'List Portfolios',
            'supports' => array( 'title', 'editor',  'author', 'thumbnail','comments','excerpt', 'page-attributes' /*'title', 'editor', 'excerpt', 'thumbnail','comments',*//*, 'post-formats'*/),
            'taxonomies' => array('portfolio_cat'),
            'public' => true,
            'show_ui' => true,
            'show_in_menu' => true,
            'menu_position' => 20,
            'menu_icon' => 'dashicons-editor-kitchensink', 
            'show_in_nav_menus' => true,
            'publicly_queryable' => true,
            'exclude_from_search' => false,
            'has_archive' => true,
            'query_var' => true,
            'can_export' => true,
            'rewrite' => true,
            'capability_type' => 'post',
            'rewrite' => array( 
                    'slug' => 'project'
                    ),
            // https://wordpress.stackexchange.com/questions/324221/enable-gutenberg-on-custom-post-type
            // 'show_in_rest'      => true,
        );
        register_post_type( $this->name, $args );
    }
    public function taxonomies(){
        $labels = array(
            'name' => __( 'Categories', 'archia-add-ons' ),
            'singular_name' => __( 'Category', 'archia-add-ons' ),
            'search_items' =>  __( 'Search Categories','archia-add-ons' ),
            'all_items' => __( 'All Categories','archia-add-ons' ),
            'parent_item' => __( 'Parent Category','archia-add-ons' ),
            'parent_item_colon' => __( 'Parent Category:','archia-add-ons' ),
            'edit_item' => __( 'Edit Category','archia-add-ons' ), 
            'update_item' => __( 'Update Category','archia-add-ons' ),
            'add_new_item' => __( 'Add New Category','archia-add-ons' ),
            'new_item_name' => __( 'New Category Name','archia-add-ons' ),
            'menu_name' => __( 'Categories','archia-add-ons' ),
          );     

        // Now register the taxonomy

        register_taxonomy('portfolio_cat',array('portfolio'), array(
            'hierarchical' => true,
            'labels' => $labels,
            'show_ui' => true,
            'show_admin_column' => true,
            'query_var' => true,
            'rewrite' => array( 'slug' => 'portfolio_cat' ),
        ));

    }
    public function tax_cat_columns_head($columns) {
        // $columns['_thumbnail'] = __('Thumbnail','archia-add-ons');
        $columns['_id'] = __('ID','archia-add-ons');
        return $columns;
    }

    public function tax_cat_columns_content($c, $column_name, $term_id) {
        if ($column_name == '_id') {
            echo $term_id;
        }
        // if ($column_name == '_thumbnail') {
        //     $term_meta = get_term_meta( $term_id, ESB_META_PREFIX.'term_meta', true );
        //     if(isset($term_meta['featured_img']) && !empty($term_meta['featured_img'])){
        //         echo wp_get_attachment_image( $term_meta['featured_img']['id'], 'thumbnail', false, array('style'=>'width:100px;height:auto;') );
                
        //     }
        // }
    }
    protected function set_meta_columns(){
        $this->has_columns = true;
    }
    public function meta_columns_head($columns){
        $columns['_id'] = __( 'ID', 'archia-add-ons' );
        $columns['_thumbnail'] = __( 'Thumbnail', 'archia-add-ons' );
        return $columns;
    }
    public function meta_columns_content($column_name, $post_ID){
        if ($column_name == '_id') {
            echo $post_ID;
        }
        if ($column_name == '_thumbnail') {
            echo get_the_post_thumbnail( $post_ID, 'thumbnail', array('style'=>'width:100px;height:auto;') );
        }
    }

}

new Esb_Class_Portfolio_CPT();