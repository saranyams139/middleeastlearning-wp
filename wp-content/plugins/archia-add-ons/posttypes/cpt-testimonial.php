<?php
/* add_ons_php */

class Esb_Class_Testimonial_CPT extends Esb_Class_CPT {
    protected $name = 'cth_testi';
    // protected function init(){
    //     parent::init();
        
    //     // add_filter('use_block_editor_for_post_type', array($this, 'enable_gutenberg'), 10, 2 );
    // }
    public function enable_gutenberg( $current_status, $post_type ){
        if ($post_type === 'cth_testi') 
            return true;

        return $current_status;
    }

    public function register(){

        $labels = array( 
            'name' => __( 'Testimonials', 'archia-add-ons' ),
            'singular_name' => __( 'Testimonial', 'archia-add-ons' ),
            'add_new' => __( 'Add New Testimonial', 'archia-add-ons' ),
            'add_new_item' => __( 'Add New Testimonial', 'archia-add-ons' ),
            'edit_item' => __( 'Edit Testimonial', 'archia-add-ons' ),
            'new_item' => __( 'New Testimonial', 'archia-add-ons' ),
            'view_item' => __( 'View Testimonial', 'archia-add-ons' ),
            'search_items' => __( 'Search Testimonials', 'archia-add-ons' ),
            'not_found' => __( 'No Testimonials found', 'archia-add-ons' ),
            'not_found_in_trash' => __( 'No Testimonials found in Trash', 'archia-add-ons' ),
            'parent_item_colon' => __( 'Parent Testimonial:', 'archia-add-ons' ),
            'menu_name' => __( 'Archia Testimonials', 'archia-add-ons' ),
        );

        $args = array( 
            'labels' => $labels,
            'hierarchical' => false,
            'description' => __( 'List Testimonials', 'archia-add-ons' ),
            'supports' => array( 'title', 'editor', 'thumbnail'/*,'comments', 'post-formats'*/),
            'public' => false,
            'show_ui' => true,
            'show_in_menu' => true,
            'menu_position' => 25,
            'menu_icon' => 'dashicons-format-chat', 
            'show_in_nav_menus' => false,
            'publicly_queryable' => true,
            'exclude_from_search' => true,
            'has_archive' => false,
            'query_var' => true,
            'can_export' => true,
            'rewrite' => array( 'slug' => __('testimonial','archia-add-ons') ),
            'capability_type' => 'post'
        );
        register_post_type( $this->name, $args );
    }
    protected function set_meta_columns(){
        $this->has_columns = true;
    }
    public function meta_columns_head($columns){
        $columns['_id'] = __( 'ID', 'archia-add-ons' );
        $columns['rating'] = __( 'Rating', 'archia-add-ons' );
        $columns['_thumbnail'] = __( 'Thumbnail', 'archia-add-ons' );
        return $columns;
    }
    public function meta_columns_content($column_name, $post_ID){
        if ($column_name == '_id') {
            echo $post_ID;
        }
        if ($column_name == '_thumbnail') {
            echo get_the_post_thumbnail( $post_ID, 'thumbnail', array('style'=>'width:100px;height:auto;') );
        }
        if ($column_name == 'rating') {
            $rated = get_post_meta($post_ID, '_cth_testim_rate', true );
            if($rated != '' && $rated != 'no'){
                $ratedval = floatval($rated);
                echo '<ul class="star-rating">';
                for ($i=1; $i <= 5; $i++) { 
                    if($i <= $ratedval){
                        echo '<li><i class="testimfa testimfa-star"></i></li>';
                    }else{
                        if($i - 0.5 == $ratedval){
                            echo '<li><i class="testimfa testimfa-star-half-o"></i></li>';
                        }else{
                            echo '<li><i class="testimfa testimfa-star-o"></i></li>';
                        }
                    }
                    
                }
                echo '</ul>';
            }else{
                esc_html_e('Not Rated','archia-add-ons' );
            }
        }

    }
    // protected function set_meta_boxes(){
    //     $this->meta_boxes = array(
    //         'social'       => array(
    //             'title'                 => __( 'Socials', 'archia-add-ons' ),
    //             'context'               => 'normal', // normal - side - advanced
    //             'priority'              => 'high', // default - high - low
    //             'callback_args'         => array(),
    //         ),
    //     );
    // }
    public function cth_testi_social_callback($post, $args){
        wp_nonce_field( 'cth-cpt-fields', '_cth_cpt_nonce' );
        $socials = get_post_meta( $post->ID, BBT_META_PREFIX.'socials', true );
        ?>
        <div class="custom-form">
            <div class="repeater-fields-wrap repeater-socials"  data-tmpl="tmpl-user-social">
                <div class="repeater-fields">
                <?php 
                if(!empty($socials)){
                    foreach ($socials as $key => $social) {
                        archia_addons_get_template_part('template-parts/social',false, array('index'=>$key,'name'=>$social['name'],'url'=>$social['url']));
                    }
                }
                ?>
                </div>
                <button class="btn addfield" type="button"><?php  esc_html_e( 'Add Social','archia-add-ons' );?></button>
            </div>
        </div>
        <?php
    }
    /**
     * Save post metadata when a post is saved.
     *
     * @param int $post_id The post ID.
     * @param post $post The post object.
     * @param bool $update Whether this is an existing post being updated or not.
     */
    public function save_post($post_id, $post, $update){
        if(!$this->can_save($post_id)) return;

        if(isset($_POST['socials'])){
            update_post_meta( $post_id, BBT_META_PREFIX.'socials', $_POST['socials'] );
        }
    }

}

new Esb_Class_Testimonial_CPT();

