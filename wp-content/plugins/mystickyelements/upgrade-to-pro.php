<link rel="stylesheet" type="text/css" href="<?php echo plugins_url('/css/upgrade-to-pro.css', __FILE__) ?>" />
<?php $pro_url = "https://go.premio.io/?edd_action=add_to_cart&download_id=3432&edd_options[price_id]=" ?>
<div id="rpt_pricr" class="rpt_plans rpt_3_plans  rpt_style_basic">
    <p class="udner-title">
        <strong class="text-primary">Unlock All Features</strong>
    </p>
    <div class="">
        <div class="rpt_plan  rpt_plan_0">
            <div style="text-align:left;" class="rpt_title rpt_title_0">Basic</div>
            <div class="rpt_head rpt_head_0">
                <div class="rpt_recurrence rpt_recurrence_0 ">For small website owners</div>
                <div class="rpt_price rpt_price_0">$25</div>
                <div class="rpt_description rpt_description_0 rpt_desc">Per year. Renewals for 25% off</div>
                <div style="clear:both;"></div>
            </div>
            <div class="rpt_features rpt_features_0">
                <div class="rpt_feature rpt_feature_0-0"><a href="javascript:;" class="rpt_tooltip"><span class="intool"><b></b>Use My Sticky Elements on 1 domain</span>1 website<span class="rpt_tooltip_plus" > +</span></a></div>
                <div class="rpt_feature rpt_feature_0-1"><a href="javascript:;" class="rpt_tooltip"><span class="intool"><b></b>You can get the emails from your contact form directy to your email</span>Get leads to email<span class="rpt_tooltip_plus" > +</span></a></div>
                <div class="rpt_feature rpt_feature_0-2"><a href="javascript:;" class="rpt_tooltip"><span class="intool"><b></b>Show unlimited social icons such as WhatsApp, Facebook, Twitter, Phone and more</span>Unlimited icons<span class="rpt_tooltip_plus" > +</span></a></div>
                <div class="rpt_feature rpt_feature_0-3"><a href="javascript:;" class="rpt_tooltip"><span class="intool"><b></b>Upload your own icons for your floating tabs</span>Custom icons<span class="rpt_tooltip_plus" > +</span></a></div>
                <div class="rpt_feature rpt_feature_0-4"><a href="javascript:;" class="rpt_tooltip"><span class="intool"><b></b>Add CSS of your own to your form and social tabs</span>Custom CSS style<span class="rpt_tooltip_plus" > +</span></a></div>
                <div class="rpt_feature rpt_feature_0-5"><a href="javascript:;" class="rpt_tooltip"><span class="intool"><b></b>Change the height position of the tabs on your website</span>Custom height position<span class="rpt_tooltip_plus" > +</span></a></div>
				<div style="color:black;" class="rpt_feature rpt_feature_0-6"><a href="javascript:void(0)" class="rpt_tooltip"><span class="intool"><b></b>Show/hide the widget on specific pages</span>Page targeting<span class="rpt_tooltip_plus" style="color:#5500ff !important;"> +</span></a></div>
				<div style="color:black;" class="rpt_feature rpt_feature_0-7"><a href="javascript:void(0)" class="rpt_tooltip"><span class="intool"><b></b>Create different bars for different pages. Show different icons, form and language based on page targeting rules</span>Multiple widgets<span class="rpt_tooltip_plus" style="color:#5500ff !important;"> +</span></a></div>
                <div class="rpt_feature rpt_feature_0-8">No "Get widget" credit</div>
                <div class="rpt_feature rpt_feature_0-9">
                    <select data-key="0" class="multiple-options">
                        <option data-header="Renewals for 25% off" data-price="25" value="<?php echo esc_url($pro_url."1") ?>">
                            <?php esc_html_e("Updates & support for 1 year") ?>
                        </option>
                        <option data-header="For 3 years" data-price="49" value="<?php echo esc_url($pro_url."4") ?>">
                            <?php esc_html_e("Updates & support for 3 years") ?>
                        </option>
                        <option data-header="For lifetime" data-price="79" value="<?php echo esc_url($pro_url."5") ?>">
                            <?php esc_html_e("Updates & support for lifetime") ?>
                        </option>
                    </select>
                </div>
            </div>
            <div style="clear:both;"></div>
            <a target="_blank" href="https://go.premio.io/?edd_action=add_to_cart&amp;download_id=3432&amp;edd_options[price_id]=1" class="rpt_foot rpt_foot_0">Buy now</a>
        </div>
        <div class="rpt_plan  rpt_plan_1 rpt_recommended_plan ">
            <div style="text-align:left;" class="rpt_title rpt_title_1">Plus<img class="rpt_recommended" src="<?php echo plugins_url('/images/rpt_recommended.png', __FILE__) ?>" style="top: 27px;"></div>
            <div class="rpt_head rpt_head_1">
                <div class="rpt_recurrence rpt_recurrence_1">For businesses with multiple websites</div>
                <div class="rpt_price rpt_price_1">$59</div>
                <div class="rpt_description rpt_description_1 rpt_desc">Per year. Renewals for 25% off</div>
                <div style="clear:both;"></div>
            </div>
            <div class="rpt_features rpt_features_1">
                <div class="rpt_feature rpt_feature_1-0"><a href="javascript:;" class="rpt_tooltip"><span class="intool"><b></b>Use My Sticky Elements on 5 domains</span>5 websites<span class="rpt_tooltip_plus" > +</span></a></div>
                <div class="rpt_feature rpt_feature_0-1"><a href="javascript:;" class="rpt_tooltip"><span class="intool"><b></b>You can get the emails from your contact form directy to your email</span>Get leads to email<span class="rpt_tooltip_plus" > +</span></a></div>
                <div class="rpt_feature rpt_feature_0-2"><a href="javascript:;" class="rpt_tooltip"><span class="intool"><b></b>Show unlimited social icons such as WhatsApp, Facebook, Twitter, Phone and more</span>Unlimited icons<span class="rpt_tooltip_plus" > +</span></a></div>
                <div class="rpt_feature rpt_feature_0-3"><a href="javascript:;" class="rpt_tooltip"><span class="intool"><b></b>Upload your own icons for your floating tabs</span>Custom icons<span class="rpt_tooltip_plus" > +</span></a></div>
                <div class="rpt_feature rpt_feature_0-4"><a href="javascript:;" class="rpt_tooltip"><span class="intool"><b></b>Add CSS of your own to your form and social tabs</span>Custom CSS style<span class="rpt_tooltip_plus" > +</span></a></div>
                <div class="rpt_feature rpt_feature_0-5"><a href="javascript:;" class="rpt_tooltip"><span class="intool"><b></b>Change the height position of the tabs on your website</span>Custom height position<span class="rpt_tooltip_plus" > +</span></a></div>
				<div style="color:black;" class="rpt_feature rpt_feature_0-6"><a href="javascript:void(0)" class="rpt_tooltip"><span class="intool"><b></b>Show/hide the widget on specific pages</span>Page targeting<span class="rpt_tooltip_plus" style="color:#5500ff !important;"> +</span></a></div>
				<div style="color:black;" class="rpt_feature rpt_feature_0-7"><a href="javascript:void(0)" class="rpt_tooltip"><span class="intool"><b></b>Create different bars for different pages. Show different icons, form and language based on page targeting rules</span>Multiple widgets<span class="rpt_tooltip_plus" style="color:#5500ff !important;"> +</span></a></div>
                <div class="rpt_feature rpt_feature_0-8">No "Get widget" credit</div>
                <div class="rpt_feature rpt_feature_0-9">
                    <select data-key="0" class="multiple-options">
                        <option data-header="Renewals for 25% off" data-price="59" value="<?php echo esc_url($pro_url."2") ?>">
                            <?php esc_html_e("Updates & support for 1 year") ?>
                        </option>
                        <option data-header="For 3 years" data-price="99" value="<?php echo esc_url($pro_url."6") ?>">
                            <?php esc_html_e("Updates & support for 3 years") ?>
                        </option>
                        <option data-header="For lifetime" data-price="149" value="<?php echo esc_url($pro_url."7") ?>">
                            <?php esc_html_e("Updates & support for lifetime") ?>
                        </option>
                    </select>
                </div>
            </div>
            <div style="clear:both;"></div>
            <a target="_blank" href="https://go.premio.io/?edd_action=add_to_cart&amp;download_id=3432&amp;edd_options[price_id]=2" class="rpt_foot rpt_foot_1">Buy now</a>
        </div>
        <div class="rpt_plan  rpt_plan_2  ">
            <div style="text-align:left;" class="rpt_title rpt_title_2">Agency</div>
            <div class="rpt_head rpt_head_2">
                <div class="rpt_recurrence rpt_recurrence_2">For agencies who manage clients</div>
                <div class="rpt_price rpt_price_2">$99</div>
                <div class="rpt_description rpt_description_2 rpt_desc">Per year. Renewals for 25% off</div>
                <div style="clear:both;"></div>
            </div>
            <div class="rpt_features rpt_features_2">
                <div class="rpt_feature rpt_feature_2-0"><a href="javascript:;" class="rpt_tooltip"><span class="intool"><b></b>Use My Sticky Elements on 50 domains</span>50 websites<span class="rpt_tooltip_plus" > +</span></a></div>
                <div class="rpt_feature rpt_feature_0-1"><a href="javascript:;" class="rpt_tooltip"><span class="intool"><b></b>You can get the emails from your contact form directy to your email</span>Get leads to email<span class="rpt_tooltip_plus" > +</span></a></div>
                <div class="rpt_feature rpt_feature_0-2"><a href="javascript:;" class="rpt_tooltip"><span class="intool"><b></b>Show unlimited social icons such as WhatsApp, Facebook, Twitter, Phone and more</span>Unlimited icons<span class="rpt_tooltip_plus" > +</span></a></div>
                <div class="rpt_feature rpt_feature_0-3"><a href="javascript:;" class="rpt_tooltip"><span class="intool"><b></b>Upload your own icons for your floating tabs</span>Custom icons<span class="rpt_tooltip_plus" > +</span></a></div>
                <div class="rpt_feature rpt_feature_0-4"><a href="javascript:;" class="rpt_tooltip"><span class="intool"><b></b>Add CSS of your own to your form and social tabs</span>Custom CSS style<span class="rpt_tooltip_plus" > +</span></a></div>
                <div class="rpt_feature rpt_feature_0-5"><a href="javascript:;" class="rpt_tooltip"><span class="intool"><b></b>Change the height position of the tabs on your website</span>Custom height position<span class="rpt_tooltip_plus" > +</span></a></div>
				<div style="color:black;" class="rpt_feature rpt_feature_0-6"><a href="javascript:void(0)" class="rpt_tooltip"><span class="intool"><b></b>Show/hide the widget on specific pages</span>Page targeting<span class="rpt_tooltip_plus" style="color:#5500ff !important;"> +</span></a></div>
				<div style="color:black;" class="rpt_feature rpt_feature_0-7"><a href="javascript:void(0)" class="rpt_tooltip"><span class="intool"><b></b>Create different bars for different pages. Show different icons, form and language based on page targeting rules</span>Multiple widgets<span class="rpt_tooltip_plus" style="color:#5500ff !important;"> +</span></a></div>
                <div class="rpt_feature rpt_feature_0-8">No "Get widget" credit</div>
                <div class="rpt_feature rpt_feature_0-9">
                    <select data-key="0" class="multiple-options">
                        <option data-header="Renewals for 25% off" data-price="99" value="<?php echo esc_url($pro_url."10") ?>">
                            <?php esc_html_e("Updates & support for 1 year") ?>
                        </option>
                        <option data-header="For 3 years" data-price="179" value="<?php echo esc_url($pro_url."11") ?>">
                            <?php esc_html_e("Updates & support for 3 years") ?>
                        </option>
                        <option data-header="For lifetime" data-price="249" value="<?php echo esc_url($pro_url."12") ?>">
                            <?php esc_html_e("Updates & support for lifetime") ?>
                        </option>
                    </select>
                </div>
            </div>
            <div style="clear:both;"></div>
            <a target="_blank" href="https://go.premio.io/?edd_action=add_to_cart&amp;download_id=3432&amp;edd_options[price_id]=10" class="rpt_foot rpt_foot_2">Buy now</a>
        </div>
    </div>
    <div style="clear:both;"></div>
    <div class="client-testimonial">
        <p class="text-center"><span class="dashicons dashicons-yes"></span> 30 days money back guaranteed</p>
        <p class="text-center"><span class="dashicons dashicons-yes"></span> The plugin will always keep working even if you don't renew your license</p>
        <div class="payment">
            <img src="<?php echo plugins_url('/images/payment.png', __FILE__) ?>" alt="Payment" class="payment-img" />
        </div>
        <div class="folder-testimonial-list">
            <div class="folder-testimonial">
                <div class="testimonial-image"> <img src="<?php echo plugins_url('/images/image.png', __FILE__) ?>"> </div>
                <div class="testimonial-data">
                    <div class="testimonial-title">Well crafted, beautiful design, furious fast support</div>
                    <div class="testimonial-desc">
                        Well crafted, beautifully designed, with multiple integration options, growing in features with extremely fast and prudent support plugins. Discovering Pro versions has been a blazing amazing experience. Long live and happy development!<br/>
                        Give us more of that high raised bar of quality products like that &#128578;
                    </div>
                    <div class="testimonial-author">- Vitaly Tulyakov</div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    jQuery(document).ready(function(){
        jQuery(".multiple-options").change(function(){
            thisValue = jQuery(this).val();
            jQuery(this).closest(".rpt_plan").find("a.rpt_foot").attr("href", thisValue);
            thisPrice = jQuery(this).find("option:selected").attr("data-price");
            jQuery(this).closest(".rpt_plan").find(".rpt_price").text("$"+thisPrice);
            priceText = jQuery(this).find("option:selected").attr("data-header");
            jQuery(this).closest(".rpt_plan").find(".rpt_desc").text(priceText);
        });
    });
</script>