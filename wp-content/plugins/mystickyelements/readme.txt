﻿=== All-in-one Floating Contact Form, Call, Chat, and 40+ Social Icon Tabs  - My Sticky Elements ===
Contributors: galdub, tomeraharon, premio
Tags: contact form, whatsapp, facebook messenger, call now button, instagram, youtube, social buttons, messegner, floating form, call button
Requires at least: 3.1
Tested up to: 5.3
Stable tag: 1.7
Plugin URI: https://premio.io/downloads/mystickyelements/

Capture leads with beautiful floating contact form tab & get followers and chat messages via Facebook Messenger, WhatsApp, Viber, Telegram, Twitter, Instagram, and more.

== Description ==

<strong>Add floating form and tabs on any side of your website to help your visitors contact you and easily find your Facebook page, YouTube channel, open hours, address, phone number, email or any other important business information </strong>

* Capture more leads and messages
* Get more followers on your business’ social pages
* Show important information such as open hours and address

Want to use the robust pro version? Check out <a href="https://premio.io/downloads/mystickyelements/?utm_source=wordpressorg" target="_blank" title="My Sticky Elements
pro plans"><strong>My Sticky Elements Pro Plans</strong></a>.

= Contact form and social buttons in one plugin =
Show customizable tabs that will help your website visitors contact you and reach out to you or follow your business on any popular social channel such as Facebook, Messenger, Instagram, WhatsApp, Telegram, Pinterest, Youtube, WeChat and more.


= Show contact and social tabs =
Contact form
Facebook page
Twitter page
Instagram page
Pinterest page
Whatsapp
YouTube channel
Click to call
Facebook Messenger
Email
Address
Open hours
WeChat
Telegram
Vimeo
Spotify
Itunes
SoundCloud
VK
Viber
Snapchat
Skype
Line
SMS
Tumblr
Qzone
QQ
Behance
Dribble
Quora
Custom Icon
Shortcode, IFrame or HTML icon
3 Custom icons (Pro)
3 Shortcode, IFrame or HTML icons (Pro)

= Live Demo =
A live demo for My Sticky Elements plugin is available at <a href="https://demo.premio.io/mystickyelements/?utm_source=wordpressorg" target="_blank" title="Premio.io demo site">Premio's demo site</a>.

= Join the Premio community =
Have any questions or feature suggestions? We'd love to hear from you. <strong><a href="https://www.facebook.com/groups/premioplugins/" target="_blank" title="Premio Facebook group">Join our Facebook group</a></strong> to share ideas and get updates about new features.

= What do you get in the Free plan? =
1. Show a floating contact form with any field you want: Name, Phone, Email, and Message.
2. Choose what fields in your floating contact form are required to be filled by your visitors.
3. Show up to 2 different social icons tabs.
4. Select the location of the widget (bottom/left/right)
5. New! Choose from a variety of contact icons templates including rounded, diamond, triangle, leaf shaped and more
6. New! Set an entry effect (slide-in or fade) for your contact form and chat icons
7. New! Bottom and top location for mobile - display the widget on the bottom part of the screen for easy access on mobile (including the contact form). Let your visitors contact you with ease! When you choose top position for the widget, all your content will be pushed down, which means your menu and other header content will stay visible and clickable.
8. Add a custom Call-To-Action beside each tab
9. Choose background colors for your tabs
10. Choose the order of your social icons tabs (with drag and drop)
11. Decide which tabs you want to show on desktop & mobile, or both.
12. Decide whether the tabs will be opened by hover or click
13. LTR/RTL support for your contact form
14. Collect all of your contact form leads into your local database
15. New! Minimize tab: from now on your visitors can minimize the floating menu on desktop and mobile. You can show the minimize icon on mobile or desktop separately, and also decide if you want to have the widget minimized by default or fully displayed by default.
16. New! Change contact form fields order: you can now decide the order of your contact forms fields using drag and drop 
17. New! 16 new channels: Vimeo, Spotify, Itunes, SoundCloud, VK, Viber, Snapchat, Skype
Line, SMS, Tumblr, Qzone, QQ, Behance, Dribbble, Quora
18. Full RTL support for RTL menus
19. Change the font of all the buttons text
20. New! Change the size of the widget bar (both the contact form and all the channels' icons)
21. New! A custom icon is now available in the free version, you can use it to link to your click to chat provider or any other relevant website or in-site page. You can use it as a plain link to any website, or as  a Javascript to trigger any chat provider or any other Javascript triggered service.
22. New! You can use the new shortcode, IFrame or HTML icon and display any embedded content on-hover or on-click. You can use it to show a youtube/vimeo video, contact form 7 or any other form, maps, or any other shortcode or IFrame.
23. New! You can add a preset message that'll automatically be inserted to the WhatsApp messages your visitors send you.
24. New! Track the URL from which your contact form was submitted.
25. New! Use Font Awesome icons for the custom icons and the shortcode icons.

= What do you get in the Pro plan? =
Unlock the Pro features for only $25/year
You will get all the features of the free plan, plus:

1. Show unlimited tabs at the same time (contact form + 29 social icons + 3 custom icons + 3 shortcode, HTML, and IFrame icons)
2. Get the leads for your contact form directly to your email
3. Upload up to 3 custom icons for your tabs. You can use it to link to your click to chat provider or any other relevant website or in-site page. You can use it as a plain link to any website, or as  a Javascript to trigger any chat provider or any other Javascript triggered service.
4. New! Page targeting - show or hide your contact form, chat, call, and social networks channels on specific pages. Use our advanced page targeting rules (contain, exact match, starting with, and ending with) to show and hide the widget
5. Create different bars for different pages. Show different icons, form and language based on page targeting rules
6. Custom CSS style for your contact form and tabs
7. Custom height position on the screen
8. No “Get widget” credit
9. Google Analytics events. this is how it work:
For Facebook, Twitter, Instagram, Pinterest, Whatsapp, YouTube, Click to call, Facebook Messenger, Email, Telegram -> the Google Analytics  event will be fired when the visitor click on their link in the open tab.
For Open hours, Address and WeChat -> The Google Analytics event will be fired when the visitor hover the tab
10. Dropdown multi select field to your contact form -> add dropdown select box to your contact form for multi choice, and give your visitors the option to select one variant from a list of predefined variants.
11. New! Redirect your visitors to another page after the form is submitted. You can use this feature to present a thank-you page for your form.
12. New! You can create 3 shortcode, IFrame or HTML icon and display any embedded content on-hover or on-click. You can use it to show a youtube/vimeo video, contact form 7 or any other form, maps, or any other shortcode or IFrame.
13. New! You can add custom text fields to your contact form. This means you can collect any kind of information in your contact form including address, zip, note, or any other kind of free text.
14. New! Send your contact form leads to as many email addresses you want. E.g. you can send your contact form leads to your client and to yourself just to keep track.
15. New! Change the thank you message that's displayed after the contact form is submitted.
<br><a href="https://premio.io/downloads/mystickyelements/?utm_source=wordpressorg" target="_blank" title="My Sticky Elements pro plans"><strong>Upgrade to pro</strong></a> to get all pro features :)

= How does each social button work? =

* Contact Form - your contact form can include Name field, Phone number field, Email field and Message field (textarea).
The phone and emails field have a built-in validation feature, and all the leads will be displayed on your “Contact Leads” tab in your admin panel (CSV download is available as well). You can also get these leads by email in the Pro plan.
The contact form fields also support RTL!


* Facebook page - add a link to your Facebook business page (fan page), and let your visitors find you on facebook, like your page and follow your posts. Build your brand on Facebook with social proof and trust by showing updates, comments and growing audience.

* Twitter page - add a link to your Twitter business page, and let your visitors find you on Twitter, follow your page and see your posts. Have lots of updates? Twitter is a great social network to keep your audience posted with new deals, services or features.

* Instagram page - add a link to your Instagram business page or Instagram profile page, and let your visitors follow your brand on Instagram, like your posts and see your promotions. If you have an eCommerce store, restaurant, hotel or any business that images are a big part of its marketing, Instagram is a must for you.

* Pinterest page - add a link to your Pinterest business page or even to a specific Pinterest board, and let your visitors follow your brand on Pinterest and like your pictures. Like Instagram, Pinterest is a popular social network when we talk about images and visual content.

* WhatsApp chat - enter your phone number and when your visitors click on the WhatsApp icon, WhatsApp chat interface will be launched. On mobile it will launch the WhatsApp app conversation window, and on desktop it will launch the desktop WhatsApp web interface. You can add a preset message that'll automatically be inserted to the WhatsApp messages your visitors send you.
Adding WhatsApp button to your site will give 25 million new users every month (833,000 active users per day) and total of 65 million active users (10% of the total worldwide users), access to contact your immediately.

* YouTube channel - add a link to your YouTube channel (YouTube page) and get more subscribers and views for your videos. In today’s world, videos are consumed more than ever before, and YouTube is probably the best platform to publish your videos and tutorials.

* Phone number (click to call) - add a phone number and let your visitors use the call now button to contact you or your business directly. A call button is very useful in mobile devices, but desktop visitors can also view your phone number or click to call you via different calling web apps (like Skype).

* Facebook Messenger - enter the direct link to the Facebook Messenger of your business page. and once the visitors click on the Facebook Messenger button, the Messenger conversation tab will be opened and they will be able to send you a message. You will get the message into your Facebook page inbox and if you have a bot (native Facebook Messenger bot, Manychat or Chatfuel) it will work automatically.

* Email - enter your business email, and let your visitors click on the email button and send you an email via their email client (Gmail or Outlook). They can also copy the email address and send you an email later.

* Address - show your business address for website visitors that want to arrive to your store.

* Open hours - show your business hours for website visitors that want to arrive to your store. You can also type your workdays there.

* WeChat - enter your WeChat User ID and let your visitors search for you easily on the WeChat app and message you about your business.

* Telegram channel - add your Telegram channel link and let your visitors join your Telegram community. The number of Telegram users keeps growing, and it is a great channel to talk privately about your business with your clients and fans.

* Vimeo -  do you have a Vimeo channel? Like on YouTube, Let your visitors see all of your videos on Vimeo, comment, like your videos and follow your channel. Just add your Vimeo channel link and you’re all set.

* Spotify - Spotify is growing and if you run a podcast, Spotify is the best distribution channel you can probably have. Let your visitor find your podcast and playlists easily and follow you.  

* Itunes - similar to Spotify but for Apple users. Add your itunes link to let your visitors find your itunes channel quickly. Just add your itunes link and you’re ready to go.

* SoundCloud - this is another great channel like itunes and Spotify to distribute music, interviews and podcasts. SoundCloud lets its users to upload, promote, and share audio, so don’t skip on this one.

* VK - VKontakte is a Russian online social media and social networking service. Add you VK username in the field and it will automatically let your visitors chat with you.

* Viber - Viber is a popular cross-platform voice over IP and instant messaging software. Add your phone number and visitors who have Viber app installed can call and chat with you easily.

* Snapchat - Very popular social messaging app used globally. Insert your Snapchat username and let your visitors message you anytime.

* Skype - Like Viber, Skype lets your visitors send you messages or call you from their phone or desktop. Just add your Skype username and you’re all set.

* Line - Line is a free app for instant communications. Enter your LIne link and let your visitors contact your easily.

* SMS - Let your visitors send you SMS via their mobile phone.

* Tumblr - Tumblr is microblogging and social networking website. Let your visitors read your Tumblr posts and images and follow your brand.

* Qzone -  QZone is a very popular  social networking website in China. Add your QZone link and let your visitors see your videos, images and read what you have to say.

* QQ - QQ was created by the same company as QZone. Tencent QQ is an instant messaging software service and web portal in China. Add your QQ username and let your visitors chat with you easily.

* Behance - Behance is an online platform to showcase and discover creative work. If you are in the design business, you should definitely show your portfolio there and let your visitors see it.

* Dribbble - Like Behance, Dribbble is made for creatives and it is an online community for showcasing user-made artwork. If you want to open a Dribbble account, you need to get an invitation by one of the existing users. Insert your Dribbble portfolio link and show your work to your visitors.

* Quora - Quora is an Q&A (question-and-answer) website. Quora  is a great place to gain and share knowledge. Answer questions, get more impressions and build your personal brand. Enter the link to your Quora profile and let your visitors follow you on Quora.

* LinkedIn - LinkedIn is a professional community. In LinkedIn you can find all kind of professionals including sales people, developers, finance, marketers and much more. Enter the link to your LinkedIn profile and let your visitors connect with you on LinkedIn.

* Custom icons - you can add up to 3 customs icons on the Pro version and link to any website you want. For example: Vimeo, VK, Viber, Skype, SMS, contact page etc.


= My Sticky Elements is GDPR complaint =
We don't store any data about your visitors who click on any of the social channels. Once your visitors click on any of the social channels buttons, they will be redirected to relevant platform or app such as Facebook Messenger, WhatsApp app, Instagram page etc.
On My Sticky Elements’ settings panel you can see a “Contact Leads” tab that will contain the leads you got via the contact form tab. You can always delete them easily if you get a GDPR request from any of them.

== Installation ==
1. Install and activate My Sticky Elements plugin on your WordPress website
2. Customize your contact form and choose your social tabs and click on “Save changes”
3. You're all set! Your contact form and social tabs menu will now appear on your site

== Frequently Asked Questions ==

= What’s included in the free plan? =
You can show a customized contact form and also up to two social channels (unlimited in the pro plan), display the floating tabs on desktop and mobile, choose LTR or RTL, change the background colors and call-to-action for each icon and more.

= Is there a time limit for the free plan? =
No. you can use the free plan as long as you want.

= Can I show only a contact form/social buttons?  =
Yes! You can customize your contact form tab and choose to show only the contact tab without any social icon, or vice versa - show only social icons without the contact form.

= I don’t see the widget on my site, what should I do? =
Please make sure the toggles of the contact form and/or social channels are turned ON and click on “SAVE CHANGES” button.

= How do I activate my pro plan? =
Just follow our <a href="https://premio.io/wordpress-premio-pro-version-installation-guide/" target="_blank" title="WordPress installation guide">pro installation guide</a>.

= Is there a live demo for My Sticky Elements =
A live demo for My Sticky Elements plugin is available at <a href="https://demo.premio.io/mystickyelements/?utm_source=wordpressorg" target="_blank" title="Premio.io demo site">Premio's demo site</a>.

== Screenshots ==

1. Floating contact form and social buttons live on site
2. Contact form settings
3. Social networks buttons
4. General settings
5. See My Sticky Elements in action on mobile
6. Contact form and icons templates and entry effects
7. Change the color of your icons, contact form, text, and anything else!

== Changelog ==
= 1.7 =
Added an accessible contact form leads
= 1.6.9 =
Contact form fields bug fixed
= 1.6.8 =
Contact form icon size when label is empty fix
= 1.6.7 =
Contact form empty name field bug
= 1.6.6 =
WP 5.3 adjustments including checkbox UI issues, and color picker height issue
= 1.6.5 =
Z-index bug fix. Also fixed icon size bug for Line.me and Facebook Messenger. My Sticky Elements is also now compatible with Google Lighthouse.
= 1.6.4 =
Top position for mobile, support for Facebook Messenger pages that don't have a custom URL, and the contact form button becomes disabled until the contact form is submitted to avoid double submission.
= 1.6.3 =
Use Font Awesome icons with the custom widget and shortcode widget. We've also fixed a line.me bug and some other minor bugs.
= 1.6.2 =
You can now change the the color of the contact icons. You can also see what was the URL  from which your contact form was submitted. 
= 1.6.1 =
We've added 7 icon templates, you can make your icons rounded, diamond-shaped, arrowed shaped and more. You can also set an entry effect (fade or slide-in) for your widget. Desktop bottom position is also available for the widget. Change the color of the contact form headline.  We've also improved our RTL menu support and added Hummingbird cache compatibility.
= 1.6 =
Added compatibility  with many cache plugins like WP-Rocket, W3Cache, LiteSpeed Cache, WP Fastest Cache, Autoptimize and more. You can also add a preset message for the WhatsApp icon. We've also fixed some mobile bugs.
= 1.5.9 =
Added LinkedIn as a new channel
= 1.5.8 =
Fixed an Android click to call and click to sms bugs
= 1.5.7 =
You can add custom Javascript to the custom icon
= 1.5.6 =
Fixed a shortcode bug
= 1.5.5 =
You can now embed any shortcode, IFrame, and HTML using the new shortcode/HTML channel. we've also unlocked one custom channel for the free version
= 1.5.4 =
Control the size of the widget + performance improvement
= 1.5.3 =
Upgrade page update
= 1.5.2 =
Improved the UI - now the preview is sticky and the navigation is much easier
= 1.5.1 =
Fixed SMS channel bug
= 1.5 =
Brand new view for mobile, show all elements on the bottom of the screen. also added separate configuration for the minimize icon for desktop and mobile, and you can also decide if you want it open by default or not
= 1.4.2 =
Added font selection, bulk removal off leads, added date col to the leads table
= 1.4.1 =
* RTL support, easier facebook messenger integration, redirect after form submission to pro version 
= 1.4 =
* Added 16 new channels: Vimeo, Spotify, Itunes, SoundCloud, VK, Viber, Snapchat, Skype
Line, SMS, Tumblr, Qzone, QQ, Behance, Dribbble, Quora
= 1.3.5 =
* Added dropdown box to your contact form + change the order of the fields in the contact form
= 1.3.4 =
* Minimize the widget * font awesome bug fix
= 1.3.3 =
* Bug fixes on mobile view
= 1.3.2 =
* Bug fixes
= 1.3.1 =
* On mobile the widgets are now closed on click
= 1.3 =
* Brand new UI, new channels, contact form and much more :)
= 1.2 =
* Fixed: close element on mobile devices (all events)
* Added: Language support.
= 1.1 =
* Fixed: close element on mobile devices (click event only)
* Added: Shortcode support.

= 1.0 =
* First version